FROM julia:1.2

ADD Project.toml
RUN julia --project -e "import Pkg; Pkg.instantiate()"