export TimeDis, explicit_mstage, Rk1, Rk2, RK4, Rk
export explicit_mstep, init_mstep, AB, AB1, AB2



# explicit based on multi stage approximation
mutable struct explicit_mstage  <: TimeDis

  order    :: Int64
  space    :: SpatialDis
  time     :: Float64
  var_temp :: Array{Float64,2}
  entropy  :: Vector{Float64}
  active_entropy  :: Int64

  function explicit_mstage(space, order)
    time = 0.0
    var_temp = zeros(Float64,(space.ndof,space.nvar)) 
    entropy = zeros(Float64,space.ndof) 
    active_entropy=0    
    new(order, space, time, var_temp, entropy,active_entropy)
  end    

end

function Rk1(self::explicit_mstage, dt::Float64, active_entropy::Int64=0)
    self.space()
    if active_entropy == 0
        for i in 1:self.space.ndof
            self.space.field[i,:] +=  dt*view(self.space.flux,i,:)
        end
    else    
        for i in 1:self.space.ndof
             eta_n=self.space.entropy(self.space.field[i,:])
             self.space.field[i,:] +=  dt*view(self.space.flux,i,:)
             self.entropy[i]=(self.space.entropy(self.space.field[i,:])-eta_n)/dt+self.space.prod_eta[i]
        end
    end
    self.time = self.time + dt 
end


function Rk2(self::explicit_mstage, dt::Float64)
    
    for i in 1:self.space.ndof
        self.var_temp[i,:] = self.space.field[i,:] 
    end  
        
    self.space()
     for i in 1:self.space.ndof
        self.space.field[i,:] +=  0.5*dt*view(self.space.flux,i,:)
    end
        
    self.space()
     for i in 1:self.space.ndof
        self.space.field[i,:] =  self.var_temp[i,:] + dt*view(self.space.flux,i,:)
    end

    self.time = self.time + dt
end

function Rk4(self::explicit_mstage, dt::Float64)
  field_temp = zeros(Float64,(self.space.ndof,self.space.nvar))
  k1 = zeros(Float64,(self.space.ndof,self.space.nvar))
  k2= zeros(Float64,(self.space.ndof,self.space.nvar))
  k3= zeros(Float64,(self.space.ndof,self.space.nvar))

   for i in 1:self.space.ndof
      field_temp[i,:] = self.space.field[i,:] 
  end  
      
  self.space()
  k1[:,:] .= self.space.flux[:,:]  
   for i in 1:self.space.ndof
      self.space.field[i,:] = field_temp[i,:] + 0.5*dt*view(k1,i,:)
  end
      
  self.space()
  k2[:,:] .= self.space.flux[:,:]  
   for i in 1:self.space.ndof
      self.space.field[i,:] = field_temp[i,:] + 0.5*dt*view(k2,i,:)
  end

  self.space()
  k3[:,:] .= self.space.flux[:,:]  
   for i in 1:self.space.ndof
      self.space.field[i,:] = field_temp[i,:] + dt*view(k3,i,:)
  end

  self.space()
  a = (1.0/6.0)
  b = (1.0/3.0)
   for i in 1:self.space.ndof
    self.space.field[i,:] .= field_temp[i,:]+dt*(a*k1[i,:]+b*k2[i,:]+b*k3[i,:]+a*self.space.flux[i,:])
  end

  self.time = self.time + dt
end


function Rk(self::explicit_mstage, dt::Float64,active_entropy::Int64=0)
  if self.order == 1
    Rk1(self,dt,active_entropy)
  end
  if self.order ==2  
    Rk2(self,dt)
  end  
  if self.order > 2  
    Rk4(self,dt)
  end 
end      




mutable struct explicit_mstep  <: TimeDis

  order    :: Int64
  space    :: SpatialDis
  time     :: Float64
  data     :: Array{Float64,3}

  function explicit_mstep(space, order)
    time = 0.0
    data = zeros(Float64,(space.ndof,space.nvar,order))
    new(order, space, time, data)
  end    

end

function init_mstep(self::explicit_mstep, dt)

    rkinit = explicit_mstage(self.space,self.order)
    self.data[:,:,1] .= self.space.field[:,:]
     for k in 1:self.order-1
      Rk(rkinit,dt)
      self.data[:,:,k+1] .= self.space.field[:,:]
      self.time = self.time + dt
    end  

    return self.order-1
end

function AB(self::explicit_mstep, dt)
  if self.order == 1
    AB1(self,dt)
  end
  if self.order ==2  
    AB2(self,dt)
  end  
  if self.order ==3  
    AB2(self,dt)
  end 
end      

function AB1(self::explicit_mstep, dt)
    
    self.space()
     for i in 1:self.space.ndof
        self.space.field[i,:] +=  dt[1]*view(self.space.flux,i,:)
    end
    self.time = self.time + dt[1]
    
end


function AB2(self::explicit_mstep, dt)
    f1 = zeros(Float64,(self.space.ndof,self.space.nvar))
    f2 = zeros(Float64,(self.space.ndof,self.space.nvar))
    
    ## xn-1
    self.space.field[:,:] .= self.data[:,:,1]
    self.space()
    f1[:,:] .= self.space.flux[:,:]

    ## xn
    self.space.field[:,:] .= self.data[:,:,2]
    self.space()
    f2[:,:] .= self.space.flux[:,:]
    
    ## xn+1
    a = dt[2]/(2.0*dt[1])
    
     for i in 1:self.space.ndof
        self.space.field[i,:] = self.data[i,:,2] + a*((dt[2]+2.0*dt[1])*f2[i,:]-dt[2]*f1[i,:])
    end
    self.time = self.time + dt[2]
    self.data[:,:,1] .= self.data[:,:,2] ## xn-1 <_-- xn
    self.data[:,:,2] .= self.space.field[:,:] ## xn <-- xn+1 
end

