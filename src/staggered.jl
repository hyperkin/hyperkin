export NullOperator, staggered, set_space, set_operator, space_to_field, field_to_space


struct NullOperator <: SpatialOperator
end
function (self::NullOperator)(v :: Vector{DeRhamSpace})
end

mutable struct staggered <: SpatialDis

  nvar       :: Int64
  ndof       :: Int64
  mh         :: Mesh
  spaces     :: Vector{DeRhamSpace}
  field      :: Array{Float64,2}
  flux       :: Array{Float64,2}
  field_bcl  :: Vector{Float64}
  field_bcr  :: Vector{Float64}
  operator   :: SpatialOperator


  function staggered(mh, n)
      nvar = n 
      ndof = mh.Nc
      spaces    = Vector{DeRhamSpace}(undef, nvar)
      field      = zeros(Float64,(mh.Nc,nvar))
      flux       = zeros(Float64,(mh.Nc,nvar))
      field_bcl   = zeros(Float64,nvar)
      field_bcr   = zeros(Float64,nvar)
      operator  = NullOperator()

      new(nvar,  ndof, mh, spaces, field,flux, field_bcl, field_bcr,operator)
  end
end 

  function set_space(self::staggered, v::DeRhamSpace, i)
    self.spaces[i] = v
  end  

  function set_operator(self::staggered, op)
    self.operator = op
  end  

  function space_to_field(self::staggered)
    for k in 1:self.nvar
        for i in 1:self.spaces[k].ndof
            self.field[i,k] = self.spaces[k].field[i] 
        end    
    end    
  end 

  function field_to_space(self::staggered)
    for k in 1:self.nvar
        for i in 1:self.spaces[k].ndof
            self.spaces[k].field[i] = self.field[i,k]
        end    
    end    
  end 

  function (self::staggered)() # generic_vf_op() is replaced by model()
    res  = Vector{DeRhamSpace}(undef,self.nvar)
    field_to_space(self)
    res = self.operator(self.spaces)
    for k in 1:self.nvar
        for i in 1:res[k].ndof
            self.flux[i,k] = res[k].field[i]
        end    
    end    

  end  
