export dg, loc_glob, glob_loc
export set_numflux, restriction_loc, interpolator_loc, inter
export lagrange_dg, initialization, reduction, diagnostics
export bc_neumann, bc_dirichlet, NoDGLimiting, minmoddg, set_dof

##### Class for 1D Discontinuous Galerkin discretization. Allows to create the ODE vector after DG discretization
mutable struct dg <: SpatialDis
  nvar          :: Int64                # Nb variables of the model
  noutvar       :: Int64                # Nb of output variables
  ndof          :: Int64                # Nb of degree of freedom       
  deg           :: Int64                # Degree of polynomial inside each cell
  mh            :: Mesh                 # 1D Mesh associated
  field         :: Array{Float64,2}     # Table of the variables/fields at each degree of freedom
  flux          :: Array{Float64,2}     # Table of the fluxes at each degree of freedom
  field_bcl     :: Array{Float64,2}     # Table of fields values at each degree of freedom on the left ghost cell
  field_bcr     :: Array{Float64,2}     # Table of fields values at each degree of freedom on the right ghost cell
  numflux       :: Flux                 # Function of the numerical fluxes
  ndiags        :: Int64                # Nb of diagnostics (like L1 norm)

  #### Constructor 1
  ## inputs: mh (1D mesh), n (nb variables), d (degree)
  ## in this case zero diagnostics, nb of output variables is n
  function dg( mh, n, d)
      nvar             = n
      noutvar          = n
      ndof             = mh.Nc*(d+1)
      deg              = d
      field            = zeros(Float64,(mh.Nc*(deg+1),nvar))
      flux             = zeros(Float64,(mh.Nc*(deg+1),nvar))
      field_bcl        = zeros(Float64,(deg+1,nvar))
      field_bcr        = zeros(Float64,(deg+1,nvar))
      numflux          = NoFlux()
      ndiags           = 0

      new(nvar, noutvar, ndof, deg, mh, field, flux, field_bcl, field_bcr, numflux, ndiags)
  end

  #### Constructor 2
  ## inputs: mh (1D mesh), n (nb variables), d (degree), ng (nb of diagnostics)
  ## in this case nb of output variables is n  
  function dg( mh, n, d, nd)
      nvar             = n
      noutvar          = n
      ndof             = mh.Nc*(d+1)
      deg              = d
      field            = zeros(Float64,(mh.Nc*(deg+1),nvar))
      flux             = zeros(Float64,(mh.Nc*(deg+1),nvar))
      field_bcl        = zeros(Float64,(deg+1,nvar))
      field_bcr        = zeros(Float64,(deg+1,nvar))
      numflux          = NoFlux()

      new(nvar, noutvar, ndof, deg, mh, field,flux, field_bcl, field_bcr, numflux, nd)
  end
    
  #### Constructor 3
  ## inputs: mh (1D mesh), n (nb variables), d (degree), ng (nb of diagnostics), no (nb of output variables)   
  function dg( mh, n, d, nd, no)
      nvar             = n
      noutvar          = no
      ndof             = mh.Nc*(d+1)
      deg              = d
      field            = zeros(Float64,(mh.Nc*(deg+1),nvar))
      flux             = zeros(Float64,(mh.Nc*(deg+1),nvar))
      field_bcl        = zeros(Float64,(deg+1,nvar))
      field_bcr        = zeros(Float64,(deg+1,nvar))
      numflux          = NoFlux()

      new(nvar, noutvar, ndof, deg, mh, field, flux, field_bcl, field_bcr, numflux, nd)
  end  
end

##### Method to give global degree of freedom using index of cell and local gauss point
## input: i (index of cell), k (index of local gauss point)
## output: index of global degree of freedom 
function loc_glob(self::dg,i,k)
  return k+(self.deg+1)*(i-1)
end  

##### Method to give index of cell and local gauss point using global degree of freedom
## input: ig (index of global of freedom)
## output: index of cell and index of local gauss point
function glob_loc(self::dg,ig)
  [div(ig-1,self.deg+1)+1,mod(ig-1,(self.deg+1))+1]
end  

function set_dof(self::dg, i_dof)
    i,jloc=glob_loc(self,i_dof)
    return pg(self.mh,i,jloc)
end

##### Method to gives the conservative numerical flux
## input: numflux (object containing the numerical flux function)
function set_numflux(self::dg, numflux)
    self.numflux = numflux
end

##### Method to gives the method of limiting reconsrtruction
## input: numflux (object containing the limiting function)
function set_limiting(self::dg, numflux)
    self.limiting = numflux
end

##### Method to give the local restriction operator in a cell.
## Restriction: compute degree of freedom of a function and store in field
## input: i (index of cell), ivar (index of variable), func (function to project in the discrete space)
function restriction_loc(self::dg, i, ivar, func )
  for k in 1:self.deg+1
    x = pg(self.mh,i,k)
    self.field[loc_glob(self,i,k),ivar]  = func(x)
  end
end 

##### Method to give the local interpolation operator in a cell.
## Interpolation: compute function using degree of freedom. 
## input: i (index of cell), ivar (index of variable)
## output: function depending of x
function interpolator_loc(self::dg, i, ivar)
  function inter(x)
    res=lagrange_dg(self,x,i,ivar)
    return res
  end
end 

##### Method to give lagrange polynomial in a cell.
## input: i (index of cell), ivar (index of variable)
## output: function depending of x
function lagrange_dg(self::dg,x,i, ivar)
  xvals = zeros(Float64,self.deg+1)
  yvals = zeros(Float64,self.deg+1)

  if i == 0 
    for k in 1:self.deg+1
      xvals[k] = pgl(self.mh,k)
      yvals[k] = self.field_bcl[k,ivar]
    end 
  else  
    for k in 1:self.deg+1
      xvals[k] = pg(self.mh,i,k)
      yvals[k] = self.field[loc_glob(self,i,k),ivar]
    end 
  end

  poly = 0.0
  for i = 1:self.deg+1
    lag = 1.0
    for j in 1:self.deg+1
        if i != j
            lag = lag*(x-xvals[j])/(xvals[i]-xvals[j])
        end    
    end  
    poly = poly + yvals[i]*lag
  end
  return poly
end

##### Method to initialize the fields.
## input: init_f (function which gives initial data)
function initialization(self::dg, init_f)
    for i in 1:self.mh.Nc
        for k in 1:self.deg+1
            x = pg(self.mh,i,k)
            v  = init_f(x,0.0)
            self.field[loc_glob(self,i,k),:]  = v[:]
        end
    end
end

##### Method to compute a quantity using a function on all the degree of freedom
## exemple: compute a max on the domain
## input: function_reduced (the fonction used for reduction), init_res (the initial value of the quantity)
## output: res (final value)
function reduction(self::dg, function_reduced, init_res)   
    res = init_res
    for i in 1:self.mh.Nc
        for k in 1:self.deg+1
            x = pg(self.mh,i,k)
            res = function_reduced(x,self.field[loc_glob(self,i,k),:],res)
        end
    end

    return res
end

##### Method to compute the output variables and the global diagnostics associated
## input: ref (ref=1 there is a exact solution, ref=0 for no), ti (time), f_ref (an reference function)
## input: mapping (object containing function to transform variables to output variables)
## input: mapping (object containing function to local computation of diagnostic using output variables)
## output: field ( output variables), diags (global quantities like norm), field_ref (exact output variables)
function diagnostics(self::dg,ref,ti,f_ref,mapping,form_diags)
    println("inside ",self.noutvar)
    diags = zeros(Float64,self.ndiags)
    loc_diags = zeros(Float64,self.ndiags)
    fg = zeros(self.noutvar)
    fg_ref = zeros(self.noutvar)
    field_ref = zeros(Float64,(self.ndof,self.noutvar))
    field = zeros(Float64,(self.ndof,self.noutvar))
    x = zeros(Float64,self.ndof)
    
    if ref == 0
       for i in 1:self.mh.Nc
        for k in 1:self.deg+1
          x[loc_glob(self,i,k)] = pg(self.mh,i,k)
          mapping(self.field[loc_glob(self,i,k),:],fg)
          field[loc_glob(self,i,k),:] .= fg[:]
          form_diags(field[loc_glob(self,i,k),:],loc_diags)
          diags[:] .+= wg(self.mh,i,k)*loc_diags[:]
        end  
      end
      [x, field, diags] 
    else
       for i in 1:self.mh.Nc
        for k in 1:self.deg+1
            x[loc_glob(self,i,k)] = pg(self.mh,i,k)
            mapping(self.field[loc_glob(self,i,k),:],fg) 
            mapping(f_ref(x[loc_glob(self,i,k)],ti),fg_ref) 
            field[loc_glob(self,i,k),:] .= fg[:]
            field_ref[loc_glob(self,i,k),:] .= fg_ref[:]
            form_diags(fg[:]-fg_ref[:],loc_diags)
            diags[:]  .+= wg(self.mh,i,k)*loc_diags[:]
        end
      end
      [x, field, field_ref, diags] 
    end
      
end

##### Operator () to compute the global flux associated to the DG scheme. Gives a "vector" for the ODE system
function (self::dg)() # generic_vf_op() is replaced by model()
   ul = zeros(Float64,self.nvar)
   ur = zeros(Float64,self.nvar)
   fu = zeros(Float64,self.nvar)

   
   ######### first  and second cell
   volumes = zeros(Float64,(self.deg+1,self.nvar))
   flux = zeros(Float64,(self.deg+1,self.nvar))
   ### Volume part
    for i in 1:self.deg+1
      for j in 1:self.deg+1
        x = pg(self.mh,1,j)
        w = wg(self.mh,1,j)    
        ul .= self.field[loc_glob(self,1,j),:]
        self.numflux(x,w,fu,ul,ul)
        volumes[i,:] .+= w * dpg(self.mh,1,i,j) .* fu[:]
      end
    end 

    ### flux part
    x = pg(self.mh,1,1)
    w = wg(self.mh,1,1)
    ul .= self.field_bcl[end,:]
    ur .= self.field[loc_glob(self,1,1),:]
    self.numflux(x,w,fu,ul,ur)
    flux[1,:] = flux[1,:] .- fu[:]
    x = pg(self.mh,1,self.deg+1)
    w = wg(self.mh,1,self.deg+1)
    ul .= self.field[loc_glob(self,1,self.deg+1),:]
    ur .= self.field[loc_glob(self,2,1),:]
    self.numflux(x,w,fu,ul,ur)
    flux[end,:] .= flux[end,:] .+  fu[:]

    ### final vector
    for i in 1:self.deg+1
      w = wg(self.mh,1,i)
      self.flux[loc_glob(self,1,i),:] .= .- ( .- volumes[i,:] .+ flux[i,:])/w
    end 
   
    volumes = zeros(Float64,(self.deg+1,self.nvar))
   flux = zeros(Float64,(self.deg+1,self.nvar))
   ### Volume part
    for i in 1:self.deg+1
      for j in 1:self.deg+1
        x = pg(self.mh,2,j)
        w = wg(self.mh,2,j)    
        ul .= self.field[loc_glob(self,2,j),:]
        self.numflux(x,w,fu,ul,ul)
        volumes[i,:] .+= w * dpg(self.mh,2,i,j) .* fu[:]
      end
    end 

    ### flux part
    x = pg(self.mh,2,1)
    w = wg(self.mh,2,1)
    ul .= self.field[loc_glob(self,1,self.deg+1),:]
    ur .= self.field[loc_glob(self,2,1),:]
    self.numflux(x,w,fu,ul,ur)
    flux[1,:] = flux[1,:] .- fu[:]
    x = pg(self.mh,1,self.deg+1)
    w = wg(self.mh,1,self.deg+1)
    ul .= self.field[loc_glob(self,2,self.deg+1),:]
    ur .= self.field[loc_glob(self,3,1),:]
    self.numflux(x,w,fu,ul,ur)
    flux[end,:] .= flux[end,:] .+  fu[:]

    ### final vector
    for i in 1:self.deg+1
      w = wg(self.mh,1,i)
      self.flux[loc_glob(self,2,i),:] .= .- ( .- volumes[i,:] .+ flux[i,:])/w
    end 

    ########### center cells part
     for icell in 3:self.mh.Nc-2
      volumes = zeros(Float64,(self.deg+1,self.nvar))
      flux = zeros(Float64,(self.deg+1,self.nvar))

      ### Volume part
      for i in 1:self.deg+1
        for j in 1:self.deg+1
          x = pg(self.mh,icell,j)
          w = wg(self.mh,icell,j)      
          ul .= self.field[loc_glob(self,icell,j),:]
          self.numflux(x,w,fu,ul,ul)
          volumes[i,:] .+= w * dpg(self.mh,icell,i,j) .* fu[:]
        end
      end 

      ### flux part
      x = pg(self.mh,icell,1)
      w = wg(self.mh,icell,1)
      ul .= self.field[loc_glob(self,icell-1,self.deg+1),:]
      ur .= self.field[loc_glob(self,icell,1),:]
      self.numflux(x,w,fu,ul,ur)
      flux[1,:] .= flux[1,:] .- fu[:]
       
      x = pg(self.mh,icell,self.deg+1)
      w = wg(self.mh,icell,self.deg+1)
      ul .= self.field[loc_glob(self,icell,self.deg+1),:]
      ur .= self.field[loc_glob(self,icell+1,1),:]
      self.numflux(x,w,fu,ul,ur)
      flux[end,:] .= flux[end,:] .+ fu[:]


    ### final vector
      for i in 1:self.deg+1
        w = wg(self.mh,icell,i)
        self.flux[loc_glob(self,icell,i),:] .= .- ( .- volumes[i,:] .+ flux[i,:])/w
      end

    end  
   
   #########  previous last cell
   volumes = zeros(Float64,(self.deg+1,self.nvar))
   flux = zeros(Float64,(self.deg+1,self.nvar))
    ### Volume part
    for i in 1:self.deg+1
      for j in 1:self.deg+1
        x = pg(self.mh,self.mh.Nc-1,j)
        w = wg(self.mh,self.mh.Nc-1,j)      
        ul .= self.field[loc_glob(self,self.mh.Nc-1,j),:]
        self.numflux(x,w,fu,ul,ul)
        volumes[i,:] .+= w * dpg(self.mh,self.mh.Nc-1,i,j) .* fu[:]
      end
    end 

    ### flux part
    x = pg(self.mh,self.mh.Nc-1,1)
    w = wg(self.mh,self.mh.Nc-1,1)
    ul .= self.field[loc_glob(self,self.mh.Nc-2,self.deg+1),:]
    ur .= self.field[loc_glob(self,self.mh.Nc-1,1),:]
    self.numflux(x,w,fu,ul,ur)
    flux[1,:] .= flux[1,:] .- fu[:]
    x = pg(self.mh,self.mh.Nc,self.deg+1)
    w = wg(self.mh,self.mh.Nc,self.deg+1)
    ul .= self.field[loc_glob(self,self.mh.Nc-1,self.deg+1),:]
    ur .= self.field[loc_glob(self,self.mh.Nc,1),:]
    self.numflux(x,w,fu,ul,ur)
    flux[end,:] .= flux[end,:] .+ fu[:]

    ### final vector
    for i in 1:self.deg+1
      w = wg(self.mh,self.mh.Nc-1,i)
      self.flux[loc_glob(self,self.mh.Nc-1,i),:] .= .- (.- volumes[i,:] .+ flux[i,:])/w
    end 

   ######### last cell
   volumes = zeros(Float64,(self.deg+1,self.nvar))
   flux = zeros(Float64,(self.deg+1,self.nvar))
    ### Volume part
    for i in 1:self.deg+1
      for j in 1:self.deg+1
        x = pg(self.mh,self.mh.Nc,j)
        w = wg(self.mh,self.mh.Nc,j)      
        ul .= self.field[loc_glob(self,self.mh.Nc,j),:]
        self.numflux(x,w,fu,ul,ul)
        volumes[i,:] .+= w * dpg(self.mh,self.mh.Nc,i,j) .* fu[:]
      end
    end 

    ### flux part
    x = pg(self.mh,self.mh.Nc,1)
    w = wg(self.mh,self.mh.Nc,1)
    ul .= self.field[loc_glob(self,self.mh.Nc-1,self.deg+1),:]
    ur .= self.field[loc_glob(self,self.mh.Nc,1),:]
    self.numflux(x,w,fu,ul,ur)
    flux[1,:] .= flux[1,:] .- fu[:]
    x = pg(self.mh,self.mh.Nc,self.deg+1)
    w = wg(self.mh,self.mh.Nc,self.deg+1)
    ul .= self.field[loc_glob(self,self.mh.Nc,self.deg+1),:]
    ur .= self.field_bcr[1,:]
    self.numflux(x,w,fu,ul,ur)
    flux[end,:] .= flux[end,:] .+ fu[:]

    ### final vector
    for i in 1:self.deg+1
      w = wg(self.mh,self.mh.Nc,i)
      self.flux[loc_glob(self,self.mh.Nc,i),:] .= .- (.- volumes[i,:] .+ flux[i,:])/w
    end
end

##### Method to put Neumann values inside ghost cells
function bc_neumann(self::dg)
  for k in 1:self.deg+1
    self.field_bcl[k,:] .= self.field[self.deg+2-loc_glob(self,1,k),:]
    self.field_bcr[k,:] .= self.field[loc_glob(self,self.mh.Nc,self.deg+2-k),:]
  end  
end

##### Method to put Diriclet values inside ghost cells
## input: bcl[:] and bcr[:] (lelf and right Dirichlet values)
function bc_dirichlet(self::dg, bcl, bcr)
  for k in 1:self.deg+1
    self.field_bcl[k,:] .= bcl[k,:]
    self.field_bcr[k,:] .= bcr[k,:]
  end  
end

