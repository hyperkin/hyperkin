export Mesh2D, Meshes1D, id_mesh2D, translate_mesh2D
export normals, lengths, neighbors, neighbors_large 

######### 2 mapping for 2D mesh #########
## intputs: x[:] (position), hx, hy (mesh step in x,y), Lx, Ly (size of domain in x,y)
## output: x[:] (modified position)
function id_mesh2D(x,hx,hy,Lx,ly)
    return x 
end
function translate_mesh2D(x,hx,hy,Lx,Ly)
    L1 = 0.5*Lx
    L2 = 0.5*Ly
    a = -L1 + x[1]
    b = -L2 + x[2]
    x[1] = a
    x[2] = b
    return x 
end 

######### Class 2D mesh ###########
mutable struct Mesh2D
    
    #### Principle 2 layers of ghost cells around the mesh.

    Nc         :: Int                # Nb total cells
    Nx         :: Int                # Nb cells for x direction
    Ny         :: Int                # Nb cells for x direction
    Lx         :: Float64            # domain size for x direction
    Ly         :: Float64            # domain size for direction
    Nv         :: Int                # Nb total vertex
    hx         :: Float64            # mesh step in direction x
    hy         :: Float64            # mesh step in direction y
    h          :: Float64            # total mesh step
    nodes      :: Array{Float64,2}   # table of nodes
    centers    :: Array{Float64,2}   # table of centers
    areas      :: Vector{Float64}    # table of cell areas
    labels     :: Vector{Int8}       # table of labels associated to the cells
    periodic   :: Int                # periodic mesh or not ( 1 or 0)
    Deg        :: Int                # Deg for DG meshes (not used now)
    
    function Mesh2D(Lx,Ly, Nx, Ny,order)
        Nv         = (Nx+3)*(Ny+3)
        Nc         = (Nx+2)*(Ny+2)
        hx         = Lx/Nx
        hy         = Ly/Ny
        h          = min(hx,hy)
        nodes      = zeros(Float64,(Nv,2))
        centers    = zeros(Float64,(Nc,2))
        areas      = zeros(Float64,Nc)
        labels     = zeros(Float64,Nc)
        periodic   = 0

        new(Nc,Nx,Ny,Lx,Ly,Nv,hx,hy,h,nodes,centers,areas,labels,periodic,order)    
    end

    function Mesh2D(Lx,Ly, Nx, Ny, periodic,order)
        Nv         = (Nx+3)*(Ny+3)
        Nc         = (Nx+2)*(Ny+2)
        hx         = Lx/Nx
        hy         = Ly/Ny
        h          = min(hx,hy)
        nodes      = zeros(Float64,(Nv,2))
        centers    = zeros(Float64,(Nc,2))
        areas      = zeros(Float64,Nc)
        labels     = zeros(Float64,Nc)

        new(Nc,Nx,Ny,Lx,Ly,Nv,hx,hy,h,nodes,centers,areas,labels,periodic,order)    
    end
    
end

########## For the following order of neighbors: bottom, right, top, left 

##### Method to compute index of the neighbor cell
## inputs: i (local index of the neighbor)
## output: index of the cell neighbor
function neighbors(self :: Mesh2D, i)
    tnei = [i-(self.Nx+2),i-1,i+(self.Nx+2),i+1]  
    if(self.periodic == 1)
        if self.labels[i] == 1 || self.labels[i] == 12 || self.labels[i] == 41 tnei[2] = i+self.Nx-1 end
        if self.labels[i] == 2 || self.labels[i] == 12 || self.labels[i] == 23 tnei[1] = i+(self.Nx+2)*(self.Ny-1) end
        if self.labels[i] == 3 || self.labels[i] == 23 || self.labels[i] == 34 tnei[4] = i-(self.Nx-1) end
        if self.labels[i] == 4 || self.labels[i] == 34 || self.labels[i] == 41 tnei[3] = i-(self.Nx+2)*(self.Ny-1) end   
    end       
    return tnei   
end

function neighbors_large(self :: Mesh2D, i)
    tnei = [i-(self.Nx+2),i-(self.Nx+2)-1,i-1,i+(self.Nx+2)-1,i+(self.Nx+2),i+(self.Nx+2)+1,i+1,i-(self.Nx+2)+1]  
    return tnei   
end

##### Method to compute index of the normal to the edge
## inputs: i (local index of the edge)
## output: normal vector
function normals(self :: Mesh2D, i)
    [[0.0,-1.0],[-1.0,0.0],[0.0,1.0],[1.0,0.0]]  
end  

##### Method to compute index of the lenght to the edge
## inputs: i (local index of the edge)
## output: lenght
function lengths(self :: Mesh2D, i)
    [self.hx,self.hy,self.hx,self.hy]
end  

##### Method to compute 2 1D vectors contained meshes for x-direction, y-direction.
## output: x[:] (1D mesh for x direction), y[:] (1D mesh for y direction)
function Meshes1D(self:: Mesh2D)
    x = zeros(Float64,self.Nx)
    y = zeros(Float64,self.Ny)
    k=2
    for i in 1:self.Nx
        x[i]= self.centers[k,1]
        k = k+1;
    end 
    k=2+2*self.Nx;
    for i in 1:self.Ny
        y[i]= self.centers[k,2]
        k = k+self.Nx+2;
    end 
    [x, y]
end    
        
##### Operator to construct the 2D mesh. Call for a mesh M: M(mapping)
## input: Function (mapping of the mesh)
function (self :: Mesh2D)(newmesh :: Function)
    
    ### Construction of nodes/centers/areas 
    k = 1
    for j in 0:self.Ny+2 
        for i in 0:self.Nx+2
            self.nodes[k,1]=(i-1)*self.hx
            self.nodes[k,2]=(j-1)*self.hy                
            k = k + 1
        end
    end    
    
    k = 1  
    for j in 1:self.Ny+2
         for i in 1:self.Nx+2
            e1 = (self.Nx + 3)*(j-1)+i
            e2 = (self.Nx + 3)*(j-1)+i +1
            e3 = (self.Nx + 3)*j+i +1
            e4 = (self.Nx + 3)*j+i
                       
            e12 = self.nodes[e1,1]*self.nodes[e2,2]-self.nodes[e1,2]*self.nodes[e2,1]
            e23 = self.nodes[e2,1]*self.nodes[e3,2]-self.nodes[e2,2]*self.nodes[e3,1]
            e34 = self.nodes[e3,1]*self.nodes[e4,2]-self.nodes[e3,2]*self.nodes[e4,1]
            e41 = self.nodes[e4,1]*self.nodes[e1,2]-self.nodes[e4,2]*self.nodes[e1,1]
        
            f1 = self.nodes[e1,:]+self.nodes[e2,:]
            f2 = self.nodes[e2,:]+self.nodes[e3,:]
            f3 = self.nodes[e3,:]+self.nodes[e4,:]
            f4 = self.nodes[e4,:]+self.nodes[e1,:]
        
            self.areas[k] = 0.5 *(e12+e23+e34+e41)  
            self.centers[k,1] = 1.0/(6.0*self.areas[i])*(f1[1]*e12+f2[1]*e23+f3[1]*e34+f4[1]*e41)
            self.centers[k,2] = 1.0/(6.0*self.areas[i])*(f1[2]*e12+f2[2]*e23+f3[2]*e34+f4[2]*e41)
   
            ### Negative label for ghost cells:
            ### -1 left, -2 bottom, -3 right, -4 up
            if self.centers[k,1] < 0.0 self.labels[k]= -1 end
            if self.centers[k,2] < 0.0 self.labels[k]= -2 end
            if self.centers[k,1] > self.Lx self.labels[k]= -3 end
            if self.centers[k,2] > self.Ly self.labels[k]= -4 end
            
            ### Positive label for internal cell. 
            ### 0 for internal cells
            ### 1,2,3,4 for first layers (left, bottom, right, up)
            ### 12,23,34,41 for coins (left, bottom, right, up)
            if (0. < self.centers[k,1] < self.hx) && (0. < self.centers[k,2] < self.Ly) 
                self.labels[k]= 1 
            end
            if (0. < self.centers[k,2] < self.hy) && (0. < self.centers[k,1] < self.Lx) 
                self.labels[k]= 2 
            end
            if (self.Lx > self.centers[k,1] > self.Lx-self.hx)  && (0. < self.centers[k,2] < self.Ly)  
                self.labels[k]= 3 
            end
            if (self.Ly > self.centers[k,2] > self.Ly-self.hy) && (0. < self.centers[k,1] < self.Lx)  
                self.labels[k]= 4 
            end
            
            if (0. < self.centers[k,1] < self.hx) && (0. < self.centers[k,2] < self.hy) 
                self.labels[k]= 12 
            end
            if (0. < self.centers[k,2] < self.hy) && (self.Lx > self.centers[k,1] > self.Lx-self.hx) 
                self.labels[k]= 23 
            end
            if (self.Lx > self.centers[k,1] > self.Lx-self.hx) && (self.Ly > self.centers[k,2] > self.Ly-self.hy)  
                self.labels[k]= 34 
            end
            if (self.Ly > self.centers[k,2] > self.Ly-self.hy) && (0. < self.centers[k,1] < self.hx) 
                self.labels[k] = 41 
            end

            k = k + 1
        end
    end
    
    for k in 1:self.Nv
        self.nodes[k,:]=newmesh(self.nodes[k,:],self.hx,self.hy,self.Lx,self.Ly)                   
    end  
    for k in 1:self.Nc
        self.centers[k,:]=newmesh(self.centers[k,:],self.hx,self.hy,self.Lx,self.Ly)                   
    end 
 
    
    
end


