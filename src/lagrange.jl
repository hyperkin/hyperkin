export interpolynome, NullPolynome, lagrange
export set_xyi, set_xi, set_xyh
export interpolator, lagrange_i
export histopolator
export celllagrange_i, dcelllagrange_i

##### Asbtract class for the polynomial interpolation
abstract type interpolynome end
struct NullPolynome<: interpolynome
end
function (self::NullPolynome)(x)
end

##### Class for interpolation/histopolation using Lagrange Polynomial order 2p+1
mutable struct lagrange

  xivals    :: Vector{Float64}      # table of the points x for interpolation
  yivals    :: Vector{Float64}      # table of the valuew y=f(x) for interpolation
  xhvals    :: Vector{Float64}      # table of the points x for histopolation
  yhvals    :: Vector{Float64}      # table of the valuew y=f(x) for histopolation 
  p        :: Int64                 # order (2p+1)

  ##### Constructor using the p value  
  function lagrange(p)
    xi = zeros(Float64,2*p+2)
    yi = zeros(Float64,2*p+2)

    xh = zeros(Float64,2*p+1)
    yh = zeros(Float64,2*p+1)
    q=p        
    new(xi,yi,xh,yh,q)
  end 
end 

##### Method to gives the values x and y at the class
## inputs: x[:] and y[:] (x and y values for interpolation)
function set_xyi(self::lagrange,x,y)
    self.xivals = x
    self.yivals = y
end  

##### Method to gives the values x at the class
## inputs: x[:] (x values for interpolation)
function set_xi(self::lagrange,x)
    self.xivals = x
end  

##### Method to gives the values x and y at the class
## inputs: x[:] and y[:] (x and y values for histopolation)
function set_xyh(self::lagrange,x,y)
    self.xhvals = x
    self.yhvals = y
end    

##### Method to compute the value of the interpolation polynomial at the point x
## input: x (spatial point)
## output: p(x) (interpolated value)
function interpolator(self::lagrange,x)
    poly = 0.0
    for i = 1:2*self.p+2
        alpha = i - 1 - self.p 
        poly = poly + self.yivals[i]*lagrange_i(self,alpha,x)
    end
    return poly
end

##### Method to compute one Lagrange polynomial evaluated at the point x for interpolation
## inputs: alpha (number of the lagrange function -p <= alpha <= p+1), x (spatial point)
## output: P_alpha(x)
function lagrange_i(self::lagrange, alpha, x)
    ialpha = alpha + 1 + self.p
    lag = 1.0
    for i in 1:2*self.p+2
        if i != ialpha
            lag = lag*(x-self.xivals[i])/(self.xivals[ialpha]-self.xivals[i])
        end    
    end  
    return lag
end    

##### Method to compute the value of the histopolation polynomial evaluated at the point x
## input: x (spatial point)
## output: p(x) (histopolation value)
function histopolator(self::lagrange,x)
    poly = 0.0
    for i = 1:2*self.p+1
        alpha = i - 1 - self.p 
        poly = poly + self.yhvals[i]*celllagrange_i(self,alpha,x)
    end
    return poly
end


##### Method to compute one Lagrange polynomial evaluated at the point x for histopolation
## inputs: alpha (number of the lagrange function -p <= alpha <= p+1), x (spatial point)
## output: P_alpha(x)
function celllagrange_i(self::lagrange, alpha, x)
    lag = 0.0
    for i in 1:2*self.p+2
        beta = i - 1 - self.p 
        if beta > alpha
            lag = lag +  dcelllagrange_i(self,beta,x)
        end    
    end  
    return lag
end   

##### Method to compute derivate of one Lagrange polynomial evaluated at the point x
## inputs: alpha (number of the lagrange function -p <= alpha <= p+1), x (spatial point)
## output: partial P_alpha(x)
function dcelllagrange_i(self::lagrange, alpha, x)
    ialpha = alpha + 1 + self.p
    sum = 0.0
    dem = 1.0
    for i in 1:2*self.p+2
        if i != ialpha
            lag = 1.0
            for k in 1:2*self.p+2
                if k != i && k!= ialpha
                    lag = lag*(x-self.xivals[k])
                end    
            end  
            sum = sum + lag
            dem = dem*(self.xivals[ialpha]-self.xivals[i])    
        end    
    end  
    return sum/dem
end    

