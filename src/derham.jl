export C0, C0bar, C1, C1bar
export bc, bc_ho, stencil, inter
export R0loc, R0, I0loc, R1loc, R1, I1loc
export H0bar, H0, H1bar, H1
export grad, diagnostics, graddual
export R0barloc, R0bar, I0barloc, I1barloc, R1bar, R1barloc


################ space ##################
mutable struct C0 <: DeRhamSpace
    ndof       :: Int64
    mh         :: Mesh
    field      :: Vector{Float64}
    bc_type    :: Int64
    order      :: Int64
    ndiags     :: Int64

    function C0(mh,bc)
        ndof = mh.Nv-1
        field     = zeros(Float64,ndof)
        bc_type   = bc
        order     = 0 # 2 order +1
        ng        = 0
        new(ndof, mh, field, bc_type, order, ng )
    end

    function C0(mh,bc,ng)
        ndof = mh.Nv-1
        field     = zeros(Float64,ndof)
        bc_type   = bc
        order     = 0 # 2 order +1
        new(ndof, mh, field, bc_type, order,ng)
    end

    function C0(mh, bc, or, ng)
        ndof = mh.Nv-1
        field     = zeros(Float64,ndof)
        bc_type   = bc
        new(ndof, mh, field, bc_type, or, ng)
    end
end   

mutable struct C0bar <: DeRhamSpace
    ndof       :: Int64
    mh         :: Mesh
    field      :: Vector{Float64}
    bc_type    :: Int64
    order      :: Int64
    ndiags     :: Int64

    function C0bar(mh, bc )
        ndof        = mh.Nc
        field       = zeros(Float64,ndof)
        bc_type     = bc
        order       = 0 # 2 order +1 
        ng          = 0 
  
        new(ndof, mh, field, bc, order, ng)
    end

    function C0bar(mh, bc, ng )
        ndof        = mh.Nc
        field       = zeros(Float64,ndof)
        bc_type     = bc
        order       = 0 # 2 order +1 
  
        new(ndof, mh, field, bc, order, ng)
    end

    function C0bar(mh, bc, or, ng )
        ndof        = mh.Nc
        field       = zeros(Float64,ndof)
        bc_type     = bc

        new(ndof, mh, field, bc, or, ng)
    end
end   

mutable struct C1 <: DeRhamSpace
    ndof       :: Int64
    mh         :: Mesh
    field      :: Vector{Float64}
    bc_type    :: Int64
    order      :: Int64
    ndiags     :: Int64

    function C1(mh, bc)
        ndof      = mh.Nc
        field     = zeros(Float64,ndof)
        bc_type   = bc
        order     = 0 # 2 order +1
        ng        = 0
  
        new(ndof, mh, field, bc_type, order,ng)
    end

    function C1(mh, bc, ng)
        ndof      = mh.Nc
        field     = zeros(Float64,ndof)
        bc_type   = bc
        order     = 0 # 2 order +1
  
        new(ndof, mh, field, bc_type, order, ng)
    end

    function C1(mh, bc, or, ng)
        ndof      = mh.Nc
        field     = zeros(Float64,ndof)
        bc_type   = bc
  
        new(ndof, mh, field, bc_type, or, ng)
    end
end   

mutable struct C1bar <: DeRhamSpace
    ndof       :: Int64
    mh         :: Mesh
    field      :: Vector{Float64}
    bc_type    :: Int64
    order      :: Int64
    ndiags     :: Int64

    function C1bar(mh, bc)
        ndof = mh.Nv-1
        field     = zeros(Float64,ndof)
        bc_type = bc
        order   = 0 # 2 order +1 
        ng      =0
  
        new(ndof, mh, field, bc_type, order, ng)
    end

    function C1bar(mh, bc, ng)
        ndof = mh.Nv-1
        field     = zeros(Float64,ndof)
        bc_type = bc
        order   = 0 # 2 order +1 
  
        new(ndof, mh, field, bc_type, order, ng)
    end

    function C1bar(mh, bc, or, ng)
        ndof = mh.Nv-1
        field     = zeros(Float64,ndof)
        bc_type = bc
  
        new(ndof, mh, field, bc_type, or, ng)
    end
end   

################## functions C0 ####################
function bc(self::C0)
    if self.bc_type ==1 # periodic
      bcl = self.field[end]
      bcr = self.field[1]
    end  
    
    if self.bc_type ==2 # neumann
        bcl = self.field[1]        
        bcr = self.field[end]
    end 
    
    if self.bc_type ==3 # dirichlet homogène
        bcl = 0.0       
        bcr = 0.0
    end 
    return [bcl, bcr]  
end  

function bc_ho(self::C0)
    if self.bc_type ==1 # periodic
      bcll = self.field[end-1]
      bcl = self.field[end]
      bcr = self.field[1]
      bcrr  = self.field[2]
    end  
    return [bcll, bcl, bcr, bcrr]  
end  
    

function R0loc(self::C0, i , func)
    x = self.mh.nodes[i]
    self.field[i] = func(x)
end

function R0(self::C0, func)
    for i in 1:self.ndof
        R0loc(self,i,func)
    end
end

function stencil(self::C0, i)
    xvals = zeros(Float64,2*self.order+2)
    yvals = zeros(Float64,2*self.order+2)
    if self.order == 0
        bcl, bcr = bc(self)
        if i == self.ndof           
            xvals = [self.mh.nodes[self.ndof], self.mh.nodes[self.ndof+1]]
            yvals = [self.field[self.ndof], bcr]
        elseif  i == 0
            xvals = [self.mh.bc_nodes[2], self.mh.nodes[1]]
            yvals = [bcl, self.field[1]]  
        else
            xvals = [self.mh.nodes[i], self.mh.nodes[i+1]]
            yvals = [self.field[i], self.field[i+1]]
        end  
    end 
    
    if self.order == 1
        bcll, bcl, bcr, bcrr = bc_ho(self)
        if i == self.ndof -1
            xvals = [self.mh.nodes[self.ndof-2], self.mh.nodes[self.ndof-1],self.mh.nodes[self.ndof], self.mh.nodes[self.ndof+1]]
            yvals = [self.field[self.ndof-2], self.field[self.ndof-1], self.field[self.ndof], bcr]                   
        elseif i == self.ndof     
            xvals = [self.mh.nodes[self.ndof-1],self.mh.nodes[self.ndof], self.mh.nodes[self.ndof+1],self.mh.bc_nodes[3]]
            yvals = [self.field[self.ndof-1], self.field[self.ndof], bcr, bcrr]         
        elseif  i == 0
            xvals = [self.mh.bc_nodes[1], self.mh.bc_nodes[2], self.mh.nodes[1], self.mh.nodes[2]]
            yvals = [bcll, bcl, self.field[1], self.field[2]]       
        elseif  i == 1
            xvals = [self.mh.bc_nodes[2], self.mh.nodes[1], self.mh.nodes[2], self.mh.nodes[3]]
            yvals = [bcl, self.field[1], self.field[2], self.field[3]] 
        else
            xvals = [self.mh.nodes[i-1], self.mh.nodes[i], self.mh.nodes[i+1], self.mh.nodes[i+2]]
            yvals = [self.field[i-1], self.field[i], self.field[i+1], self.field[i+2]]
        end  
    end

    [xvals, yvals]
end   

function I0loc(self::C0, i)
    xvals = zeros(Float64,2*self.order+2)
    yvals = zeros(Float64,2*self.order+2)

    xvals, yvals = stencil(self,i)
    la=lagrange(self.order)
    set_xyi(la,xvals,yvals)
    
    function inter(x)
        res=interpolator(la,x)
        return res
    end
    return inter 
end

function H1bar(self::C0, other::C1bar)
    for i in 2:other.ndof
        funcl=I0loc(self,i-1)
        funcr=I0loc(self,i)
        intl = 0.0
        intr = 0.0
        for k in 1:self.mh.Deg+1
            wl = wgdual1(self.mh,i,k)     
            xl = pgdual1(self.mh,i,k)
            wr = wgdual2(self.mh,i,k)     
            xr = pgdual2(self.mh,i,k)
            intl += wl*funcl(xl)
            intr += wr*funcr(xr)           
        end    
        other.field[i] = intl+intr
    end 
    funcl=I0loc(self,0)
    funcr=I0loc(self,1)  
    intl = 0.0
    intr = 0.0
    for k in 1:self.mh.Deg+1
        wl = wgdual1(self.mh,1,k)  
        xl = pgdual1(self.mh,1,k)
        wr = wgdual2(self.mh,1,k)     
        xr = pgdual2(self.mh,1,k)
        intl += wl*funcl(xl)
        intr += wr*funcr(xr)           
    end 
    other.field[1] = intl+intr
end 


function grad(self::C0, other::C1)
    for i in 1:self.ndof-1
        other.field[i] = self.field[i+1]-self.field[i]
    end    
    bcl, bcr = bc(self)
    other.field[end] = bcr - self.field[end]
end 

function diagnostics(self::C0,ref,ti,f_ref,mapping,form_diags)
    diags = zeros(self.ndiags)
    fg = 0.0
    fg_ref = 0.0
    field_ref = zeros(Float64,self.ndof)
    field    = zeros(Float64,self.ndof)
    x = zeros(Float64,self.ndof)

    if ref == 0
       for i in 1:self.ndof 
        x[i] = self.mh.nodes[i]
        field[i] = mapping(self.field[i])
        diags[:] += self.mh.dualareas[i]*form_diags(field[i])
      end
      [x, field, diags]
    else
       for i in 1:self.ndof
     
        x[i] = self.mh.nodes[i]
        field[i] = mapping(self.field[i])
        field_ref[i] = mapping(f_ref(x[i],ti))

        diags[:]  += self.mh.dualareas[i]*form_diags(field[i]-field_ref[i])
      end
      [x, field, field_ref, diags] 
    end
end

################## functions C0bar ####################
function bc(self::C0bar)
    if self.bc_type ==1 # periodic
      bcl = self.field[end]
      bcr = self.field[1]
    end  
    
    if self.bc_type ==2 # neumann
        bcl = self.field[1]        
        bcr = self.field[end]
    end 
    
    if self.bc_type ==3 # dirichlet homogène
        bcl = 0.0       
        bcr = 0.0
    end 
    return [bcl, bcr]  
end   

function bc_ho(self::C0bar)
    if self.bc_type ==1 # periodic
      bcll = self.field[end-1]
      bcl = self.field[end]
      bcr = self.field[1]
      bcrr = self.field[2]
    end  
    return [bcll, bcl, bcr, bcrr]  
end    


function R0barloc(self::C0bar,i, func)
    x = self.mh.centers[i]
    self.field[i] = func(x)
end

function R0bar(self::C0bar, func)
    for i in 1:self.ndof
        R0barloc(self,i,func)
    end
end

function stencil(self::C0bar, i)

    xvals = zeros(Float64,2*self.order+2)
    yvals = zeros(Float64,2*self.order+2)
    if self.order == 0
        bcl, bcr = bc(self)
        if i == 1
            xvals = [self.mh.bc_centers[2], self.mh.centers[1]]
            yvals = [bcl, self.field[1]]   
        elseif i == self.ndof  +1     
            xvals = [self.mh.centers[end],self.mh.bc_centers[3]]
            yvals = [self.field[end], bcr] 
        else
            xvals = [self.mh.centers[i-1], self.mh.centers[i]]
            yvals = [self.field[i-1], self.field[i]]
        end  
    end  
    if self.order == 1
        bcll, bcl, bcr, bcrr = bc_ho(self)
        if i == 1
            xvals = [self.mh.bc_centers[1], self.mh.bc_centers[2], self.mh.centers[1], self.mh.centers[2]]
            yvals = [bcll, bcl, self.field[1], self.field[2]]   
        elseif i == 2
            xvals = [self.mh.bc_centers[2], self.mh.centers[1], self.mh.centers[2], self.mh.centers[3]]
            yvals = [bcl, self.field[1], self.field[2], self.field[3]]    
        elseif i == self.ndof      
            xvals = [self.mh.centers[end-2], self.mh.centers[end-1], self.mh.centers[end],self.mh.bc_centers[3]]
            yvals = [self.field[end-2], self.field[end-1], self.field[end], bcr, bcrr]        
        elseif i == self.ndof  +1     
            xvals = [self.mh.centers[end-1], self.mh.centers[end],self.mh.bc_centers[3],self.mh.bc_centers[4]]
            yvals = [self.field[end-1], self.field[end], bcr, bcrr] 
        else
            xvals = [self.mh.centers[i-2], self.mh.centers[i-1], self.mh.centers[i], self.mh.centers[i+1]]
            yvals = [self.field[i-2], self.field[i-1], self.field[i], self.field[i+1]]
        end  
    end  
    [xvals, yvals]
end     

function I0barloc(self::C0bar, i)
    xvals = zeros(Float64,2*self.order+2)
    yvals = zeros(Float64,2*self.order+2)
    
    xvals, yvals =stencil(self,i)
    la=lagrange(self.order)
    set_xyi(la,xvals,yvals)
    
    function inter(x)
        res=interpolator(la,x)
        return res
    end
    return inter         
end

function H1(self::C0bar, other::C1)
    intl = 0.0
    intr = 0.0
    for i in 1:other.ndof
        funcl=I0barloc(self,i)
        funcr=I0barloc(self,i+1)
        intl =0.0
        intr =0.0
        for k in 1:self.mh.Deg+1
            wl = wg1(self.mh,i,k)  
            xl = pg1(self.mh,i,k)
            wr = wg2(self.mh,i,k)     
            xr = pg2(self.mh,i,k)
            intl += wl*funcl(xl)
            intr += wr*funcr(xr)           
        end 
        other.field[i] = intl+intr
    end    
end 

function graddual(self::C0bar, other:: C1bar)
    for i in 2:self.ndof
        other.field[i] = self.field[i-1]-self.field[i]
    end    
    bcl, bcr = bc(self)
    other.field[1] =  bcl - self.field[1]
end 

function diagnostics(self::C0bar,ref,ti,f_ref,mapping,form_diags)
    diags = zeros(self.ndiags)
    fg = 0.0
    fg_ref = 0.0
    field_ref = zeros(Float64,self.ndof)
    x = zeros(Float64,self.ndof)
    field = zeros(Float64,self.ndof)

    if ref == 0
       for i in 1:self.ndof 
        x[i] = self.mh.centers[i]
        field[i] = mapping(self.field[i])
        diags[:] += self.mh.areas[i]*form_diags(field[i])
      end
      [x, field, diags] 
    else
       for i in 1:self.ndof
        
        x[i] = self.mh.centers[i]
        field_ref[i]= mapping(f_ref(x[i],ti))
        field[i]= mapping(self.field[i])
        diags[:]  += self.mh.areas[i]*form_diags(field[i]-field_ref[i])
      end
      [x, field, field_ref, diags] 
    end
end
 

################## functions C1 ####################
function bc(self::C1)
    if self.bc_type ==1 # periodic
      bcl = self.field[end]
      bcr = self.field[1]
    end  
    
    if self.bc_type ==2 # neumann
        bcl = self.field[1]        
        bcr = self.field[end]
    end 
    
    if self.bc_type ==3 # dirichlet homogène
        bcl = 0.0       
        bcr = 0.0
    end 
    return [bcl, bcr]  
end    

function bc_ho(self::C1)
    if self.bc_type ==1 # periodic
      bcll = self.field[end-1]
      bcl = self.field[end]
      bcr = self.field[1]
      bcrr = self.field[2]
    end  
    return [bcll, bcl, bcr, bcrr]  
end 

function R1loc(self::C1, i, func)
    self.field[i] = 0.0
    for k in 1:self.mh.Deg+1
        w = wg(self.mh,i,k)     
        x = pg(self.mh,i,k)
        self.field[i] += w*func(x)
    end    
end

function R1(self::C1, func)
    for i in 1:self.ndof
        R1loc(self,i,func)
    end
end

function stencil(self::C1, i)
    xivals = zeros(Float64,2*self.order+2)
    xhvals = zeros(Float64,2*self.order+1)
    yhvals = zeros(Float64,2*self.order+1)

    if self.order == 0
        bcl, bcr = bc(self)
        xivals = [self.mh.nodes[i], self.mh.nodes[i+1]]  
        xhvals = [self.mh.centers[i]]
        yhvals = [self.field[i]]
    end  
    if self.order == 1
        bcll, bcl, bcr, bcrr = bc_ho(self)
        if i ==1
            xivals = [self.mh.bc_nodes[2], self.mh.nodes[1], self.mh.nodes[2], self.mh.nodes[3]]  
            xhvals = [self.mh.bc_centers[2], self.mh.centers[1], self.mh.centers[2]]
            yhvals = [bcl, self.field[1], self.field[2]]  
        elseif i== self.ndof
            xivals = [self.mh.nodes[i-1], self.mh.nodes[i], self.mh.nodes[i+1], self.mh.bc_nodes[3]]  
            xhvals = [self.mh.centers[i-1], self.mh.centers[i], self.mh.bc_centers[3]]
            yhvals = [self.field[i-1], self.field[i], bcr]
        else   
            xivals = [self.mh.nodes[i-1], self.mh.nodes[i], self.mh.nodes[i+1], self.mh.nodes[i+2]]  
            xhvals = [self.mh.centers[i-1], self.mh.centers[i], self.mh.centers[i+1]]
            yhvals = [self.field[i-1], self.field[i], self.field[i+1]]
        end    
    end  
    [xivals, xhvals, yhvals]
end

function I1loc(self::C1, i)
    xivals = zeros(Float64,2*self.order+2)
    xhvals = zeros(Float64,2*self.order+1)
    yhvals = zeros(Float64,2*self.order+1)

    xivals, xhvals, yhvals = stencil(self,i)
    la=lagrange(self.order)
    set_xi(la,xivals)
    set_xyh(la,xhvals,yhvals)
    
    function inter(x)
        res=histopolator(la,x)
        return res
    end
    return inter 
end

function H0bar(self::C1, other::C0bar)
    for i in 1:other.ndof
        func=I1loc(self,i)
        R0barloc(other,i,func)
    end    
end 


function diagnostics(self::C1,ref,ti,f_ref,mapping,form_diags)
    diags = zeros(self.ndiags)
    fg = 0.0
    field_ref = zeros(Float64,self.ndof)
    x = zeros(Float64,self.ndof)
    field = zeros(Float64,self.ndof)

    if ref == 0
       for i in 1:self.ndof 
        x[i] = self.mh.centers[i]
        field[i] = mapping(self.field[i])/self.mh.areas[i]
        diags[:] += self.mh.areas[i]*form_diags(field[i])
      end
      [x, field, diags]
    else
       for i in 1:self.ndof
        x[i] = self.mh.centers[i]
        field[i] = mapping(self.field[i])/self.mh.areas[i]
        sum = 0.0
        for  k in 1:self.mh.Deg+1
            sum = sum + wg(self.mh,i,k)*mapping(f_ref(pg(self.mh,i,k),ti))
        end    
        field_ref[i] = sum/self.mh.areas[i]
        diags[:]  += self.mh.areas[i]*form_diags(field[i]-field_ref[i])
      end
      [x, field, field_ref, diags] 
    end
end

################## functions C1bar ####################
function bc(self::C1bar)
    if self.bc_type ==1 # periodic
      bcl = self.field[end]
      bcr = self.field[1]
    end  
    
    if self.bc_type ==2 # neumann
        bcl = self.field[1]        
        bcr = self.field[end]
    end
    
    if self.bc_type ==3 # dirichlet homogène
        bcl = 0.0       
        bcr = 0.0
    end 
    return [bcl, bcr]  
end    

function bc_ho(self::C1bar)
    if self.bc_type ==1 # periodic
      bcll = self.field[end-1]
      bcl = self.field[end]
      bcr = self.field[1]
      bcrr  = self.field[2]
    end  
    return [bcll, bcl, bcr, bcrr]  
end   

function R1barloc(self::C1bar, i, func)
    self.field[i] = 0.0
   # println(">>> i",i)
    for k in 1:self.mh.Deg+1
        w = wgdual(self.mh,i,k)     
        x = pgdual(self.mh,i,k)
        self.field[i] += w*func(x)
        #println("mmm ",w," ",x," ",func(x))
    end   
   #println("final result",self.field[i])
end

function R1bar(self::C1bar, func)
    for i in 1:self.ndof
        R1barloc(self,i,func) 
    end
end


function stencil(self::C1bar, i)
    xivals = zeros(Float64,2*self.order+2)
    xhvals = zeros(Float64,2*self.order+1)
    yhvals = zeros(Float64,2*self.order+1)
    if self.order == 0
        bcl, bcr = bc(self)
        if i == 1
            xivals = [self.mh.bc_centers[2], self.mh.centers[1]]
            xhvals = [self.mh.nodes[1]]  
            yhvals = [self.field[i]]
        elseif i== self.ndof+1
            xivals = [self.mh.centers[end], self.mh.bc_centers[3]]
            xhvals = [self.mh.nodes[self.ndof+1]]  
            yhvals = [bcr]
        else    
            xivals = [self.mh.centers[i-1], self.mh.centers[i]]
            xhvals = [self.mh.nodes[i]]  
            yhvals = [self.field[i]]
        end
    end  

    if self.order == 1
        bcll, bcl, bcr, bcrr = bc_ho(self)
        if i ==1
            xivals = [self.mh.bc_centers[1], self.mh.bc_centers[2], self.mh.centers[1], self.mh.centers[2]]
            xhvals = [self.mh.bc_nodes[2], self.mh.nodes[1], self.mh.nodes[2]]  
            yhvals = [bcl, self.field[1], self.field[2]]
        elseif i ==2
            xivals = [self.mh.bc_centers[2], self.mh.centers[1], self.mh.centers[2], self.mh.centers[3]]  
            xhvals = [self.mh.nodes[1], self.mh.nodes[2], self.mh.nodes[3]]  
            yhvals = [self.field[1], self.field[2], self.field[3]]
        elseif i== self.ndof
            xivals = [self.mh.centers[end-2], self.mh.centers[end-1], self.mh.centers[end], self.mh.bc_centers[3]]
            xhvals = [self.mh.nodes[self.ndof-1], self.mh.nodes[self.ndof], self.mh.nodes[self.ndof+1]]  
            yhvals = [self.field[self.ndof-1], self.field[self.ndof], bcr]
        elseif i== self.ndof +1
            xivals = [self.mh.centers[end-1], self.mh.centers[end], self.mh.bc_centers[3], self.mh.bc_centers[4]]
            xhvals = [self.mh.nodes[self.ndof], self.mh.nodes[self.ndof+1], self.mh.bc_nodes[3]]  
            yhvals = [self.field[self.ndof], bcr, bcrr]    
        else
            xivals = [self.mh.centers[i-2], self.mh.centers[i-1], self.mh.centers[i], self.mh.centers[i+1]] 
            xhvals = [self.mh.nodes[i-1], self.mh.nodes[i], self.mh.nodes[i+1]]  
            yhvals = [self.field[i-1], self.field[i], self.field[i+1]]
        end    
    end 
    [xivals, xhvals, yhvals]
end

function I1barloc(self::C1bar, i)
    xivals = zeros(Float64,2*self.order+2)
    xhvals = zeros(Float64,2*self.order+1)
    yhvals = zeros(Float64,2*self.order+1)

    xivals, xhvals, yhvals = stencil(self,i)
    
    la=lagrange(self.order)
    set_xi(la,xivals)
    set_xyh(la,xhvals,yhvals)
    
    function inter(x)
        res=histopolator(la,x)
        return res
    end
    return inter  
end

function H0(self:: C1bar, other::C0)
    for i in 1:other.ndof
        func=I1barloc(self,i)
        R0loc(other,i,func)
    end    
end 

function diagnostics(self::C1bar,ref,ti,f_ref,mapping,form_diags)
    diags = zeros(self.ndiags)
    fg = 0.0
    field_ref = zeros(Float64,self.ndof)
    x = zeros(Float64,self.ndof)
    field = zeros(Float64,self.ndof)

    if ref == 0
       for i in 1:self.ndof 
        x[i] = self.mh.nodes[i]
        field[i] = mapping(self.field[i])/self.mh.dualareas[i]
        diags[:] += self.mh.dualareas[i]*form_diags(field[i])
      end
      [x, field, diags]
    else
       for i in 1:self.ndof
        x[i] = self.mh.nodes[i]
        field[i] = mapping(self.field[i])/self.mh.dualareas[i]
        sum = 0.0
        for  k in 1:self.mh.Deg+1
            sum = sum + wgdual(self.mh,i,k)*mapping(f_ref(pgdual(self.mh,i,k),ti))
        end    
        field_ref[i] = sum/self.mh.dualareas[i]

        diags[:]  += self.mh.dualareas[i]*form_diags(field[i]-field_ref[i])
      end
      [x, field, field_ref, diags] 
    end
end
