using Random
export Mesh
export pg, pg1, pg2, pgl, pgr, dpg, wg, wg1, wg2, wgl, wgr
export pgdual, wgdual, pgdual1, pgdual2, wgdual1, wgdual2
export smooth_mesh, translate_mesh, id_mesh, random_mesh
export refine_mesh, de_refine_mesh, plot_mesh_ratio

######### 3 mapping for 1D mesh #########
## intputs: x (position), h (mesh step), L (size of domain)
## output: x (modified position)
function smooth_mesh(x,h,L)

    if x != 0.0 && x != L
        a = x + 0.12 * sin(2π/L*x)
    else
        a = x
    end
    return a
end    

function random_mesh(x,h,L)

    if x != 0.0 && x != L
        r= rand(Float64)
        p= rand(Float64)
        if p <0.5
            a = x + r*0.35*h
        else
            a = x - r*0.35*h
        end
    else
        a = x
    end
    return a
end    


function translate_mesh(x,h,L)
    Lx = 0.5*L
    a = -Lx + x
    x = a
    return x 
end 

function id_mesh(x,h,L)
    return x 
end

######### Class 1D mesh ###########
mutable struct Mesh

    Nc         :: Int               # nb cells
    L          :: Float64           # size of domain 
    Nv         :: Int               # nb vertex
    h          :: Float64           # mesh step 
    nodes      :: Vector{Float64}   # table of nodes
    dualareas  :: Vector{Float64}   # table of volumes control area associated at each nodes
    centers    :: Vector{Float64}   # table of cell centers
    areas      :: Vector{Float64}   # table of cell areas
    bc_centers :: Vector{Float64}   # table of ghost cell centers (two at each side)
    bc_nodes   :: Vector{Float64}   # table of ghost cell nodes   (two at each side)
    bc_areas   :: Vector{Float64}   # table of ghost cell areas (two at each side)
    pgauss     :: Vector{Float64}   # table of reference gauss points on [0,1]
    wgauss     :: Vector{Float64}   # table of reference gauss points on [0,1]
    dpgauss    :: Array{Float64,2}  # table of derivartive to Lagrange polynomial at each gauss point. Lign: Lagrange fonction, colonn: Gauss point
    Deg        :: Int               # Degree of polynomial in eahc cell. Nb Gauss point = Deg+1
       
    ##### Contructor 1
    ## inputs: L (size of domain), Nc (nb cells)
    ## By definition the degree is 0.
    function Mesh(L, Nc)
        Nv         = (Nc+1)
        h          = L/Nc
        nodes      = zeros(Float64,Nv)
        dualareas  = zeros(Float64,Nv)
        centers    = zeros(Float64,Nc)
        areas      = zeros(Float64,Nc)
        bc_centers = zeros(Float64,4)
        bc_nodes   = zeros(Float64,4)
        bc_areas    = zeros(Float64,4)
        pgauss     = zeros(Float64,1)
        wgauss     = zeros(Float64,1)
        dpgauss    = zeros(Float64,(1,1))
        Deg      = 0

        new(Nc,L,Nv,h,nodes,dualareas,centers,areas,bc_centers,bc_nodes,bc_areas,pgauss,wgauss,dpgauss,Deg)    
    end

    ##### Contructor 2
    ## inputs: L (size of domain), Nc (nb cells), d (degree)
    function Mesh(L, Nc, d)
        Nv         = (Nc+1)
        h          = L/Nc
        nodes      = zeros(Float64,Nv)
        dualareas  = zeros(Float64,Nv)
        centers    = zeros(Float64,Nc)
        areas      = zeros(Float64,Nc)
        bc_centers = zeros(Float64,4)
        bc_nodes   = zeros(Float64,4)
        bc_areas    = zeros(Float64,4)
        pgauss     = zeros(Float64,d+1)
        wgauss     = zeros(Float64,d+1)
        dpgauss    = zeros(Float64,(d+1,d+1))

        new(Nc,L,Nv,h,nodes,dualareas,centers,areas,bc_centers,bc_nodes,bc_areas,pgauss,wgauss,dpgauss,d)    
    end
end
   
##### Operator to construct the mesh. Call for a mesh M: M(mapping)
## input: Function (mapping of the mesh)
function (self :: Mesh)(newmesh :: Function)
     
    ### Construction of nodes/centers/areas and Bc
    for i in 1:self.Nv 
        self.nodes[i]=(i-1)*self.h
        self.nodes[i]=newmesh(self.nodes[i],self.h,self.L)
    end
    
    construct_mesh(self)
    
end

## input: Function (mapping of the mesh)
function plot_mesh_ratio(self::Mesh,inside)
    minr=0.0
    ### Construction of nodes/centers/areas and Bc
    for i in 2:self.Nc-1 
       if inside == true
           println(">>> cell >>>",i," ratio >>",(self.dualareas[i]+self.dualareas[i+1])/(2.0*self.areas[i]))
       end
       minr=max(minr,(self.dualareas[i]+self.dualareas[i+1])/(2.0*self.areas[i])) 
    end
    println(">>> max ratio >>>",minr)
end


function construct_mesh(self::Mesh)
    for i in 1:self.Nc
        self.centers[i] = 0.5*(self.nodes[i]+self.nodes[i+1])
        self.areas[i] = (self.nodes[i+1]-self.nodes[i])
    end
   
    for i in 2:self.Nv-1
        self.dualareas[i] = (self.centers[i]-self.centers[i-1])  
    end
    
    self.bc_areas   = [ self.h, self.h, self.h, self.h  ]
    self.bc_nodes   = [ -2.0*self.h+self.nodes[1],-self.h+self.nodes[1],self.nodes[end]+self.h,self.nodes[end]+2.0*self.h ]
    self.bc_centers = [ -1.5*self.h+self.nodes[1],-0.5*self.h+self.nodes[1],self.nodes[end]+0.5*self.h,self.nodes[end]+ 1.5*self.h]
        
    self.dualareas[1]= self.centers[1]-self.bc_centers[2]    
    self.dualareas[self.Nv]= self.bc_centers[3]-self.centers[self.Nc]   
  
    mini=1000.0
    for i in 1:self.Nc
        mini=min(mini,self.areas[i])  
    end
    self.h=mini

    ###  Construction of Gauss point (domain reference for gauss point are on [0,1])
    if self.Deg == 0
        self.pgauss[1]=0.5
        self.wgauss[1]=1.0
        self.dpgauss[1] =0.0
    end    
    if self.Deg == 1   
        self.pgauss[1]=0.0
        self.pgauss[2]=1.0
        self.wgauss[1]=0.5
        self.wgauss[2]=0.5  
        self.dpgauss[1,1] = -1.0  
        self.dpgauss[1,2] = -1.0
        self.dpgauss[2,1] = 1.0
        self.dpgauss[2,2] = 1.0   
    end 
    if self.Deg == 2  
        self.pgauss[1]=0.0
        self.pgauss[2]=0.5
        self.pgauss[3]=1.0
        self.wgauss[1]=0.166666666666666666666666666667
        self.wgauss[2]=0.666666666666666666666666666668
        self.wgauss[3]=0.166666666666666666666666666667 
        self.dpgauss[1,1] = -3.0  
        self.dpgauss[1,2] = -1.0
        self.dpgauss[1,3] = 1.0
        self.dpgauss[2,1] = 4.0
        self.dpgauss[2,2] = 0.0       
        self.dpgauss[2,3] = -4.0 
        self.dpgauss[3,1] = -1.0  
        self.dpgauss[3,2] = 1.0
        self.dpgauss[3,3] = 3.0
    end       
    if self.Deg == 3  
        self.pgauss[1]=0.0
        self.pgauss[2]=0.276393202250021030359082633127
        self.pgauss[3]=0.723606797749978969640917366873
        self.pgauss[4]=1.0
        self.wgauss[1]=0.0833333333333333333333333333333
        self.wgauss[2]=0.416666666666666666666666666666
        self.wgauss[3]=0.416666666666666666666666666666 
        self.wgauss[4]=0.0833333333333333333333333333333
        self.dpgauss[1,1] = -6.0  
        self.dpgauss[1,2] = -1.61803398874989484820458683436
        self.dpgauss[1,3] = 0.618033988749894848204586834362
        self.dpgauss[1,4] = -1.0
        self.dpgauss[2,1] = 8.09016994374947424102293417177
        self.dpgauss[2,2] = 0.0       
        self.dpgauss[2,3] = -2.23606797749978969640917366872
        self.dpgauss[2,4] = 3.09016994374947424102293417184 
        self.dpgauss[3,1] = -3.09016994374947424102293417184
        self.dpgauss[3,2] = 2.23606797749978969640917366872
        self.dpgauss[3,3] = 0.0
        self.dpgauss[3,4] = -8.09016994374947424102293417177
        self.dpgauss[4,1] = 1.0
        self.dpgauss[4,2] = -0.618033988749894848204586834362       
        self.dpgauss[4,3] = 1.61803398874989484820458683436
        self.dpgauss[4,4] = 6.0
    end   
    if self.Deg == 4  
        self.pgauss[1]=0.0
        self.pgauss[2]=0.172673164646011428100853771877
        self.pgauss[3]=0.5
        self.pgauss[4]=0.827326835353988571899146228123
        self.pgauss[5]=1.0
        self.wgauss[1]=0.05
        self.wgauss[2]=0.272222222222222222222222222223
        self.wgauss[3]=0.355555555555555555555555555556
        self.wgauss[4]=0.272222222222222222222222222223
        self.wgauss[5]=0.05
        self.dpgauss[1,1] = -10.0  
        self.dpgauss[1,2] = -2.48198050606196571569743868436
        self.dpgauss[1,3] = 0.75
        self.dpgauss[1,4] = -0.518019493938034284302561315632
        self.dpgauss[1,5] = 1.0
        self.dpgauss[2,1] = 13.5130049774484800076860550594
        self.dpgauss[2,2] = 0.0
        self.dpgauss[2,3] = -2.67316915539090667050969419631       
        self.dpgauss[2,4] = 1.52752523165194666886268239794
        self.dpgauss[2,5] = -2.82032835588485332564727827404
        self.dpgauss[3,1] = -5.33333333333333333333333333336
        self.dpgauss[3,1] = 3.49148624377587810025755976667
        self.dpgauss[3,3] = 0.0
        self.dpgauss[3,4] = -3.49148624377587810025755976667
        self.dpgauss[3,5] = 5.33333333333333333333333333336
        self.dpgauss[4,1] = 2.82032835588485332564727827404 
        self.dpgauss[4,2] = -1.52752523165194666886268239794
        self.dpgauss[4,3] = 2.67316915539090667050969419631
        self.dpgauss[4,4] = 0.0
        self.dpgauss[4,5] = -13.5130049774484800076860550594
        self.dpgauss[5,1] = -1.0
        self.dpgauss[5,2] = 0.518019493938034284302561315632      
        self.dpgauss[5,3] = -0.75 
        self.dpgauss[5,4] = 2.48198050606196571569743868436
        self.dpgauss[5,5] = 10.0
    end     
end

function refine_mesh(self::Mesh, list_cells::Vector{Int64})
    n= size(list_cells)[1]
    NewMh=Mesh(self.L,self.Nc+n,self.Deg)
    j=1
    k=1
    for i in 1:self.Nv
        NewMh.nodes[j]=self.nodes[i]
        j=j+1
        if k<=n
            if list_cells[k]==i 
                NewMh.nodes[j]=self.centers[i]
                k=k+1     
                j=j+1
            end
        end
    end
            
    construct_mesh(NewMh)
    return NewMh     
end

function de_refine_mesh(self::Mesh, list_cells::Vector{Int64}) ## convention on fusionne avec celle de droite
    n= size(list_cells)[1]
    NewMh=Mesh(self.L,self.Nc-n,self.Deg)
    i=1
    k=1
    for j in 1:NewMh.Nv
        NewMh.nodes[j]=self.nodes[i]
        if k<=n
            if list_cells[k]==i 
                k=k+1     
                i=i+1
            end
        end
        i=i+1
    end
            
    construct_mesh(NewMh)
    return NewMh
     
end



##### Method gives a Gauss point (gp) of a cell
## inputs: icell (index of cell), ig (local index of gp)
## output: value of gp
function pg(self::Mesh, icell :: Int64, ig :: Int64)
  return self.areas[icell]*self.pgauss[ig]+self.nodes[icell]
end

##### Method gives a Gauss point (gp) of a left half cell
## inputs: icell (index of cell), ig (local index of gp)
## output: value of gp
function pg1(self::Mesh, icell :: Int64, ig :: Int64)
    return (self.centers[icell]-self.nodes[icell])*self.pgauss[ig]+self.nodes[icell]
end

##### Method gives a Gauss point (gp) of a right half cell
## inputs: icell (index of cell), ig (local index of gp)
## output: value of gp
function pg2(self::Mesh, icell :: Int64, ig :: Int64)
    return (self.nodes[icell+1]-self.centers[icell])*self.pgauss[ig]+self.centers[icell]
  end

##### Method gives a Gauss point (gp) of a left bc cell
## inputs: ig (local index of gp)
## output: value of gp
function pgl(self::Mesh, ig :: Int64)
  return self.bc_areas[2]*self.pgauss[ig]+self.bc_nodes[2]
end

##### Method gives a Gauss point (gp) of a right bc cell
## inputs: ig (local index of gp)
## output: value of gp
function pgr(self::Mesh, ig :: Int64)
  return self.bc_areas[3]*self.pgauss[ig]+self.nodes[end]
end

##### Method gives the derivative of Lagrange polynomial at a Gauss point
## inputs: cell (index of the cell), il( index of lagrange polynomial), ig (local index of gp)
## output: value of the derivative
function dpg(self::Mesh, icell :: Int64, il :: Int64, ig :: Int64) 
   return self.dpgauss[il,ig]/self.areas[icell]
end


##### Method gives a Gauss weight (wp) of a cell
## inputs: icell (index of cell), ig (local index of wp)
## output: value of wp
function wg(self::Mesh, icell :: Int64, ig :: Int64)
  return self.areas[icell]*self.wgauss[ig]
end

##### Method gives a Gauss weight (wp) of a half left cell
## inputs: icell (index of cell), ig (local index of wp)
## output: value of wp
function wg1(self::Mesh, icell :: Int64, ig :: Int64)
    return (self.centers[icell]-self.nodes[icell])*self.wgauss[ig]
end

##### Method gives a Gauss weight (wp) of a right half cell
## inputs: icell (index of cell), ig (local index of wp)
## output: value of wp
function wg2(self::Mesh, icell :: Int64, ig :: Int64)
    return (self.nodes[icell+1]-self.centers[icell])*self.wgauss[ig]
  end

##### Method gives a Gauss weight (wp) of the left BC cell
## inputs: icell (index of cell), ig (local index of wp)
## output: value of wp
function wgl(self::Mesh, ig :: Int64)
  return self.bc_areas[2]*self.wgauss[ig]
end

##### Method gives a Gauss weight (wp) of the right BC cell
## inputs: icell (index of cell), ig (local index of wp)
## output: value of wp
function wgr(self::Mesh, ig :: Int64)
  return self.bc_areas[3]*self.wgauss[ig]
end

##### Method gives a Gauss point (gp) of a dual cell
## inputs: inode (index of node), ig (local index of gp)
## output: value of gp
function pgdual(self::Mesh,inode :: Int64, ig :: Int64)
    if inode ==1
        res = (self.centers[1] - self.bc_centers[2])*self.pgauss[ig] + self.bc_centers[2]
    elseif inode == self.Nv
        res = (self.bc_centers[3] - self.centers[end])*self.pgauss[ig] + self.centers[end]
    else    
        res = self.dualareas[inode]*self.pgauss[ig]+self.centers[inode-1]
    end
    return res
  end

##### Method gives a Gauss weight (wp) of a dual cell
## inputs: inode (index of node), ig (local index of wp)
## output: value of wp
function wgdual(self::Mesh,inode :: Int64, ig :: Int64)
    if inode ==1
        res = (self.centers[1]-self.bc_centers[2])*self.wgauss[ig]
    elseif inode == self.Nv
        res = (self.bc_centers[3]-self.centers[end])*self.wgauss[ig]
    else    
        res =self.dualareas[inode]*self.wgauss[ig]
    end
    return res
end

##### Method gives a Gauss point (gp) of a left half dual cell
## inputs: inode (index of node), ig (local index of gp)
## output: value of gp
function pgdual1(self::Mesh,inode :: Int64, ig :: Int64)
    if inode ==1
        res = (self.nodes[1] - self.bc_centers[2])*self.pgauss[ig] + self.bc_centers[2]
    elseif inode == self.Nv
        res = (self.nodes[end] - self.centers[end])*self.pgauss[ig] + self.centers[end]
    else    
        res = (self.nodes[inode] - self.centers[inode-1])*self.pgauss[ig]+self.centers[inode-1]
    end
    return res
end

##### Method gives a Gauss point (gp) of a left half dual cell
## inputs: inode (index of node), ig (local index of gp)
## output: value of gp
function pgdual2(self::Mesh,inode :: Int64, ig :: Int64)
    if inode ==1
        res = (self.centers[1] - self.nodes[1])*self.pgauss[ig] + self.nodes[1]
    elseif inode == self.Nv
        res = (self.bc_centers[3] - self.nodes[end])*self.pgauss[ig] + self.nodes[end]
    else    
        res = (self.centers[inode] - self.nodes[inode])*self.pgauss[ig]+self.nodes[inode]
    end
    return res
end

##### Method gives a Gauss weight (wp) of a left half dual cell
## inputs: inode (index of node), ig (local index of wp)
## output: value of wp
function wgdual1(self::Mesh,inode :: Int64, ig :: Int64)
    if inode ==1
        res = (self.nodes[1] - self.bc_centers[2])*self.wgauss[ig] 
    elseif inode == self.Nv
        res = (self.nodes[end] - self.centers[end])*self.wgauss[ig] 
    else    
        res = (self.nodes[inode] - self.centers[inode-1])*self.wgauss[ig]
    end
    return res
end

##### Method gives a Gauss weight (wp) of a right half dual cell
## inputs: inode (index of node), ig (local index of wp)
## output: value of wp
function wgdual2(self::Mesh,inode :: Int64, ig :: Int64)
    if inode ==1
        res = (self.centers[1] - self.nodes[1])*self.wgauss[ig] 
    elseif inode == self.Nv
        res = (self.bc_centers[3] - self.nodes[end])*self.wgauss[ig] 
    else    
        res = (self.centers[inode] - self.nodes[inode])*self.wgauss[ig]
    end
    return res
end
