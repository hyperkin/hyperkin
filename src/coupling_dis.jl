
export vf_to_stagC0bar, vf_to_stagC0, vf_to_stagC1bar, vf_to_stagC1
export stagC0bar_to_vf, stagC0_to_vf, stagC1bar_to_vf, stagC1_to_vf

export dg_to_stagC0bar, dg_to_stagC0, dg_to_stagC1bar, dg_to_stagC1
export stagC0bar_to_dg, stagC0_to_dg, stagC1bar_to_dg, stagC1_to_dg

################# vf #########################
function vf_to_stagC0bar(self::vf, other::C0bar, ivar)
    for i in 1:self.mh.Nc
        px=interpolator_loc(self,i,ivar)
        R0barloc(other,i,px)
    end    
end    

function vf_to_stagC0(self::vf, other::C0, ivar) 
    for i in 1:self.mh.Nc
        px1=interpolator_loc(self,i-1,ivar)
        px2=interpolator_loc(self,i,ivar)
        function px(x)
            res=0.5*(px1(x)+px2(x))
            return res
        end
        R0loc(other,i,px)
    end    
end    

function vf_to_stagC1(self::vf, other::C1, ivar)
    for i in 1:self.mh.Nc
        px=interpolator_loc(self,i,ivar)
        R1loc(other,i,px)
    end    
end    

function vf_to_stagC1bar(self::vf, other::C1bar, ivar)
    for i in 1:self.mh.Nc
       px1=interpolator_loc(self,i-1,ivar)
       px2=interpolator_loc(self,i,ivar)
       function px(x)
           res = 0.5*(px1(x)+px2(x)) 
           return res
       end
       R1barloc(other,i,px)
    end    
end    

function stagC0_to_vf(self::C0, other::vf, ivar)
    for i in 1:self.mh.Nc
        px=I0loc(self,i)
        restriction_loc(other,i,ivar,px)
    end    
end 

function stagC0bar_to_vf(self::C0bar, other::vf, ivar)
    for i in 1:self.mh.Nc
        px1=I0barloc(self,i)
        px2=I0barloc(self,i+1)
        function px(x)
           if x < self.mh.centers[i] 
               res=px1(x)
           else
               res=px2(x)
           end     
           return res
       end
       restriction_loc(other,i,ivar,px)
    end    
end 

function stagC1_to_vf(self::C1, other::vf, ivar)
    for i in 1:self.mh.Nc
        px=I1loc(self,i)
        restriction_loc(other,i,ivar,px)
    end    
end 

function stagC1bar_to_vf(self::C1bar, other::vf, ivar)
    for i in 1:self.mh.Nc
        px1=I1barloc(self,i)
        px2=I1barloc(self,i+1)
        function px(x)
           res = 0.5*(px1(x)+px2(x)) 
           return res
       end
      restriction_loc(other,i,ivar,px)
    end    
end 



################# DG #########################
function dg_to_stagC0bar(self::dg, other::C0bar, ivar)
    for i in 1:self.mh.Nc
        px=interpolator_loc(self,i,ivar)
        R0barloc(other,i,px)
    end    
end    

function dg_to_stagC0(self::dg, other::C0, ivar) 
    for i in 1:self.mh.Nc
        px1=interpolator_loc(self,i-1,ivar)
        px2=interpolator_loc(self,i,ivar)
        function px(x)
            res=0.5*(px1(x)+px2(x))
            return res
        end
        R0loc(other,i,px)
    end    
end    

function dg_to_stagC1(self::dg, other::C1, ivar)
    for i in 1:self.mh.Nc
        px=interpolator_loc(self,i,ivar)
        R1loc(other,i,px)
    end    
end    

function dg_to_stagC1bar(self::dg, other::C1bar, ivar)
    for i in 1:self.mh.Nc
       px1=interpolator_loc(self,i-1,ivar)
       px2=interpolator_loc(self,i,ivar)
       function px(x)
           if x<self.mh.nodes[i]
               res = px1(x)
           else     
               res = px2(x)
           end     
           return res
       end
       R1barloc(other,i,px)
    end    
end    

function stagC0_to_dg(self::C0, other::dg, ivar)
    for i in 1:self.mh.Nc
        px=I0loc(self,i)
        restriction_loc(other,i,ivar,px)
    end    
end 

function stagC0bar_to_dg(self::C0bar, other::dg, ivar)
    for i in 1:self.mh.Nc
        px1=I0barloc(self,i)
        px2=I0barloc(self,i+1)
        function px(x)
           if x < self.mh.centers[i] 
               res=px1(x)
           else
               res=px2(x)
           end     
           return res
       end
       restriction_loc(other,i,ivar,px)
    end    
end 

function stagC1_to_dg(self::C1, other::dg, ivar)
    for i in 1:self.mh.Nc
        px=I1loc(self,i)
        restriction_loc(other,i,ivar,px)
    end    
end 

function stagC1bar_to_dg(self::C1bar, other::dg, ivar)
    for i in 1:self.mh.Nc
        px1=I1barloc(self,i)
        px2=I1barloc(self,i+1)
        function px(x)
           if x < self.mh.centers[i] 
               res=px1(x)
           else
               res=px2(x)
           end     
           return res
       end
       restriction_loc(other,i,ivar,px)
    end    
end 





