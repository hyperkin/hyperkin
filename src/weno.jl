export weno, set_numflux, initialization, diagnostics
export bc_neumann, bc_dirichlet, bc_periodic


##### Class for 1D third order WENO discretization. Allows to create the ODE vector after FV discretization
mutable struct weno <: SpatialDis

  nvar         :: Int64              # Nb variables of the model 
  noutvar      :: Int64              # Nb of output variables
  ndof         :: Int64              # Nb of degree of freedom
  mh           :: Mesh               # 1D Mesh associated
  field        :: Array{Float64,2}   # Table of the variables/fields on each cell 
  flux         :: Array{Float64,2}   # Table of the fluxes on each cell
  field_bcl    :: Array{Float64,2}   # Table of fields values on the left ghost cells
  field_bcr    :: Array{Float64,2}   # Table of fields values on the right ghost cells
  numflux      :: Flux               # Numerical conservative flux (function)
  ndiags       :: Int64              # Nb of diagnostics (like L1 norm)
  type_weight  :: Flux               # type of weight construction

  #### Constructor 1
  ## inputs: mh (1D mesh), n (nb variables), or (order)
  ## in this case zero diagnostics, nb of output variables is n
  function weno(mh, n, or)
      nvar           = n
      noutvar        = n  
      ndof           = mh.Nc
      field          = zeros(Float64,(mh.Nc,nvar))
      flux           = zeros(Float64,(mh.Nc,nvar))
      field_bcl      = zeros(Float64,(3,nvar))
      field_bcr      = zeros(Float64,(3,nvar)) 
      numflux        = NoFlux()
      ndiags         = 0 

      new(nvar, noutvar, ndof, mh, field, hofield, flux, field_bcl, field_bcr, numflux,ndiags)   
  end

  #### Constructor 2
  ## inputs: mh (1D mesh), n (nb variables), or (order), ng (nb of diagnostics)
  ## in this case nb of output variables is n  
  function weno(mh, n, or, ng)
      nvar           = n
      noutvar        = n  
      ndof           = mh.Nc
      field          = zeros(Float64,(mh.Nc,nvar))
      flux           = zeros(Float64,(mh.Nc,nvar))
      field_bcl      = zeros(Float64,(3,nvar))
      field_bcr      = zeros(Float64,(3,nvar)) 
      numflux        = NoFlux()
      ndiags         = 0 

      new(nvar, noutvar, ndof, mh, field, hointfield, flux, field_bcl, field_bcr, numflux,ng)   
  end
    
  #### Constructor 3
  function weno(mh, n, or, ng, no)
      nvar           = n
      noutvar        = no  
      ndof           = mh.Nc
      field          = zeros(Float64,(mh.Nc,nvar))
      flux           = zeros(Float64,(mh.Nc,nvar))
      field_bcl      = zeros(Float64,(3,nvar))
      field_bcr      = zeros(Float64,(3,nvar)) 
      numflux        = NoFlux() 

      new(nvar, noutvar, ndof, mh, field, hointfield, flux, field_bcl, field_bcr, numflux,ng)   
  end
end 

##### Method to gives the conservative numerical flux
## input: numflux (object containing the numerical flux function)
function set_numflux(self::weno, numflux)
    self.numflux = numflux
end

##### Method to initialize the fields
## input: init_f (object containing the initialization function)
function initialization(self::weno, init_f)
    for i in 1:self.mh.Nc
        for k in 1:self.deg+1
            x = pg(self.mh,i,k)
            w = wg(self.mh,i,k)
            v = init_f(x,0.0)
            self.field[i,:]  .+= w .* v[:]
        end
        self.field[i,:]  ./= self.mh.areas[i]
    end
end


##### Operator () to compute the global flux associated to the VF scheme. Gives a "vector" for the ODE system
function (self::weno)() 
    Fl = zeros(Float64,self.nvar)
    Fr = Fl = zeros(Float64,self.nvar)
    
    ##### For one cell the final vector is given by the conservative flux
    if self.mh.Nc < 2
        x  = self.mh.centers[1]     
        self.numflux(x,F,self.field[1,:],self.field[1,:])
        self.flux[1,:] = F[:]
    else
        ##### Computation of the total fluxes
        x = self.mh.centers[1] 
        h = self.mh.dualareas[1]  
        self.numflux(x,h,Fl,self.field_bcl[3,:],self.field[1,:])
        self.numflux(x,h,Fr,self.field[2,:],self.field[3,:])   
        self.flux[1,:] .= .-(Fr-Fl)./self.mh.areas[i]

        for i in 2:self.mh.Nc-1
            x = self.mh.centers[i] 
            h = self.mh.dualareas[i]  
            self.numflux(x,h,Fl,self.field[i-1,:],self.hointfield[i,:])
            self.numflux(x,h,Fr,self.field[i,:],self.hointfield[i+1,:])   
            self.flux[i,:] .= .-(Fr-Fl)./self.mh.areas[i]  
        end
        
        x = self.mh.centers[end] 
        h = self.mh.dualareas[end]  
        self.numflux(x,h,Fl,self.field[end-1,:],self.field[end,:])
        self.numflux(x,h,Fr,self.field[end,:],self.field_bcr[4,:])   
        self.flux[end,:] .= .-(Fr-Fl)./self.mh.areas[end]
    end
end

##### Method to compute a quantity using a function on all the degree of freedom
## exemple: compute a max on the domain
## input: function_reduced (the fonction used for reduction), init_res (the initial value of the quantity)
## output: res (final value)
function reduction(self::weno, function_reduced, init_res)   
    res = init_res
     for i in 1:self.ndof
        res = function_reduced(self.field[i,:],res)
    end
    return res
end

##### Method to compute the output variables and the global diagnostics associated
## input: ref (ref=1 there is a exact solution, ref=0 for no), ti (time), f_ref (an reference function)
## input: mapping (object containing function to transform variables to output variables)
## input: mapping (object containing function to local computation of diagnostic using output variables)
## output: field ( output variables), diags (global quantities like norm), field_ref (exact output variables)
function diagnostics(self::weno,ref,ti,f_ref,mapping,form_diags)
    diags = zeros(self.ndiags)
    loc_diags = zeros(self.ndiags)
    fg = zeros(self.noutvar)
    fg_ref = zeros(self.noutvar)
    field_ref = zeros(Float64,(self.ndof,self.noutvar))
    field = zeros(Float64,(self.ndof,self.noutvar))
    if ref == 0
       for i in 1:self.ndof 
        mapping(self.field[i,:],fg)   
        field[i,:] .= fg[:]    
        form_diags(field[i,:],loc_diags)    
        diags[:] .+= self.mh.areas[i]*loc_diags[:]
      end
      [field, diags] 
    else
       for i in 1:self.ndof
        x = self.mh.centers[i]
        mapping(self.field[i,:],fg) # we canno use dorect field[:,i] why ??
        mapping(f_ref(x,ti),fg_ref)
        field[i,:] .= fg[:] 
        field_ref[i,:] .= fg_ref[:]      
        form_diags(field[i,:]-field_ref[i,:],loc_diags)   
        diags[:]  .+= self.mh.areas[i]*loc_diags[:]
      end
      [field, field_ref, diags] 
    end
end


function bc_dirichlet(self::weno,bcl, bcr)
    self.field_bcl[1,:] .= bcl[:]
    self.field_bcl[2,:] .= bcl[:]
    self.field_bcl[3,:] .= bcl[:]
    self.field_bcl[4,:] .= bcr[:]
    self.field_bcl[5,:] .= bcr[:]
    self.field_bcl[6,:] .= bcr[:]
end

##### Method to put Neumann values inside ghost cells
function bc_neumann(self::weno)
  
end

##### Method to put Periodic values inside ghost cells
function bc_periodic(self::weno)
  
end

  
