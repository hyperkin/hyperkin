export vf, set_numflux, set_numfluxd, set_limiting, set_ncnumflux, set_entropyH, set_Qentropy
export initialization, restriction_loc, interpolator_loc, reduction, diagnostics, minmod
export bc_neumann, bc_dirichlet,bc_periodic, NoNcFlux, NoDiffFlux, NoLimiting, NoFlux, NoHighOrderSource
export refine_space, de_refine_space, newspace_vf, norm_exact, set_dof


mutable struct vf_locdata
    
    uLr :: Vector{Float64}            # right local reconstructed value for the left cell
    uCl :: Vector{Float64}            # left local reconstructed value for the current cell
    uCr :: Vector{Float64}            # right local reconstructed value for the current cell
    uRl :: Vector{Float64}            # left local reconstructed value for the right cell
    ##### if there is no limiting (no reconstructed variables) uLr=uL, uCl=uCr=uC, uRl=uR
    
    Fr :: Vector{Float64}             # local right conservative flux
    Fl :: Vector{Float64}             # local left conservative flux
    Fdr :: Vector{Float64}            # local right diffusion conservative flux
    Fdl :: Vector{Float64}            # local left diffusion conservative flux
    Fn :: Vector{Float64}             # local non-conservative flux
    
    Qr :: Float64                     # local right entropy flux
    Ql :: Float64                     # local left entropy flux
    
    SHol :: Float64                   # local HighOrder Source 
    SHor :: Float64                   # local HighOrder Source 
    
    function vf_locdata(nvar)
        uLr = zeros(Float64,nvar)    
        uCl = zeros(Float64,nvar)   
        uCr = zeros(Float64,nvar)    
        uRl = zeros(Float64,nvar)    
    
        Fr = zeros(Float64,nvar)     
        Fl = zeros(Float64,nvar)     
        Fdr = zeros(Float64,nvar)   
        Fdl = zeros(Float64,nvar)   
        Fn = zeros(Float64,nvar)   
        
        Ql=0.0
        Qr=0.0
        
        new(uLr,uCl,uCr,uRl,Fr,Fl,Fdr,Fdl,Fn,Ql,Qr)
    end   
    
end    

##### Class for 1D finite volumes discretization. Allows to create the ODE vector after FV discretization
mutable struct vf <: SpatialDis

  nvar       :: Int64              # Nb variables of the model 
  noutvar    :: Int64              # Nb of output variables
  ndof       :: Int64              # Nb of degree of freedom
  mh         :: Mesh               # 1D Mesh associated
  field      :: Array{Float64,2}   # Table of the variables/fields on each cell 
  flux       :: Array{Float64,2}   # Table of the fluxes on each cell
  prod_eta   :: Vector{Float64}    # Table for entropy production
  field_bcl  :: Vector{Float64}    # Table of fields values on the left ghost cells
  field_bcr  :: Vector{Float64}    # Table of fields values on the right ghost cells
  numflux    :: Flux               # Numerical conservative flux (function)
  ncnumflux  :: Flux               # Numerical non-conservative flux (function)
  numfluxd   :: Flux               # Numerical diffusive flux (function)
  limiting   :: Flux               # Limiting/Muscl formula to obtain interface reconstruction (function)
  entropy    :: Flux              
  Qentropy   :: Flux      
  ndiags     :: Int64              # Nb of diagnostics (like L1 norm)
  ld         :: vf_locdata         # pre-allocate local data   

  #### Constructor 1
  ## inputs: mh (1D mesh), n (nb variables), or (order)
  ## in this case zero diagnostics, nb of output variables is n
  function vf(mh, n, or)
      nvar = n
      noutvar = n  
      ndof = mh.Nc
      field       = zeros(Float64,(mh.Nc,nvar))
      flux        = zeros(Float64,(mh.Nc,nvar))
      prod_eta    = zeros(Float64,mh.Nc)  
      field_bcl   = zeros(Float64,nvar)
      field_bcr   = zeros(Float64,nvar) 
      numflux     = NoFlux()
      ncnumflux   = NoNcFlux()
      numfluxd    = NoDiffFlux()  
      limiting    = NoLimiting()
      entropy     = NoEntropyH()              
      Qentropy    = NoEntropyFlux()  
      ndiags      = 0
      ld          = vf_locdata(n)  

      new(nvar,noutvar,ndof,mh,field,flux,prod_eta,field_bcl,field_bcr,numflux,ncnumflux,numfluxd,limiting,entropy,Qentropy, ndiags,ld)   
  end

  #### Constructor 2
  ## inputs: mh (1D mesh), n (nb variables), or (order), ng (nb of diagnostics)
  ## in this case nb of output variables is n  
  function vf(mh, n, or, ng)
      nvar = n
      noutvar = n  
      ndof = mh.Nc
      field     = zeros(Float64,(mh.Nc,nvar))
      flux      = zeros(Float64,(mh.Nc,nvar))
      prod_eta    = zeros(Float64,mh.Nc)
      field_bcl   = zeros(Float64,nvar)
      field_bcr   = zeros(Float64,nvar)
      numflux   = NoFlux()
      ncnumflux = NoNcFlux()
      numfluxd  = NoDiffFlux() 
      limiting  = NoLimiting() 
      entropy   = NoEntropyH()              
      Qentropy  = NoEntropyFlux()  
      ndiags    = ng
      ld        = vf_locdata(n)      
            new(nvar,noutvar,ndof,mh,field,flux,prod_eta,field_bcl,field_bcr,numflux,ncnumflux,numfluxd,limiting,entropy,Qentropy,ng,ld)
  end
    
  #### Constructor 3
  ## inputs: mh (1D mesh), n (nb variables), or (order), ng (nb of diagnostics), no (nb of output variables)   
  function vf(mh, n, or, ng, no)
      nvar = n
      noutvar = no  
      ndof = mh.Nc
      field     = zeros(Float64,(mh.Nc,nvar))
      flux      = zeros(Float64,(mh.Nc,nvar))
      prod_eta    = zeros(Float64,mh.Nc)
      field_bcl   = zeros(Float64,nvar)
      field_bcr   = zeros(Float64,nvar)
      numflux   = NoFlux()
      ncnumflux = NoNcFlux()
      numfluxd  = NoDiffFlux() 
      limiting  = NoLimiting() 
      entropy   = NoEntropyH()              
      Qentropy  = NoEntropyFlux()   
      ndiags    = ng
      ld        = vf_locdata(n)    
      new(nvar,noutvar,ndof,mh,field,flux,prod_eta,field_bcl,field_bcr,numflux,ncnumflux,numfluxd,limiting,entropy,Qentropy,ng,ld)
  end  
end 


function newspace_vf(other::vf, mh)
    
     space=vf(mh,other.nvar,1)
     space.noutvar = other.noutvar
     space.numflux   = other.numflux
     space.ncnumflux = other.ncnumflux
     space.numfluxd  = other.numfluxd 
     space.limiting  = other.limiting
     space.ndiags    = other.ndiags  
     space.entropy   = other.entropy
     space.Qentropy  = other.Qentropy
     return space   

 end 


function set_dof(self::vf, i_dof)
    return self.mh.centers[i_dof]
end

##### Method to gives the conservative numerical flux
## input: numflux (object containing the numerical flux function)
function set_numflux(self::vf, numflux)
    self.numflux = numflux
end

##### Method to gives the diffusive flux
## input: numflux (object containing the diffusive flux function)
function set_numfluxd(self::vf, numflux)
    self.numfluxd = numflux
end

##### Method to gives the non conservative flux
## input: numflux (object containing the nonconservative flux function)
function set_ncnumflux(self::vf, numflux)
    self.ncnumflux = numflux
end

##### Method to gives the method of limiting reconsrtruction
## input: numflux (object containing the limiting function)
function set_limiting(self::vf, limiting)
    self.limiting = limiting
end


##### Method to gives the entropy
## input: entropy (object containing the entropy function)
function set_entropyH(self::vf, entropy)
    self.entropy = entropy
end

##### Method to gives the entropy
## input: Qentropy (object containing the entropy flux function)
function set_Qentropy(self::vf, Qentropy)
    self.Qentropy = Qentropy
end


##### Method to initialize the fields
## input: init_f (object containing the initialization function)
function initialization(self::vf, init_f)
    for i in 1:self.mh.Nc
        x = self.mh.centers[i]
        v = init_f(x,0.0)
        self.field[i,:] = v[:]
    end
end

##### Method to compute local restriction operator (function to degree of freedom)
## input: i (index of cell), ivar (index of var), func (function to restrict)
function restriction_loc(self::vf, i, ivar, func )
    xl = self.mh.nodes[i]
    xr = self.mh.nodes[i+1]
    self.field[i,ivar]  = func(self.mh.centers[i])
end 

##### Method to compute local interpolation operator (degree of freedom to function)
## input: i (index of cell), ivar (index of var)
## output: inter (function inter(x))
function interpolator_loc(self::vf, i, ivar)
  function inter(x)  
      if i== 0
          res= self.field_bcl[ivar] 
      else        
         res= self.field[i,ivar]
      end   
  return res    
  end
  return inter 
end 

##### Operator () to compute the global flux associated to the VF scheme. Gives a "vector" for the ODE system
function (self::vf)() 
    
    ##### For one cell the final vector is given by the conservative flux
    if self.mh.Nc < 2
        F = zeros(Float64,self.nvar)
        x  = self.mh.centers[1] 
        h  = self.mh.dualareas[1]
        self.numflux(x,h,F,self.field[1,:],self.field[1,:])
        self.flux[1,:] = F[:]
    else
        ##### Computation of the total fluxes for the cells 1 and 2 
  
        xl  = self.mh.nodes[1]
        hl  = self.mh.dualareas[1]  
        xr  = self.mh.nodes[2]
        hr  = self.mh.dualareas[2]  
        self.limiting(xl,self.ld.uLr,self.field_bcl,self.field_bcl,self.field[1,:],1.0)
        self.limiting(xl,self.ld.uCl,self.field[1,:],self.field_bcl,self.field[2,:],-1.0)
        self.limiting(xr,self.ld.uCr,self.field[1,:],self.field_bcl,self.field[2,:],1.0)
        self.limiting(xr,self.ld.uRl,self.field[2,:],self.field[1,:],self.field[3,:],-1.0) 
        self.numflux(xl,hl,self.ld.Fl,self.ld.uLr,self.ld.uCl)
        self.numflux(xr,hr,self.ld.Fr,self.ld.uCr,self.ld.uRl)   
        self.numfluxd(xl,hl,self.ld.Fdl,self.field_bcl,self.field[1,:])
        self.numfluxd(xr,hr,self.ld.Fdr,self.field[1,:],self.field[2,:])
        self.ncnumflux(xl,xr,hl,hr,self.ld.Fn,self.field_bcl,self.field[1,:],self.field[2,:])
        self.flux[1,:] .= .-(self.ld.Fr-self.ld.Fl+self.ld.Fdr-self.ld.Fdl+self.ld.Fn)./self.mh.areas[1] 
        
        self.Qentropy(xl,hl,self.ld.Ql,self.field_bcl,self.field[1,:])
        self.Qentropy(xr,hr,self.ld.Qr,self.field[1,:],self.field[2,:]) 
        self.prod_eta[1] = +(self.ld.Qr-self.ld.Ql)/self.mh.areas[1] 
   
        xl  = self.mh.nodes[2]
        hl  = self.mh.dualareas[2]  
        xr  = self.mh.nodes[3]
        hr  = self.mh.dualareas[3]       
        self.limiting(xl,self.ld.uLr,self.field[1,:],self.field_bcl,self.field[2,:],1.0)
        self.limiting(xl,self.ld.uCl,self.field[2,:],self.field[1,:],self.field[3,:],-1.0)
        self.limiting(xr,self.ld.uCr,self.field[2,:],self.field[1,:],self.field[3,:],1.0)
        self.limiting(xr,self.ld.uRl,self.field[3,:],self.field[2,:],self.field[4,:],-1.0) 
        self.numflux(xl,hl,self.ld.Fl,self.ld.uLr,self.ld.uCl)
        self.numflux(xr,hr,self.ld.Fr,self.ld.uCr,self.ld.uRl)  
        self.numfluxd(xl,hl,self.ld.Fdl,self.field[1,:],self.field[2,:])
        self.numfluxd(xr,hr,self.ld.Fdr,self.field[2,:],self.field[3,:])
        self.ncnumflux(xl,xr,hl,hr,self.ld.Fn,self.field[1,:],self.field[2,:],self.field[3,:])    
        self.flux[2,:] .= .-(self.ld.Fr-self.ld.Fl+self.ld.Fdr-self.ld.Fdl+self.ld.Fn)./self.mh.areas[2]  
        
        self.Qentropy(xl,hl,self.ld.Ql,self.field[1,:],self.field[2,:])
        self.Qentropy(xr,hr,self.ld.Qr,self.field[2,:],self.field[3,:]) 
        self.prod_eta[2] = +(self.ld.Qr-self.ld.Ql)/self.mh.areas[2] 
        
        ##### Computation of the total fluxes for the cells 3 to n-2
        for i in 3:self.mh.Nc-2   
            xl = self.mh.nodes[i] 
            hl = self.mh.dualareas[i]  
            xr = self.mh.nodes[i+1] 
            hr = self.mh.dualareas[i+1] 
            self.limiting(xl,self.ld.uLr,self.field[i-1,:],self.field[i-2,:],self.field[i,:],1.0)
            self.limiting(xl,self.ld.uCl,self.field[i,:],self.field[i-1,:],self.field[i+1,:],-1.0)
            self.limiting(xr,self.ld.uCr,self.field[i,:],self.field[i-1,:],self.field[i+1,:],1.0)
            self.limiting(xr,self.ld.uRl,self.field[i+1,:],self.field[i,:],self.field[i+2,:],-1.0) 
            self.numflux(xl,hl,self.ld.Fl,self.ld.uLr,self.ld.uCl)
            self.numflux(xr,hr,self.ld.Fr,self.ld.uCr,self.ld.uRl)    
            self.numfluxd(xl,hl,self.ld.Fdl,self.field[i-1,:],self.field[i,:])
            self.numfluxd(xr,hr,self.ld.Fdr,self.field[i,:],self.field[i+1,:])
            self.ncnumflux(xl,xr,hl,hr,self.ld.Fn,self.field[i-1,:],self.field[i,:],self.field[i+1,:])
            self.flux[i,:] .= .-(self.ld.Fr-self.ld.Fl+self.ld.Fdr-self.ld.Fdl+self.ld.Fn)./self.mh.areas[i]

            self.Qentropy(xl,hl,self.ld.Ql,self.field[i-1,:],self.field[i,:])
            self.Qentropy(xr,hr,self.ld.Qr,self.field[i,:],self.field[i+1,:]) 
            self.prod_eta[i] = +(self.ld.Qr-self.ld.Ql)/self.mh.areas[i] 
        end
        
        ##### Computation of the total fluxes for the cells n-1 and n 
        xl = self.mh.nodes[end-2] 
        hl = self.mh.dualareas[end-2]    
        xr = self.mh.nodes[end-1] 
        hr = self.mh.dualareas[end-1] 
        self.limiting(xl,self.ld.uLr,self.field[end-2,:],self.field[end-3,:],self.field[end-1,:],1.0)
        self.limiting(xl,self.ld.uCl,self.field[end-1,:],self.field[end-2,:],self.field[end,:],-1.0)
        self.limiting(xr,self.ld.uCr,self.field[end-1,:],self.field[end-2,:],self.field[end,:],1.0)
        self.limiting(xr,self.ld.uRl,self.field[end,:],self.field[end-1,:],self.field_bcr,-1.0) 
        self.numflux(xl,hl,self.ld.Fl,self.ld.uLr,self.ld.uCl)
        self.numflux(xr,hr,self.ld.Fr,self.ld.uCr,self.ld.uRl)   
        self.numfluxd(xl,hl,self.ld.Fdl,self.field[end-2,:],self.field[end-1,:])
        self.numfluxd(xr,hr,self.ld.Fdr,self.field[end-1,:],self.field[end,:])
        self.ncnumflux(xl,xr,hl,hr,self.ld.Fn,self.field[end-2,:],self.field[end-1,:],self.field[end,:])
        self.flux[end-1,:] .= .-(self.ld.Fr-self.ld.Fl+self.ld.Fdr-self.ld.Fdl+self.ld.Fn)./self.mh.areas[end-1]   
      
        self.Qentropy(xl,hl,self.ld.Ql,self.field[end-2,:],self.field[end-1,:])
        self.Qentropy(xr,hr,self.ld.Qr,self.field[end-1,:],self.field[end,:]) 
        self.prod_eta[end-1] = +(self.ld.Qr-self.ld.Ql)/self.mh.areas[end-1] 
         
        xl = self.mh.nodes[end-1]
        hl = self.mh.dualareas[end-1]
        xr = self.mh.nodes[end]
        hr = self.mh.dualareas[end]
        self.limiting(xl,self.ld.uLr,self.field[end-1,:],self.field[end-2,:],self.field[end,:],1.0)
        self.limiting(xl,self.ld.uCl,self.field[end,:],self.field[end-1,:],self.field_bcr,-1.0)
        self.limiting(xr,self.ld.uCr,self.field[end,:],self.field[end-1,:],self.field_bcr,1.0)
        self.limiting(xr,self.ld.uRl,self.field_bcr,self.field[end,:],self.field_bcr,-1.0) 
        self.numflux(xl,hl,self.ld.Fl,self.ld.uLr,self.ld.uCl)
        self.numflux(xr,hr,self.ld.Fr,self.ld.uCr,self.ld.uRl)  
        self.numfluxd(xl,hl,self.ld.Fdl,self.field[end-1,:],self.field[end,:])
        self.numfluxd(xr,hr,self.ld.Fdr,self.field[end,:],self.field_bcr)
        self.ncnumflux(xl,xr,hl,hr,self.ld.Fn,self.field[end-1,:],self.field[end,:],self.field_bcr)
        self.flux[end,:] .= .-(self.ld.Fr-self.ld.Fl+self.ld.Fdr-self.ld.Fdl+self.ld.Fn)./self.mh.areas[end]  
        
        self.Qentropy(xl,hl,self.ld.Ql,self.field[end-1,:],self.field[end,:])
        self.Qentropy(xr,hr,self.ld.Qr,self.field[end,:],self.field_bcr) 
        self.prod_eta[end] = +(self.ld.Qr-self.ld.Ql)/self.mh.areas[end] 
    end
end

##### Method to compute a quantity using a function on all the degree of freedom
## exemple: compute a max on the domain
## input: function_reduced (the fonction used for reduction), init_res (the initial value of the quantity)
## output: res (final value)
function reduction(self::vf, function_reduced, init_res)   
    res = init_res
     for i in 1:self.ndof
        res = function_reduced(self.mh.centers[i],self.field[i,:],res)
    end
    return res
end

##### Method to compute the output variables and the global diagnostics associated
## input: ref (ref=1 there is a exact solution, ref=0 for no), ti (time), f_ref (an reference function)
## input: mapping (object containing function to transform variables to output variables)
## input: mapping (object containing function to local computation of diagnostic using output variables)
## output: field ( output variables), diags (global quantities like norm), field_ref (exact output variables)
function diagnostics(self::vf,ref,ti,f_ref,mapping,form_diags)
    diags = zeros(self.ndiags)
    loc_diags = zeros(self.ndiags)
    fg = zeros(self.noutvar)
    fg_ref = zeros(self.noutvar)
    field_ref = zeros(Float64,(self.ndof,self.noutvar))
    field = zeros(Float64,(self.ndof,self.noutvar))
    if ref == 0
       for i in 1:self.ndof 
        mapping(self.field[i,:],fg)   
        field[i,:] .= fg[:]    
        form_diags(field[i,:],loc_diags)    
        diags[:] .+= self.mh.areas[i]*loc_diags[:]
      end
      [field, diags] 
    else
       for i in 1:self.ndof
        x = self.mh.centers[i]
        mapping(self.field[i,:],fg) # we canno use dorect field[:,i] why ??
        mapping(f_ref(x,ti),fg_ref)
        field[i,:] .= fg[:] 
        field_ref[i,:] .= fg_ref[:]     
        form_diags(field[i,:]-field_ref[i,:],loc_diags)   
        diags[:]  .+= self.mh.areas[i]*loc_diags[:]
      end
      [field, field_ref, diags] 
    end
end

function norm_exact(self::vf,ti,f_ref,mapping,form_diags)
    diags = zeros(self.ndiags)
    loc_diags = zeros(self.ndiags)
    fg_ref = zeros(self.noutvar)
    field_ref = zeros(Float64,(self.ndof,self.noutvar))
    
    for i in 1:self.ndof
        x = self.mh.centers[i]
        mapping(f_ref(x,ti),fg_ref)
        field_ref[i,:] .= fg_ref[:]     
        form_diags(field_ref[i,:],loc_diags)   
        diags[:]  .+= self.mh.areas[i]*loc_diags[:]
     end
     diags
end


##### function for minmod limiting
## input: a and b (two numbers)
## output: the minmod value
function minmod(a,b)
    res1=max(0,min(a,b))
    res2=min(0,max(a,b)) 
    return res1+res2
end

##### Method to put Diriclet values inside ghost cells
## input: bcl[:] and bcr[:] (lelf and right Dirichlet values)
function bc_dirichlet(self::vf,bcl, bcr)
    self.field_bcl[:] .= bcl[:]
    self.field_bcr[:] .= bcr[:]
end

##### Method to put Neumann values inside ghost cells
function bc_neumann(self::vf)
    self.field_bcl[:] .= self.field[1,:]
    self.field_bcr[:] .= self.field[end,:]
end

##### Method to put Periodic values inside ghost cells
function bc_periodic(self::vf)
    self.field_bcl[:] .= self.field[end,:]
    self.field_bcr[:] .= self.field[1,:]
end


function refine_space(self::vf, list_cells::Vector{Int64})
    
    NewMh = refine_mesh(self.mh,list_cells)
    nr= size(list_cells)[1]
    Newspace=newspace_vf(self,NewMh)
    j=1
    k=1
    for i in 1:self.mh.Nc
        Newspace.field[j,:]=self.field[i,:]
        j=j+1
        if k<=nr
            if list_cells[k]==i 
                Newspace.field[j,:]=self.field[i,:]
                k=k+1     
                j=j+1
            end
        end
    end
            
    return Newspace    
end

function de_refine_space(self::vf, list_cells::Vector{Int64}) ## convention on fusionne avec celle de droite
    NewMh = de_refine_mesh(self.mh,list_cells)
    nr= size(list_cells)[1]
    Newspace=newspace_vf(self,NewMh)
    i=1
    k=1
    for j in 1:Newspace.mh.Nc
        Newspace.field[j,:]=self.field[i,:]
        if k<=nr
            if list_cells[k]==i 
                k=k+1     
                i=i+1
            end
        end
        i=i+1
    end
     
    return Newspace
end


##### Class containing the function for the case without limiting
struct NoLimiting <: Flux
end
function (self::NoLimiting)(x,data::Vector{Float64},vC::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64},d)
   data .= vC
end

##### Class containing the function for the case without diffusive flux
struct NoDiffFlux <: Flux
end
function (self::NoDiffFlux)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    nothing
end

##### Class containing the function for the case without no-conservative flux
struct NoNcFlux <: Flux
end
function (self::NoNcFlux)(xl,xr,hl,hr,data::Vector{Float64},vL::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64})
    nothing
end

##### Class containing the function for the case without conservative flux
struct NoFlux <: Flux
end
function (self::NoFlux)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    nothing
end


##### Class containing the function for the case without conservative flux
struct NoEntropyFlux <: Flux
end
function (self::NoEntropyFlux)(x,h,data,vL::Vector{Float64},vR::Vector{Float64})
    nothing
end



struct NoEntropyH <: Flux
end
function (self::NoEntropyH)(v::Vector{Float64})
    nothing
end


    
  
