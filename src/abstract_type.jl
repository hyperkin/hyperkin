export Flux, SpatialDis, DeRhamSpace, Operator, SpatialOperator, Filter
export TimeDis, Equilibrium, TimeSource


##### Abstract class for generic function like flux
abstract type TimeSource end

##### Abstract class for generic function like flux
abstract type Flux end

##### Abstract class for generic filter function
abstract type Filter end

##### Abstract class for generic equilibrium fonction for relaxation
abstract type Equilibrium end

##### Abstract class for the spatial discretization
abstract type SpatialDis end

#### Abstract class for the time discretization
abstract type TimeDis end

#### Abstract class for the global space operator used in staggered scheme
abstract type SpatialOperator end

#### Abstract class for the global operator used in implicit scheme
abstract type Operator end 

##### Abstract class for the DeRhamSpace
abstract type DeRhamSpace end

    
  
