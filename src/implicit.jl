
export NullSpaceOp, NullSource, NullCorrector, NullIdOp, IdGenericOp
export Implicit_mstage, set_spaceop, set_idop, set_rhs, solve, solve2, SDirk
export Elliptic, set_coefs, SchurImplicit_mstage, set_spaceop1, set_spaceop2, set_spaceop3, set_schur
export SDirkTheta, SDirk_Schur, SDirkTheta_Schur, set_tol


struct NullSpaceOp <: Operator
end
function (self::NullSpaceOp)(sp :: SpatialDis,v :: Vector{Float64})
end

struct NullSource <: Operator
end
function (self::NullSource)(sp :: SpatialDis,v :: Vector{Float64})
end

struct NullCorrector <: Operator
end
function (self::NullCorrector)(sp :: SpatialDis,v :: Vector{Float64})
end

struct NullIdOp <: Operator
end
function (self::NullIdOp)(sp :: SpatialDis,v :: Vector{Float64})
end

struct IdGenericOp <: Operator
end
function (self::IdGenericOp)(sp :: SpatialDis,v :: Vector{Float64}) 
    return v
end

# mutistage implicit sep. We solve dt X(t) = AX(t)

mutable struct Implicit_mstage <: TimeDis

    stage     :: Int64
    order     :: Int64
    space     :: SpatialDis
    time      :: Float64
    bc_type   :: Int64
    spaceop   :: Operator
    idop      :: Operator
    source    :: Operator
    n         :: Int64
    m         :: Int64
    alphai    :: Vector{Float64}
    betai     :: Vector{Float64}
    alphae    :: Vector{Float64}
    istage    :: Int64
    apply_src  :: Int64 
    sym       :: Int64
    tol       :: Float64

    function Implicit_mstage(space,stage,order, bc)
  
      spaceop    = NullSpaceOp()
      source     = NullSource()
      idop       = IdGenericOp()
      n = 0
      m = 0
      alphai = zeros(Float64,stage)
      betai = zeros(Float64,stage)   
      alphae = zeros(Float64,stage+1)
      time = 0.0  
      istage = 0  
      apply_src  = 0
      sym = 0
      tol = 1.0e-12
      if stage< order-1
        println("Th RK possible are order =s+1, stage = s,s+1,s+2")
      end   
      new(stage, order, space, time, bc, spaceop, idop, source, n, m, alphai, betai, alphae, istage, apply_src,sym,tol)

    end

    function Implicit_mstage(space,stage,order, bc, s)
  
      spaceop    = NullSpaceOp()
      source     = NullSource()
      idop       = IdGenericOp()
      n = 0
      m = 0
      alphai = zeros(Float64,stage)
      betai = zeros(Float64,stage)   
      alphae = zeros(Float64,stage+1)
      time = 0.0  
      istage = 0  
      apply_src  = 0
      tol = 1.0e-12
      if stage< order-1
        println("Th RK possible are order =s+1, stage = s,s+1,s+2")
      end   
      new(stage, order, space, time, bc, spaceop, idop, source, n, m, alphai, betai, alphae, istage, apply_src,s,tol)

    end
    
end

function set_spaceop(self::Implicit_mstage,n,op)
  self.n = n 
  self.spaceop = op
end

function set_idop(self::Implicit_mstage,op)
  self.idop = op
end

function set_tol(self::Implicit_mstage,tol)
  self.tol = tol
end

function set_rhs(self::Implicit_mstage,sr)
    self.source = sr
    self.apply_sr = 1
end

function (self::Implicit_mstage)( v :: Vector{Float64} ) 
  #println("product MV")
  res = zeros(Float64, self.n)
  w1  = zeros(Float64,self.n)
  w2  = zeros(Float64,self.n)
  w1 =  self.idop(self.space,v)
  w2 =  self.spaceop(self.space,v)
  res[:] .= self.alphai[self.istage].*w1[:] .+ self.betai[self.istage].*w2[:]
  return res
end
        
        
function solve(self::Implicit_mstage, b, xinit)
  x = zeros(Float64,self.n)
  A = LinearMap{Float64}(self, self.n; ismutating=false )
  if self.sym == 0
    x, his=cg!(xinit,A,b,abstol=self.tol,maxiter=10*self.n,log=true)
  else 
    x, his=idrs!(xinit,A,b;s=20,abstol=self.tol,maxiter=10*self.n,log=true)
  end  
  println(his)
  return x    
end

function SDirk(self::Implicit_mstage, xinit, dt)

  x = zeros(Float64,self.n)
  xtemp = zeros(Float64,self.n)
  s = zeros(Float64,self.n)

  if self.order == 1 
    self.alphai[1] = 1.0
    self.betai[1] = -dt
    self.alphae[1] = 1.0
    self.alphae[2] = 0.0
  end

  if self.order ==2 && self.stage==1 
    self.alphai[1] = 1.0
    self.betai[1] = -0.5*dt
    self.alphae[1] = 1.0
    self.alphae[2] = 0.5*dt
  end

  if self.order ==3 && self.stage==2
    gamma = 0.5 + 1.0/(2.0*sqrt(3.0))
    self.alphai[1] = 1.0
    self.alphai[2] = 1.0
    self.betai[1] = -gamma*dt
    self.betai[2] = -gamma*dt
    self.alphae[1] = 1.0
    self.alphae[2] = (1.0-2.0* gamma)*dt
    self.alphae[3] = (0.5-2.0* gamma + gamma*gamma)*dt*dt
  end

  if self.order ==4 && self.stage==3
    gamma = (1.0/sqrt(3.0))*cos(pi/18.0)+0.5
    self.alphai[1] = 1.0
    self.alphai[2] = 1.0
    self.alphai[3] = 1.0
    self.betai[1] = -gamma*dt
    self.betai[2] = -gamma*dt
    self.betai[3] = -gamma*dt
    self.alphae[1] = 1.0
    self.alphae[2] = (1.0-3.0* gamma)*dt
    self.alphae[3] = (0.5-3.0* gamma + 3.0*gamma*gamma)*dt*dt
    self.alphae[4] = (1.0/6.0-1.5* gamma + 3.0*gamma*gamma-gamma*gamma*gamma)*dt*dt*dt
  end

  if self.apply_src == 1
    s = self.source(self.space,x)
  end  

  x[:] .= xinit[:]
  b = self.alphae[1].*x[:] + s
  for i in 2:self.stage+1
    x= self.spaceop(self.space,x)
    b[:] = b[:] + self.alphae[i]*x[:]
  end 
  x[:] .= b[:]
  
  for i in 1:self.stage
    self.istage = i
    x = solve(self,x,xinit)
  end    
  self.time = self.time + dt
  return x
end

function SDirkTheta(self::Implicit_mstage, theta, xinit, dt)

  x = zeros(Float64,self.n)
  xtemp = zeros(Float64,self.n)
  s = zeros(Float64,self.n)

  self.alphai[1] = 1.0
  self.betai[1] = -theta*dt
  self.alphae[1] = 1.0
  self.alphae[2] = (1-theta)*dt

  if self.apply_src == 1
    s = self.source(self.space,x)
  end  

  x[:] .= xinit[:]
  b = self.alphae[1].*x[:] + s
  for i in 2:self.stage+1
    x= self.spaceop(self.space,x)
    b[:] = b[:] + self.alphae[i]*x[:]
  end 
  x[:] .= b[:]
  
  for i in 1:self.stage
    self.istage = i
    x = solve(self,x,xinit)
  end    
  self.time = self.time + dt
  return x
end


mutable struct Elliptic <: TimeDis

  space     :: SpatialDis
  bc_type   :: Int64
  spaceop   :: Operator
  idop      :: Operator
  n         :: Int64
  alpha     :: Float64
  beta       :: Float64
  apply_src  :: Int64 

  function Elliptic(space, bc)

    spaceop    = NullSpaceOp()
    idop       = IdGenericOp()
    n = 0
    alpha = 0.0
    beta = 0.0  
    new( space, bc, spaceop, idop, n, alpha, beta)

  end
  
end

function set_spaceop(self::Elliptic,n,op)
self.n = n 
self.spaceop = op
end

function set_idop(self::Elliptic,op)
self.idop = op
end

function set_coefs(self::Elliptic,a,b)
  self.alpha = a 
  self.beta = b
end


function (self::Elliptic)( v :: Vector{Float64} ) 
#println("product MV")
res = zeros(Float64, self.n)
w1  = zeros(Float64,self.n)
w2  = zeros(Float64,self.n)
w1 =  self.idop(self.space,v)
w2 =  self.spaceop(self.space,v)
res[:] .= self.alpha.*w1[:] .+ self.beta.*w2[:]
return res
end
      
      
function solve(self::Elliptic, b)
    x = zeros(Float64,self.n)
    xinit = zeros(Float64,self.n)
    A = LinearMap{Float64}(self, self.n; ismutating=false )
    print(typeof(A)) 
    x, his=cg!(xinit,A,b;abstol=1.0e-11,maxiter=2*self.n,log=true)
    return x    
end

function solve2(self::Elliptic, b)
    x = zeros(Float64,self.n)
    xinit = zeros(Float64,self.n)
    A = LinearMap{Float64}(self, self.n; ismutating=false )
   
    x, his=idrs!(xinit,A,b;s=20,abstol=1.0e-10,maxiter=20*self.n,log=true)
    return x    
end

function solve_with_xinit(self::Elliptic, b, xinit)
    x = zeros(Float64,self.n)
    A = LinearMap{Float64}(self, self.n; ismutating=false )
    x, his=cg!(xinit,A,b;abstol=1.0e-11,maxiter=5000,log=true) 
    return x    
end

# mutistage implicit sep based on schur . We solve dt (X(t),Y(t)) = A(X(t),Y(t)) with A = (0, B1\\ B2, 0) et Schur = B1B2

mutable struct SchurImplicit_mstage <: TimeDis

  stage     :: Int64
  order     :: Int64
  space     :: SpatialDis
  time      :: Float64
  bc_type   :: Int64
  spaceop1  :: Operator
  spaceop2  :: Operator
  spaceop3  :: Operator
  schur     :: Operator
  idop      :: Operator
  source    :: Operator
  n         :: Int64
  m         :: Int64
  p         :: Int64 
  alphai    :: Vector{Float64}
  betai     :: Vector{Float64}
  alphae    :: Vector{Float64}
  istage    :: Int64
  apply_src :: Int64 
  apply_op3 :: Int64    
  solver    :: Int64  

  function SchurImplicit_mstage(space,stage,order, bc)
    spaceop1   = NullSpaceOp()
    spaceop2   = NullSpaceOp()
    spaceop3   = NullSpaceOp()
    schur      = NullSpaceOp()
    idop       = IdGenericOp()
    source     = NullSource()
    n = 0
    m = 0
    p = 0    
    alphai = zeros(Float64,stage)
    betai = zeros(Float64,stage)   
    alphae = zeros(Float64,stage+1)
    time = 0.0  
    istage = 0  
    apply_src  = 0
    apply_op3  = 0
    solver = 1
        
    if stage< order-1
      println("Th RK possible are order =s+1, stage = s,s+1,s+2")
    end              
        
    new(stage,order,space,time,bc,spaceop1,spaceop2,spaceop3,schur,idop,source,n,m,p,alphai,betai,alphae,istage,apply_src, apply_op3,solver)

  end 
    
  function SchurImplicit_mstage(space,stage,order, bc,solver)
    spaceop1   = NullSpaceOp()
    spaceop2   = NullSpaceOp()
    spaceop3   = NullSpaceOp()
    schur      = NullSpaceOp()
    idop       = IdGenericOp()
    source     = NullSource()
    n = 0
    m = 0
    p = 0    
    alphai = zeros(Float64,stage)
    betai = zeros(Float64,stage)   
    alphae = zeros(Float64,stage+1)
    time = 0.0  
    istage = 0  
    apply_src  = 0
    apply_op3  = 0
        
    if stage< order-1
      println("Th RK possible are order =s+1, stage = s,s+1,s+2")
    end       
        
    new(stage,order,space,time,bc,spaceop1,spaceop2,spaceop3,schur,idop,source,n,m,p,alphai,betai,alphae,istage,apply_src, apply_op3,solver)
       
  end   
    
end

function set_spaceop1(self::SchurImplicit_mstage,n,op)
  self.n = n 
  self.spaceop1 = op
end

function set_idop(self::SchurImplicit_mstage,op)
  self.idop = op
end

function set_spaceop2(self::SchurImplicit_mstage,m,op)
  self.m = m 
  self.spaceop2 = op
end

function set_spaceop3(self::SchurImplicit_mstage,p,op)
  self.p = p 
  self.spaceop3 = op
  self.apply_op3 = 1   
end

function set_schur(self::SchurImplicit_mstage,n,op)
  self.schur = op
end

function set_rhs(self::SchurImplicit_mstage,sr)
    self.source = sr
    self.apply_src = 1
end

function (self::SchurImplicit_mstage)( v :: Vector{Float64} ) 
  res = zeros(Float64, self.n)
  w1  = zeros(Float64,self.n)
  w2  = zeros(Float64,self.n)
  w2 = self.schur(self.space,v)
  w1 = self.idop(self.space,v)
 # println("inside before pi>>> ",v) 
  res[:] .= self.alphai[self.istage].*w1[:] .- (self.betai[self.istage]*self.betai[self.istage]).*w2[:] 
#  println("inside after pi>>> ",res)   
  return res
end

function solve_initzero(self::SchurImplicit_mstage, b)
  x = zeros(Float64,self.n)
  xinit = zeros(Float64,self.n)
  A = LinearMap{Float64}(self, self.n; ismutating=false )

  if self.solver==1  
      x, his=cg!(xinit,A,b;abstol=1.0e-11,maxiter=10*self.n,log=true)
  else      
      x, his=idrs!(xinit,A,b;s=8,abstol=1.0e-12,maxiter=5*self.n,log=true)  
  end   
  return x    
end
               
function solve(self::SchurImplicit_mstage, b, xinit)
  x = zeros(Float64,self.n)
  #xinit = zeros(Float64,self.n)
  A = LinearMap{Float64}(self, self.n; ismutating=false )
 
  if self.solver == 1  
      x, his=cg!(xinit,A,b;abstol=1.0e-11,maxiter=10*self.n,log=true)
  else  
      x, his=idrs!(xinit,A,b;s=8,abstol=1.0e-12,maxiter=5*self.n,log=true)  
  end      
  #println("klll ",his," ",sqrt(dot(A*x-b,A*x-b)))
  return x    
end



function SDirk_Schur(self::SchurImplicit_mstage, xinit, dt)
  
   if self.order == 1 
        self.alphai[1] = 1.0
        self.betai[1] = -dt
        self.alphae[1] = 1.0
        self.alphae[2] = 0.0
    end

    if self.order ==2 && self.stage==1 
        self.alphai[1] = 1.0
        self.betai[1] = -0.5*dt
        self.alphae[1] = 1.0
        self.alphae[2] = 0.5*dt
    end

    if self.order ==3 && self.stage==2
        gamma = 0.5 + 1.0/(2.0*sqrt(3.0))
        self.alphai[1] = 1.0
        self.alphai[2] = 1.0
        self.betai[1] = -gamma*dt
        self.betai[2] = -gamma*dt
        self.alphae[1] = 1.0
        self.alphae[2] = (1.0-2.0* gamma)*dt
        self.alphae[3] = (0.5-2.0* gamma + gamma*gamma)*dt*dt
    end

    if self.order ==4 && self.stage==3
        gamma = (1.0/sqrt(3.0))*cos(pi/18.0)+0.5
        self.alphai[1] = 1.0
        self.alphai[2] = 1.0
        self.alphai[3] = 1.0
        self.betai[1] = -gamma*dt
        self.betai[2] = -gamma*dt
        self.betai[3] = -gamma*dt
        self.alphae[1] = 1.0
        self.alphae[2] = (1.0-3.0* gamma)*dt
        self.alphae[3] = (0.5-3.0* gamma + 3.0*gamma*gamma)*dt*dt
        self.alphae[4] = (1.0/6.0-1.5* gamma + 3.0*gamma*gamma-gamma*gamma*gamma)*dt*dt*dt
    end      
    
  if self.apply_op3 ==0
      x = zeros(Float64,self.n)
      y = zeros(Float64,self.m)  
      b1 = zeros(Float64,self.n)
      b2 = zeros(Float64,self.m)
      ytemp = zeros(Float64,self.m)
      s = zeros(Float64,self.n+self.m)  
        
      if self.apply_src == 1
        s = self.source(self.space,xinit)
      end    

      x[:] .= xinit[1:self.n]
      y[:] .= xinit[self.n+1:end]
      b1[:] .= dt*s[1:self.n] .+ self.alphae[1].*x[:]
      b2[:] .= self.alphae[1].*y[:] 

      for i in 2:self.stage+1
        ytemp = self.spaceop2(self.space,x)
        x = self.spaceop1(self.space,y)
        y[:] .= ytemp[:]
        b1[:] .= b1[:] + self.alphae[i]*x[:]
        b2[:] .= b2[:] + self.alphae[i]*y[:]
      end
 
      for i in 1:self.stage        
          b1 = b1 - self.betai[i]*self.spaceop1(self.space,b2)  
          self.istage = i
          x = solve(self,b1,xinit[1:self.n])  
          y = b2 - self.betai[i]*self.spaceop2(self.space,x)
          b1[:] .= x[:]
          b2[:] .= y[:]
      end    
      self.time = self.time + dt
      xinit[1:self.n] .= x[:]
      xinit[self.n+1:end] .= y[:] + dt*s[self.n+1:end]  
   else
        x = zeros(Float64,self.n)
        y = zeros(Float64,self.m)  
        z = zeros(Float64,self.p)
        b1 = zeros(Float64,self.n)
        b2 = zeros(Float64,self.m)
        b3 = zeros(Float64,self.p)
        ytemp = zeros(Float64,self.m)
        s = zeros(Float64,self.n+self.m+self.p) 
        
        if self.apply_src == 1
            s = self.source(self.space,xinit)
        end        
       
       x[:] .= xinit[1:self.n]
       y[:] .= xinit[self.n+1:self.n+self.m]
       z[:] .= xinit[self.n+self.m+1:end]
       b1[:] .= dt*s[1:self.n] .+ self.alphae[1].*x[:] 
       b2[:] .= self.alphae[1].*y[:] 
       b3[:] .= self.alphae[1].*z[:]  

       for i in 2:self.stage+1
            ytemp = self.spaceop2(self.space,x)
            x = self.spaceop1(self.space,y)
            z = self.spaceop3(self.space,x,y)
            y[:] .= ytemp[:]
            b1[:] .= b1[:] + self.alphae[i]*x[:]
            b2[:] .= b2[:] + self.alphae[i]*y[:]
            b3[:] .= b3[:] + self.alphae[i]*z[:]
       end
        
       x = zeros(Float64,self.n)
       y = zeros(Float64,self.m)  
       z = zeros(Float64,self.p)
       
       for i in 1:self.stage
           b1 = b1 - self.betai[i]*self.spaceop1(self.space,b2)  
           self.istage = i 
           x = solve(self,b1,xinit[1:self.n]) 
           y = b2 - self.betai[i]*self.spaceop2(self.space,x)
           z = b3 - self.betai[i]*self.spaceop3(self.space,x,y)
           b1[:] .= x[:]
           b2[:] .= y[:]
           b3[:] .= z[:]
       end    
        
       self.time = self.time + dt
       xinit[1:self.n] .= x[:]
       xinit[self.n+1:self.n+self.m] .= y[:] .+ dt*s[self.n+1:self.n+self.m]
       xinit[self.n+self.m+1:end] .= z[:] .+ dt*s[self.n+self.m+1:end]
   end     
   return xinit
end


function SDirkTheta_Schur(self::SchurImplicit_mstage, theta, xinit, dt)

    self.alphai[1] = 1.0
    self.betai[1]  = -(theta)*dt
    self.alphae[1] = 1.0
    self.alphae[2] = (1-theta)*dt
    
  if self.apply_op3 ==0
      x = zeros(Float64,self.n)
      y = zeros(Float64,self.m)  
      b1 = zeros(Float64,self.n)
      b2 = zeros(Float64,self.m)
      ytemp = zeros(Float64,self.m)
      s = zeros(Float64,self.n+self.m)  
        
      if self.apply_src == 1
        s = self.source(self.space,xinit)
      end    

      x[:] .= xinit[1:self.n]
      y[:] .= xinit[self.n+1:end]
      b1[:] .= dt*s[1:self.n] .+ self.alphae[1].*x[:]
      b2[:] .= self.alphae[1].*y[:] 

      for i in 2:self.stage+1
        ytemp = self.spaceop2(self.space,x)
        x = self.spaceop1(self.space,y)
        y[:] .= ytemp[:]
        b1[:] .= b1[:] + self.alphae[i]*x[:]
        b2[:] .= b2[:] + self.alphae[i]*y[:]
      end

      for i in 1:self.stage
        b1 = b1 - self.betai[i]*self.spaceop1(self.space,b2)   
        self.istage = i
        x = solve(self,b1,xinit[1:self.n])  
        y = b2 - self.betai[i]*self.spaceop2(self.space,x)
        b1[:] .= x[:]
        b2[:] .= y[:]
      end    
      self.time = self.time + dt
      xinit[1:self.n] .= x[:]
      xinit[self.n+1:end] .= y[:] + dt*s[self.n+1:end]
   else
        x = zeros(Float64,self.n)
        y = zeros(Float64,self.m)  
        z = zeros(Float64,self.p)
        b1 = zeros(Float64,self.n)
        b2 = zeros(Float64,self.m)
        b3 = zeros(Float64,self.p)
        ytemp = zeros(Float64,self.m)
        s = zeros(Float64,self.n+self.m+self.p) 
        
        if self.apply_src == 1
            s = self.source(self.space,xinit)
        end        
       
       x[:] .= xinit[1:self.n]
       y[:] .= xinit[self.n+1:self.n+self.m]
       z[:] .= xinit[self.n+self.m+1:end]
       b1[:] .= dt*s[1:self.n] .+ self.alphae[1].*x[:] 
       b2[:] .= self.alphae[1].*y[:] 
       b3[:] .= self.alphae[1].*z[:]  

       for i in 2:self.stage+1
            ytemp = self.spaceop2(self.space,x)
            x = self.spaceop1(self.space,y)
            z = self.spaceop3(self.space,x,y)
            y[:] .= ytemp[:]
            b1[:] .= b1[:] + self.alphae[i]*x[:]
            b2[:] .= b2[:] + self.alphae[i]*y[:]
            b3[:] .= b3[:] + self.alphae[i]*z[:]
       end
        
       x = zeros(Float64,self.n)
       y = zeros(Float64,self.m)  
       z = zeros(Float64,self.p)
       
       for i in 1:self.stage
           b1 = b1 - self.betai[i]*self.spaceop1(self.space,b2)
           self.istage = i 
           x = solve(self,b1,xinit[1:self.n])   
           y = b2 - self.betai[i]*self.spaceop2(self.space,x)
           z = b3 - self.betai[i]*self.spaceop3(self.space,x,y)
           b1[:] .= x[:]
           b2[:] .= y[:]
           b3[:] .= z[:]
       end    
        
       self.time = self.time + dt
       xinit[1:self.n] .= x[:]
       xinit[self.n+1:self.n+self.m] .= y[:] .+ dt*s[self.n+1:self.n+self.m]
       xinit[self.n+self.m+1:end] .= z[:] .+ dt*s[self.n+self.m+1:end]
   end     
   return xinit
end