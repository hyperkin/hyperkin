export vf2D, set_numflux, set_numfluxd, set_source, set_numncflux
export initialization, reduction, diagnostics, time_diagnostics_optimized
export bc_neumann,bc_periodic, bc_dirichlet, No2DNcFlux, No2DDiffFlux, No2DLimiting, No2DFlux
export set_dof

##### Class for 2D finite volumes discretization. Allows to create the ODE vector after FV discretization
mutable struct vf2D <: SpatialDis

      nvar       :: Int64                 # Nb variables of the model 
      noutvar    :: Int64                 # Nb of output variables 
      ndof       :: Int64                 # Nb of degree of freedom
      mh         :: Mesh2D                # 2D Mesh associated
      field      :: Array{Float64,2}      # Table of the variables/fields on each cell 
      flux       :: Array{Float64,2}      # Table of the fluxes on each cell
      numflux    :: Flux                  # Numerical conservative flux (function)
      source     :: Flux                  # Numerical source term (function)
      numfluxd   :: Flux                  # Numerical diffusion flux (function)
      numncflux  :: Flux 
      ndiags     :: Int64                 # Nb of diagnostics (like L1 norm)
    
   #### Constructor 1
   ## inputs: mh (2D mesh), n (nb variables)
   ## in this case zero diagnostics, nb of output variables is n 
   function vf2D(mh, n)
      nvar     = n
      noutvar  = n  
      ndof = mh.Nc
      field     = zeros(Float64,(ndof,nvar))
      flux      = zeros(Float64,(ndof,nvar))
      numflux   = No2DFlux()
      source    = No2DSource()
      numfluxd  = No2DDiffFlux()
      numncflux = No2DncFlux() 
      ndiags    = 0

      new(nvar, noutvar, ndof, mh, field,flux, numflux,source, numfluxd, numncflux,ndiags)   
  end

  #### Constructor 2
  ## inputs: mh (2D mesh), n (nb variables), ng (nb of diagnostics)
  ## in this case nb of output variables is n    
  function vf2D(mh, n, ng)
      nvar = n
      noutvar = n
      ndof = mh.Nc
      field     = zeros(Float64,(ndof,nvar))
      flux      = zeros(Float64,(ndof,nvar))
      numflux   = No2DFlux()
      source    = No2DSource()
      numfluxd  = No2DDiffFlux()  
      numncflux = No2DncFlux()   

      new(nvar, noutvar, ndof, mh, field,flux, numflux,source, numfluxd, numncflux,ng) 
  end
    
  #### Constructor 3
  ## inputs: mh (2D mesh), n (nb variables), ng (nb of diagnostics), no (nb of output variables)    
  function vf2D(mh, n, ng,  no)
      nvar = n
      noutvar = no
      ndof = mh.Nc
      field     = zeros(Float64,(ndof,nvar))
      flux      = zeros(Float64,(ndof,nvar))
      numflux   = No2DFlux()
      source    = No2DSource()
      numfluxd  = No2DDiffFlux() 
      numncflux = No2DncFlux() 

      new(nvar, noutvar, ndof, mh, field,flux, numflux,source, numfluxd, numncflux,ng) 
  end 

end

##### Method to gives the conservative numerical flux
## input: numflux (object containing the numerical flux function)
function set_numflux(self::vf2D, numflux)
    self.numflux = numflux
end

##### Method to gives the diffusion flux
## input: numflux (object containing the diffusion function)
function set_numfluxd(self::vf2D, numflux)
    self.numfluxd = numflux
end

##### Method to gives the source term
## input: numflux (object containing the source term function)
function set_source(self::vf2D, numflux)
    self.source = numflux
end

##### Method to gives the non conservative flux
## input: numflux (object containing the flux function)
function set_numncflux(self::vf2D, numncflux)
    self.numncflux = numncflux
end

function set_dof(self::vf2D, i_dof)
    return self.mh.centers[i_dof,:]
end

##### Method to initialize the fields
## input: init_f (object containing the initialization function)
function initialization(self::vf2D, init_f)
    for i in 1:self.mh.Nc
        if self.mh.labels[i] >= 0 
            x = self.mh.centers[i,:]
            for (j,v) in enumerate(init_f(x,0.0))
                self.field[i,j] = v
            end
        end    
    end
end

##### Method to call the boundary function
## input: bc_type (integer to choose the BC)
function bc(self::vf2D,bc_type)
    if bc_type == 1
        bc_neumann(self)
    end
    if bc_type == 2
        bc_periodic(self)
    end
end

##### Method to put Neumann values inside ghost cells
function bc_neumann(self::vf2D)
    @simd for i in 1:self.mh.Nx+2
        self.field[i,:] = self.field[i + self.mh.Nx+2,:] 
        self.field[end-i+1,:] = self.field[end-i+1-(self.mh.Nx+2),:] 
    end
    
    @simd for i in 1:self.mh.Ny+2
        self.field[1+(i-1)*(self.mh.Nx+2),:] = self.field[1+(i-1)*(self.mh.Nx+2) + 1,:]
        self.field[i*(self.mh.Nx+2),:] = self.field[i*(self.mh.Nx+2)- 1,:]
    end  
end
        
##### Method to put Periodic values inside ghost cells
function bc_periodic(self::vf2D)   
    @simd for i in 1:self.mh.Nx+2
        self.field[i,:] = self.field[self.mh.Nc+i-2*(self.mh.Nx+2),:]
        self.field[self.mh.Nc+i-(self.mh.Nx+2),:] = self.field[i+(self.mh.Nx+2),:]
    end
    
    @simd for i in 1:self.mh.Ny+2
        self.field[1+(i-1)*(self.mh.Nx+2),:] = self.field[1+(i-1)*(self.mh.Nx+2)+self.mh.Ny,:]
        self.field[i*(self.mh.Nx+2),:] = self.field[i*(self.mh.Nx+2)-self.mh.Ny,:]
    end
end

function bc_dirichlet(self::vf2D, func)   
    @simd for i in 1:self.mh.Nx+2
        self.field[i,:] = func(self.mh.centers[i,:])
        self.field[self.mh.Nc+i-(self.mh.Nx+2),:] = func(self.mh.centers[self.mh.Nc+i-(self.mh.Nx+2),:])
    end
    
    @simd for i in 1:self.mh.Ny+2
        self.field[1+(i-1)*(self.mh.Nx+2),:] = func(self.mh.centers[1+(i-1)*(self.mh.Nx+2),:])
        self.field[i*(self.mh.Nx+2),:] = func(self.mh.centers[i*(self.mh.Nx+2),:])
    end
end

##### Operator () to compute the global flux associated to the VF scheme. Gives a "vector" for the ODE system
function (self::vf2D)() # generic_vf_op() is replaced by model()
    F = zeros(Float64,self.nvar)
    Fd = zeros(Float64,self.nvar)
    S = zeros(Float64,self.nvar)
    Fnc = zeros(Float64,self.nvar)
    
    for i in 1:self.mh.Nc   
        if  self.mh.labels[i] >= 0
            self.flux[i,:] = zeros(Float64,self.nvar)
            tnei=neighbors(self.mh,i)
            tnei_l=neighbors_large(self.mh,i)
            tnor=normals(self.mh,i)
            tl=lengths(self.mh,i)
            x  = self.mh.centers[i,:]
            h = self.mh.areas[i]  
            for j in 1:4  
                self.numflux(x[:],h,F,self.field[i,:],self.field[tnei[j],:],tnor[j])
                self.numfluxd(x[:],h,Fd,self.field[i,:],self.field[tnei[j],:],tnor[j])
                self.source(x[:],h,S,self.field[i,:],self.field[tnei[j],:],tnor[j])               
                self.flux[i,:] += tl[j]*F[:] + tl[j]*Fd[:] - h*S[:] 
            end
            tv=[self.field[tnei_l[j],:] for j in 1:8]
            self.numncflux(x,h,Fnc,self.field[i,:],tv,tl,tnor)
            self.flux[i,:] = -(self.flux[i,:]+Fnc[:]) /h
        end
    end 
end
  
##### Method to compute a quantity using a function on all the degree of freedom
## exemple: compute a max on the domain
## input: function_reduced (the fonction used for reduction), init_res (the initial value of the quantity)
## output: res (final value)
function reduction(self::vf2D, function_reduced, init_res)
        
    res = init_res
    for i in 1:self.mh.Nc
        if self.mh.labels[i] >= 0
            res = function_reduced(self.mh.centers[i,:],self.field[i,:],res)
        end    
    end
    return res
end


##### Class containing the function for the case without diffusive flux
struct No2DDiffFlux <: Flux
end
function (self::No2DDiffFlux)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    nothing
end

##### Class containing the function for the case without source term
struct No2DSource <: Flux
end
function (self::No2DSource)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    nothing
end

##### Class containing the function for the case without conservative flux
struct No2DFlux <: Flux
end
function (self::No2DFlux)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    nothing
end

struct No2DncFlux <: Flux
end
    function (self::No2DncFlux)(x,h,data::Vector{Float64},vC::Vector{Float64},tv::Vector{Vector{Float64}},tl::Vector{Float64},tn::Vector{Vector{Float64}})
    nothing
end
    
##### Method to compute the output variables and the global diagnostics associated
## input: ref (ref=1 there is a exact solution, ref=0 for no), ti (time), f_ref (an reference function)
## input: mapping (object containing function to transform variables to output variables)
## input: mapping (object containing function to local computation of diagnostic using output variables)
## output: field ( output variables), diags (global quantities like norm), field_ref (exact output variables)
function diagnostics(self::vf2D,ref,ti,f_ref,mapping,form_diags)
    diags = zeros(self.ndiags)
    loc_diags = zeros(self.ndiags)
    fg = zeros(self.noutvar)
    fg_ref = zeros(self.noutvar)
    field_ref = zeros(Float64,(self.mh.Nx,self.mh.Ny,self.noutvar))
    field = zeros(Float64,(self.mh.Nx,self.mh.Ny,self.noutvar))
          
    k=1; i=1; j=1
    if ref == 0
       for k in 1:self.mh.Nc
           if  self.mh.labels[k] >= 0   
                mapping(self.field[k,:],fg)
                field[i,j,:] .= fg[:] 
                form_diags(field[i,j,:],loc_diags)    
                diags[:] .+= self.mh.areas[i]*loc_diags[:]
                j = j+1
                if j > self.mh.Nx
                    j =1; i=i+1
                end
                if i > self.mh.Ny
                    break
                end
           end   
       end 
       [field, diags] 
    else
       for k in 1:self.mh.Nc
           if  self.mh.labels[k] >= 0          
               x = self.mh.centers[k,:]
               mapping(self.field[k,:],fg)
               mapping(f_ref(x,ti),fg_ref)
               field[i,j,:] .= fg[:] 
               field_ref[i,j,:] .= fg_ref[:] 
               form_diags(field[i,j,:]-field_ref[i,j,:],loc_diags)    
               diags[:] .+= self.mh.areas[k]*loc_diags[:]  
               j = j+1
               if j > self.mh.Nx
                   j =1; i=i+1
               end
               if i > self.mh.Ny
                   break
               end    
           end    
        end
      [field, field_ref, diags] 
    end
end

function time_diagnostics_optimized(self::vf2D,ti,mapping,form_diags)
    diags = zeros(self.ndiags)
    loc_diags = zeros(self.ndiags)
    fg = zeros(self.noutvar)

    for k in 1:self.mh.Nc
        if  self.mh.labels[k] >= 0   
            mapping(self.field[k,:],fg)
            form_diags(fg[:],loc_diags)    
            diags[:] .+= self.mh.areas[k]*loc_diags[:]
         end   
    end 
    return diags 
end