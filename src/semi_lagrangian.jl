export SemiLagrangian
export bc_sl
export stencil
export cell_characteristic
export interpolation
export solve_sl

################ space ##################
mutable struct SemiLagrangian <: SpatialDis
    ndof       :: Int64
    mh         :: Mesh
    field      :: Vector{Float64}
    bc_type    :: Int64
    order      :: Int64

    function SemiLagrangian(mh,bc,or)
        ndof = mh.Nv-1
        field     = zeros(Float64,ndof)
        bc_type   = bc
        order     = or # 2 order +1
        new(ndof, mh, field, bc_type, order)
    end 
end   


function bc_sl(self::SemiLagrangian)
    bc = zeros(Float64,2*self.order+2)
    xc = zeros(Float64,2*self.order+2)
    if self.order ==0
        if self.bc_type ==1 # periodic
            xc[1] = self.mh.centers[end]
            xc[2] = self.mh.centers[1]
            bc[1]= self.field[end]
            bc[2]= self.field[1]
        end
    end  
    if self.order ==1
        if self.bc_type ==1 # periodic
            xc[1]= self.mh.centers[end-1]
            xc[2]= self.mh.centers[end]
            xc[3]= self.mh.centers[1]
            xc[4]= self.mh.centers[2]
            bc[1]= self.field[end-1]
            bc[2]= self.field[end]
            bc[3]= self.field[1]
            bc[4]= self.field[2]
        end
    end  
    return [xc, bc]
end  
    

function stencil(self::SemiLagrangian, i)
    xvals = zeros(Float64,2*self.order+2)
    yvals = zeros(Float64,2*self.order+2)
    xc, bc = bc_sl(self)
    if self.order == 0
        if i == self.ndof           
            xvals = [self.mh.centers[self.ndof], xc[3]]
            yvals = [self.field[self.ndof], bc[3]]
        else
            xvals = [self.mh.centers[i], self.mh.centers[i+1]]
            yvals = [self.field[i], self.field[i+1]]
        end  
    end 
    
    if self.order == 1
        if i == self.ndof-1          
            xvals = [self.mh.centers[i-1], self.mh.centers[i], self.mh.centers[i+1],xc[3]]
            yvals = [self.field[i-1],self.field[i],self.field[i+1],bc[3]]
        elseif i == self.ndof          
            xvals = [self.mh.centers[i-1], self.mh.centers[i], xc[3],xc[4]]
            yvals = [self.field[i-1],self.field[i],bc[3],bc[4]]
        elseif  i == 1
            xvals = [xc[2],self.mh.centers[i], self.mh.centers[i+1], self.mh.centers[i+2]]
            yvals = [bc[2],self.field[i],self.field[i+1],self.field[i+2]]
        else
            xvals = [self.mh.centers[i-1],self.mh.centers[i], self.mh.centers[i+1], self.mh.centers[i+2]]
            yvals = [self.field[i-1],self.field[i],self.field[i+1],self.field[i+2]]
        end 
       
    end
    [xvals, yvals]
end

function interpolation(self::SemiLagrangian, i)
    xvals = zeros(Float64,2*self.order+2)
    yvals = zeros(Float64,2*self.order+2)
    xvals, yvals =stencil(self,i)
    la=lagrange(self.order)
    set_xyi(la,xvals,yvals)
    function inter(x)
        res=interpolator(la,x)
        return res
    end    
    return inter 
end

function foot_characteristic(self::SemiLagrangian, x, shift)
    xn = x + shift
    if self.bc_type ==1
         if xn < self.mh.nodes[1]
             xn = xn + self.mh.L
         elseif xn > self.mh.nodes[end]   
             xn = xn - self.mh.L
         end     
    end    
    return xn
end

function cell_characteristic(self::SemiLagrangian, ix, x, shift)
    
    xn = foot_characteristic(self,x,shift)
    ls = sign(shift)
    
    icell=ix  
    for k in 1:self.mh.Nc
        if xn >= self.mh.nodes[ix] && xn <= self.mh.nodes[ix+1]
            break
        end
        icell = icell + floor(Int,ls)
      
        if self.bc_type == 1
            if icell < 1
                icell = self.mh.Nc
            elseif icell > self.mh.Nc
                icell = 1
            end  
        end    
        if xn >= self.mh.nodes[icell] && xn <= self.mh.nodes[icell+1]
            break
        end
    end
    return icell
end

function solve_sl(self::SemiLagrangian, field::Vector{Float64}, v::Float64)
    res= zeros(Float64,self.ndof)
    self.field[:] .= field[:]
    for i in 1:self.ndof
        x = self.mh.centers[i] 
        xn = foot_characteristic(self,x,v)
        icell =cell_characteristic(self, i, x, v)
        pc = interpolation(self,icell)
        res[i]= pc(xn)
    end    
    
    return res
end    
