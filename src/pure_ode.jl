export pure_ode, set_time, set_odeflux, initialization, NoOdeFlux, diagnostics, diagnostic_error, push_timefield  
export poisson_ode, set_hamiltonian, set_entropy, set_poisson_matrix

##### Class for pure ODe equations (without spatial representation). Allows to create the ODE vector
mutable struct pure_ode <: SpatialDis
  nvar         :: Int64              # Nb variables of the model 
  noutvar      :: Int64           # Nb output variables of the model 
  ndof         :: Int64              # Nb of spatial degree of freedom ( always one)
  field        :: Array{Float64,2}   # Table of the current variables 
  flux         :: Array{Float64,2}   # Table of the current flux
  localflux    :: Vector{Float64}    # local data for flux computation 
  timefield    :: Array{Array,1}     # List of time variables  
  odeflux      :: Flux               # Flux ode (function)
  current_time :: Float64            # current time  
  ndiags       :: Int64              # Nb of diagnostics 

  #### Constructor 1
  ## inputs: n (nb variables)
  function pure_ode(n)
      nvar = n
      noutvar = n  
      ndof = 1 
      field = zeros(Float64,(1,n))    
      flux = zeros(Float64,(1,n))
      localflux = zeros(Float64,n)  
      timefield = []  
      odeflux     = NoOdeFlux()
      current_time =0.0  
      ndiags   = 0 

      new(nvar, noutvar, ndof, field, flux, localflux, timefield, odeflux, current_time, ndiags)   
  end
    
  #### Constructor 2
  ## inputs: n (nb variables), n0 (nb output variables)
  function pure_ode(n,no)
      nvar = n
      noutvar = no  
      ndof = 1 
      field = zeros(Float64,(1,n))    
      flux = zeros(Float64,(1,n))
      localflux = zeros(Float64,n)  
      timefield = []  
      odeflux     = NoOdeFlux()
      current_time =0.0  
      ndiags   = 0 

      new(nvar, noutvar, ndof, field, flux, localflux, timefield, odeflux, current_time,ndiags)   
  end
    
   function pure_ode(n,no,nd)
      nvar = n
      noutvar = no  
      ndof = 1 
      field = zeros(Float64,(1,n))    
      flux = zeros(Float64,(1,n))
      localflux = zeros(Float64,n)  
      timefield = []  
      odeflux     = NoOdeFlux()
      current_time =0.0  
      ndiags   = nd

      new(nvar, noutvar, ndof, field, flux, localflux, timefield, odeflux, current_time,ndiags)   
  end
end 

##### Method to gives the flux of the ode
## input: flux (object containing the flux function of the ode)
function set_odeflux(self::pure_ode, flux)
    self.odeflux = flux
end

##### Method to gives the time of the ode
## input: t (current time) 
function set_time(self::pure_ode, t)
    self.current_time = t
end

##### Method to initialize the fields
## input: v (vector containing the initialization data)
function initialization(self::pure_ode,v::Vector{Float64})
    push!(self.timefield,v)
    self.field[1,:] .= v[:]
end

##### Method to push the fields
## input: v (vector containing the initialization data)
function push_timefield(self::pure_ode)
    push!(self.timefield,self.field[1,:])
end

##### Operator to construct the flux
function (self::pure_ode)() 
    self.odeflux(self.current_time,self.localflux,self.field[1,:])
    self.flux[1,:] .= self.localflux[:]
end


##### Method to compute the output variables and the global diagnostics associated
## input: dt (time step), ti (time) 
## input: mapping (object containing function to transform variables to output variables)
## input: form_diags (object containing function to local computation of diagnostic using output variables)
## output: field ( output variables), diags (global quantities like norm), field_ref (exact output variables)
function diagnostics(self::pure_ode,dt,ti,mapping,form_diags)
   diags = zeros(self.ndiags)
   loc_diags = zeros(self.ndiags)
   fg = zeros(self.noutvar)
   field = zeros(Float64,(size(self.timefield)[1],self.noutvar))
   for i in 1:size(self.timefield)[1]
       mapping(self.timefield[i][:],fg) 
       field[i,:] .= fg[:]    
       form_diags(field[i,:],loc_diags)    
       diags[:] .+= dt*loc_diags[:]
   end
   [field, diags] 
end

##### Method to compute the output variables and the global diagnostics associated
## input: ref (ref=1 there is a exact solution, ref=0 for no), dt (time step), ti (time), f_ref (an reference function)
## input: mapping (object containing function to transform variables to output variables)
## input: form_diags  (object containing function to local computation of diagnostic using output variables)
## output: field ( output variables), diags (global quantities like norm), field_ref (exact output variables)
function diagnostics_error(self::pure_ode,ref,dt,ti,f_ref,mapping,form_diags)
   diags = zeros(self.ndiags)
   loc_diags = zeros(self.ndiags)
   fg_ref = zeros(self.noutvar)
   field_ref = zeros(Float64,(size(self.timefield)[1],self.noutvar))
   for i in 1:size(self.timefield)[1]
       mapping(self.timefield[i][:],fg)
       mapping(f_ref(ti[i]),fg)  
       field[i,:] .= fg[:]  
       field_ref[i,:] .= fg_ref[:]
       form_diags(field[i,:]-field_ref[i,:],loc_diags)   
       diags[:]  .+= dt*loc_diags[:]
   end
   [field, field_ref, diags] 
end

##### Class containing the function for the case without conservative flux
struct NoOdeFlux <: Flux
end
function (self::NoOdeFlux)(t,data::Vector{Float64},v::Vector{Float64})
    nothing
end


struct NoHamiltonian <: Flux
end
function (self::NoHamiltonian)(v::Vector)
    return 0.0
end 

struct Hamiltonian_matrix <: Flux
end
function (self::Hamiltonian_matrix)(data::Vector{Float64},v::Vector{Float64})
   # data= J = (0 Id \\ - Id 0) v
   n= size(v)[1]
   p=trunc(Int,n/2)  
   data[1:p] .= v[p+1:end]# flux the q variables
   data[p+1:end] .= -v[1:p] # flux for the p variables
end 

struct NoEntropy <: Flux
end
function (self::NoEntropy)(v::Vector)
    return 0.0
end 

struct DiagonalDissipation <: Flux
end
function (self::DiagonalDissipation)(data::Vector{Float64},v::Vector{Float64})
   # data= G = ( Id 0 \\ 0 Id) v
   data[:] .= v[:]
end 

mutable struct poisson_ode <: SpatialDis
  npar           :: Int64              # nb of partible 
  nvar           :: Int64              # Nb variables of the model 
  noutvar        :: Int64              # Nb output variables of the model 
  ndof           :: Int64              # Nb of spatial degree of freedom ( always one)
  field          :: Array{Float64,2}   # Table of the current variables 
  flux           :: Array{Float64,2}   # Table of the current flux
  gdr            :: Vector{Float64}    # local data for gradiant of H or S
  data           :: Vector{Float64}    # local data 
  timefield      :: Array{Array,1}     # List of time variables  
  Hamiltonian    :: Flux               # Hamiltonian fonction
  Entropy        :: Flux               # Entropy function for metriplectic case
  poisson_m      :: Flux               # Poisson matrix
  dissip_m       :: Flux               # Entropy matrix for metriplectic case  
  cutime         :: Float64            # current time  
  ndiags         :: Int64              # Nb of diagnostics  
  h_system       :: Int64              # 1 if it is a Hamiltonian, 0 if poisson system
 
  #### Constructor 1
  ## inputs: n (nb variables), poisson_system ( logical to determinate if poisson system)
  function poisson_ode(n, ham_system)
      npar = trunc(Int,n/2)   
      nvar = n
      noutvar = nvar  
      ndof = 1 
      field = zeros(Float64,(1,nvar))    
      flux = zeros(Float64,(1,nvar))
      gdr = zeros(Float64,n) 
      data = zeros(Float64,n)  
      timefield = []  
      Hamiltonian = NoHamiltonian()  
      poisson_m =  Hamiltonian_matrix()
      Entropy =  NoEntropy() 
      dissip_m = DiagonalDissipation() 
      cutime   = 0.0  
      ndiags   = 0 
      h_system = ham_system 
        
      new(npar,nvar,noutvar,ndof,field,flux,gdr,data,timefield,Hamiltonian,Entropy,poisson_m,dissip_m,cutime,ndiags,h_system)                                          
  end 
    
  #### Constructor 2
  ## inputs: n (nb variables), poisson_system (logical to determinate if poisson system), no (nb of output variables)
  function poisson_ode(n,no, ham_system)
      npar = trunc(Int,n/2)   
      nvar = n
      noutvar = no  
      ndof = 1 
      field = zeros(Float64,(1,nvar))    
      flux = zeros(Float64,(1,nvar))
      gdr = zeros(Float64,n) 
      data = zeros(Float64,n)  
      timefield = []  
      Hamiltonian = NoHamiltonian()  
      poisson_m =  Hamiltonian_matrix()
      Entropy =  NoEntropy() 
      dissip_m = DiagonalDissipation() 
      cutime   = 0.0  
      ndiags   = 0 
      h_system = ham_system 
        
      new(npar,nvar,noutvar,ndof,field,flux,gdr,data,timefield,Hamiltonian,Entropy,poisson_m,dissip_m,cutime,ndiags,h_system)                                          
  end 
    
  #### Constructor 3
  ## inputs: n (nb variables), poisson_system (logical to determinate if poisson system), no (nb of output variables), ng (nb diags)
  function poisson_ode(n,no,ng, ham_system)
      npar = trunc(Int,n/2)  
      nvar = n
      noutvar = no  
      ndof = 1 
      field = zeros(Float64,(1,nvar))    
      flux = zeros(Float64,(1,nvar))
      gdr = zeros(Float64,n) 
      data = zeros(Float64,n)  
      timefield = []  
      Hamiltonian = NoHamiltonian()  
      poisson_m =  Hamiltonian_matrix()
      Entropy =  NoEntropy() 
      dissip_m = DiagonalDissipation() 
      cutime   = 0.0  
      h_system = ham_system 
        
      new(npar,nvar,noutvar,ndof,field,flux,gdr,data,timefield,Hamiltonian,Entropy,poisson_m,dissip_m,cutime,ng,h_system)                                          
  end 
end 

##### Method to gives the hamiltonian of the ode
## input: flux (object containing the flux function of the ode)
function set_hamiltonian(self::poisson_ode,h)
    self.Hamiltonian = h
end

function set_poisson_matrix(self::poisson_ode,m)
    self.poisson_m = m
end

##### Method to gives the entropy of the ode
## input: flux (object containing the flux function of the ode)
function set_entropy(self::poisson_ode,s,ms)
    self.Entropy  = s
    self.dissip_m = ms
end

##### Method to gives the time of the ode
## input: t (current time) 
function set_time(self::poisson_ode, t)
    self.cutime = t
end

##### Method to initialize the fields
## input: v (vector containing the initialization data)
function initialization(self::poisson_ode,v::Vector{Float64})
    push!(self.timefield,v)
    self.field[1,:] .= v[:]
end

##### Method to push the fields
## input: v (vector containing the initialization data)
function push_timefield(self::poisson_ode)
    push!(self.timefield,self.field[1,:])
end

##### Operator to construct the flux
function (self::poisson_ode)() 
    g = x -> ForwardDiff.gradient(self.Hamiltonian, x) 
    self.gdr = g(self.field[1,:])
    self.poisson_m(self.data,self.gdr[:])
    self.flux[1,:] = self.data[:]
    
    g = x -> ForwardDiff.gradient(self.Entropy, x)
    self.gdr = g(self.field[1,:])
    self.dissip_m(self.data,self.gdr[:])
    self.flux[1,:] = self.flux[1,:] + self.data[:]
end


##### Method to compute the output variables and the global diagnostics associated
## input: dt (time step), ti (time) 
## input: mapping (object containing function to transform variables to output variables)
## input: form_diags (object containing function to local computation of diagnostic using output variables)
## output: field ( output variables), diags (global quantities like norm), field_ref (exact output variables)
function diagnostics(self::poisson_ode,dt,ti,mapping,form_diags)
   diags = zeros(self.ndiags)
   loc_diags = zeros(self.ndiags)
   fg = zeros(self.noutvar)
   field = zeros(Float64,(size(self.timefield)[1],self.noutvar))
   for i in 1:size(self.timefield)[1]
       mapping(self.timefield[i][:],fg) 
       field[i,:] .= fg[:]    
       form_diags(field[i,:],loc_diags)    
       diags[:] .+= dt*loc_diags[:]
   end
   [field, diags] 
end

##### Method to compute the output variables and the global diagnostics associated
## input: ref (ref=1 there is a exact solution, ref=0 for no), dt (time step), ti (time), f_ref (an reference function)
## input: mapping (object containing function to transform variables to output variables)
## input: form_diags  (object containing function to local computation of diagnostic using output variables)
## output: field ( output variables), diags (global quantities like norm), field_ref (exact output variables)
function diagnostics_error(self::poisson_ode,ref,dt,ti,f_ref,mapping,form_diags)
   diags = zeros(self.ndiags)
   loc_diags = zeros(self.ndiags)
   fg_ref = zeros(self.noutvar)
   field_ref = zeros(Float64,(size(self.timefield)[1],self.noutvar))
   for i in 1:size(self.timefield)[1]
       mapping(self.timefield[i][:],fg)
       mapping(f_ref(ti[i]),fg)  
       field[i,:] .= fg[:]  
       field_ref[i,:] .= fg_ref[:]
       form_diags(field[i,:]-field_ref[i,:],loc_diags)   
       diags[:]  .+= dt*loc_diags[:]
   end
   [field, field_ref, diags] 
end
