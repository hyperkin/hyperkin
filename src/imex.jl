export Nullvar_to_vect,Null_vect_to_var
export Imex_mstage, set_spaceop, set_idop, set_tol, solve, SDirk, SDirk_Imex, set_var_to_vec, set_vec_to_var


struct Nullvar_to_vect <: Operator
end
function (self::Nullvar_to_vect)(sp :: SpatialDis,v :: Vector{Float64})
end

struct Nullvect_to_var <: Operator
end
function (self::Nullvect_to_var)(v :: Vector{Float64},sp :: SpatialDis)
end


# mutistage implicit sep. We solve dt X(t) = AX(t)

mutable struct Imex_mstage <: TimeDis

    order     :: Int64
    space     :: SpatialDis
    relax     :: relaxation
    time      :: Float64
    bc_type   :: Int64
    spaceop   :: Operator
    idop      :: Operator
    source    :: Operator
    var_to_vec:: Operator
    vec_to_var:: Operator
    n         :: Int64
    m         :: Int64
    alphai    :: Vector{Float64}
    betai     :: Vector{Float64}
    istage    :: Int64
    sym       :: Int64
    tol       :: Float64

    function Imex_mstage(space,relax,order, bc)
  
      spaceop    = NullSpaceOp()
      source     = NullSource()
      idop       = IdGenericOp()
      var_to_vec = Nullvar_to_vect()
      vec_to_var = Nullvect_to_var() 
      n = 0
      m = 0
      alphai = zeros(Float64,order)
      betai = zeros(Float64,order)   
      time = 0.0 
      istage=1
      sym = 0
      tol = 1.0e-12 
      new(order,space,relax,time,bc,spaceop,idop,source,var_to_vec,vec_to_var, n, m, alphai,betai,istage,sym,tol)

    end

    function Imex_mstage(space,relax,order, bc, s)
  
      spaceop    = NullSpaceOp()
      source     = NullSource()
      idop       = IdGenericOp()
      var_to_vec = Nullvar_to_vect()
      vec_to_var = Nullvect_to_var() 
      n = 0
      m = 0
      alphai = zeros(Float64,order)
      betai = zeros(Float64,order)   
      time = 0.0  
      istage=1
      sym=s
      tol = 1.0e-12  
      new(order,space,relax,time,bc,spaceop,idop,source,var_to_vec,vec_to_var, n, m, alphai, betai,istage,sym,tol)

    end
    
end

function set_spaceop(self::Imex_mstage,n,op)
  self.n = n 
  self.spaceop = op
end

function set_idop(self::Imex_mstage,op)
  self.idop = op
end

function set_tol(self::Imex_mstage,tol)
  self.tol = tol
end

function set_vec_to_var(self::Imex_mstage,op)
  self.vec_to_var = op
end

function set_var_to_vec(self::Imex_mstage,op)
  self.var_to_vec = op
end


function (self::Imex_mstage)( v :: Vector{Float64} ) 
  #println("product MV")
  res = zeros(Float64, self.n)
  w1  = zeros(Float64,self.n)
  w2  = zeros(Float64,self.n)
  w1 =  self.idop(self.space,v)
  w2 =  self.spaceop(self.space,v)
  
  res[:] .= self.alphai[self.istage].*w1[:] .+ self.betai[self.istage].*w2[:]
  return res
end
        
        
function solve(self::Imex_mstage, b, xinit)
  x = zeros(Float64,self.n)
  A = LinearMap{Float64}(self, self.n; ismutating=false )
  if self.sym == 0
    x, his=cg!(xinit,A,b,abstol=self.tol,maxiter=10*self.n,log=true)
  else 
    x, his=idrs!(xinit,A,b;s=20,abstol=self.tol,maxiter=5*self.n,log=true)
  end  
  print(his)
  return x    
end

function SDirk_Imex(self::Imex_mstage, dt)

  if self.order == 1
    self.istage=1
    x = zeros(Float64,self.n)
    x1 = zeros(Float64,self.n)    
    b = zeros(Float64,self.n)
    self.alphai[1] = 1.0
    self.betai[1] = -dt
        
    self.var_to_vec(self.space,x) ### init with xn
    b[:] .= x[:] ## The rhs is xn
    x1 = solve(self,b,x)
    self.vec_to_var(x1,self.relax.space)
    self.relax()
    self.var_to_vec(self.relax.space,x1)
    #x = x + dt*self.spaceop(self.space,x1) 
    #self.vec_to_var(x,self.relax.space)
    self.vec_to_var(x1,self.space) 
  end

  if self.order ==2 
    self.istage=1
    gamma=1.0-sqrt(2.0)/2.0
    self.alphai[1] = 1.0
    self.alphai[2] = 1.0
    self.betai[1] = -gamma*dt
    self.betai[2] = -gamma*dt    
    a21 = -(1.0-gamma)*dt
    b11 = -(1.0-gamma)*dt    
    b12 = -gamma*dt    
        
    x1 = zeros(Float64,self.n)
    x2 = zeros(Float64,self.n)
    b1 = zeros(Float64,self.n)
    b2 = zeros(Float64,self.n) 
    x = zeros(Float64,self.n)
        
    self.var_to_vec(self.space,x)
    b1[:] .= x[:]
        
    x1 = solve(self,b1,x)
    self.vec_to_var(x1,self.relax.space)
    self.relax()
    self.var_to_vec(self.relax.space,x1)
    self.istage = self.istage+1
        
    b2 = x - a21*self.spaceop(self.space,x1)
    x2 = solve(self,b2,x1)
    self.vec_to_var(x2,self.relax.space)
    self.relax()
    self.var_to_vec(self.relax.space,x2)
        
    #x= x - b11*self.spaceop(self.space,x1)- b12*self.spaceop(self.space,x2)
    self.vec_to_var(x2,self.space)    
  end
    
  self.time = self.time + dt
  return x
end






