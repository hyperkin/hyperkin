module Hyperkin

using Reexport
using SpecialFunctions
@reexport using LaTeXStrings

@reexport using LinearMaps
@reexport using IterativeSolvers
@reexport using LinearAlgebra
@reexport using ForwardDiff

@reexport using JuMP
@reexport using GLPK
@reexport using Test
@reexport using LightGraphs
@reexport using GraphPlot
@reexport using SimpleWeightedGraphs

@reexport using Interact

include("abstract_type.jl") # file with some abstract type used
include("mesh.jl") # 1D mesh class 
include("mesh2D.jl") # 2D mesh class
include("vf.jl") # 1D Finite Volume class to construct: variables data, ODE vector and diagnostics
include("vf2D.jl") # 2D Finite Volume class to construct: variables data, ODE vector and diagnostics
#include("weno.jl") # 1D Weno class to construct: variables data, ODE vector and diagnostics
include("lagrange.jl") # Class containing basic functions par 1D Lagrange interpolation/histopolation
include("dg.jl") # 1D Discontinuous Galerkin class to construct: variables data, ODE vector and diagnostics
include("derham.jl") # Some class to describe DeRham sequence in 1D
include("staggered.jl") # 1d staggered finite difference class for system based DeRham sequence
include("pure_ode.jl") # Class containing pure ode data
include("explicit.jl") # Class containing explicit time integrators
include("implicit.jl")  # Class containing implicit time integrators
include("relaxation.jl")  # Class containing time integrators for relaxation ODE
include("imex.jl")  # Class containing imex time integrators for relaxation PDE (Andrea Thomann method)
include("coupling_dis.jl") # Class containing some projectors between VF/DG representation and Staggered representation in 1D
include("semi_lagrangian.jl") # Class for Semi Lagrangian solver in 1D
include("traffic_network/Road.jl") # Class for the description of a Road in 1D
include("traffic_network/Traffic.jl") # Class for the description of a VF solver for a Road in 1D
include("traffic_network/Junction.jl") # Class for the description of a junction between two roads
include("traffic_network/Network.jl") # Class for the description of a traffic road on network, composed to road and junction
end # module


######### Following
## Implement Relaxation to a gradient formula
## Finish Weno
## implement Imex
## implement splitting et composition ?
