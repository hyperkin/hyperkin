export Junction, set_in_data, set_out_data, set_Jmat
export LinProg

mutable struct Junction
    Nin      :: Int64
    Nout     :: Int64
    InNeigh  :: Vector{Int64}
    OutNeigh :: Vector{Int64}
    Jmat     :: Array{Float64,2}
    
    function Junction()
        Nin = 0
        Nout = 0
        InNeigh  = Int64[]
        OutNeigh = Int64[]
        Jmat     = zeros(Float64, (Nout, Nin))
        println("Buiding a junction")
        new( Nin, Nout, InNeigh, OutNeigh, Jmat )
    end
end # class Junction

# ----- methods of class Junction -----

function set_in_data(self::Junction, neigh::Int64)
    push!(self.InNeigh, neigh)
    self.Nin += 1
end

function set_out_data(self::Junction, neigh::Int64)
    push!(self.OutNeigh, neigh)
    self.Nout += 1
end

function set_Jmat(self::Junction, Jmat::Array{Float64,2})
    self.Jmat = Jmat
end

# call 1x1, 2x1 & 2x2 
function LinProg(self::Junction, f_in_max::Vector{Float64}, f_out_max::Vector{Float64})
    
    new_f_in = copy(f_in_max)
    new_f_out = copy(f_out_max)
    if length(f_in_max) == 1
        if length(f_out_max) == 1 # Bottleneck problem
         #   println("case : 1x1")
            new_f_in = [min(f_in_max[1],f_out_max[1])]
            new_f_out = [min(f_in_max[1],f_out_max[1])]
        end
    elseif length(f_in_max) == 2
        if length(f_out_max) == 1 # call 2x1
        #    println("case : 2x1")
            new_f_in = two_one(f_in_max[1], f_in_max[2], f_out_max[1])
            new_f_out = self.Jmat*new_f_in # A = [1 1]
        elseif length(f_out_max) == 2 # call 2x2
        #    println("case : 2x2")
            #     | alpha    beta   |
            # A = | 1-alpha  1-beta | (2x2 matrix)
            alpha = self.Jmat[1,1]
            beta = self.Jmat[1,2]
            new_f_in = two_two(f_in_max[1], f_in_max[2], f_out_max[1], f_out_max[2], alpha, beta)
            new_f_out = self.Jmat*new_f_in
        end
    end 
    return [new_f_in, new_f_out]
end

# TOOODOOO optimiser en initialisant une seule fois le probleme d'optimisation

#two ingoing roads, one outgoing road
function two_one(γ_a_max, γ_b_max, γ_c_max;
                 verbose = false, q = 0.5, with_test = true)
    
    if with_test == false
        model = Model(with_optimizer(GLPK.Optimizer))
    
        @variable(model, 0. <= γ_a <= γ_a_max)
        @variable(model, 0. <= γ_b <= γ_b_max)
    
        @objective(model, Max, γ_a + γ_b)
    
        @constraint(model, γ_a + γ_b <= γ_c_max) # A*γ_in ≤ γ_out_max with A = [1 1]
    
        if γ_a_max + γ_b_max > γ_c_max 
          #  println(">>> over-saturation")
            @constraint(model, γ_b == ((1 - q)/q) * γ_a ) # to obtain the uniqueness of the solution
        else
          #  println("under-saturation")
        end
        JuMP.optimize!(model)
        γ_c_value = JuMP.objective_value(model)
        γ_a_value = JuMP.value(γ_a)
        γ_b_value = JuMP.value(γ_b)
    end    

    # Tests 
    if with_test
        γ_a_value = 0.0
        γ_b_value = 0.0
        if γ_a_max + γ_b_max <= γ_c_max 
            # page 29 condition (35)
            γ_a_value = γ_a_max
            γ_b_value = γ_b_max
        else
            # page 29 condition (36)
            if γ_a_max >= q * γ_c_max && γ_b_max >= (1 - q) * γ_c_max
                γ_a_value = q * γ_c_max
                γ_b_value = (1 - q) * γ_c_max
            elseif γ_a_max < q * γ_c_max && γ_b_max >= (1 - q) * γ_c_max
                γ_a_value = γ_a_max
                γ_b_value = γ_c_max - γ_a_max
            elseif γ_a_max >= q * γ_c_max && γ_b_max < (1 - q) * γ_c_max
                γ_a_value = γ_c_max - γ_b_max 
                γ_b_value = γ_b_max
            end
        end
    end
    
    return [γ_a_value , γ_b_value]
end
#------------------------------------------------------------
#two ingoing roads, two outgoing roads
function two_two(γ_a_max, γ_b_max, γ_c_max, γ_d_max, α, β;
                 verbose = false, with_test = true)
    
    if with_test == false
        model = Model(with_optimizer(GLPK.Optimizer))
    
        @variable(model, 0. <= γ_a <= γ_a_max)
        @variable(model, 0. <= γ_b <= γ_b_max)
    
        @objective(model, Max, γ_a + γ_b)
    
        # A*γ_in ≤ γ_out_max with A = [α β ; 1-α 1-β]
        @constraint(model, α*γ_a + β*γ_b <= γ_c_max) 
        @constraint(model, (1-α)*γ_a + (1-β)*γ_b <= γ_d_max)
    
        JuMP.optimize!(model)
        sum_value = JuMP.objective_value(model)
        γ_a_value = JuMP.value(γ_a)
        γ_b_value = JuMP.value(γ_b)
    end
    
    # to use we must define gamma_a, gamma_b
    if with_test
        γ_a_value = 0.0
        γ_b_value = 0.0
        det= α*(1.0-β)-β*(1.0-α)
        γ_a = ((1.0-β)*γ_c_max-β*γ_d_max)/det
        γ_b = (-(1.0-α)*γ_c_max+α*γ_d_max)/det
        if γ_a <= γ_a_max && γ_b <= γ_b_max
            γ_a_value = γ_a
            γ_b_value = γ_b
        end
        if γ_a > γ_a_max && γ_b > γ_b_max
            γ_a_value = γ_a_max
            γ_b_value = γ_b_max
        end
        if γ_a > γ_a_max && γ_b <= γ_b_max
            if α < β
                γ_a_value = γ_a_max
                γ_b_value = min((γ_c_max - α*γ_a_max)/β , γ_b_max)
            else
                γ_a_value = γ_a_max
                γ_b_value = min( (γ_d_max - (1-α)*γ_a_max)/(1-β) , γ_b_max)
            end
        end
        if γ_a <= γ_a_max && γ_b > γ_b_max
            if α > β
                γ_b_value = γ_b_max
                γ_a_value = min( (γ_c_max - β*γ_b_max)/α , γ_a_max)
            else
                γ_b_value = γ_b_max
                γ_a_value = min( (γ_d_max - (1-β)*γ_b_max)/(1-α) , γ_a_max)
            end
        end
    end
    return [γ_a_value , γ_b_value]
end
    
#-------------------------------------------------------------------------------
# TODO : GENERIC for 1x1, 1x2, 2x1, 2x2 (maybe 1x3, 2x3, 3x3, 3x2, 3x1)
#-------------------------------------------------------------------------------

# function LinProg(self::Junction, f_in::Vector{Float64}, 
#                  f_in_max::Vector{Float64}, f_out_max::Vector{Float64} ; verbose=false)
    
#     #############################################################################
#     # JuMP
#     # An algebraic modeling language for Julia
#     # See http://github.com/JuliaOpt/JuMP.jl
#     #############################################################################
    
#     one = ones(eltype(f_in), size(f_in))
#     A = self.Jmat
#     f_out = A*f_in
#     model = Model(with_optimizer(GLPK.Optimizer))
    
#     @variable(model, f_in[i=1:self.Nin])
    
#     @constraint(model, [j=1:self.Nout], f_out[j] <= f_out_max[j])
#     @constraint(model, [i=1:self.Nin], 0. <= f_in[i] <=  f_in_max[i])
    
#     @objective(model, Max, sum(f_in))
    
#     # Over-saturation
#     if sum(f_in) > sum(f_out_max)
#         if self.Nin == 2 && self.Nout == 1
#             ### right-of-way parameter 'q' to ensure unicity
#             q = 0.32 # must belong to [0.30 ,0.35] (empirical)
#             @constraint(model, f_in[2] == ((1 - q)/q) * f_in[1] )
#         else 
#             println("Warning : Over-saturation in junction. Uniqueness not granted.") 
#         end  
#     end
    
#     if verbose 
#             print(model) 
#     end
    
#     JuMP.optimize!(model)
     
#     new_f_in = JuMP.value(f_in)
#     new_f_out = A*new_f_in
        
#     if verbose
#         println("Objective value : ", JuMP.objective_value(model))
#         println("Solution : ", new_f_in)
#     end
    
#     return [new_f_in, new_f_out]
# end