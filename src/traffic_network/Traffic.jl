export TrafficRoad, set_numflux, set_road
export reduction, diagnostics, minmod, bc_traffic, NoTrafficFlux
export get_fmax, outputs

mutable struct TrafficRoad <: SpatialDis

  road       :: Road
  numflux    :: Flux
  ndiags     :: Int64
  nvar       :: Int64
  ndof       :: Int64

  function TrafficRoad(ndiags, nvar, road)

      numflux   = NoTrafficFlux()
      ndof      = road.mh.Nc

      new( road, numflux, ndiags, nvar, ndof )
  end

end # class TrafficRoad

# ----- methods of class TrafficRoad -----

function set_numflux(self::TrafficRoad, numflux)
    self.numflux = numflux
end

function set_road(self::TrafficRoad, road::Road)
    self.road = road
    if (self.nvar != road.nvar)
        println(" ERROR : incompatibles nvar sizes (must be equals)")
    end
end

function get_fmax(self::TrafficRoad,r,dir)
    rmax  = self.road.rmax
    vmax  = self.road.vmax
    sig   = self.road.sigma(rmax)
    
    if dir == -1 # entrant
        fmax = (0 <= r <= sig) ? self.road.fluxphy(rmax,vmax,r) : self.road.fluxphy(rmax,vmax,sig)
    elseif dir == 1 # sortant
        fmax = (0 <= r <= sig) ? self.road.fluxphy(rmax,vmax,sig) : self.road.fluxphy(rmax,vmax,r) 
    end
    return fmax
end

function (self::TrafficRoad)() # generic_vf_op() is replaced by model()
    Fr  = zeros(Float64,self.nvar)
    Fl  = zeros(Float64,self.nvar)
 
    x  = self.road.mh.centers[1]
    h  = self.road.mh.dualareas[1]
    Fl[:] .= self.road.flux_bcl
    self.numflux(x,h,Fr,self.road.field[1,:],self.road.field[2,:])
    self.road.flux[1,:] .= .-(Fr-Fl)./self.road.mh.areas[1]

    ### spatial loop
    for i in 2:self.road.mh.Nc-1
        x = self.road.mh.centers[i]
        h = self.road.mh.dualareas[i]
        self.numflux(x,h,Fl,self.road.field[i-1,:],self.road.field[i,:])
        self.numflux(x,h,Fr,self.road.field[i,:],self.road.field[i+1,:])
        self.road.flux[i,:] .= .-(Fr-Fl)./self.road.mh.areas[i]
    end
    # -----
    x = self.road.mh.centers[end]
    h = self.road.mh.dualareas[end]
    self.numflux(x,h,Fl,self.road.field[end-1,:],self.road.field[end,:])
    Fr .= self.road.flux_bcr
    self.road.flux[end,:] .= .-(Fr-Fl)./self.road.mh.areas[end]
end

function reduction(self::TrafficRoad, function_reduced, init_res)
    res = init_res
     for i in 1:self.ndof
        res = function_reduced(self.road.field[i,:],res)
    end

    return res
end

function outputs(self::TrafficRoad,ti,mapping,form_diags)
    diags = zeros(self.ndiags)
    loc_diags = zeros(self.ndiags)
    fg = zeros(self.nvar)
    field = zeros(Float64,(self.ndof,self.nvar))
    for i in 1:self.ndof
        mapping(self.road.field[i,:],fg)
        field[i,:] .= fg[:]
        form_diags(field[i,:],loc_diags)
        diags[:] .+= self.road.mh.areas[i]*loc_diags[:]
    end
      [field, diags]
end

function diagnostics(self::TrafficRoad,ref,ti,f_ref,mapping,form_diags)
    diags = zeros(self.ndiags)
    loc_diags = zeros(self.ndiags)
    fg = zeros(self.nvar)
    fg_ref = zeros(self.nvar)
    field_ref = zeros(Float64,(self.ndof,self.nvar))
    field = zeros(Float64,(self.ndof,self.nvar))
    if ref == 0
       for i in 1:self.ndof
        mapping(self.road.field[i,:],fg)
        field[i,:] .= fg[:]
        form_diags(field[i,:],loc_diags)
        diags[:] .+= self.road.mh.areas[i]*loc_diags[:]
      end
      [field, diags]
    else
       for i in 1:self.ndof
        x = self.road.mh.centers[i]
        mapping(self.road.field[i,:],fg) # we canno use dorect field[:,i] why ??
        mapping(f_ref(x,ti),fg_ref)
        field[i,:] .= fg[:]
        field_ref[i,:] .= fg_ref[:]
        form_diags(field[i,:]-field_ref[i,:],loc_diags)
        diags[:]  .+= self.road.mh.areas[i]*loc_diags[:]
      end
      [field, field_ref, diags]
    end
end

struct NoTrafficFlux <: Flux
end

function (self::NoTrafficFlux)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    nothing
end
