export Network, init_Network, set_list_leaves, set_density_leaves
export Update_Junctions

mutable struct Network
    nb_roads       :: Int
    nb_junctions   :: Int
    nb_leaves      :: Int   
    solvers        :: Vector{TrafficRoad}
    graph          :: SimpleWeightedDiGraph # oriented weighted graph
    Junctions      :: Vector{Junction}
    Adj_mat        :: Array{Int64,2}
    list_leaves    :: Vector{Int64}
    density_leaves :: Vector{Float64}
    
    function Network(nb_r, nb_j, nb_s)
        nb_roads       = nb_r
        nb_junctions   = nb_j
        nb_leaves      = nb_s   
        solvers        = Vector{TrafficRoad}(undef, nb_r)
        Junctions      = Vector{Junction}(undef, nb_j)
        graph          = SimpleWeightedDiGraph(nb_j)
        Adj_mat        = zeros(Int64, nb_j, nb_j )
        list_leaves    = zeros(Int64, nb_s)
        density_leaves = zeros(Float64, nb_s)
        
      new(nb_roads, nb_junctions, nb_leaves, solvers, graph, Junctions, Adj_mat, list_leaves, density_leaves)
    end
end # class Network

# ----- methods of class Network -----

function set_list_leaves(self::Network, v ::Vector{Int64})
    self.list_leaves .= v
end   

function set_density_leaves(self::Network, v ::Vector{Float64})
    self.density_leaves .= v
end   

function init_Network(self::Network, L_t::Vector{Float64} ,Nx_t::Vector{Int}, 
                      rmax_t::Vector{Float64}, vmax_t::Vector{Float64}, nvar, 
                      fluxphy, dfluxphy, sigma)
    ndiags = 1
    for (k,e) in enumerate(edges(self.graph))

        # Construct Mesh
        mh = Mesh(L_t[k], Nx_t[k], nvar)
        mh(id_mesh)
        # Construct Road
        rd = Road(L_t[k], mh, rmax_t[k], vmax_t[k], nvar)
        set_fluxphy(rd, fluxphy)
        set_dfluxphy(rd, dfluxphy)
        set_sigma(rd, sigma)
        # Construct TrafficRoad
        self.solvers[k] = TrafficRoad(ndiags, nvar, rd)
        
        # Construct Junctions
        vL = src(e)
        vR = dst(e)
        ##### check if assigned, create if needed
        if ! isassigned(self.Junctions, vL) self.Junctions[vL] = Junction() end
        if ! isassigned(self.Junctions, vR) self.Junctions[vR] = Junction() end
        ##### push neighbors
        set_out_data(self.Junctions[vL], k )
        set_in_data(self.Junctions[vR], k )
        # Map edge to junctions
        
        self.Adj_mat[vL,vR] = k
    end
end

function Update_Junctions(self::Network)
    g = self.graph
    #println("---------------------")
    for v in vertices(g)
        Inc = inneighbors(g, v)
        Out = outneighbors(g, v)
        Inc_roads = [self.Adj_mat[idx_in, v] for idx_in in Inc]
        Out_roads = [self.Adj_mat[v, idx_out] for idx_out in Out]
        spaces = self.solvers
        if Base.length(Inc)*Base.length(Out) > 0 # i.e. not a leaf       
            # Get max fluxes 
            f_in_max = [get_fmax(spaces[idx_in],spaces[idx_in].road.field[end],-1) for idx_in in Inc_roads]
            f_out_max = [get_fmax(spaces[idx_out],spaces[idx_out].road.field[1],1) for idx_out in Out_roads]
            # Solve LP problem
            new_f_in, new_f_out = LinProg(self.Junctions[v], f_in_max, f_out_max)
            
            # Update boundaries conditions
            # CORRECTION ==> in flux gives right BC flux for incoming roads
            # CORRECTION ==> out flux gives left BC flux for outcoming roads
            for (k,idx_in) in enumerate(Inc_roads)
               rd_in = spaces[idx_in].road
               rd_in.flux_bcr[1] = new_f_in[k]
            end
            for (k,idx_out) in enumerate(Out_roads)
               rd_out = spaces[idx_out].road
               rd_out.flux_bcl[1] = new_f_out[k]
            end
          #  println("---------------------")
        else
            if Base.length(Inc) == 0
               f_in_max = [1.0]
               f_out_max = [1.0] 
               idx_out = Out_roads[1]
               index =0
               for (k,l) in enumerate(self.list_leaves)
                   if v == l
                       index = k
                       break      
                   end
               end     
               r = self.density_leaves[index]                
               f_in_max = [spaces[idx_out].road.fluxphy(spaces[idx_out].road.rmax,spaces[idx_out].road.vmax,r)]
               f_out_max = [get_fmax(spaces[idx_out],spaces[idx_out].road.field[1],1)]
                               
               new_f_in, new_f_out = LinProg(self.Junctions[v], f_in_max, f_out_max)
               spaces[idx_out].road.flux_bcl[1] = new_f_out[1]
            else    
               f_in_max = [1.0]
               f_out_max = [1.0] 
               idx_int = Inc_roads[1]
               index =0
               for (k,l) in enumerate(self.list_leaves)
                   if v == l
                       index = k
                       break      
                   end
               end    
               r = self.density_leaves[index]                
               f_in_max = [get_fmax(spaces[idx_int],spaces[idx_int].road.field[end],-1)]
               f_out_max = [spaces[idx_int].road.fluxphy(spaces[idx_int].road.rmax,spaces[idx_int].road.vmax,r)]
                  
               new_f_in, new_f_out = LinProg(self.Junctions[v], f_in_max, f_out_max)
               spaces[idx_int].road.flux_bcr[1] = new_f_in[1]
            end    
        end    
    end
end