export Road, initialization, set_fluxphy, set_dfluxphy, set_bc_road

mutable struct Road
    L           :: Float64
    mh          :: Mesh
    field       :: Array{Float64}   # density : ρ[i] = ρ_i
    rmax        :: Float64
    vmax        :: Float64
    flux        :: Array{Float64}   # VF : flux[i,:] = - ( fnum_{i+1/2} - fnum_{i-1/2} ) / Δxi
    fluxphy     :: Flux             # (map) ρ ↦ f(ρ)
    dfluxphy    :: Flux             # (map) ρ ↦ ∂f(ρ)
    sigma       :: Flux
    flux_bcl    :: Vector{Float64}  # boundary condition LEFT : f(ρ_L) = f_1
    flux_bcr    :: Vector{Float64}  # boundary condition RIGHT: f(ρ_R) = f_Nc
    nvar        :: Int64
    time_data   :: Array{Array,1}
    
    # constructors

    function Road(L, mh, rmax, vmax)

        flux_bcl  = zeros(Float64, nvar)
        flux_bcr  = zeros(Float64, nvar)
        field     = zeros(Float64, (mh.Nc, nvar))
        flux      = zeros(Float64, (mh.Nc, nvar))
        fluxphy   = NoFlux()
        dfluxphy  = NoFlux()
        sigma     = NoFlux()
        nvar      = 1
        time_data = []
        rmax      = rmax
        vmax      = vmax
        
        new(L, mh, field, rmax, flux, fluxphy, dfluxphy, sigma, flux_bcl, flux_bcr, nvar, time_data)
    end

    function Road(L, mh, rmax, vmax, nvar)

        flux_bcl  = zeros(Float64, nvar)
        flux_bcr  = zeros(Float64, nvar)
        field     = zeros(Float64, (mh.Nc, nvar))
        flux      = zeros(Float64, (mh.Nc, nvar))
        fluxphy   = NoFlux()
        dfluxphy  = NoFlux()
        sigma     = NoFlux()
        time_data = []
        rmax      = rmax
        vmax      = vmax
        
        new(L,mh,field,rmax,vmax,flux,fluxphy,dfluxphy, sigma, flux_bcl, flux_bcr, nvar, time_data)
    end

end # class Road

# methods of class Road

function initialization(self::Road, init_f)
    for i in 1:self.mh.Nc
        x = self.mh.centers[i]
        v = init_f(x,0.0)
        self.field[i,:] = v[:]
    end
end

function set_sigma(self::Road, sigma)
    self.sigma = sigma
end

function set_fluxphy(self::Road, fluxphy)
    self.fluxphy = fluxphy
end

function set_dfluxphy(self::Road, dfluxphy)
    self.dfluxphy = dfluxphy
end

function set_bc_road(self::Road, fL::Float64, fR::Float64)
    self.flux_bcl .= fL
    self.flux_bcr .= fR
end

function push_time_data(self::Road)
    rm = 0.
    vm = 0. # vitesse = flux / rho
    # integrale en espace ( \sum dx * val ) / Nx
    push!(self.time_data, [rm, vm])
end