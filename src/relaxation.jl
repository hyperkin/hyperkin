export relaxation, set_eq, set_omega_matrix, set_w, NullEq

struct NullEq <: Equilibrium
end

mutable struct relaxation

    nvar          :: Int64
    space         :: SpatialDis
    eq            :: Equilibrium
    omega         :: Float64
    omega_matrix  :: Equilibrium
    matrix_used   :: Int64

    function relaxation(n, space, o_value)
        nvar           = n

        eq             = NullEq()
        omega_matrix   = NullEq()
        omega          = o_value
        matrix_used    = 0

        new(n, space , eq, omega, omega_matrix, matrix_used)
    end

    function relaxation(n,space)
        nvar           = n

        eq             = NullEq()
        omega_matrix   = NullEq()
        omega          = 1.0
        matrix_used    = 1

        new(n, space , eq, omega, omega_matrix, matrix_used)
    end
end

function set_eq(self::relaxation, eq)
    self.eq = eq
end


function set_w(self::relaxation, w)
    self.omega = w
end

function set_omega_matrix(self::relaxation, matrix)
    self.matrix_used = 1
    self.omega_matrix = matrix
end

function (self::relaxation)()
    feq = zeros(self.nvar)
    product = zeros(self.nvar)
    if self.matrix_used ==0
         for i in 1:self.space.ndof
            self.eq(self.space.field[i,:],feq)
            self.space.field[i,:] .+= self.omega*(feq[:]-self.space.field[i,:])
        end
    else
         for i in 1:self.space.ndof
            self.eq(self.space.field[i,:],feq)
            x=set_dof(self.space,i)
            self.omega_matrix(x,self.space.field[i,:],feq[:]-self.space.field[i,:],product)
            self.space.field[i,:] .+= product[:]
        end
    end    
end
