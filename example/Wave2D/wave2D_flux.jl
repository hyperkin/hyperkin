

struct AcousticFlux <: Flux
    Lx :: Float64
    Ly :: Float64
end

function (self::AcousticFlux)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    
    pl = vC[1]
    pr = vR[1]
    uxl = vC[2]
    uxr = vR[2]
    uyl = vC[3]
    uyr = vR[3]
    
    unl = uxl*n[1]+uyl*n[2]
    unr = uxr*n[1]+uyr*n[2]

    data[1] = 0.5 *(unl+unr) - 0.5 * (pr-pl)
    data[2] = 0.5 *(pl+pr)*n[1] - 0.5 * (uxr-uxl)
    data[3] = 0.5 *(pl+pr)*n[2] - 0.5 * (uyr-uyl)
end

struct AcousticFlux2 <: Flux
    Lx :: Float64
    Ly :: Float64
end

function (self::AcousticFlux2)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    
    pl = vC[1]
    pr = vR[1]
    uxl = vC[2]
    uxr = vR[2]
    uyl = vC[3]
    uyr = vR[3]
    
    unl = uxl*n[1]+uyl*n[2]
    unr = uxr*n[1]+uyr*n[2]

    data[1] = 0.5 *(unl+unr) - 0.5 * (pr-pl)
    data[2] = 0.5 *(pl+pr)*n[1] - 0.5 * (unr-unl)*n[1]
    data[3] = 0.5 *(pl+pr)*n[2] - 0.5 * (unr-unl)*n[2]
end

struct wave_cfl <: Flux
end

function (self::wave_cfl)(x, v, res)
    r   =   v[1]
    lf      = 1.0
    max(res,lf)
end

struct l2_norm <: Flux
end

function (self::l2_norm)(v::Array{Float64}, data::Array{Float64})
    r, ux, uy  =  v[:] 
    data[1]  =   r*r
    data[2]  =   ux*ux + uy*uy
    nothing
end

struct id_wave_mapping <: Flux
end

function (self::id_wave_mapping)(v::Array{Float64}, data::Array{Float64})  
    r, ux, uy  =  v[:]
    data[1] = r
    data[2] = ux
    data[3] = uy
    nothing
end
