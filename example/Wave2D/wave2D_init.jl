abstract type InitFunction end

struct PressureWave <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
end

function (self::PressureWave)(x, t) 
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    x1x    = self.Lx/3.0
    x1y    = self.Ly/3.0
    x2x    = 2.0*self.Lx/3.0
    x2y    = 2.0*self.Ly/3.0
    sigma2 = 0.005*0.005
    sigma  = 0.005 
    norm2 = (x[1]-x0x)^2 + (x[2]-x0y)^2
    norm21 = (x[1]-x1x)^2 + (x[2]-x1y)^2
    norm22 = (x[1]-x2x)^2 + (x[2]-x2y)^2
    p     = 0.1 +  0.25*(1.0/(sigma*sqrt(pi)))*exp(-0.5*(norm2/sigma2))
    ##+  0.4*(1.0/(sigma*sqrt(pi)))*exp(-0.5*(norm21/sigma2))
    ##p =  p    +  0.15*(1.0/(sigma*sqrt(pi)))*exp(-0.5*(norm22/sigma2))
    ux    = 0.0
    uy    = 0.0
    [p, ux, uy]
end

struct Shock <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
end

function (self::Shock)(x, t) 
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    sigma2 = 0.05*0.05
    sigma  = 0.05 
    norm2 = (x[1]-x0x)^2 + (x[2]-x0y)^2
    p = 0.1
      ux    = 0.0
    uy    = 0.0
    if sqrt(norm2) < 0.1
        p     = 1.0
    end    
    [p, ux, uy]
end
