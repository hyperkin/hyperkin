struct LocalLaxD1Q3 <: Flux
    L :: Float64
    lambda_m  :: Float64
    lambda_0  :: Float64
    lambda_p  :: Float64
end

function (self::LocalLaxD1Q3)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    fml, f0l, fpl = vL
    fmr, f0r, fpr = vR
    

    data[1] = 0.5 * self.lambda_m*(fml+fmr) - 0.5 * abs(self.lambda_m) * (fmr-fml)
    data[2] = 0.5 * self.lambda_0*(f0l+f0r) - 0.5 * abs(self.lambda_0) * (f0r-f0l)
    data[3] = 0.5 * self.lambda_p*(fpl+fpr) - 0.5 * abs(self.lambda_p) * (fpr-fpl)
    
    nothing
end

struct d1q3_cfl <: Flux
    lambda_m :: Float64
    lambda_0 :: Float64
    lambda_p :: Float64
end

function (self::d1q3_cfl)(x,v, res)
    return max(abs(self.lambda_m),max(abs(self.lambda_p),abs(self.lambda_0)))
end

struct l2_norm <: Flux
end

function (self::l2_norm)(v::Vector{Float64},data::Vector{Float64})
    r, u   =  v
    data[1] = r*r
    data[2] = u*u
    nothing
end

struct eq_d1q3_burgers <: Equilibrium
    lambda_m  :: Float64
    lambda_0  :: Float64
    lambda_p  :: Float64
    alpha     :: Float64
    scheme    :: Integer
end

function (self::eq_d1q3_burgers)(v::Vector{Float64},data::Vector{Float64})
    lp   = self.lambda_p
    lm   = self.lambda_m
    l0   = self.lambda_0 
    fm, f0, fp   =  v
    r = fm + f0 + fp

    if self.scheme==1
        F_m=-lm/(lp-lm)*(0.5*r^2-lp*r)
        F_p=lp/(lp-lm)*(0.5*r^2-lm*r)
    elseif self.scheme==2
        if r<l0
            F_m=0.5*r^2-l0*r
            F_p=0.0
        else
            F_p=0.5*r^2-l0*r
            F_m=0.0
        end
    else
        F_m=0.25*r^2+self.alpha*r^3/(6.0*lm)
        F_p=0.25*r^2+self.alpha*r^3/(6.0*lp)
    end
        
    data[1] = -F_m/(l0-lm)
    data[2] = r-(F_p/(lp-l0) -F_m/(l0-lm)) 
    data[3] = F_p/(lp-l0)
    nothing
end


struct id_d1q3_mapping <: Flux
    lambda_m   :: Float64
    lambda_0   :: Float64
    lambda_p   :: Float64 
end

function (self::id_d1q3_mapping)(v::Vector{Float64},data::Vector{Float64})
    r   =   v[1] + v[2] + v[3]
    u   =   self.lambda_m*v[1] + self.lambda_0*v[2] + self.lambda_p*v[3]
    data[1] = r
    data[2] = u
end


struct ope_d1q3dg <: Operator

end

function (self::ope_d1q3dg)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.nvar*sp.ndof)
    
    for k in 1:sp.nvar
        sp.field[:,k] .= v[(k-1)*sp.ndof+1:k*sp.ndof]
    end   
    bc_neumann(sp)
    sp()
    for k in 1:sp.nvar
        res[(k-1)*sp.ndof+1:k*sp.ndof] .= sp.flux[:,k] ### The operator Dg is -Adx and the right side of ode gives is the same 
    end    
    return res
end   


struct vect_to_var_d1q3 <: Operator

end

function (self::vect_to_var_d1q3)(v :: Vector{Float64},sp :: SpatialDis)
    
     sp.field[:,1] .= v[1:sp.ndof] 
     sp.field[:,2] .= v[sp.ndof+1:2*sp.ndof]  
     sp.field[:,3] .= v[2*sp.ndof+1:end]  
end 

struct var_to_vect_d1q3 <: Operator

end

function (self::var_to_vect_d1q3)(sp :: SpatialDis,v :: Vector{Float64})
    
    v[1:sp.ndof] .=  sp.field[:,1]
    v[sp.ndof+1:2*sp.ndof]  .=  sp.field[:,2]
    v[2*sp.ndof+1:end] .= sp.field[:,3] 
end 

