abstract type InitFunction end


struct Gaussian_d1q3 <: InitFunction
    L       :: Float64
    lambda_m  :: Float64
    lambda_0  :: Float64
    lambda_p  :: Float64
    scheme    :: Integer
    alpha     :: Float64
end

function (self::Gaussian_d1q3)(x, t)
    lp   = self.lambda_p
    lm   = self.lambda_m
    l0   = self.lambda_0  
    sigma = 0.05
    x0    = 0.5*self.L
    r     = exp(-0.5*((x-x0)/sigma)^2)+0.01
    fr = 0.5*r*r
    
    if self.scheme==1
        F_m=-lm/(lp-lm)*(0.5*r^2-lp*r)
        F_p=lp/(lp-lm)*(0.5*r^2-lm*r)
    elseif self.scheme==2
        if r<l0
            F_m=0.5*r^2-l0*r
            F_p=0.0
        else
            F_p=0.5*r^2-l0*r
            F_m=0.0
        end
    else
        F_m=0.25*r^2+self.alpha*r^3/(6.0*lm)
        F_p=0.25*r^2+self.alpha*r^3/(6.0*lp)
    end
 
    [-F_m/(l0-lm), r-(F_p/(lp-l0) -F_m/(l0-lm)) ,F_p/(lp-l0)]
end

 

