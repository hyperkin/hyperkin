struct MinModLimiting <: Flux
end

function (self::MinModLimiting)(x,data::Vector{Float64}, vC::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64},d)
    rc, uc = vC  
    rl, ul = vL      
    rr, ur = vR 
    
    dr = 0.5*minmod(rc-rl,rr-rc)
    deltac = rc*max(-1.0,min(1.0,dr/rc))
    data[1] = rc + d*deltac

    du = 0.5*minmod(uc-ul,ur-uc)
    deltau = uc*max(-1.0,min(1.0,du/uc))
    data[2] = uc + d*deltau
    nothing
end


struct LocalLax <: Flux
    L :: Float64
    lambda :: Float64
end

function (self::LocalLax)(x,h,data::Vector{Float64}, vL::Vector{Float64},vR::Vector{Float64})
    l = self.lambda
    rl, ul = vL
    rr, ur = vR
    
    data[1] = 0.5 * (ul+ur) - 0.5 * l * (rr-rl)
    data[2] = 0.5 * (l*l*rl+l*l*rr) - 0.5 * l * (ur-ul)
    nothing
end

struct xinjin_cfl <: Flux
    lambda :: Float64
end

function (self::xinjin_cfl)(x,v, res)
    return self.lambda
end

struct l2_norm <: Flux
end

function (self::l2_norm)(v::Vector{Float64},data::Vector{Float64})
    r, u   =  v
    data[1] = r*r
    data[2] = u*u
    nothing
end

struct id_xj_mapping <: Flux
end

function (self::id_xj_mapping)(v::Vector{Float64},data::Vector{Float64})
    r, u  =  v
    data[1] = r
    data[2] = u
    nothing
end

struct eq_burgers <: Equilibrium
end

function (self::eq_burgers)(v::Vector{Float64},data::Vector{Float64})
    r, u   =  v
    data[1] = r
    data[2] = 0.5*r*r
    nothing
end