struct MinModLimiting <: Flux
    L :: Float64
end

function (self::MinModLimiting)(x,data::Vector{Float64},vC::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64},d)
    rc = vC[1]  
    rl = vL[1]      
    rr = vR[1]  
    
    dr = 0.5*minmod(rc-rl,rr-rc)
    deltac = rc*max(-1.0,min(1.0,dr/rc))
    rf = rc + d*deltac
    data[1]=rf
    nothing
end



struct LocalLax <: Flux
    L :: Float64
end

function (self::LocalLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})

    rl = vL[1]
    rr = vR[1]
    lf = max(abs(rr),abs(rl))
    data[1] = 0.5 * (0.5*rl*rl+0.5*rr*rr) - 0.5* lf * (rr-rl)
end

struct  burgers_cfl <: Flux
end

function (self::burgers_cfl)(x,v, res)
    r   =   v[1]
    lf      = abs(r)
    max(res,lf)
end

struct l1l2_norm <: Flux
end

function (self::l1l2_norm)(v::Array{Float64}, data::Array{Float64})
    r  =  v[1] # we cannot use direct v. why ?
    data[1]  =   abs(r)
    data[2]  =   r*r
    nothing
end

struct id_burgers_mapping <: Flux
end

function (self::id_burgers_mapping)(v::Array{Float64}, data::Array{Float64})  
    r  =  v[1]
    data[1] = r
    nothing
end



