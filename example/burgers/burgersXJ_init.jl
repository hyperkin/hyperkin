abstract type InitFunction end

struct gaussian <: InitFunction
    L     :: Float64
end

function (self::gaussian)(x, t)
    sigma = 0.05
    x0    = 0.5*self.L
    r     = exp(-0.5*((x-x0)/sigma)^2)+0.01

    [r,0.5*r*r]
end
