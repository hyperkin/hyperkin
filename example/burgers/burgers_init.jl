abstract type InitFunction end

struct Gaussian <: InitFunction
    L     :: Float64
end

function (self::Gaussian)(x, t)
    sigma = 0.05
    x0    = 0.5*self.L
    xr    = x
    r     = exp(-0.5*((xr-x0)/sigma)^2)+0.01

    [r]
end

struct Sinus <: InitFunction
    L     :: Float64
end

function (self::Sinus)(x, t)

    r     = sin(2.0*pi/L*x)

    [r]
end

struct Discontinuity <: InitFunction
    L     :: Float64
end

function (self::Discontinuity)(x, t)
    sigma = 0.05
    x0    = 0.5*self.L
    if x-x0 < 0.0
        r = 2.0
    else
        r = 1.0    
    end    

    [r]
end
