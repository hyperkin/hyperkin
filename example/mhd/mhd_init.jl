abstract type InitFunction end
struct Gaussianp <: InitFunction
    L      :: Float64
    gamma  :: Float64
    Bx     :: Float64    
end

function (self::Gaussianp)(x, t)
    g = self.gamma
    Bx = self.Bx
    sigma = 0.04
    x0    = 0.5*self.L
    r     = 1.0
    u     = 0.0
    u1    = 0.0
    u2    = 0.0
    p     = 0.1*exp(-0.5*((x-x0)/sigma)^2)+0.1
    b1    = 0.0
    b2    = 0.0
    
    E = p/(g-1.0) + 0.5*r*(u^2+u1^2+u2^2) + 0.5*(b1^2+b2^2+Bx^2)

    [r, r*u, r*u1, r*u2, E, b1, b2]
end

struct BrioWu <: InitFunction
    L      :: Float64
    gamma  :: Float64
    Bx     :: Float64    
end

function (self::BrioWu)(x, t)
    g = self.gamma
    Bx = self.Bx
    sigma = 0.04
    x0    = 0.5*self.L
    
    if x<x0
        r     = 1.0
        u     = 0.0
        u1    = 0.0
        u2    = 0.0
        p     = 1.0
        b1    = 1.0
        b2    = 0.0
    else
        r     = 0.125
        u     = 0.0
        u1    = 0.0
        u2    = 0.0
        p     = 0.1
        b1    = -1.0
        b2    = 0.0
    end
    
    E = p/(g-1.0) + 0.5*r*(u^2+u1^2+u2^2) + 0.5*(b1^2+b2^2+Bx^2)

    [r, r*u, r*u1, r*u2, E, b1, b2]
end
