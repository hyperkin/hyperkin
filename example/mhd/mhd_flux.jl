struct PrimLimiting <: Flux
    L      :: Float64
    gamma  :: Float64
    Bx     :: Float64    
end

function (self::PrimLimiting)(x,data::Vector{Float64},vC::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64},d)
    Bx = self.Bx
    g = self.gamma
    
    rc, qc, q1c, q2c, ec, b1c, b2c = vC  
    uc = qc/rc
    u1c = q1c/rc
    u2c = q2c/rc
    pc = (g-1.0) * (ec- 0.5 * (qc*uc + q1c*u1c + q1c*u1c) -0.5 * (b1c^2+b2c^2+Bx^2)) 
    
    rl, ql, q1l, q2l, el, b1l, b2l = vL    
    ul = ql/rl
    u1l = q1l/rl
    u2l = q2l/rl
    pl = (g-1.0) * (el- 0.5 * (ql*ul + q1l*u1l + q2l*u2l) -0.5 * (b1l^2+b2l^2+Bx^2)) 
    
    rr, qr, q1r, q2r, er, b1r, b2r = vR    
    ur = qr/rr
    u1r = q1r/rr
    u2r = q2r/rr
    pr = (g-1.0) * (er- 0.5 * (qr*ur + q1r*u1r + q2r*u2r) -0.5 * (b1r^2+b2r^2+Bx^2)) 
    
    dr = 0.5*minmod(rc-rl,rr-rc)
    du = 0.5*minmod(uc-ul,ur-uc)
    du1 = 0.5*minmod(u1c-u1l,u1r-u1c)
    du2 = 0.5*minmod(u2c-u2l,u2r-u2c)
    dp = 0.5*minmod(pc-pl,pr-pc)
    db1 = 0.5*minmod(b1c-b1l,b1r-b1c)
    db2 = 0.5*minmod(b2c-b2l,b2r-b2c)
    
    deltac = rc*max(-1.0,min(1.0,dr/rc))
    pcc = (gamma-1.0)*rc*(1.0+2.0*deltac*deltac/(rc*rc))
    deltau = sign(du)*sqrt(min(du*du,pc/pcc))
    deltau1 = sign(du1)*sqrt(min(du1*du1,pc/pcc))
    deltau2 = sign(du2)*sqrt(min(du2*du2,pc/pcc))
    deltap = pc*max(-1.0,min(1.0,dp/pc)) 
    deltab1 = sign(db1)*sqrt(min(db1*db1,pc/pcc))
    deltab2 = sign(db2)*sqrt(min(db2*db2,pc/pcc))
    
    rf = rc + d*deltac
    uf = uc + d*deltau
    u1f = u1c + d*deltau1
    u2f = u2c + d*deltau2
    pf = pc + d*deltap
    b1f = b1c + d*deltab1
    b2f = b2c + d*deltab2
    ef = pf/(g-1.0) + 0.5*rf*(uf^2+u1f^2+u2f^2) + 0.5*(b1f^2+b2f^2+Bx^2)
    
    data[1] = rf
    data[2] = rf*uf
    data[3] = rf*u1f
    data[4] = rf*u2f
    data[5] = ef
    data[6] = b1f
    data[7] = b2f
    nothing
end


struct LocalLax <: Flux
    L      :: Float64
    gamma  :: Float64
    Bx     :: Float64  
end

function (self::LocalLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    rl, ql, q1l, q2l, el, b1l, b2l = vL
    rr, qr, q1r, q2r, er, b1r, b2r = vR
    g = self.gamma
    Bx = self.Bx
    
    ul = ql/rl
    ur = qr/rr
    u1l = q1l/rl
    u1r = q1r/rr
    u2l = q2l/rl
    u2r = q2r/rr
    
    pl = (g-1.0)*(el- 0.5 * (ql*ul + q1l*u1l + q2l*u2l) -0.5 * (b1l^2+b2l^2+Bx^2)) 
    pr = (g-1.0)*(er- 0.5 * (qr*ur + q1r*u1r + q2r*u2r) -0.5 * (b1r^2+b2r^2+Bx^2))
    
    axl = abs(Bx)/sqrt(rl)
    axr = abs(Bx)/sqrt(rr)
    apl = sqrt((b1l^2+b2l^2)/rl) 
    apr = sqrt((b1r^2+b2r^2)/rr) 
    al = sqrt(axl*axl+apl*apl)
    ar = sqrt(axr*axr+apr*apr)
    cl = sqrt(g*pl/rl)
    cr = sqrt(g*pr/rr)
    Bnl = 0.5*(b1l^2+b2l^2)
    Bnr = 0.5*(b1r^2+b2r^2)
    
    lml = 0.5*(cl^2+al^2)-0.5*sqrt((cl^2+al^2)^2-4.0*axl^2*cl^2)
    lmr = 0.5*(cr^2+ar^2)-0.5*sqrt((cr^2+ar^2)^2-4.0*axr^2*cr^2)
    lpl = 0.5*(cl^2+al^2)+0.5*sqrt((cl^2+al^2)^2-4.0*axl^2*cl^2)
    lpr = 0.5*(cr^2+ar^2)+0.5*sqrt((cr^2+ar^2)^2-4.0*axr^2*cr^2)
    
    wavel = [ul,ul-axl,ul+axl,ul-sqrt(lml),ul+sqrt(lml),ul-sqrt(lpl),ul+sqrt(lpl)]
    waver = [ur,ur-axr,ur+axr,ur-sqrt(lmr),ur+sqrt(lmr),ur-sqrt(lpr),ur+sqrt(lpr)]
    Sl = maximum(wavel)
    Sr = maximum(waver)
    S = max(Sl,Sr)   
    
    
    center_r = ql + qr
    center_ru = ul*ql + pl + 0.5*(b1l^2+b2l^2-Bx^2) + ur*qr + pr + 0.5*(b1r^2+b2r^2-Bx^2)
    center_ru1 = ul*q1l - Bx*b1l + ur*q1r - Bx*b1r
    center_ru2 = ul*q2l - Bx*b2l + ur*q2r - Bx*b2r
    center_e  = (el+pl+Bnl-0.5*Bx^2)*ul - Bx*(u1l*b1l+u2l*b2l) + (er+pr+Bnr-0.5*Bx^2)*ur - Bx*(u1r*b1r+u2r*b2r)
    center_b1 = ul*b1l - Bx*u1l + ur*b1r - Bx*u1r
    center_b2 = ul*b2l - Bx*u2l + ur*b2r - Bx*u2r
    
    data[1] = 0.5 * center_r - 0.5 * abs(S) * (rr-rl)
    data[2] = 0.5 * center_ru - 0.5 * abs(S) * (qr-ql)
    data[3] = 0.5 * center_ru1 - 0.5 * abs(S) * (q1r-q1l)
    data[4] = 0.5 * center_ru2 - 0.5 * abs(S) * (q2r-q2l)
    data[5] = 0.5 * center_e - 0.5 * abs(S) * (er-el)
    data[6] = 0.5 * center_b1 - 0.5 * abs(S) * (b1r-b1l)
    data[7] = 0.5 * center_b2 - 0.5 * abs(S) * (b2r-b2l)
    nothing
end

struct HLLD <: Flux
    L      :: Float64
    gamma  :: Float64
    Bx     :: Float64  
end

function (self::HLLD)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    rl, ql, q1l, q2l, el, b1l, b2l = vL
    rr, qr, q1r, q2r, er, b1r, b2r = vR
    g = self.gamma
    Bx = self.Bx
    
    ul = ql/rl
    ur = qr/rr
    u1l = q1l/rl
    u1r = q1r/rr
    u2l = q2l/rl
    u2r = q2r/rr
    
    pl = (g-1.0)*(el- 0.5 * (ql*ul + q1l*u1l + q2l*u2l) -0.5 * (b1l^2+b2l^2+Bx^2)) 
    pr = (g-1.0)*(er- 0.5 * (qr*ur + q1r*u1r + q2r*u2r) -0.5 * (b1r^2+b2r^2+Bx^2))
    ptl = pl + 0.5 * (b1l^2+b2l^2+Bx^2)
    ptr = pr + 0.5 * (b1r^2+b2r^2+Bx^2)
    
    al = sqrt((b1l^2+b2l^2+Bx^2)/rl) 
    ar = sqrt((b1r^2+b2r^2+Bx^2)/rr)
    axl = sqrt(Bx^2/rl) 
    axr = sqrt(Bx^2/rr)
    cl = sqrt(g*pl/rl)
    cr = sqrt(g*pr/rr)
    cfl = 0.5*(cl^2+al^2)+0.5*sqrt((cl^2+al^2)^2-4.0*axl^2*cl^2)
    cfr = 0.5*(cr^2+ar^2)+0.5*sqrt((cr^2+ar^2)^2-4.0*axr^2*cr^2)
    
    ubl = ul*Bx+u1l*b1l+u2l*b2l
    ubr = ur*Bx+u1r*b1r+u2r*b2r
    
    SL = min(ul,ur)-max(cfl,cfr)
    SR = max(ul,ur)+max(cfl,cfr)
    SM = ((SR-ur)*qr-(SL-ul)*ql -(ptr-ptl))/((SR-ur)*rr-(SL-ul)*rl)
    ustar = SM
    ptstar = ((SR-ur)*rr*ptl-(SL-ul)*rl*ptr +rr*rl*(SR-ur)*(SL-ul)*(ur-ul))/((SR-ur)*rr-(SL-ul)*rl)
    
    rlstar = rl*(SL-ul)/(SL-ustar)
    rrstar = rr*(SR-ur)/(SR-ustar)
    if abs((SL-ustar)*(SL-ul)-al^2) < 0.000000001
        b1lstar = b1l
        b2lstar = b2l
    else
        b1lstar = b1l*((SL-ul)^2-al^2)/((SL-ustar)*(SL-ul)-al^2)
        b2lstar = b2l*((SL-ul)^2-al^2)/((SL-ustar)*(SL-ul)-al^2)
    end    
    if abs((SR-ustar)*(SR-ur)-ar^2) < 0.000000001
        b1rstar = b1r
        b2rstar = b2r
    else
        b1rstar = b1r*((SR-ur)^2-ar^2)/((SR-ustar)*(SR-ur)-ar^2)
        b2rstar = b2r*((SR-ur)^2-ar^2)/((SR-ustar)*(SR-ur)-ar^2)
    end
    u1lstar = u1l-Bx*b1l/rl*(ustar-ul)/((SL-ustar)*(SL-ul)-al^2)
    u1rstar = u1r-Bx*b1r/rr*(ustar-ur)/((SR-ustar)*(SR-ur)-ar^2)
    u2lstar = u2l-Bx*b2l/rl*(ustar-ul)/((SL-ustar)*(SL-ul)-al^2)
    u2rstar = u2r-Bx*b2r/rr*(ustar-ur)/((SR-ustar)*(SR-ur)-ar^2)
    
    ublstar= ustar*Bx+u1lstar*b1lstar+u2lstar*b2lstar
    elstar = el*(SL-ul)/(SL-ustar)+(ptstar*ustar-ptl*ul-Bx*(ublstar-ubl))/(SR-ustar)
    ubrstar= ustar*Bx+u1rstar*b1rstar+u2rstar*b2rstar
    erstar = er*(SR-ur)/(SR-ustar)+(ptstar*ustar-ptr*ur-Bx*(ubrstar-ubr))/(SR-ustar)
    
    SLstar=ustar-sqrt(Bx^2/rlstar)
    SRstar=ustar+sqrt(Bx^2/rrstar)
    
    rc = sqrt(rrstar)+sqrt(rlstar)
    rc2= sqrt(rrstar*rlstar)
    rlstar2 = rlstar
    rrstar2 = rrstar
    ustar2  = ustar
    u1star2 = (sqrt(rrstar)*u1rstar+sqrt(rlstar)*u1lstar+sign(Bx)*(b1rstar-b1lstar))/rc
    u2star2 = (sqrt(rrstar)*u2rstar+sqrt(rlstar)*u2lstar+sign(Bx)*(b2rstar-b2lstar))/rc
    b1star2 = (sqrt(rrstar)*b1rstar+sqrt(rlstar)*b1lstar+sign(Bx)*rc2*(u1rstar-u1lstar))/rc
    b2star2 = (sqrt(rrstar)*b2rstar+sqrt(rlstar)*b2lstar+sign(Bx)*rc2*(u2rstar-u2lstar))/rc
    ubstar2 = ustar*Bx+u1star2*b1star2+u2star2*b2star2
    elstar2 = elstar +sign(Bx)*sqrt(rlstar)*(ubstar2-ublstar)
    erstar2 = erstar -sign(Bx)*sqrt(rrstar)*(ubstar2-ubrstar)
    
    if SL >0
        data[1] = ql
        data[2] = ql*ul+ptl-Bx^2
        data[3] = q1l*ul - Bx*b1l
        data[4] = q2l*ul - Bx*b2l
        data[5] = (el+ptl)*ul-ubl*Bx
        data[6] = ul*b1l-Bx*u1l
        data[7] = ul*b2l-Bx*u2l
    elseif SL <=0 && SLstar > 0    
        data[1] = ql + SL*(rlstar-rl)
        data[2] = ql*ul+ptl-Bx^2 + SL*(rlstar*ustar-ql)
        data[3] = q1l*ul - Bx*b1l + SL*(rlstar*u1lstar-q1l)
        data[4] = q2l*ul - Bx*b2l + SL*(rlstar*u2lstar-q2l)
        data[5] = (el+ptl)*ul-ubl*Bx + SL*(elstar-el)
        data[6] = ul*b1l-Bx*u1l + SL*(b1lstar-b1l)
        data[7] = ul*b2l-Bx*u2l + SL*(b2lstar-b2l)
    elseif SLstar <=0 && SM > 0 
        data[1] = ql + SLstar*rlstar2 -(SLstar-SL)*rlstar-SL*rl
        data[2] = ql*ul+ptl-Bx^2 + SLstar*rlstar2*ustar2 -(SLstar-SL)*rlstar*ustar-SL*ql
        data[3] = q1l*ul - Bx*b1l + SLstar*rlstar2*u1star2 -(SLstar-SL)*rlstar*u1lstar-SL*q1l
        data[4] = q2l*ul - Bx*b2l + SLstar*rlstar2*u2star2 -(SLstar-SL)*rlstar*u2lstar-SL*q2l
        data[5] = (el+ptl)*ul-ubl*Bx + SLstar*elstar2 -(SLstar-SL)*elstar-SL*el
        data[6] = ul*b1l-Bx*u1l + SLstar*b1star2 -(SLstar-SL)*b1lstar-SL*b1l
        data[7] = ul*b2l-Bx*u2l + SLstar*b2star2 -(SLstar-SL)*b2lstar-SL*b2l
    elseif SM <=0 && SRstar > 0 
        data[1] = qr + SRstar*rrstar2 -(SRstar-SR)*rrstar-SR*rr
        data[2] = qr*ur+ptr-Bx^2 + SRstar*rrstar2*ustar2 -(SRstar-SR)*rrstar*ustar-SR*qr
        data[3] = q1r*ur - Bx*b1r + SRstar*rrstar2*u1star2 -(SRstar-SR)*rrstar*u1rstar-SR*q1r
        data[4] = q2r*ur - Bx*b2r + SRstar*rrstar2*u2star2 -(SRstar-SR)*rrstar*u2rstar-SR*q2r
        data[5] = (er+ptr)*ur-ubr*Bx + SRstar*erstar2 -(SRstar-SR)*erstar-SR*er
        data[6] = ur*b1r-Bx*u1r + SRstar*b1star2 -(SRstar-SR)*b1rstar-SR*b1r
        data[7] = ur*b2r-Bx*u2r + SRstar*b2star2 -(SRstar-SR)*b2rstar-SR*b2r
    elseif SRstar <=0 && SR > 0
        data[1] = qr + SR*(rrstar-rr)
        data[2] = qr*ur+ptr-Bx^2 + SR*(rrstar*ustar-qr)
        data[3] = q1r*ur - Bx*b1r + SR*(rrstar*u1rstar-q1r)
        data[4] = q2r*ur - Bx*b2r + SR*(rrstar*u2rstar-q2r)
        data[5] = (er+ptr)*ur-ubr*Bx + SR*(erstar-er)
        data[6] = ur*b1r-Bx*u1r + SR*(b1rstar-b1r)
        data[7] = ur*b2r-Bx*u2r + SR*(b2rstar-b2r)
    else    
        data[1] = qr
        data[2] = qr*ur+ptr-Bx^2
        data[3] = q1r*ur - Bx*b1r
        data[4] = q2r*ur - Bx*b2r
        data[5] = (er+ptr)*ur-ubr*Bx
        data[6] = ur*b1r-Bx*u1r
        data[7] = ur*b2r-Bx*u2r
    end    
    nothing
end

####### not work
struct HLLC <: Flux
    L      :: Float64
    gamma  :: Float64
    Bx     :: Float64  
end

function (self::HLLC)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    rl, ql, q1l, q2l, el, b1l, b2l = vL
    rr, qr, q1r, q2r, er, b1r, b2r = vR
    g = self.gamma
    Bx = self.Bx
    
    ul = ql/rl
    ur = qr/rr
    u1l = q1l/rl
    u1r = q1r/rr
    u2l = q2l/rl
    u2r = q2r/rr
    
    pl = (g-1.0)*(el- 0.5 * (ql*ul + q1l*u1l + q2l*u2l) -0.5 * (b1l^2+b2l^2+Bx^2)) 
    pr = (g-1.0)*(er- 0.5 * (qr*ur + q1r*u1r + q2r*u2r) -0.5 * (b1r^2+b2r^2+Bx^2))
    ptl = pl + 0.5 * (b1l^2+b2l^2+Bx^2)
    ptr = pr + 0.5 * (b1r^2+b2r^2+Bx^2)
    
    al = sqrt((b1l^2+b2l^2+Bx^2)/rl) 
    ar = sqrt((b1r^2+b2r^2+Bx^2)/rr)
    axl = sqrt(Bx^2/rl) 
    axr = sqrt(Bx^2/rr)
    cl = sqrt(g*pl/rl)
    cr = sqrt(g*pr/rr)
    cfl = 0.5*(cl^2+al^2)+0.5*sqrt((cl^2+al^2)^2-4.0*axl^2*cl^2)
    cfr = 0.5*(cr^2+ar^2)+0.5*sqrt((cr^2+ar^2)^2-4.0*axr^2*cr^2)
    
    ubl = ul*Bx+u1l*b1l+u2l*b2l
    ubr = ur*Bx+u1r*b1r+u2r*b2r
    
    SL = min(ul,ur)-max(cfl,cfr)
    SR = max(ul,ur)+max(cfl,cfr)
    ustar = ((SR-ur)*qr-(SL-ul)*ql -(ptr-ptl))/((SR-ur)*rr-(SL-ul)*rl)
    
    b1lstar = (SL-ul-Bx^2/(rl*(SL-ul)))/(SL-ustar-Bx^2/(rl*(SL-ul)))*b1l
    b1rstar = (SR-ur-Bx^2/(rr*(SR-ur)))/(SR-ustar-Bx^2/(rr*(SR-ur)))*b1r
    b2lstar = (SL-ul-Bx^2/(rl*(SL-ul)))/(SL-ustar-Bx^2/(rl*(SL-ul)))*b2l
    b2rstar = (SR-ur-Bx^2/(rr*(SR-ur)))/(SR-ustar-Bx^2/(rr*(SR-ur)))*b2r
    plstar = rl*(SL-ul)*(ustar-ul)+pl
    prstar = rr*(SR-ur)*(ustar-ur)+pr
    
    rlstar = rl*(SL-ul)/(SL-ustar)
    rrstar = rr*(SR-ur)/(SR-ustar)
    qlstar = rlstar*ustar
    qrstar = rrstar*ustar
    q1lstar = q1l*(SL-ul)/(SL-ustar)-Bx*(b1lstar-b1l)/(SL-ustar)
    q1rstar = q1r*(SR-ur)/(SR-ustar)-Bx*(b1rstar-b1r)/(SR-ustar)
    q2lstar = q2l*(SL-ul)/(SL-ustar)-Bx*(b2lstar-b1l)/(SL-ustar)
    q2rstar = q2r*(SR-ur)/(SR-ustar)-Bx*(b2rstar-b1r)/(SR-ustar)
    
    b1hll = (SR*b1r-SL*b1l-(ur*b1r-Bx*u1r -(ul*b1l-Bx*u1l)))/(SR-SL)
    b2hll = (SR*b2r-SL*b2l-(ur*b2r-Bx*u2r -(ul*b2l-Bx*u2l)))/(SR-SL)
    
    uhll = (SR*ur-SL*ul-(qr*ur+ptr- ql*ul-ptl))/(SR-SL)
    u1hll = (SR*u1r-SL*u1l-(q1r*ur - Bx*b1r -(q1l*ul - Bx*b1l)))/(SR-SL)
    u2hll = (SR*u2r-SL*u2l-(q2r*ur - Bx*b2r -(q2l*ul - Bx*b2l)))/(SR-SL)
    
    ubhll = uhll*Bx+b1hll*u1hll+b2hll*u2hll
    
    elstar = el*(SL-ul)/(SL-ustar) +(plstar*ustar-pl*ul - Bx*(ubhll-ubl))/(SL-ustar)
    erstar = er*(SR-ur)/(SR-ustar) +(prstar*ustar-pr*ur - Bx*(ubhll-ubr))/(SR-ustar)
    
    if SL >0
        data[1] = ql
        data[2] = ql*ul+ptl-Bx^2
        data[3] = q1l*ul - Bx*b1l
        data[4] = q2l*ul - Bx*b2l
        data[5] = (el+ptl)*ul-ubl*Bx
        data[6] = ul*b1l-Bx*u1l
        data[7] = ul*b2l-Bx*u2l
    elseif SL <=0 && ustar > 0    
        data[1] = ql + SL*(rlstar-rl)
        data[2] = ql*ul+ptl-Bx^2 + SL*(qlstar-ql)
        data[3] = q1l*ul - Bx*b1l + SL*(q1lstar-q1l)
        data[4] = q2l*ul - Bx*b2l + SL*(q2lstar-q2l)
        data[5] = (el+ptl)*ul-ubl*Bx + SL*(elstar-el)
        data[6] = ul*b1l-Bx*u1l + SL*(b1lstar-b1l)
        data[7] = ul*b2l-Bx*u2l + SL*(b2lstar-b2l)
    elseif ustar <=0 && SR > 0
        data[1] = qr + SR*(rrstar-rr)
        data[2] = qr*ur+ptr-Bx^2 + SR*(qrstar-qr)
        data[3] = q1r*ur - Bx*b1r + SR*(q1rstar-q1r)
        data[4] = q2r*ur - Bx*b2r + SR*(q2rstar-q2r)
        data[5] = (er+ptr)*ur-ubr*Bx + SR*(erstar-er)
        data[6] = ur*b1r-Bx*u1r + SR*(b1rstar-b1r)
        data[7] = ur*b2r-Bx*u2r + SR*(b2rstar-b2r)
    else    
        data[1] = qr
        data[2] = qr*ur+ptr-Bx^2
        data[3] = q1r*ur - Bx*b1r
        data[4] = q2r*ur - Bx*b2r
        data[5] = (er+ptr)*ur-ubr*Bx
        data[6] = ur*b1r-Bx*u1r
        data[7] = ur*b2r-Bx*u2r
    end    
    nothing
end


struct DiffDis <: Flux
    L :: Float64
    gamma  :: Float64
    Bx :: Float64
    kappa :: Float64
    nu :: Float64
    eta :: Float64
    di :: Float64
end

function (self::DiffDis)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    rl, ql, q1l, q2l, el, b1l, b2l = vL
    rr, qr, q1r, q2r, er, b1r, b2r = vR
    wave = zeros(Float64,7)
    g = self.gamma
    Bx = self.Bx
    
    ul = ql/rl
    ur = qr/rr
    u1l = q1l/rl
    u1r = q1r/rr
    u2l = q2l/rl
    u2r = q2r/rr
    
    pl = (g-1.0)*(el- 0.5 * (ql*ul + q1l*u1l + q2l*u2l) -0.5 * (b1l^2+b2l^2+Bx^2)) 
    pr = (g-1.0)*(er- 0.5 * (qr*ur + q1r*u1r + q2r*u2r) -0.5 * (b1r^2+b2r^2+Bx^2))
    tl = pl/rl
    tr = pr/rr
    
    rc = 0.5*(rl+rr)
    uc = 0.5*(ul+ur)
    u1c = 0.5*(u1l+u1r)
    u2c = 0.5*(u2l+u2r)
    b1c = 0.5*(b1l+b1r)
    b2c = 0.5*(b2l+b2r)
    
    j1 = -(b2r-b2l)/h
    j2 = (b1r-b1l)/h
    
    visu = -self.nu*rc*(ur-ul)/h
    visu1 = -self.nu*rc*(u1r-u1l)/h
    visu2 = -self.nu*rc*(u2r-u2l)/h
    vist = -self.kappa*(tr-tl)/h
    heatu = self.nu*rc*(uc*(u1r-u1l)+u1c*(u1r-u1l)+u2c*(u2r-u2l))/h
    heatb = self.eta*(-j2*b1c+j1*b2c)
    visb1 = -self.eta*(b1r-b1l)/h
    visb2 = -self.eta*(b2r-b2l)/h
    
    disb1 = self.di/rc*Bx*(b2r-b2l)/h
    disb2 = -self.di/rc*Bx*(b1r-b1l)/h
    dise = disb1*b1c+disb2*b2c

    data[1] = 0.0
    data[2] = visu
    data[3] = visu1
    data[4] = visu2
    data[5] = vist + heatu + heatb + dise
    data[6] = visb1 + disb1
    data[7] = visb2 + disb2
    nothing
end


struct mhd_cfl <: Flux
    L      :: Float64
    gamma  :: Float64
    Bx     :: Float64  
end

function (self::mhd_cfl)(x, v, res)
    r, q, q1, q2, e, b1, b2 = v
    wave = zeros(Float64,7)
    g = self.gamma
    Bx = self.Bx
    
    u = q/r
    u1 = q1/r
    u2 = q2/r
    
    p = (g-1.0)*(e- 0.5 * (q*u + q1*u1 + q2*u2) -0.5 * (b1^2+b2^2+Bx^2)) 
    ax = abs(Bx)/sqrt(r)
    ap = sqrt((b1^2+b2^2)/r) 
    a = sqrt(ax*ax+ap*ap)
    c = sqrt(g*p/r)
    
    lm = 0.5*(c^2+a^2)-0.5*sqrt((c^2+a^2)^2-4.0*ax^2*c^2)
    lp = 0.5*(c^2+a^2)+0.5*sqrt((c^2+a^2)^2-4.0*ax^2*c^2)
    
    wave = [u,u-ax,u+ax,u-sqrt(lm),u+sqrt(lm),u-sqrt(lp),u+sqrt(lp)]
    S = maximum(wave)
    max(res,abs(S))
end

struct l2norm <: Flux
    L      :: Float64
    gamma  :: Float64
    Bx     :: Float64  
end

function (self::l2norm)(v::Vector{Float64}, data::Vector{Float64})
    r, u, u1, u2, p, b1, b2   =   v
    data[1] =r
    data[2] = u*u+u1*u1*u2*u2
    data[3] = p*p
    data[4] = b1*b1+b2*b2
    nothing
end

struct Primitive_mapping <: Flux
    L      :: Float64
    gamma  :: Float64
    Bx     :: Float64 
end

function (self::Primitive_mapping)(v::Vector{Float64}, data::Vector{Float64})
    r, q, q1, q2, e, b1, b2 = v
    g = self.gamma
    Bx = self.Bx
    
    u = q/r
    u1 = q1/r
    u2 = q2/r
    p = (g-1.0)*(e- 0.5 * (q*u + q1*u1 + q2*u2) -0.5 * (b1^2+b2^2+Bx^2)) 
    data[1] =r
    data[2] = u
    data[3] = u1
    data[4] = u2
    data[5] = p
    data[6] = b1
    data[7] = b2
    nothing  
end


  