function AnisoMatrix_d2qn(xloc,yloc,rho,type_ani)
    A=zeros(2,2)
    if type_ani==1
        A[1,1]=1.0
        A[2,2]=1.0
    elseif type_ani==2
        A[1,1]=abs(rho)^2
        A[2,2]=abs(rho)^2
    elseif type_ani==3
        r=sqrt(xloc^2+yloc^2)
        bx=-yloc/r
        by=xloc/r
        A[1,1]=bx*bx+ 0.00001   ; A[1,2]=bx*by
        A[2,1]=bx*by; A[2,2]=by*by+ 0.00001   
   elseif type_ani==4 
       bx=1.0
       by=0.5
       A[1,1]=bx*bx + 0.00001; A[1,2]=bx*by
       A[2,1]=bx*by; A[2,2]=by*by + 0.00001      
    else
       bx=1.0
       by=0.5
       A[1,1]=rho^(5.0/2.0)*bx*bx/0.1+1.0-bx*bx; A[1,2]=rho^(5.0/2.0)*bx*by/0.1-bx*by
       A[2,1]=rho^(5.0/2.0)*bx*by/0.1-bx*by; A[2,2]=rho^(5.0/2.0)*by*by/0.1+1.0-by*by     
    end   
    return A
end


function construct_vel_space(nv)
   if nv==4
       vel=[[0.0,-1.0],[-1.0,0.0],[0.0,1.0],[1.0,0.0]]
   elseif nv==5
       vel=[[0.0,0.0],[0,-1.0],[-1,0],[0,1.0],[1.0,0.0]]
   else nv==9
       vel=[[0,0],[0,-1.0],[-1.0,-1.0],[-1,0],[-1.0,1.0],[0.0,1.0],[1.0,1.0],[1.0,0.0],[1.0,-1.0]]
   end
   return vel
end
        

struct LocalLax_d2qn <: Flux
    lambda :: Float64
    eps    :: Float64
    vel    :: Vector{Vector{Float64}} 
end

function (self::LocalLax_d2qn)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    l = self.lambda
    eps = self.eps

    for i in 1:length(self.vel)
        data[i] =0.5 * -l/eps*(dot(self.vel[i],n)*(vC[i]+vR[i])) - 0.5 * l/eps * abs(dot(self.vel[i],n))*(vR[i]-vC[i])
    end
    nothing
end


struct Centered_d2qn <: Flux
    lambda :: Float64
    eps    :: Float64
    vel    :: Vector{Vector{Float64}} 
end

function (self::Centered_d2qn)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    l = self.lambda
    eps = self.eps

    for i in 1:length(self.vel)
        data[i] =0.5 * -l/eps*(dot(self.vel[i],n)*(vC[i]+vR[i]))
    end
    nothing
end


struct l2_norm <: Flux
end

function (self::l2_norm)(v::Vector{Float64},data::Vector{Float64})
    r, u1, u2   =  v
    data[1] = r*r
    nothing
end



struct eq_d2qn_diff <: Equilibrium
    lambda :: Float64
    vel    :: Vector{Vector{Float64}} 
end

function (self::eq_d2qn_diff)(v::Vector{Float64},data::Vector{Float64})
    l = self.lambda

    r=0.0
    for i in 1:length(self.vel)
        r=r+v[i]
    end
    for i in 1:length(self.vel)   
        data[i] = r/length(self.vel) 
    end
    nothing
end


struct omega_d2qn_diff <: Equilibrium
    lambda :: Float64
    type_ani   :: Integer 
    dt :: Float64
    vel    :: Vector{Vector{Float64}} 
    s      :: Float64     
end

function (self::omega_d2qn_diff)(x,f::Vector{Float64},v::Vector{Float64},data::Vector{Float64})
     #### TOOOO DOOOO
    l = self.lambda
    nv=length(self.vel)
    if nv==4 
       alpha = 0.5*l*l
    elseif nv==5
        alpha=2.0/5.0*l*l
        else nv==9
        alpha=6.0/9.0*l*l
    end
    
    dt= self.dt
    rho=0.0
    for i in 1:nv
        rho=rho+f[i]
    end
    D=AnisoMatrix_d2qn(x[1],x[2],rho,self.type_ani)
    InvD=inv(D)    
    
    eps = sqrt(dt)
    theta=0.5
    
    R= zeros(nv,nv); P= zeros(nv,nv); L= zeros(nv,nv); B=zeros(nv,nv)
    if length(self.vel) >4
        R=Diagonal([alpha for i in 1:length(self.vel)])
    elseif length(self.vel)==4
        P[1,1]=1.0;  P[1,2]=1.0;   P[1,3]=1.0;  P[1,4]=1.0
        P[2,1]=0.0;  P[2,2]=-1.0;   P[2,3]=0.0;  P[2,4]=1.0
        P[3,1]=-1.0;  P[3,2]=0.0;   P[3,3]=1.0;  P[3,4]=0.0
        P[4,1]=1.0;  P[4,2]=-1.0;   P[4,3]=1.0;  P[4,4]=-1.0
        
        L[1,1]=0.0;  L[1,2]=0.0;   L[1,3]=0.0;  L[1,4]=0.0
        L[2,1]=0.0;  L[2,2]=alpha*InvD[1,1];   L[2,3]=alpha*InvD[1,2];  L[2,4]=0.0
        L[3,1]=0.0;  L[3,2]=alpha*InvD[2,1];   L[3,3]=alpha*InvD[2,2];  L[3,4]=0.0
        L[4,1]=0.0;  L[4,2]=0.0;   L[4,3]=0.0;  L[4,4]=self.s
        R=inv(P)*(L*P)
    end
    B=Diagonal([eps^2.0 for i in 1:length(self.vel)])+theta*dt.*R     
    Omega=inv(B)*(dt.*R) 
    res=Omega*v
    for i in 1:length(self.vel)
        data[i]=res[i]
    end

    nothing
end




struct d2qn_mapping <: Flux
    lambda :: Float64
    vel    :: Vector{Vector{Float64}} 
end

function (self::d2qn_mapping)(v::Vector{Float64},data::Vector{Float64})
    r=0.0
    u=[0.0,0.0]
    for i in 1:length(self.vel)
        r   =   r+v[i]
        u   =   u + (self.lambda*v[i]).*self.vel[i]
    end
    data[1] = r
    data[2] = u[1]
    data[3] = u[2]
    nothing
end



struct ope_d2qnVF <: Operator

end

function (self::ope_d2qnVF)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.nvar*sp.ndof)
    
    for k in 1:sp.nvar
        sp.field[:,k] .= v[(k-1)*sp.ndof+1:k*sp.ndof]
    end   
    bc_neumann(sp)
    sp()
    for k in 1:sp.nvar
        res[(k-1)*sp.ndof+1:k*sp.ndof] .= sp.flux[:,k] ### The operator Dg is -Adx and the right side of ode gives is the same 
    end    
    return res
end   




function d2qn_fields_to_xn(nv, sp :: SpatialDis,v :: Vector{Float64})
    k=1
    @simd for i in 1:nv
        @simd for j in 1:sp.ndof
            v[k] = sp.field[j,i]   
            k=k+1
        end
    end
end    

function d2qn_xn_to_fields(nv, sp :: SpatialDis,v :: Vector{Float64})
    k=1
    @simd for i in 1:nv
        @simd for j in 1:sp.ndof
            sp.field[j,i]  = v[k] 
            k=k+1
        end
    end
end  






