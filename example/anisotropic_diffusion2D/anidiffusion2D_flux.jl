function AnisoMatrix(xloc,yloc,rho,type_ani)
    A=zeros(2,2)
    if type_ani==1
        A[1,1]=1.0
        A[2,2]=1.0
    elseif type_ani==2
        A[1,1]=abs(rho)^2
        A[2,2]=abs(rho)^2
    elseif type_ani==3
        r=sqrt(xloc^2+yloc^2)
        bx=-yloc/r
        by=xloc/r
        A[1,1]=bx*bx+0.0001; A[1,2]=bx*by
        A[2,1]=bx*by; A[2,2]=by*by+0.00001
   elseif type_ani==4 
       bx=1.0
       by=0.5
       A[1,1]=bx*bx+0.00001; A[1,2]=bx*by
       A[2,1]=bx*by; A[2,2]=by*by    +0.00001   
    else
       bx=1.0
       by=0.5
       A[1,1]=rho^(5.0/2.0)*bx*bx/0.1+1.0-bx*bx; A[1,2]=rho^(5.0/2.0)*bx*by/0.1-bx*by
       A[2,1]=rho^(5.0/2.0)*bx*by/0.1-bx*by; A[2,2]=rho^(5.0/2.0)*by*by/0.1+1.0-by*by     
    end   
    return A
end


struct AniDiff <: Flux
    Lx         :: Float64
    Ly         :: Float64
    type_ani   :: Integer 
    
end

function (self::AniDiff)(x,h,data::Vector{Float64},vC::Vector{Float64},tv::Vector{Vector{Float64}},tl::Vector{Float64},tn::Vector{Vector{Float64}})

    r_c = vC[1]
    #[i-(self.Nx+2),i-(self.Nx+2)-1,i-1,i+(self.Nx+2)-1,i+(self.Nx+2),i+(self.Nx+2)+1,i+1,i-(self.Nx+2)+1]  
    rloc_d,rloc_dl,rloc_l,rloc_ul,rloc_u,rloc_ur,rloc_r,rloc_dr=tv[:,1]
    
    r_d=rloc_d[1]; r_dl= rloc_dl[1]; r_l = rloc_l[1]
    r_ul=rloc_ul[1]; r_u =rloc_u[1]; r_ur= rloc_ur[1]
    r_r= rloc_r[1]; r_dr=rloc_dr[1]

    dx_ur=((r_ur+r_r)-(r_u+r_c))/(2.0*sqrt(h))
    dy_ur=((r_ur+r_u)-(r_r+r_c))/(2.0*sqrt(h))
    
    dx_ul=((r_u+r_c)-(r_ul+r_l))/(2.0*sqrt(h))
    dy_ul=((r_ul+r_u)-(r_l+r_c))/(2.0*sqrt(h))
    
    dx_dr=((r_dr+r_r)-(r_d+r_c))/(2.0*sqrt(h))
    dy_dr=((r_r+r_c)-(r_dr+r_d))/(2.0*sqrt(h))
    
    dx_dl=((r_d+r_c)-(r_dl+r_l))/(2.0*sqrt(h))
    dy_dl=((r_l+r_c)-(r_dl+r_d))/(2.0*sqrt(h))
    
    xloc=x[1]+sqrt(h/2.0); yloc=x[2]+sqrt(h/2.0)
    rho=0.25*(r_c+r_u+r_r+r_ur)
    A=AnisoMatrix(xloc,yloc,rho,type_ani)
    q_ur=A*[dx_ur,dy_ur]
    
    xloc=x[1]-sqrt(h/2.0); yloc=x[2]+sqrt(h/2.0)
    rho=0.25*(r_c+r_u+r_l+r_ul)
    A=AnisoMatrix(xloc,yloc,rho,type_ani)
    q_ul=A*[dx_ul,dy_ul]
    
    xloc=x[1]+sqrt(h/2.0); yloc=x[2]-sqrt(h/2.0)
    rho=0.25*(r_c+r_d+r_r+r_dr)
    A=AnisoMatrix(xloc,yloc,rho,type_ani)
    q_dr=A*[dx_dr,dy_dr]
    
    xloc=x[1]-sqrt(h/2.0); yloc=x[2]-sqrt(h/2.0)
    rho=0.25*(r_c+r_d+r_l+r_dl)
    A=AnisoMatrix(xloc,yloc,rho,type_ani)
    q_dl=A*[dx_dl,dy_dl]
    
    data[1]=-0.5*(tl[1]*dot((q_dl+q_dr),tn[1])+tl[2]*dot((q_dl+q_ul),tn[2])+tl[3]*dot((q_ul+q_ur),tn[3])+tl[4]*dot((q_ur+q_dr),tn[4])) 
       
end

struct anidiff_cfl <: Flux
    type_ani    :: Integer 
end

function (self::anidiff_cfl)(x, v, res)

    A=AnisoMatrix(x[1],x[2],v[1],self.type_ani)
    r   =   v[1]
    if type_ani==2
        lf  = 1.0
    elseif type_ani==5
        lf = 0.5
    else    
        lf  = sqrt(A[1,1]^2+A[1,2]^2+A[2,1]^2+A[2,2]^2)
    end
    max(res,lf)
end

struct l1l2_norm <: Flux
end

function (self::l1l2_norm)(v::Array{Float64}, data::Array{Float64})
    r  =  v[1]
    data[1]  =   r*r
    nothing
end

struct id_anidiff_mapping <: Flux
end

function (self::id_anidiff_mapping)(v::Array{Float64}, data::Array{Float64})  
    r  =  v[1]
    data[1] = r
    nothing
end



