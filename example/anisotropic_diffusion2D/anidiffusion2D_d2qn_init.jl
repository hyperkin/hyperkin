abstract type InitFunction end

struct solfond_d2qn <: InitFunction
     Lx        :: Float64
     Ly        :: Float64
     type_ani  :: Integer
     vel       :: Vector{Vector{Float64}}
end

function (self::solfond_d2qn)(x, t) 
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    r=0.0
    if type_ani< 3
        t0=0.01
        norm2 = (x[1]-x0x)^2+(x[2]-x0y)^2
        r = 0.2+(1.0/(4.0*pi*(t+t0)))*exp(-norm2/(4.0*(t0+t)))
    elseif type_ani==3
         r=0.1+10*exp(-((x[1]-0.6)^2+x[2]^2)/0.02)
    else
        norm2=(x[1])^2+(x[2])^2
        if sqrt(norm2)<pi/5.0
            r=1.0+3.0*exp(-2.0*norm2)
        else
            r=1.0
        end
    end
    [ r*(1.0/length(self.vel)) for i in 1:length(self.vel)]
end
