export GreshoVortexBar

abstract type InitFunction end

struct GreshoVortexBar <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
     type_p :: Float64
     Mach   :: Float64
     gamma  :: Float64
end

function (self::GreshoVortexBar)(x, t)
    
    x0 = zeros(Float64,2)
    x0[1]    = self.Lx/2.0
    x0[2]    = self.Ly/2.0
    norm2 = (x[1]-x0[1])^2 + (x[2]-x0[2])^2
    M     = self.Mach
    gamma = self.gamma
    
    pc= 1.0/(M*M*gamma)
    c= sqrt(pc)
    
    ep_x = -(x[2]-x0[2])/sqrt(norm2)
    ep_y = (x[1]-x0[1])/sqrt(norm2)
    
    if sqrt(norm2) <0.2  
        ux    = ep_x * 5*sqrt(norm2)
        uy    = ep_y * 5*sqrt(norm2)
        r     = 1 + 25.0/2.0*norm2/pc    
    elseif sqrt(norm2) <0.4
        ux    = ep_x * (2.0-5.0*sqrt(norm2))
        uy    = ep_y * (2.0-5.0*sqrt(norm2))
        r     = 1 + (4.0*log(5.0*sqrt(norm2))+4-20*sqrt(norm2)+ 25.0/2.0*norm2)/pc           
    else 
        ux    = 0.0
        uy    = 0.0
        r     = 1 + (4.0*log(2.0) -2.0)/pc    
    end  
    p=pc*r   
    [r, r*ux, r*uy, r*ux, r*uy, r*ux*ux+p, r*ux*uy, r*uy*ux, r*uy*uy+p]
end

