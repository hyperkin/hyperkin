export XinJin_EulerBar_Eq,XinJin_EulerBar_CFL,Mach_EulerBar_mapping
export l2norm_EulerBar, ope1_BaroXJ, ope2_BaroXJ, schur_BaroXJ, BaroXJ_fields_to_xn, BaroXJd_xn_to_fields 


mutable struct XinJin_EulerBar_Eq <: Equilibrium
    type_p :: Float64
    Mach   :: Float64
    gamma  :: Float64
end

function (self::XinJin_EulerBar_Eq)(v::Vector{Float64},data::Vector{Float64})
    type_p = self.type_p
    gamma  = self.gamma
    M      = self.Mach
    r, qx, qy, wr_x, wr_y, wux_x, wux_y, wuy_x, wuy_y = v
    ux       = qx/r
    uy       = qy/r
    
    if type_p==1
        p=r/(M*M*gamma)
    else 
        p=r^(gamma)/(M*M)
    end
     
    data[1] = r
    data[2] = qx
    data[3] = qy
    
    data[4] = qx
    data[5] = qy
    
    data[6] = qx*ux+p
    data[7] = qx*uy
    data[8] = qy*ux
    data[9] = qy*uy+p
end


mutable struct XinJin_EulerBar_CFL <: Flux
    type_p :: Float64
    Mach   :: Float64
    gamma  :: Float64
end

function (self::XinJin_EulerBar_CFL)(v, res)
    type_p = self.type_p
    gamma  = self.gamma
    M      = self.Mach

    c=sqrt(1.0/(M*M*gamma))

    max(res,c)
end

struct Mach_EulerBar_mapping <: Flux
    type_p :: Float64
    Mach   :: Float64
    gamma  :: Float64
end

function (self::Mach_EulerBar_mapping)(v::Array{Float64}, data::Array{Float64}) 
    type_p = self.type_p
    gamma  = self.gamma
    M      = self.Mach
    r, qx, qy, wr_x, wr_y, wux_x, wux_y, wuy_x, wuy_y = v
    ux = qx/r
    uy = qy/r
    p = r/(M*M*gamma)
    c = sqrt(p/r)
    data[1] = sqrt(ux*ux+uy*uy)/c
    data[2] = ux
    data[3] = uy
    data[4] = r
    nothing
end



struct l2norm_EulerBar <: Flux
end

function (self::l2norm_EulerBar)(v::Vector{Float64},data::Vector{Float64})
    u2, ux, uy ,r = v
    data[1] = r*r
    data[2] = ux*ux+uy*uy
    nothing
end





mutable struct ope1_BaroXJ <: Operator
    bc    :: Int64
    la    :: Float64
end


function (self::ope1_BaroXJ )(sp :: SpatialDis,v :: Vector{Float64})
    # v contains, v1, v2, w1,w2, y1, y2
   res = zeros(Float64, 3*sp.ndof) 
   la = self.la
   
   @inbounds for i in 1:sp.ndof
        if  sp.mh.labels[i] >=0
            tnei=neighbors(sp.mh,i)
            div_v1 = - (v[tnei[4]]-v[tnei[2]])/(2.0*sp.mh.hx) 
            div_v2 = - (v[sp.ndof+tnei[3]]-v[sp.ndof+tnei[1]])/(2.0*sp.mh.hy) 
            res[i] = div_v1 + div_v2 
            div_w1 = - (v[2*sp.ndof+tnei[4]]-v[2*sp.ndof+tnei[2]])/(2.0*sp.mh.hx) 
            div_w2 = - (v[3*sp.ndof+tnei[3]]-v[3*sp.ndof+tnei[1]])/(2.0*sp.mh.hy) 
            res[sp.ndof+i] = div_w1 + div_w2 
            div_y1 = - (v[4*sp.ndof+tnei[4]]-v[4*sp.ndof+tnei[2]])/(2.0*sp.mh.hx) 
            div_y2 = - (v[5*sp.ndof+tnei[3]]-v[5*sp.ndof+tnei[1]])/(2.0*sp.mh.hy) 
            res[2*sp.ndof+i] = div_y1 + div_y2 
        else
            res[i] = 0.0
            res[sp.ndof+i] = 0.0
            res[2*sp.ndof+i] = 0.0
        end
    end  
   return res
end   

mutable struct ope2_BaroXJ  <: Operator
    bc    :: Int64
    la    :: Float64
end
 

function (self::ope2_BaroXJ )(sp :: SpatialDis,v :: Vector{Float64})
   # v contains, rho, rhou1, rhou2
   res = zeros(Float64, 6*sp.ndof) 
   la = self.la
    
   @inbounds for i in 1:sp.ndof
       if  sp.mh.labels[i] >=0
           tnei=neighbors(sp.mh,i)
           gradx_r = - la*la * (v[tnei[4]]-v[tnei[2]])/(2.0*sp.mh.hx) 
           grady_r = - la*la * (v[tnei[3]]-v[tnei[1]])/(2.0*sp.mh.hy) 
           res[i] = gradx_r
           res[i+sp.ndof] = grady_r
           gradx_ru1 = - la*la * (v[sp.ndof+tnei[4]]-v[sp.ndof+tnei[2]])/(2.0*sp.mh.hx) 
           grady_ru1 = - la*la * (v[sp.ndof+tnei[3]]-v[sp.ndof+tnei[1]])/(2.0*sp.mh.hy) 
           res[i+2*sp.ndof] = gradx_ru1
           res[i+3*sp.ndof] = grady_ru1
           gradx_ru2 = - la*la * (v[2*sp.ndof+tnei[4]]-v[2*sp.ndof+tnei[2]])/(2.0*sp.mh.hx) 
           grady_ru2 = - la*la * (v[2*sp.ndof+tnei[3]]-v[2*sp.ndof+tnei[1]])/(2.0*sp.mh.hy) 
           res[i+4*sp.ndof] = gradx_ru2
           res[i+5*sp.ndof] = grady_ru2
       else
           res[i] = 0.0
           res[sp.ndof+i] = 0.0
           res[2*sp.ndof+i] = 0.0
           res[3*sp.ndof+i] = 0.0
           res[4*sp.ndof+i] = 0.0
           res[5*sp.ndof+i] = 0.0
       end
   end    
   return res
end   




mutable struct schur_BaroXJ  <: Operator
    bc    :: Int64
    la    :: Float64
end

function (self::schur_BaroXJ)(sp :: SpatialDis,v :: Vector{Float64})
    # v contains, pi 
   res = zeros(Float64, 3*sp.ndof) 
    
   for i in 1:sp.ndof
       if  sp.mh.labels[i] >= 0
           tnei=neighbors(sp.mh,i)
           v1, v2, v3, v4 = v[tnei[1]], v[tnei[2]], v[tnei[3]], v[tnei[4]]
           if sp.mh.labels[i] == 2 v2 = v[i] end
           if sp.mh.labels[i] == 4 v4 = v[i] end
           lapx_r = self.la^2 * ((v4-v[i])-(v[i]-v2))/(sp.mh.hx^2)
           lapy_r = self.la^2 * ((v3-v[i])-(v[i]-v1))/(sp.mh.hy^2)
           res[i] = lapx_r + lapy_r 
            
           v1, v2, v3, v4 = v[sp.ndof+tnei[1]], v[sp.ndof+tnei[2]], v[sp.ndof+tnei[3]], v[sp.ndof+tnei[4]]
           if sp.mh.labels[i] == 2 v2 = v[sp.ndof+i] end
           if sp.mh.labels[i] == 4 v4 = v[sp.ndof+i] end 
           lapx_ru1 = self.la^2 * ((v4-v[sp.ndof+i])-(v[sp.ndof+i]-v2))/(sp.mh.hx^2)
           lapy_ru1 = self.la^2 * ((v3-v[sp.ndof+i])-(v[sp.ndof+i]-v1))/(sp.mh.hy^2)       
           res[sp.ndof+i] = lapx_ru1 + lapy_ru1 
            
           v1, v2, v3, v4 = v[2*sp.ndof+tnei[1]], v[2*sp.ndof+tnei[2]], v[2*sp.ndof+tnei[3]], v[2*sp.ndof+tnei[4]]
           if sp.mh.labels[i] == 2 v2 = v[2*sp.ndof+i] end
           if sp.mh.labels[i] == 4 v4 = v[2*sp.ndof+i] end 
           lapx_ru2 = self.la^2 * ((v4-v[2*sp.ndof+i])-(v[2*sp.ndof+i]-v2))/(sp.mh.hx^2)
           lapy_ru2 = self.la^2 * ((v3-v[2*sp.ndof+i])-(v[2*sp.ndof+i]-v1))/(sp.mh.hy^2)       
           res[2*sp.ndof+i] = lapx_ru2 + lapy_ru2  
         
       else
           res[i] = 0.0
           res[sp.ndof+i] = 0.0
           res[2*sp.ndof+i] = 0.0
       end
   end  
   return res
end   


function BaroXJ_fields_to_xn(sp :: SpatialDis,v :: Vector{Float64})
    #v contains , rho, ru1, ru2, v1, v2, w1, w2, y1, y2 
    @simd for i in 1:sp.ndof
        v[i] = sp.field[i,1]
        v[sp.ndof+i] = sp.field[i,2]
        v[2*sp.ndof+i] = sp.field[i,3]
        v[3*sp.ndof+i] = sp.field[i,4]
        v[4*sp.ndof+i] = sp.field[i,5]
        v[5*sp.ndof+i] = sp.field[i,6]
        v[6*sp.ndof+i] = sp.field[i,7]
        v[7*sp.ndof+i] = sp.field[i,8]
        v[8*sp.ndof+i] = sp.field[i,9]
    end
end    

function BaroXJ_xn_to_fields(sp :: SpatialDis,v :: Vector{Float64})
    @simd for i in 1:sp.ndof
        sp.field[i,1] = v[i]
        sp.field[i,2] = v[sp.ndof+i]
        sp.field[i,3] = v[2*sp.ndof+i]
        sp.field[i,4] = v[3*sp.ndof+i]
        sp.field[i,5] = v[4*sp.ndof+i]
        sp.field[i,6] = v[5*sp.ndof+i]
        sp.field[i,7] = v[6*sp.ndof+i]
        sp.field[i,8] = v[7*sp.ndof+i]
        sp.field[i,9] = v[8*sp.ndof+i]
    end
end  






