struct SEIR_flux <: Flux
    alpha :: Float64
    beta  :: Float64
    gamma :: Float64
    nu    :: Float64
    eta   :: Float64
end

function (self::SEIR_flux)(t,data::Vector{Float64},v::Vector{Float64})
    s, e, i, r = v
    data[1] = -self.beta*s*i/(s+e+i+r) + self.eta*(s+e+i+r) - self.nu*s
    data[2] = self.beta*s*i/(s+e+i+r) - self.alpha*e - self.nu*e
    data[3] = self.alpha*e - self.gamma*i - self.nu*i
    data[4] = self.gamma*i - self.nu*r
end

struct fbetat <: TimeSource
end
function (self::fbetat)(t)
    if t<15
       beta=0.5500
    elseif t<37    
       beta = 0.5944
    else    
       beta=0.5500
    end   
    return beta
end

struct falphat <: TimeSource
    active :: Int64
end
function (self::falphat)(t)
    if t<48
       alpha=0.0
    elseif t<54    
       alpha = active*0.4249 
    else    
       alpha= active*0.8478
    end   
    return alpha
end

struct fFt <: TimeSource
end
function (self::fFt)(t)
    if t<15
       F=10.0
    else    
       F=0.0
    end   
    return F
end

struct fnut <: TimeSource
end
function (self::fnut)(t)
    if t<15
       nu=0.0
    elseif t<37    
       nu = 0.0205
    else    
       nu=0.0
    end   
    return nu
end

struct SEIRmod_covid19_flux <: Flux
    alpha  :: TimeSource
    beta   :: TimeSource
    gamma  :: Float64
    sigma  :: Float64
    nu     :: TimeSource
    lambda :: Float64
    kappa  :: Float64
    F      :: TimeSource
    dd     :: Float64
end

function (self::SEIRmod_covid19_flux)(t,data::Vector{Float64},v::Vector{Float64})
    s, e, i, r, n, d, c= v
    
    b = self.beta(t)*(1.0-self.alpha(t))*(1-d/n)^kappa
    
    data[1] = -self.beta(t)*self.F(t)*s/n-b*s*i/n - self.nu(t)*s
    data[2] = self.beta(t)*self.F(t)*s/n + b*s*i/n  - self.sigma*e - self.nu(t)*e
    data[3] = self.sigma*e - self.gamma*i - self.nu(t)*i
    data[4] = self.gamma*i - self.nu(t)*r
    data[5] = - self.nu(t)*n
    data[6] = self.dd*self.gamma*i-self.lambda*d
    data[7] = self.sigma*e
   
end

    
struct l1_normI <: Flux
end

function (self::l1_normI)(v::Array{Float64}, data::Array{Float64})
    s, e, i  =  v[1:3]
    data[1]  =   abs(i)
    nothing
end

struct SEIR_mapping <: Flux
end

function (self::SEIR_mapping)(v::Array{Float64}, data::Array{Float64})  
    s, e, i ,r  =  v
    data[1] = s
    data[2] = e
    data[3] = i
    data[4] = r
    data[5] = s+e+i+r
    nothing
end
struct SEIRmod_covid19_mapping <: Flux
end

function (self::SEIRmod_covid19_mapping)(v::Array{Float64}, data::Array{Float64})  
    data[:] .= v[:]
    nothing
end


