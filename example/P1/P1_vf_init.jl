abstract type InitFunction end

struct Gaussian <: InitFunction
    L     :: Float64
    eps     :: Float64
    sigma   :: Float64
end

function (self::Gaussian)(x, t)
    t0 = 0.002
    x0    = 0.5*self.L
    u     = 0.0
    if self.sigma>0
        sig = self.sigma
    end
    k=1.0/sig
    p     = (1.0/sqrt(4.0*pi*k*(t+t0)))*exp(-(x-x0)^2/(4.0*k*(t+t0)))

    [p,u,sig]
end

struct GaussianWave <: InitFunction
    L     :: Float64
    eps     :: Float64
    sigma   :: Float64
end

function (self::GaussianWave)(x, t)
    t0 = 0.01
    x0     = 0.5*self.L
    xp     = (x-1.0/eps*t)
    xm     = (x+1.0/eps*t)
    p_p_u  = (1.0/(2.0*pi*t0))*exp(-0.5*(xp-x0)^2/(2.0*t0))
    p_m_u  = (1.0/(2.0*pi*t0))*exp(-0.5*(xm-x0)^2/(2.0*t0))
    p= 0.5*(p_p_u+p_m_u)
    u= 0.5*(p_p_u-p_m_u)
    sigma = 0.0

    [p,u,sigma]
end


struct Steady_state1 <: InitFunction
    L     :: Float64
    eps     :: Float64
    sigma   :: Float64
end

function (self::Steady_state1)(x, t)
    if self.sigma>0
        sig = self.sigma
    end
    u = 2.0 
    p = -sig/self.eps * u * x +1.0

    [p,u,sig]
end

struct Steady_state2 <: InitFunction
    L     :: Float64
    eps     :: Float64
    sigma   :: Float64
end

function (self::Steady_state2)(x, t)
    t0 = 0.002
    x0    = 0.5*self.L
    sig = sin(2.0*pi*x/L)
 
    u = 2.0 
    p = (1.0/(2.0*pi/L))*cos(2.0*pi*x/L)/eps*u 
    [p,u,sig]
end


struct UniformSol <: InitFunction
    L     :: Float64
    eps     :: Float64
    sigma   :: Float64
end

function (self::UniformSol)(x, t)
    # eps <0.075
    L= self.L 
    eps= self.eps
    sig= self.sigma 
    l1 = -sig*(sqrt(1.0-(eps^2/sig^2)*4.0*pi^2*L^2) +1.0)/(2.0*eps*eps)    
    l2 = sig*(sqrt(1.0-(eps^2/sig^2)*4.0*pi^2*L^2) -1.0)/(2.0*eps*eps)   
    alpha=l2/(l2-l1)*exp(l1*t)-l1/(l2-l1)*exp(l2*t)
    alpha_prime=l2*l1/(l2-l1)*(exp(l1*t)-exp(l2*t))
    p= alpha*cos(L*pi*x)+eps*eps/sig*alpha_prime*cos(L*pi*x)
    u= eps/sig*alpha*L*pi*sin(L*pi*x)   
    
    [p,u,sig]
end