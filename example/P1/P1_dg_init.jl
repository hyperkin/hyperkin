abstract type InitFunction end
struct Gaussian <: InitFunction
    L     :: Float64
    c     :: Float64
end

function (self::Gaussian)(x, t)
    sigma = 0.05
    x0    = 0.5*self.L
    xr    = x-self.c*t
    xl    = x+self.c*t
    rpu   = exp(-0.5*((xr-x0)/sigma)^2)+0.01
    rmu   = exp(-0.5*((xl-x0)/sigma)^2)+0.01
    r     = 0.5*(rpu+rmu)
    u     = 0.5*(rpu-rmu)

    [r, u]
end
