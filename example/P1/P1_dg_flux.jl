struct LocalLax <: Flux
    L :: Float64
    c :: Float64
end

function (self::LocalLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})

    c = self.c
    rl, ul = vL
    rr, ur = vR
    data[1] = 0.5 * (c*ul+c*ur) - 0.5 * abs(c) * (rr-rl)
    data[2] = 0.5 * (c*rl+c*rr) - 0.5 * abs(c) * (ur-ul)
end

struct wave_cfl <: Flux
    c :: Float64
end

function (self::wave_cfl)(x, v, res)
    c = self.c

    r, u   =   v
    lf      = abs(c)
    max(res,c)
end

struct l2_norm <: Flux
end

function (self::l2_norm)(v::Vector{Float64}, data::Vector{Float64})
    r, u   =   v
    data[1] = r*r
    data[2] = u*u 
end

struct id_wave_mapping <: Flux
end

function (self::id_wave_mapping)(v::Vector{Float64}, data::Vector{Float64})
    r, u   =   v
    data[1] = r
    data[2] = u
end