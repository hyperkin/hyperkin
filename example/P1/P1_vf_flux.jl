struct LocalLax <: Flux
    L :: Float64
    eps :: Float64
end

function (self::LocalLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})

    eps = self.eps
    pl,ul,sl = vL
    pr,ur,sr = vR
    data[1] = 0.5/eps * (ul+ur) - 0.5 /eps* (pr-pl)
    data[2] = 0.5/eps * (pl+pr) - 0.5 /eps* (ur-ul)
    data[3] = 0.0
end

struct JinLevermore <: Flux
    L :: Float64
    eps :: Float64
end

function (self::JinLevermore)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})

    eps = self.eps
    pl,ul,sl = vL
    pr,ur,sr = vR
    s=0.5*(sl+sr)
    M = 2.0*eps/(2.0*eps+h*s)
    data[1] = M*(0.5/eps * (ul+ur) - 0.5 /eps* (pr-pl))
    data[2] = 0.5/eps * (pl+pr) - 0.5 /eps* (ur-ul)
    data[3] = 0.0
end

struct GosseToscani <: Flux
    L :: Float64
    eps :: Float64
end

function (self::GosseToscani)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})

    eps = self.eps
    pl,ul,sl = vL
    pr,ur,sr = vR
    s=0.5*(sl+sr)
    M = 2.0*eps/(2.0*eps+h*s)
    data[1] = M*(0.5/eps * (ul+ur) - 0.5 /eps* (pr-pl))
    data[2] = M*(0.5/eps * (pl+pr) - 0.5 /eps* (ur-ul))
    data[3] = 0.0
end

struct NonConv_GT <: Flux
    L :: Float64
    eps :: Float64
end

function (self::NonConv_GT)(xl,xr,hl,hr,data::Vector{Float64},vL::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64})

    eps = self.eps
    pc,uc,sc = vC
    pl,ul,sl = vL
    pr,ur,sr = vR
    sig_l=0.5*(sl+sc)
    sig_r=0.5*(sc+sr)
    
    Ml = 2.0*eps/(2.0*eps+hl*sig_l)
    Mr = 2.0*eps/(2.0*eps+hr*sig_r)
    Delta = abs(xr-xl)
    data[1] = 0.0
    data[2] = -((Mr-Ml)/eps)*pc
    data[3] = 0.0
end


struct NonConv_BT <: Flux
    L :: Float64
    eps :: Float64
end

function (self::NonConv_BT)(xl,xr,hl,hr,data::Vector{Float64},vL::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64})

    eps = self.eps
    pc,uc,sc = vC
    pl,ul,sl = vL
    pr,ur,sr = vR
    sig_l=0.5*(sl+sc)
    sig_r=0.5*(sc+sr)
    
    Ml = 2.0*eps/(2.0*eps+hl*sig_l)
    Mr = 2.0*eps/(2.0*eps+hr*sig_r)
    Delta = abs(xr-xl)
    data[1] = -((Mr-Ml)/eps)*uc
    data[2] = -((Mr-Ml)/eps)*pc
    data[3] = 0.0
end



struct P1_cfl <: Flux
    eps :: Float64
end

function (self::P1_cfl)(x,v, res)
    eps = self.eps

    lf      = 1.0/self.eps
    max(res,lf)
end

struct JL_cfl <: Flux
    eps :: Float64
end

function (self::JL_cfl)(x,v, res)
    lf      = 1/self.eps
    max(res,lf)
end


struct GT_cfl <: Flux
    eps :: Float64
end

function (self::GT_cfl)(x,v, res)
    min(res,v[3])
end



struct P1_l2_norm <: Flux
end

function (self::P1_l2_norm)(v::Array{Float64}, data::Array{Float64})
    p,u,sigma  =  v # we cannot use direct v. why ?
    data[1]  = p*p  
    data[2]  = u*u
    nothing
end

struct id_P1_mapping <: Flux
end

function (self::id_P1_mapping)(v::Array{Float64}, data::Array{Float64})  
    p,u,sigma  = v
    data[1] = p
    data[2] = u
    data[3]= sigma
    
    nothing
end




