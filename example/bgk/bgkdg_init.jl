abstract type InitFunction end

struct Gaussian <: InitFunction
    L       :: Float64
    Mv      :: Mesh     
end

function (self::Gaussian)(x, t)
    res = zeros(Float64,self.Mv.Nc)
    sigma = 0.06
    x0    = 0.5*self.L
    r     = exp(-0.5*((x-x0)/sigma)^2)+0.01
    u     = 0.0
    t     = 1.0
    
    for i in 1:self.Mv.Nc
        res[i] = (r/sqrt(2.0*pi*t))*exp(-(self.Mv.centers[i]-u)^2/(2.0*t))
    end       
    return res
end
