struct MinModLimiting <: Flux
    Mv      :: Mesh
end


function (self::MinModLimiting)(x,data::Vector{Float64}, vC::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64},d)
    for i in 1:self.Mv.Nc
        dv = 0.5*minmod(vC[i]-vL[i],vR[i]-vC[i])
        deltav = max(-abs(vC[i]),min(abs(vC[i]),dv))
        data[i] = vC[i] + d*deltav
    end    
    nothing 
end

struct LocalLax <: Flux
    L       :: Float64
    Mv      :: Mesh
end

function (self::LocalLax)(x,h,data::Vector{Float64}, vL::Vector{Float64},vR::Vector{Float64})
    for i in 1:self.Mv.Nc
        vel = self.Mv.centers[i] 
        data[i] = 0.5*vel*(vL[i]+vR[i]) - 0.5*abs(vel)*(vR[i]-vL[i])
    end    
    nothing
end

struct l2_norm <: Flux
end

function (self::l2_norm)(v::Vector{Float64},data::Vector{Float64})
    r, u, p  =  v[1:3]
    data[1] = r*r
    data[2] = u*u
    data[3] = p*p
    nothing
end

struct eq_bgk <: Equilibrium
    Mv      :: Mesh
end

function (self::eq_bgk)(v::Vector{Float64},data::Vector{Float64})
    r = 0.0
    q = 0.0
    p = 0.0
    for i in 1:self.Mv.Nc
        r = r + self.Mv.areas[i]*v[i]
        q = q + self.Mv.areas[i]*self.Mv.centers[i]*v[i]   
    end  
    u = q/r
    for i in 1:self.Mv.Nc
        p = p + self.Mv.areas[i]*(u-self.Mv.centers[i])^2*v[i]   
    end 
    t = p/r
    
    for i in 1:self.Mv.Nc
        data[i] = (r/sqrt(2.0*pi*t))*exp(-(u-self.Mv.centers[i])^2/(2.0*t))
    end    
    nothing
end


struct moment_mapping <: Flux
    Mv      :: Mesh
end

function (self::moment_mapping)(v::Vector{Float64},data::Vector{Float64})
    r = 0.0
    q = 0.0
    p = 0.0
    for i in 1:self.Mv.Nc
        r = r + self.Mv.areas[i]*v[i]
        q = q + self.Mv.areas[i]*self.Mv.centers[i]*v[i]   
    end  
    u = q/r
    sum =0.0
    for i in 1:self.Mv.Nc   
        p = p + self.Mv.areas[i]*(u-self.Mv.centers[i])^2*v[i]   
    end 
    data[1] = r
    data[2] = u
    data[3] = p
    nothing
end


struct ope_transport <: Operator
end

function (self::ope_transport)(sp :: SpatialDis,v :: Vector{Float64})

    res= zeros(Float64,sp.nvar*sp.ndof)
    for k in 1:sp.nvar
        sp.field[:,k] .= v[(k-1)*sp.ndof+1:k*sp.ndof]
    end   
    bc_neumann(sp)
    sp()
    for k in 1:sp.nvar
        res[(k-1)*sp.ndof+1:k*sp.ndof] .= sp.flux[:,k] 
    end    
    return res
end   