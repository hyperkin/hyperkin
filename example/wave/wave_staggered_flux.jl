
struct wave_cfl <: Flux
    c :: Float64
end

function (self::wave_cfl)(x,v, res)
    c = self.c

    r, u   =   v
    lf      = abs(c)
    max(res,c)
end

struct l1l2_norm <: Flux
end

function (self::l1l2_norm)(v)
    [abs(v),v*v]
end

struct id_wave_mapping <: Flux
end

function (self::id_wave_mapping)(v)
    return v
end


struct ope_wave <: SpatialOperator

end

function (self::ope_wave)(v :: Vector{DeRhamSpace})  
    # model 
    # - dx u = tilde G u
    # - dx r  = - G r
    res1 = C1bar(v[1].mh,v[1].bc_type,v[1].order,v[1].ndiags)
    res2 = C1(v[2].mh,v[2].bc_type,v[2].order,v[2].ndiags)

    temp1 = C0(v[1].mh,v[1].bc_type,v[1].order,v[1].ndiags)
    temp2 = C0bar(v[2].mh,v[2].bc_type,v[2].order,v[2].ndiags)

    H0bar(v[2],temp2)
    H0(v[1],temp1)
    grad(temp1,res2)
    graddual(temp2,res1)
 
    res2.field[:] .= -res2.field[:]  
    [res1, res2]
end   

struct ope_imp <: Operator

end

function (self::ope_imp)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,2*sp.ndof)
    sp.field[:,1] .= v[1:sp.ndof]
    sp.field[:,2] .= v[sp.ndof+1:end]
    sp()

    res[1:sp.ndof] .= sp.flux[:,1]
    res[sp.ndof+1:end] .= sp.flux[:,2]
    return res
end   


struct ope1_wave <: Operator
end

function (self::ope1_wave)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.ndof)
    
    temp1 = C1(sp.spaces[2].mh,sp.spaces[2].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)
    temp2 = C0bar(sp.spaces[2].mh,sp.spaces[2].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)
    res1 = C1bar(sp.spaces[1].mh,sp.spaces[1].bc_type,sp.spaces[1].order,sp.spaces[1].ndiags)

    temp1.field[:] .= v[:]
    H0bar(temp1,temp2)
    graddual(temp2,res1)

    res[:] .= res1.field[:]
    return res
end   

struct ope2_wave <: Operator
end

function (self::ope2_wave)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.ndof)
    
    temp1 = C1bar(sp.spaces[2].mh,sp.spaces[2].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)
    temp2 = C0(sp.spaces[2].mh,sp.spaces[2].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)
    res1 = C1(sp.spaces[1].mh,sp.spaces[1].bc_type,sp.spaces[1].order,sp.spaces[1].ndiags)

    temp1.field[:] .= v[:]
    H0(temp1,temp2)
    grad(temp2,res1)

    res[:] .= -res1.field[:]
    return res
end   

struct schur_wave <: Operator
end

function (self::schur_wave)(sp :: SpatialDis,v :: Vector{Float64})
    # Laplacian
    res= zeros(Float64,sp.ndof)
    
    temp1 = C1bar(sp.spaces[2].mh,sp.spaces[2].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)
    temp2 = C0(sp.spaces[2].mh,sp.spaces[2].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)
    temp3 = C1(sp.spaces[1].mh,sp.spaces[1].bc_type,sp.spaces[1].order,sp.spaces[1].ndiags)
    temp4 = C0bar(sp.spaces[1].mh,sp.spaces[1].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)

    temp1.field[:] .= v[:]
    H0(temp1,temp2)
    grad(temp2,temp3)
    H0bar(temp3,temp4)
    graddual(temp4,temp1)

    res[:] .= -temp1.field[:]
    return res
end   

struct id_wave2 <: Operator
end





struct invertH1bar 
     sp1 :: C0
     sp2 :: C1bar
end    

function (self::invertH1bar)(v :: Vector{Float64})
    res= zeros(Float64,self.sp1.ndof)
    
    temp1 = C0(self.sp1.mh,self.sp1.bc_type,self.sp1.order,self.sp1.ndiags)
    res1 = C1bar(self.sp2.mh,self.sp2.bc_type,self.sp2.order,self.sp2.ndiags)

    temp1.field[:] .= v[:]
    H1bar(temp1,res1)
    res[:] .= res1.field[:]
    return res
end  


struct ope1_wave2 <: Operator
end

function (self::ope1_wave2)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.ndof)
    
    temp0 = C0(sp.spaces[2].mh,sp.spaces[2].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)
    temp1 = C1(sp.spaces[2].mh,sp.spaces[2].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)
    temp2 = C0bar(sp.spaces[2].mh,sp.spaces[2].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)
    res1 = C1bar(sp.spaces[1].mh,sp.spaces[1].bc_type,sp.spaces[1].order,sp.spaces[1].ndiags)

    temp1.field[:] .= v[:]
    H0bar(temp1,temp2)
    graddual(temp2,res1)
    
    inv = invertH1bar(temp0,res1)
    A = LinearMap{Float64}(inv, sp.ndof; ismutating=false )
    res, his=cg!(res,A,res1.field[:];abstol=1.0e-11,maxiter=20*sp.ndof,log=true)
    
    return res
end   

struct ope2_wave2 <: Operator
end

function (self::ope2_wave2)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.ndof)
    
    temp1 = C0(sp.spaces[2].mh,sp.spaces[2].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)
    res1 = C1(sp.spaces[1].mh,sp.spaces[1].bc_type,sp.spaces[1].order,sp.spaces[1].ndiags)

    temp1.field[:] .= v[:]
    grad(temp1,res1)

    res[:] .= -res1.field[:]
    return res
end 

struct schur_wave2 <: Operator
end

function (self::schur_wave2)(sp :: SpatialDis,v :: Vector{Float64})
    # Laplacian
    res= zeros(Float64,sp.ndof)
   
    resf  = C1bar(sp.spaces[1].mh,sp.spaces[1].bc_type,sp.spaces[1].order,sp.spaces[1].ndiags)
    temp1 = C0(sp.spaces[2].mh,sp.spaces[2].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)
    temp2 = C1(sp.spaces[1].mh,sp.spaces[1].bc_type,sp.spaces[1].order,sp.spaces[1].ndiags)
    temp3 = C0bar(sp.spaces[1].mh,sp.spaces[1].bc_type,sp.spaces[2].order,sp.spaces[2].ndiags)

    temp1.field[:] .= v[:]
    grad(temp1,temp2)
    H0bar(temp2,temp3)
    graddual(temp3,resf)
    
    inv = invertH1bar(temp1,resf)
    A = LinearMap{Float64}(inv, sp.ndof; ismutating=false )
    res, his=cg!(res,A,-resf.field[:];abstol=1.0e-11,maxiter=20*sp.ndof,log=true)
    return res
end  