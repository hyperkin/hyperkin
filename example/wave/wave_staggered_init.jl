abstract type InitFunction end
struct Gaussian_r <: InitFunction
    L     :: Float64
    c     :: Float64
end

function (self::Gaussian_r)(x, t)
    sigma = 0.05
    x0    = 0.5*self.L
    xr    = x-self.c*t
    xl    = x+self.c*t
    rpu   = exp(-0.5*((xr-x0)/sigma)^2)+0.01
    rmu   = exp(-0.5*((xl-x0)/sigma)^2)+0.01
    r     = 0.5*(rpu+rmu)
    u     = 0.5*(rpu-rmu)
    return r
end

struct Gaussian_u <: InitFunction
    L     :: Float64
    c     :: Float64
end
function (self::Gaussian_u)(x, t)
    sigma = 0.05
    x0    = 0.5*self.L
    xr    = x-self.c*t
    xl    = x+self.c*t
    rpu   = exp(-0.5*((xr-x0)/sigma)^2)+0.01
    rmu   = exp(-0.5*((xl-x0)/sigma)^2)+0.01
    r     = 0.5*(rpu+rmu)
    u     = 0.5*(rpu-rmu)
    return u
end

struct Gaussian_rinit <: InitFunction
    L     :: Float64
    c     :: Float64
end

function (self::Gaussian_rinit)(x)
    sigma = 0.05
    x0    = 0.5*self.L
    r     = exp(-0.5*((x-x0)/sigma)^2)+0.01
    return r
end

struct Gaussian_uinit <: InitFunction
    L     :: Float64
    c     :: Float64
end
function (self::Gaussian_uinit)(x)
    u     = 0.0
    return u
end