struct MinModLimiting <: Flux
end

function (self::MinModLimiting)(x,data::Vector{Float64},vC::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64},d)
    
    for i in 1:7
        dv = 0.5*minmod(vC[i]-vL[i],vR[i]-vC[i])
        deltav = max(-abs(vC[i]),min(abs(vC[i]),dv))
        data[i] = vC[i] + d*deltav
    end    
    nothing
end


struct LocalLax <: Flux
    L :: Float64
    r0 :: Float64
    u0 :: Float64
    c0 :: Float64
    Bx :: Float64
    B01 :: Float64
    B02 :: Float64
end

function (self::LocalLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    
    rl, ul, u1l, u2l, pl, b1l, b2l = vL
    rr, ur, u1r, u2r, pr, b1r, b2r = vR
    wave = zeros(Float64,7)
    r0 = self.r0
    u0 = self.u0
    c0 = self.c0
    b01 = self.B01
    b02 = self.B02
    
    ax = self.Bx/sqrt(self.r0)
    ap = sqrt((self.B01^2+self.B02^2)/self.r0) 
    a = sqrt(ax*ax+ap*ap)
    lm = 0.5*(c0^2+a^2)-0.5*sqrt((c0^2+a^2)^2-4.0*ax^2*c0^2)
    lp = 0.5*(c0^2+a^2)+0.5*sqrt((c0^2+a^2)^2-4.0*ax^2*c0^2)
    
    wave = [u0,u0-ax,u0+ax,u0-sqrt(lm),u0+sqrt(lm),u0-sqrt(lp),u0+sqrt(lp)]
    S = maximum(wave)
    
    center_r = (u0*rl+r0*ul+u0*rr+r0*ur)
    center_u = (u0*ul+(1.0/r0)*(pl+b01*b1l+b02*b2l)+ u0*ur+(1.0/r0)*(pr+b01*b1r+b02*b2r))
    center_u1 = (u0*u1l-self.Bx/r0*b1l+u0*u1r-self.Bx/r0*b1r)
    center_u2 = (u0*u2l-self.Bx/r0*b2l+u0*u2r-self.Bx/r0*b2r)
    center_p = (u0*pl+c0^2*r0*ul+u0*pr+c0^2*r0*ur)
    center_b1 = (u0*b1l+b01*ul-self.Bx*u1l+u0*b1r+b01*ur-self.Bx*u1r)
    center_b2 = (u0*b2l+b02*ul-self.Bx*u2l+u0*b2r+b02*ur-self.Bx*u2r)
    
    data[1] = 0.5 * center_r - 0.5 * abs(S) * (rr-rl)
    data[2] = 0.5 * center_u - 0.5 * abs(S) * (ur-ul)
    data[3] = 0.5 * center_u1 - 0.5 * abs(S) * (u1r-u1l)
    data[4] = 0.5 * center_u2 - 0.5 * abs(S) * (u2r-u2l)
    data[5] = 0.5 * center_p - 0.5 * abs(S) * (pr-pl)
    data[6] = 0.5 * center_b1 - 0.5 * abs(S) * (b1r-b1l)
    data[7] = 0.5 * center_b2 - 0.5 * abs(S) * (b2r-b2l)
    nothing
end

struct DiffDis <: Flux
    L :: Float64
    r0 :: Float64 
    Bx :: Float64
    kappa :: Float64
    nu :: Float64
    eta :: Float64
    di :: Float64
end

function (self::DiffDis)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    
    rl, ul, u1l, u2l, pl, b1l, b2l = vL
    rr, ur, u1r, u2r, pr, b1r, b2r = vR
    
    visu = -self.nu*(ur-ul)/h
    visu1 = -self.nu*(u1r-u1l)/h
    visu2 = -self.nu*(u2r-u2l)/h
    visp = -self.kappa/self.r0*(pr-pl)/h
    visb1 = -self.eta*(b1r-b1l)/h
    visb2 = -self.eta*(b2r-b2l)/h
    
    dis1 = self.di/self.r0*self.Bx*(b2r-b2l)/h
    dis2 = -self.di/self.r0*self.Bx*(b1r-b1l)/h
    
    data[1] = 0.0
    data[2] = visu
    data[3] = visu
    data[4] = visu2
    data[5] = visp
    data[6] = visb1 + dis1
    data[7] = visb2 + dis2
    nothing
end


struct linearmhd_cfl <: Flux
    L :: Float64
    r0 :: Float64
    u0 :: Float64
    c0 :: Float64
    Bx :: Float64
    B01 :: Float64
    B02 :: Float64
    di :: Float64
end

function (self::linearmhd_cfl)(x,v, res)
    
    u0 = self.u0
    c0 = self.c0
    ax = self.Bx/sqrt(self.r0)
    ap = sqrt((self.B01^2+self.B02^2)/self.r0) 
    a = sqrt(ax*ax+ap*ap)
    lm = 0.5*(c0^2+a^2)-0.5*sqrt((c0^2+a^2)^2-4.0*ax^2*c0^2)
    lp = 0.5*(c0^2+a^2)+0.5*sqrt((c0^2+a^2)^2-4.0*ax^2*c0^2)
    
    wave = [u0,u0-ax,u0+ax,u0-sqrt(lm),u0+sqrt(lm),u0-sqrt(lp),u0+sqrt(lp)]
    S = maximum(wave)
    
    lf      = abs(S)
    max(res,lf)
end

struct mass_energy <: Flux
    L :: Float64
    r0 :: Float64
    u0 :: Float64
    c0 :: Float64
    Bx :: Float64
    B01 :: Float64
    B02 :: Float64
end

function (self::mass_energy)(v::Vector{Float64}, data::Vector{Float64})
    r, u, u1, u2, p, b1, b2   =   v
    data[1] =r 
end

struct id_mhd_mapping <: Flux
    Bx :: Float64
end

function (self::id_mhd_mapping)(v::Vector{Float64}, data::Vector{Float64})
    data[:] .= v[:]
    
end

struct pressuretotal_mapping <: Flux
    Bx :: Float64
end

function (self::pressuretotal_mapping)(v::Vector{Float64}, data::Vector{Float64})
    data[:] .= v[:]
    data[5] = v[5]+0.5*v[6]*v[6]+0.5*v[7]*data[7]-0.5*self.Bx*self.Bx
    data[6] = - self.Bx*v[6]
    data[7] = - self.Bx*v[7]
    
end


  