abstract type InitFunction end
struct Gaussianp <: InitFunction
    L     :: Float64
end

function (self::Gaussianp)(x, t)
    sigma = 0.04
    x0    = 0.5*self.L
    r     = 1.0
    u     = 0.0
    u1    = 0.0
    u2    = 0.0
    p     = exp(-0.5*((x-x0)/sigma)^2)+0.01
    b1    = 0.2*exp(-0.5*((x-x0)/sigma)^2)
    b2    = 0.0

    [r, u, u1, u2, p, b1, b2]
end
