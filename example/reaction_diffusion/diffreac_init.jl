abstract type InitFunction end

struct initff <: InitFunction
    L     :: Float64
    a     :: Float64
end

function (self::initff)(x, t)
    sigma = 0.05
    x0    = 0.5*self.L
    xr    = x-self.a*t
    r     = exp(-0.5*((xr-x0)/sigma)^2)+0.01

    [r]
end
