abstract type InitFunction end

struct Gaussian <: InitFunction
    L     :: Float64
    a     :: Float64
end

function (self::Gaussian)(x, t)
    sigma = 0.05
    x0    = 0.5*self.L
    xr    = x-self.a*t
    r     = exp(-0.5*((xr-x0)/sigma)^2)+0.01

    [r]
end

struct Discontinuity <: InitFunction
    L     :: Float64
    a     :: Float64
end

function (self::Discontinuity)(x, t)
    sigma = 0.05
    x0    = 0.5*self.L
    xr    = x-self.a*t
    if abs(xr-x0) < 0.1
        r = 1.0
    else
        r = 0.0    
    end    

    [r]
end
