struct MinModLimiting <: Flux
    L :: Float64
    a :: Float64
end

function (self::MinModLimiting)(x,data::Vector{Float64},vC::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64},d)
    rc = vC[1]  
    rl = vL[1]      
    rr = vR[1]  
    
    dr = 0.5*minmod(rc-rl,rr-rc)
    deltac = rc*max(-1.0,min(1.0,dr/rc))
    rf = rc + d*deltac
    data[1]=rf
    nothing
end


struct LocalLax <: Flux
    L :: Float64
    a :: Float64
end

function (self::LocalLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})

    a = self.a
    rl = vL[1]
    rr = vR[1]
    data[1] = 0.5 * (a*rl+a*rr) - 0.5 * abs(a) * (rr-rl)
end

struct advection_cfl <: Flux
    a :: Float64
end

function (self::advection_cfl)(x, v, res)
    a = self.a 

    r   =   v[1]
    lf      = abs(a)
    max(res,a)
end

struct l1l2_norm <: Flux
end

function (self::l1l2_norm)(v::Array{Float64}, data::Array{Float64})
    r  =  v[1] # we cannot use direct v. why ?
    data[1]  =   abs(r)
    data[2]  =   r*r
    nothing
end

struct id_adv_mapping <: Flux
end

function (self::id_adv_mapping)(v::Array{Float64}, data::Array{Float64})  
    r  =  v[1]
    data[1] = r
    nothing
end


struct ope_impdg <: Operator

end

function (self::ope_impdg)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.nvar*sp.ndof)
    
    for k in 1:sp.nvar
        sp.field[:,k] .= v[(k-1)*sp.ndof+1:k*sp.ndof]
    end   
    bc_neumann(sp)
    sp()
    for k in 1:sp.nvar
        res[(k-1)*sp.ndof+1:k*sp.ndof] .= sp.flux[:,k] ### The operator Dg is -Adx and the right side of ode gives is the same   
    end    
    return res
end   


struct TVD_LimitingDG <: Flux
    M :: Float64 
end

function (self::TVD_LimitingDG)(h,data::Vector{Float64},vC::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64},vI::Vector{Float64},d)
    rc = vC[1] 
    rl = vL[1]      
    rr = vR[1] 
    ri = vI[1] # interface value
    
    dr = minmoddg(self.M,h,d*(ri-rc),rr-rc,rc-rl)
    data[1] = rc + d*dr #ri
    nothing
end

struct entropyL2 <: Flux
end

function (self::entropyL2)(v::Vector{Float64})
    rc = v[1]  

    return 0.5*rc*rc
end

struct fluxentropyL2 <: Flux
    a :: Float64
end

function (self::fluxentropyL2)(x,h,data,vL::Vector{Float64},vR::Vector{Float64})
    rl = vL[1]      
    rr = vR[1]  
    
    data= 0.5*self.a*(0.5*rl*rl+0.5*rr*rr) - 0.5*abs(self.a)*(0.5*rr*rr-0.5*rl*rl)
    nothing
end
