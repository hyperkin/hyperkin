

struct AcousticFlux <: Flux
    Lx :: Float64
    Ly :: Float64
    lambda :: Float64
end

function (self::AcousticFlux)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    l = self.lambda
    
    pl = vC[1]
    pr = vR[1]
    uxl = vC[2]
    uxr = vR[2]
    uyl = vC[3]
    uyr = vR[3]
    
    unl = uxl*n[1]+uyl*n[2]
    unr = uxr*n[1]+uyr*n[2]

    data[1] = 0.5 *l*(unl+unr) - 0.5 *l*(pr-pl)
    data[2] = 0.5 *l*(pl+pr)*n[1] - 0.5 *l* (uxr-uxl)
    data[3] = 0.5 *l*(pl+pr)*n[2] - 0.5 *l* (uyr-uyl)
end

struct xinjin2D_cfl <: Flux
    lambda :: Float64
end

function (self::xinjin2D_cfl)(x,v, res)
    r   =   v[1]
    lf      = self.lambda
    max(res,lf)
end

struct l2_norm <: Flux
end

function (self::l2_norm)(v::Array{Float64}, data::Array{Float64})
    r, ux, uy  =  v[:] 
    data[1]  =   r*r
    data[2]  =   ux*ux + uy*uy
    nothing
end

struct id_burgersXJ_mapping <: Flux
    ax :: Float64
    ay :: Float64
    lambda :: Float64
end

function (self::id_burgersXJ_mapping)(v::Array{Float64}, data::Array{Float64})  
    r, ux, uy  =  v[:]
    data[1] = r
    data[2] = self.ax*0.5*r*r 
    data[3] = self.ay*0.5*r*r 
    nothing
end

struct eq_burgers2D <: Equilibrium
    ax :: Float64
    ay :: Float64
end

function (self::eq_burgers2D)(v::Vector{Float64},data::Vector{Float64})
    r, ux, uy   =  v
    data[1] = r
    data[2] = self.ax*0.5*r*r
    data[3] = self.ay*0.5*r*r
    nothing
end
