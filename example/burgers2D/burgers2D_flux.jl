

struct LocalLaxKpp <: Flux
    Lx :: Float64
    Ly :: Float64
    ax :: Float64
    ay :: Float64
end

function (self::LocalLaxKpp)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    rl = vC[1]
    rr = vR[1]
    ll = cos(rl)*n[1]-sin(rl)*n[2]
    lr = cos(rr)*n[1]-sin(rr)*n[2]
    fl=  sin(rl)*n[1]+cos(rl)*n[2]
    fr=  sin(rr)*n[1]+cos(rr)*n[2]
    data[1] = 0.5 * (fl+fr) - 0.5 * max(abs(ll),abs(lr)) * (rr-rl)
end

struct LocalLax <: Flux
    Lx :: Float64
    Ly :: Float64
    ax :: Float64
    ay :: Float64
end

function (self::LocalLax)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})

    an = self.ax*n[1]+self.ay*n[2]
    rl = vC[1]
    rr = vR[1]
    rc = 0.5*(rl+rr)
    data[1] = 0.5 * 0.5 *(an*rl*rl+an*rr*rr) - 0.5 * abs(an*rc) * (rr-rl)
end

struct burgers_cfl <: Flux
    ax :: Float64
    ay :: Float64
end

function (self::burgers_cfl)(x,v, res)
    ax = self.ax 
    ay = self.ay

    r   =   v[1]
    lf      = sqrt(ax*ax+ay*ay)*r
    max(res,lf)
end

struct kpp_cfl <: Flux
    ax :: Float64
    ay :: Float64
end

function (self::kpp_cfl)(x,v, res)
    ax = self.ax 
    ay = self.ay

    r   =   v[1]
    lf      = sqrt(sin(r)^2+cos(r)^2)
    max(res,lf)
end

struct l1l2_norm <: Flux
end

function (self::l1l2_norm)(v::Array{Float64}, data::Array{Float64})
    r  =  v[1]
    data[1]  =   abs(r)
    data[2]  =   r*r
    nothing
end

struct id_burgers_mapping <: Flux
end

function (self::id_burgers_mapping)(v::Array{Float64}, data::Array{Float64})  
    r  =  v[1]
    data[1] = r
    nothing
end
