abstract type InitFunction end

struct AdvGaussian <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
     ax     :: Float64
     ay     :: Float64
end

function (self::AdvGaussian)(x, t) 
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    sigma2 = 0.05*0.05
    sigma  = 0.05 
    norm2 = (x[1]-x0x)^2 + (x[2]-x0y)^2
    r     = 0.1 +  0.2*(1.0/(sigma*sqrt(pi)))*exp(-0.5*(norm2/sigma2))
    [r]
end

struct Shock <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
     ax     :: Float64
     ay     :: Float64
end

function (self::Shock)(x, t) 
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    sigma2 = 0.05*0.05
    sigma  = 0.05 
    norm2 = (x[1]-x0x)^2 + (x[2]-x0y)^2
    r = 0.1
    if sqrt(norm2) < 0.1
        r     = 1.0
    end    
    [r]
end

struct Rarefact <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
     ax     :: Float64
     ay     :: Float64
end

function (self::Rarefact)(x, t) 
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    sigma2 = 0.05*0.05
    sigma  = 0.05 
    norm2 = (x[1]-x0x)^2 + (x[2]-x0y)^2
    r = 1.0
    if sqrt(norm2) < 0.1
        r     = 0.1
    end    
    [r]
end

struct RotatingWave <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
     ax     :: Float64
     ay     :: Float64
end

function (self::RotatingWave)(x, t) 
    sigma2 = 0.05*0.05
    sigma  = 0.05 
    norm2 = (x[1])^2 + (x[2])^2
    r = 0.25*pi
    if norm2 < 1.0
        r     = 3.5*pi
    end    
    [r]
end
