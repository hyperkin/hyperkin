abstract type InitFunction end

struct LineProblemPn <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
     N      :: Int64
end

function (self::LineProblemPn)(x, t) 
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    sigma2 = 0.005*0.005
    sigma  = 0.005
    norm2 = (x[1]-x0x)^2 + (x[2]-x0y)^2
    p     = 0.1 + exp(-0.5*(norm2/sigma2))
   
    res = zeros(Float64,nb_varPn(self.N))
    res[1] =p
    return res
end

struct LineProblemM1 <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
end

function (self::LineProblemM1)(x, t) 
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    sigma2 = 0.005*0.005
    sigma  = 0.005
    norm2 = (x[1]-x0x)^2 + (x[2]-x0y)^2
    p     = 0.1 + exp(-0.5*(norm2/sigma2))
   
    res = zeros(Float64,4)
    res[1] =p
    return res
end

