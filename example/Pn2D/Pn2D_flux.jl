struct NoFilter <: Filter
end
function (self::NoFilter)(x)
    return x
end   

struct LancosFilter <: Filter
end
function (self::LancosFilter)(x)
    return sin(x)/x
end 

struct Splines4Filter <: Filter
end
function (self::Splines4Filter)(x)
    return 1.0/(x^4+1.0)
end

struct Expo2Filter <: Filter
end
function (self::Expo2Filter)(x)
    alpha=2.0
    return exp(log(0.0000000000001)*x^(alpha))
end

struct Expo4Filter <: Filter
end
function (self::Expo4Filter)(x)
    alpha=4.0
    return exp(log(0.0000000000001)*x^(alpha))
end

function nb_varPn(N)
   nb= convert(Int64,(N+1)^2)
   return nb
end  


function var_to_2Dharm(l,m)
   index = convert(Int64,l*l+l+m+1) 
   return index
end

function Alm(l,m)
   return sqrt((l-m)*(l+m)/((2*l+1)*(2*l-1)))     
end 
    
function Blm(l,m)
   return sqrt((l+m-1)*(l+m)/((2*l-1)*(2*l+1)))      
end   

mutable struct RusanovPnFlux <: Flux
    N    :: Int64
    Ax   :: Array{Float64,2}
    Ay   :: Array{Float64,2}
    vmax :: Float64
    
    function RusanovPnFlux(n)
        nb = nb_varPn(n)
        Ax = zeros(Float64,(nb,nb))
        Ay = zeros(Float64,(nb,nb))  
        vmax = 0.0
        new(n,Ax,Ay,vmax)   
    end
end

function kro(i,j)
    res=0.0
    if (i==j)
        res =1.0
    end       
    return res
end


function init_PnMatrices(self::RusanovPnFlux)
    self.vmax = 1.0
    if N==1
        self.vmax =1.0/sqrt(3.0)
    end  
    a =0.0
    b =0.0
    c1 =0.0; c2 =0.0
    d1 =0.0; d2 =0.0; d3=0.0; d4=0.0
                       
    for li in 0:N                       
       for mi in -li:li      
           for lj in 0:N                       
               for mj in -lj:lj 
                   a = kro(lj,li-1)*kro(mj,mi)*Alm(li,mi) 
                   b = kro(lj,li+1)*kro(mj,mi)*Alm(lj,mj) 
                   self.Ax[var_to_2Dharm(li,mi),var_to_2Dharm(lj,mj)]=a+b 
                    
                   c1=0.5*sign(mi)*(1.0+(sqrt(2.0)-1.0)*kro(mi,1))
                   c2=0.5*sign(mi+1)*(1.0+(sqrt(2.0)-1.0)*kro(mi,0))
                   d1=-kro(lj,li-1)*kro(mj,mi-1)*Blm(li,mi)
                   d2=kro(lj,li+1)*kro(mj,mi-1)*Blm(lj,-mj)   
                   d3=kro(lj,li-1)*kro(mj,mi+1)*Blm(li,-mi)
                   d4=-kro(lj,li+1)*kro(mj,mi+1)*Blm(lj,mj)     
                   self.Ay[var_to_2Dharm(li,mi),var_to_2Dharm(lj,mj)]=c1*(d1+d2)+c2*(d3+d4)          
               end                         
            end                                                       
       end                         
    end                                            
end  

function (self::RusanovPnFlux)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})  
    data[:] = 0.5 * (self.Ax*n[1]+self.Ay*n[2])*(vR[:]+vC[:])- 0.5*self.vmax*(vR[:]-vC[:])
end


mutable struct ScatteringPn <: Flux
    N      :: Int64
    Sigma  :: Float64
    Sigmaf :: Float64
    R      :: Array{Float64,2}
    f      :: Filter 
    
    function ScatteringPn(n,sig,sigf)
        nb = nb_varPn(n)
        R = zeros(Float64,(nb,nb))
        f = NoFilter()
        new(n,sig,sigf,R,f)   
    end
end

function set_filter(self::ScatteringPn, filter)
    self.f = filter
end

function (self::ScatteringPn)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    
    data[1] = 0.0
    for li in 1:N                       
       for mi in -li:li
            k = var_to_2Dharm(li,mi)
            coef=log(self.f(li/(self.N+1)))/log(self.f(self.N/(self.N+1)))
            data[k] = -(0.25)*self.Sigma*vC[k] -(0.25)*self.Sigmaf*coef*vC[k] 
       end
    end     
end


struct Pn_cfl <: Flux
    N :: Int64
end

function self::Pn_cfl(x, v, res)
    r   =   v[1]
    lf      = min((1.0/sqrt(3.0))*N,1.0)
    max(res,lf)
end

struct Pnl2_norm <: Flux
     N :: Int64
end

function (self::Pnl2_norm)(v::Array{Float64}, data::Array{Float64})
    for li in 0:N    
        data[li+1] =0.0
        for mi in -li:li  
            k= var_to_2Dharm(li,mi)
            data[li+1] = data[li+1] + v[k]^2
        end
    end  
    data[end] = sum(data[1:end-1])
    nothing
end

struct id_Pn_mapping <: Flux
end

function (self::id_Pn_mapping)(v::Array{Float64}, data::Array{Float64})  
    data[:] = v[:]
    nothing
end

function khi_eddington(f)
    return (3.0+4.0*f*f)/(5.0+2.0*sqrt(4.0-3.0*f*f))
end   

struct RusanovM1Flux <: Flux
end

function (self::RusanovM1Flux)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    
    el = vC[1]
    er = vR[1]
    fxl = vC[2]
    fxr = vR[2]
    fyl = vC[3]
    fyr = vR[3]
    
    fnl = fxl*n[1]+fyl*n[2]
    fnr = fxr*n[1]+fyr*n[2]
        
    normfl = sqrt(fxl*fxl+fyl*fyl) + 0.00000001   
    norml =normfl/abs(el)   
    normfr = sqrt(fxr*fxr+fyr*fyr) + 0.00000001  
    normr =normfr/abs(er)   
    
    p11l = 0.5*(1.0-khi_eddington(norml) +(3.0*khi_eddington(norml)-1.0)*fxl*fxl/normfl)*el
    p12l = 0.5*((3.0*khi_eddington(norml)-1.0)*fxl*fyl/normfl)*el   
    p21l = 0.5*((3.0*khi_eddington(norml)-1.0)*fyl*fxl/normfl)*el  
    p22l = 0.5*(1.0-khi_eddington(norml) +(3.0*khi_eddington(norml)-1.0)*fyl*fyl/normfl)*el    
        
    p11r = 0.5*(1.0-khi_eddington(normr) +(3.0*khi_eddington(normr)-1.0)*fxr*fxr/normfr)*er
    p12r = 0.5*((3.0*khi_eddington(normr)-1.0)*fxr*fyr/normfr)*er  
    p21r = 0.5*((3.0*khi_eddington(normr)-1.0)*fyr*fxr/normfr)*er  
    p22r = 0.5*(1.0-khi_eddington(normr) +(3.0*khi_eddington(normr)-1.0)*fyr*fyr/normfr)*er    
    
    s = 1.0
    data[1] = 0.5 *(fnl+fnr) - 0.5 * s*  (er-el)
    data[2] = 0.5 *((p11l+p11r)*n[1]+(p12l+p12r)*n[2]) - 0.5 * s * (fxr-fxl)
    data[3] = 0.5 *((p21l+p21r)*n[1]+(p22l+p22r)*n[2]) - 0.5 * s * (fyr-fyl)
    data[4] = 0.0
end

struct ScatteringM1 <: Flux
    simga :: Float64
end

function (self::ScatteringM1)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    
   data[1] = 0.0    
   data[2:end] = -(0.25)*sigma*vC[2:end]
end

struct M1_cfl <: Flux
    N :: Int64
end

function (self::M1_cfl)(v, res)
    r   =   v[1]
    lf      = 1.0
    max(res,lf)
end

struct M1l2_norm <: Flux
     N :: Int64
end

function (self::M1l2_norm)(v::Array{Float64}, data::Array{Float64}) 
    data[1] = v[1]^2    
    data[2] = v[2]^2 +v[3]^2  
    data[end] = sum(data[1:end-1])
    nothing
end


struct id_M1_mapping <: Flux
end

function (self::id_M1_mapping)(v::Array{Float64}, data::Array{Float64})  

    data[1:3] = v[1:3]
    nothing
end