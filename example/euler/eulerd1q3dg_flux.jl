struct LocalLax <: Flux
    L :: Float64
    lambda_m  :: Float64
    lambda_0  :: Float64
    lambda_p  :: Float64
end

function (self::LocalLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    fr_ml, fr_0l, fr_pl,  fru_ml, fru_0l, fru_pl, fe_ml, fe_0l, fe_pl= vL
    fr_mr, fr_0r, fr_pr,  fru_mr, fru_0r, fru_pr, fe_mr, fe_0r, fe_pr= vR
    

    data[1] = 0.5 * self.lambda_m*(fr_ml+fr_mr) - 0.5 * abs(self.lambda_m) * (fr_mr-fr_ml)
    data[2] = 0.5 * self.lambda_0*(fr_0l+fr_0r) - 0.5 * abs(self.lambda_0) * (fr_0r-fr_0l)
    data[3] = 0.5 * self.lambda_p*(fr_pl+fr_pr) - 0.5 * abs(self.lambda_p) * (fr_pr-fr_pl)
    data[4] = 0.5 * self.lambda_m*(fru_ml+fru_mr) - 0.5 * abs(self.lambda_m) * (fru_mr-fru_ml)
    data[5] = 0.5 * self.lambda_0*(fru_0l+fru_0r) - 0.5 * abs(self.lambda_0) * (fru_0r-fru_0l)
    data[6] = 0.5 * self.lambda_p*(fru_pl+fru_pr) - 0.5 * abs(self.lambda_p) * (fru_pr-fru_pl)
    data[7] = 0.5 * self.lambda_m*(fe_ml+fe_mr) - 0.5 * abs(self.lambda_m) * (fe_mr-fe_ml)
    data[8] = 0.5 * self.lambda_0*(fe_0l+fe_0r) - 0.5 * abs(self.lambda_0) * (fe_0r-fe_0l)
    data[9] = 0.5 * self.lambda_p*(fe_pl+fe_pr) - 0.5 * abs(self.lambda_p) * (fe_pr-fe_pl)
    
    nothing
end

struct d1q3n3_cfl <: Flux
    lambda_m :: Float64
    lambda_0 :: Float64
    lambda_p :: Float64
end

function (self::d1q3n3_cfl)(x,v, res)
    return max(abs(self.lambda_m),max(abs(self.lambda_p),abs(self.lambda_0)))
end

struct l2_norm <: Flux
end

function (self::l2_norm)(v::Vector{Float64},data::Vector{Float64})
    r, u, p   =  v
    data[1] = r*r
    data[2] = u*u
    data[2] = p*p
    nothing
end

struct eq_d1q3n3_euler <: Equilibrium
    lambda_m  :: Float64
    lambda_0  :: Float64
    lambda_p  :: Float64
    scheme    :: Integer
    alpha     :: Float64
    gamma     :: Float64
end

function (self::eq_d1q3n3_euler)(v::Vector{Float64},data::Vector{Float64})
    lp   = self.lambda_p
    lm   = self.lambda_m
    l0   = self.lambda_0 
    fr_m, fr_0, fr_p,  fru_m, fru_0, fru_p, fe_m, fe_0, fe_p   =  v
   
    r     = fr_m + fr_0 + fr_p
    ru    = fru_m + fru_0 + fru_p
    e     = fe_m + fe_0 + fe_p
    u     = ru/r
    p     = (e-0.5*r*u*u)*(self.gamma-1.0)
    
    if self.scheme==1
        Fr_m=-lm/(lp-lm)*(r*u-lp*r)
        Fr_p=lp/(lp-lm)*(r*u-lm*r)
        Fru_m=-lm/(lp-lm)*(r*u*u+p-lp*ru)
        Fru_p=lp/(lp-lm)*(r*u*u+p-lm*ru)
        Fe_m=-lm/(lp-lm)*((e+p)*u-lp*e)
        Fe_p=lp/(lp-lm)*((e+p)*u-lm*e)
    else
        Fr_m= 0.5*(r*u +self.alpha*r*u*u/lm +p/lm)
        Fr_p= 0.5*(r*u +self.alpha*r*u*u/lp +p/lp)
        Fru_m= 0.5*(r*u*u +self.alpha*r*u*u*u/lm +p*(1+self.gamma*u/lm))
        Fru_p= 0.5*(r*u*u +self.alpha*r*u*u*u/lp +p*(1+self.gamma*u/lp))
        Fe_m= 0.5*(e*u+ self.alpha*e*u*u/lm+(p*u+self.gamma*(u*u+lm*lm)*p/lm))
        Fe_p= 0.5*(e*u+ self.alpha*e*u*u/lp+(p*u+self.gamma*(u*u+lp*lp)*p/lp))
    end
 
    data[1] = -Fr_m/(l0-lm)
    data[2] = r-(Fr_p/(lp-l0) -Fr_m/(l0-lm)) 
    data[3] = Fr_p/(lp-l0)
    data[4] = -Fru_m/(l0-lm)
    data[5] = ru-(Fru_p/(lp-l0) -Fru_m/(l0-lm))
    data[6] = Fru_p/(lp-l0)
    data[7] = -Fe_m/(l0-lm)
    data[8] = e-(Fe_p/(lp-l0) -Fe_m/(l0-lm)) 
    data[9] = Fe_p/(lp-l0)
    
    nothing
end


struct id_d1q3n3_mapping <: Flux
    lambda_m   :: Float64
    lambda_0   :: Float64
    lambda_p   :: Float64 
    gamma     :: Float64
end

function (self::id_d1q3n3_mapping)(v::Vector{Float64},data::Vector{Float64})
    r    =   v[1] + v[2] + v[3]
    ru   =   v[4] + v[5] + v[6]
    e    =   v[7] + v[8] + v[9]
    
    p    = (e-0.5*ru*ru/r)*(self.gamma-1.0)
    
    data[1] = r
    data[2] = ru/r
    data[3] = p
end


struct ope_d1q3n3dg <: Operator

end

function (self::ope_d1q3n3dg)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.nvar*sp.ndof)
    
    for k in 1:sp.nvar
        sp.field[:,k] .= v[(k-1)*sp.ndof+1:k*sp.ndof]
    end   
    bc_neumann(sp)
    sp()
    for k in 1:sp.nvar
        res[(k-1)*sp.ndof+1:k*sp.ndof] .= sp.flux[:,k] ### The operator Dg is -Adx and the right side of ode gives is the same 
    end    
    return res
end   


function d1q3n3_fields_to_xn(sp :: SpatialDis,v :: Vector{Float64})
    k=1
    @simd for i in 1:9
        @simd for j in 1:sp.ndof
            v[k] = sp.field[j,i]   
            k=k+1
        end
    end
end    

function d1q3n3_xn_to_fields(sp :: SpatialDis,v :: Vector{Float64})
    k=1
    @simd for i in 1:9
        @simd for j in 1:sp.ndof
            sp.field[j,i]  = v[k] 
            k=k+1
        end
    end
end  