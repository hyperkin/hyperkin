struct euler_XJ_mapping <: Flux
    gamma :: Float64
end

function (self::euler_XJ_mapping)(var::Vector{Float64},data::Vector{Float64})
    r, ru, e, u, v, w = var
    p=(self.gamma-1.0)*(e-0.5*ru*ru/r)
    data[1] = r
    data[2] = ru/r
    data[3] = p
    data[4] = r
    data[5] = ru
    data[6] = e
    nothing
end

struct eulerXJ_cfl <: Flux
    gamma :: Float64
    la :: Float64
end

function (self::eulerXJ_cfl)(x,v, res)
    gamma = self.gamma
    
    max(res,self.la)
end

struct eq_euler <: Equilibrium
    gamma :: Float64
end

function (self::eq_euler)(var::Vector{Float64},data::Vector{Float64})
    r, ru, e, u, v, w = var
    p=(self.gamma-1.0)*(e-0.5*ru*ru/r)
    data[1] = r
    data[2] = ru
    data[3] = e
    data[4] = ru
    data[5] = ru*ru/r+ p
    data[6] = (e+p)*ru/r
    nothing
end


mutable struct ope1_EulerXJ <: Operator
    bc    :: Int64
    la    :: Float64
end

function (self::ope1_EulerXJ )(sp :: SpatialDis,var :: Vector{Float64})
   # v contains, u,v,w
   res = zeros(Float64, 3*sp.ndof) 

   ## Periodic
   if self.bc == 1
        u_l, u_r = var[sp.ndof],var[1]
        v_l, v_r = var[2*sp.ndof],var[sp.ndof+1] 
        w_l, w_r = var[3*sp.ndof],var[2*sp.ndof+1] 
   end
   ## Neumann
   if self.bc == 2
       u_l, u_r = var[1],var[sp.ndof]
       v_l, v_r = var[sp.ndof+1],var[2*sp.ndof]
       w_l, w_r = var[2*sp.ndof+1],var[3*sp.ndof] 

   end
   res[1]   = - (var[2]-u_l)/(2.0*sp.mh.h)        
   res[sp.ndof] = - (u_r-var[sp.ndof-1])/(2.0*sp.mh.h) 
    
   res[sp.ndof+1]   = - (var[sp.ndof+2]-v_l)/(2.0*sp.mh.h)        
   res[2*sp.ndof]   = - (v_r-var[2*sp.ndof-1])/(2.0*sp.mh.h) 
    
   res[2*sp.ndof+1] = - (var[2*sp.ndof+2]-w_l)/(2.0*sp.mh.h)        
   res[3*sp.ndof]   = - (w_r-var[3*sp.ndof-1])/(2.0*sp.mh.h)  

   @simd for i in 2:sp.ndof-1
       res[i] = - (var[i+1]-var[i-1])/(2.0*sp.mh.h)  
       res[sp.ndof+i] = - (var[sp.ndof+i+1]-var[sp.ndof+i-1])/(2.0*sp.mh.h) 
       res[2*sp.ndof+i] = - (var[2*sp.ndof+i+1]-var[2*sp.ndof+i-1])/(2.0*sp.mh.h) 
   end
   return res
end   

mutable struct ope2_EulerXJ  <: Operator
    bc    :: Int64 
    la    :: Float64
end
 
function (self::ope2_EulerXJ )(sp :: SpatialDis,v :: Vector{Float64})
   # v contains, r, ru, E
   res = zeros(Float64, 3*sp.ndof) 
   la = self.la

   ## Periodic
   if self.bc == 1
        r_l, r_r = v[sp.ndof],v[1]
        ru_l, ru_r = v[2*sp.ndof],v[sp.ndof+1] 
        e_l, e_r = v[3*sp.ndof],v[2*sp.ndof+1] 
   end
   ## Neumann
   if self.bc == 2
       r_l, r_r = v[1],v[sp.ndof]
       ru_l, ru_r = v[sp.ndof+1],v[2*sp.ndof]
       e_l, e_r = v[2*sp.ndof+1],v[3*sp.ndof] 
   end
    
   res[1]   = -la*la * (v[2]-r_l)/(2.0*sp.mh.h)        
   res[sp.ndof] = -la*la * (r_r-v[sp.ndof-1])/(2.0*sp.mh.h) 
    
   res[sp.ndof+1]   = -la*la * (v[sp.ndof+2]-ru_l)/(2.0*sp.mh.h)        
   res[2*sp.ndof]   = -la*la * (ru_r-v[2*sp.ndof-1])/(2.0*sp.mh.h) 
    
   res[2*sp.ndof+1] = -la*la * (v[2*sp.ndof+2]-e_l)/(2.0*sp.mh.h)        
   res[3*sp.ndof]   = -la*la * (e_r-v[3*sp.ndof-1])/(2.0*sp.mh.h)  

   @simd for i in 2:sp.ndof-1
       res[i] = -la*la * (v[i+1]-v[i-1])/(2.0*sp.mh.h)  
       res[sp.ndof+i] = -la*la * (v[sp.ndof+i+1]-v[sp.ndof+i-1])/(2.0*sp.mh.h) 
       res[2*sp.ndof+i] = -la*la * (v[2*sp.ndof+i+1]-v[2*sp.ndof+i-1])/(2.0*sp.mh.h) 
   end
   return res
end   


mutable struct schur_EulerXJ  <: Operator
    bc    :: Int64 
    la    :: Float64
end

function (self::schur_EulerXJ)(sp :: SpatialDis,v :: Vector{Float64})
   # v contains, r, ru, E
   res = zeros(Float64, 3*sp.ndof) 
   la = self.la

   ## Periodic
   if self.bc == 1
        r_l, r_r = v[sp.ndof],v[1]
        ru_l, ru_r = v[2*sp.ndof],v[sp.ndof+1] 
        e_l, e_r = v[3*sp.ndof],v[2*sp.ndof+1] 
   end
   ## Neumann
   if self.bc == 2
       r_l, r_r = v[1],v[sp.ndof]
       ru_l, ru_r = v[sp.ndof+1],v[2*sp.ndof]
       e_l, e_r = v[2*sp.ndof+1],v[3*sp.ndof] 
   end
        
   res[1]   = la^2 * (v[2]-2.0*v[1]+r_l)/(sp.mh.h^2)       
   res[sp.ndof] = la^2 *(r_r-2.0*v[sp.ndof]+v[sp.ndof-1])/(sp.mh.h^2) 
    
   res[sp.ndof+1]   = la^2 * (v[sp.ndof+2]-2.0*v[sp.ndof+1]+ru_l)/(sp.mh.h^2)       
   res[2*sp.ndof]   = la^2 *(ru_r-2.0*v[2*sp.ndof]+v[2*sp.ndof-1])/(sp.mh.h^2) 
    
   res[2*sp.ndof+1] = la^2 * (v[2*sp.ndof+2]-2.0*v[2*sp.ndof+1]+e_l)/(sp.mh.h^2)        
   res[3*sp.ndof]   = la^2 *(e_r-2.0*v[3*sp.ndof]+v[3*sp.ndof-1])/(sp.mh.h^2)

   @simd for i in 2:sp.ndof-1
       res[i] = self.la^2 *(v[i+1]-2.0*v[i]+v[i-1])/(sp.mh.h^2) 
       res[sp.ndof+i] = self.la^2 *(v[sp.ndof+i+1]-2.0*v[sp.ndof+i]+v[sp.ndof+i-1])/(sp.mh.h^2) 
       res[2*sp.ndof+i] = self.la^2 *(v[2*sp.ndof+i+1]-2.0*v[2*sp.ndof+i]+v[2*sp.ndof+i-1])/(sp.mh.h^2) 
   end
   return res
end   


function EulerXJ_fields_to_xn(sp :: SpatialDis,v :: Vector{Float64})
    #v contains , r, ru, E, ru, ruu+p, (e+p)u
    @simd for i in 1:sp.ndof
        v[i] = sp.field[i,1]
        v[sp.ndof+i] = sp.field[i,2]
        v[2*sp.ndof+i] = sp.field[i,3]
        v[3*sp.ndof+i] = sp.field[i,4]
        v[4*sp.ndof+i] = sp.field[i,5]
        v[5*sp.ndof+i] = sp.field[i,6]
    end
end    

function EulerXJ_xn_to_fields(sp :: SpatialDis,v :: Vector{Float64})
    @simd for i in 1:sp.ndof
        sp.field[i,1] = v[i]
        sp.field[i,2] = v[sp.ndof+i]
        sp.field[i,3] = v[2*sp.ndof+i]
        sp.field[i,4] = v[3*sp.ndof+i]
        sp.field[i,5] = v[4*sp.ndof+i]
        sp.field[i,6] = v[5*sp.ndof+i]
    end
end 


struct LocalLaxAdv <: Flux
    L :: Float64
    a :: Float64
end

function (self::LocalLaxAdv)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})

    a = self.a
    rl = vL[1]
    rr = vR[1]
    data[1] = 0.5 * (a*rl+a*rr) - 0.5 * abs(a) * (rr-rl)
end

struct advection_cfl <: Flux
    a :: Float64
end

function (self::advection_cfl)(v, res)
    a = self.a 

    r   =   v[1]
    lf      = abs(a)
    max(res,a)
end

struct l1l2_norm <: Flux
end

function (self::l1l2_norm)(v::Array{Float64}, data::Array{Float64})
    r  =  v[1] # we cannot use direct v. why ?
    data[1]  =   abs(r)
    data[2]  =   r*r
    nothing
end

struct id_adv_mapping <: Flux
end

function (self::id_adv_mapping)(v::Array{Float64}, data::Array{Float64})  
    r  =  v[1]
    data[1] = r
    nothing
end

