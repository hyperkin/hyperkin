struct LocalLax <: Flux
    L     :: Float64
    gamma :: Float64
    pinf  :: Float64
end

function (self::LocalLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    gamma = self.gamma

    rl, ql, el = vL
    rr, qr, er = vR

    ul = ql/rl
    ur = qr/rr

    pl = (gamma-1.0) * (el - 0.5 * ql*ul) - gamma * pinf
    pr = (gamma-1.0) * (er - 0.5 * qr*ur) - gamma * pinf

    cl = sqrt(gamma *(pl+pinf)/rl)
    cr = sqrt(gamma *(pr+pinf)/rr)
    
    lf = 1.1*max(abs(ul-cl),abs(ur+cr))
    
    data[1] = 0.5 * (ql+qr) - 0.5 * lf* (rr-rl)
    data[2] = 0.5 * (ql*ul+pl+qr*ur+pr) - 0.5 * lf* (qr-ql)
    data[3] = 0.5 * ((el+pl)*ul +(er+pr)*ur) - 0.5 * lf* (er-el)
    nothing
end


struct euler_cfl <: Flux
    gamma :: Float64
    pinf :: Float64
end

function (self::euler_cfl)(x,v, res)
    gamma = self.gamma
    pinf = self.pinf
    
    r, q, e = v
    u       = q/r
    p      = (gamma-1.0) * (e- 0.5 * q*u) - gamma * pinf
    c       = sqrt(gamma *(p+pinf)/r)
    lf      = max(abs(u-c),abs(u+c))
    max(res,lf)
end

struct Primitive_mappingE <: Flux
    gamma :: Float64
    pinf :: Float64
end

function (self::Primitive_mappingE)(v::Vector{Float64}, data::Vector{Float64})
    gamma = self.gamma
    pinf = self.pinf
    r, q, e = v
    data[1] =  r 
    data[2] = q/r
    data[3] = (gamma-1.0) * (e- 0.5 * q*q/r) - gamma * pinf
    nothing 
end

struct l2normE <: Flux
end

function (self::l2normE)(v::Vector{Float64}, data::Vector{Float64})
    r, u, p = v
    data[1] =r*r
    data[2] =u*u
    data[3] =p*p
    nothing
end


struct LagrangeRemap <: Flux
    L     :: Float64
    gamma :: Float64
    pinf :: Float64    
end

function (self::LagrangeRemap)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    gamma = self.gamma
    rl, ql, el = vL
    rr, qr, er = vR
    ul, ur = ql/rl, qr/rr

    pl = (gamma-1.0) * (el- 0.5 * ql*ul) - gamma * pinf
    pr = (gamma-1.0) * (er- 0.5 * qr*ur) - gamma * pinf

    cl = sqrt(gamma *(pl+pinf)/rl)
    cr = sqrt(gamma *(pr+pinf)/rr)
    rc = 0.5*(rl*cl+rr*cr)
            
    ustar = 0.5*(ul+ur)+(0.5/rc)*(pl-pr)
    pstar = 0.5*(pl+pr)+(rc/2.0)*(ul-ur)              
    
    data[1] = 0.5 * ustar*(rl+rr) - 0.5 * abs(ustar)* (rr-rl)
    data[2] = 0.5 * ustar*(ql+qr) - 0.5 * abs(ustar)* (qr-ql) + pstar
    data[3] = 0.5 * ustar*(el+er) - 0.5 * abs(ustar)* (er-el) + pstar*ustar
end


struct PrimLimitingE <: Flux
    gamma :: Float64
    pinf  :: Float64
end

function (self::PrimLimitingE)(x,data::Vector{Float64},vC::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64},d)
    rc, qc, ec = vC  
    uc = qc/rc
    pc = (gamma-1.0) * (ec- 0.5 * qc*uc) - gamma * pinf
    
    rl, ql, el = vL  
    ul = ql/rl
    pl = (gamma-1.0) * (el- 0.5 * ql*ul) - gamma * pinf
    
    rr, qr, er = vR  
    ur = qr/rr
    pr = (gamma-1.0) * (er- 0.5 * qr*ur) - gamma * pinf
    
    dr = 0.5*minmod(rc-rl,rr-rc)
    du = 0.5*minmod(uc-ul,ur-uc)
    dp = 0.5*minmod(pc-pl,pr-pc)
    
    deltac = rc*max(-1.0,min(1.0,dr/rc))
    pcc = (gamma-1.0)*rc*(1.0+2.0*deltac*deltac/(rc*rc))
    deltau = sign(du)*sqrt(min(du*du,pc/pcc))
    deltap = pc*max(-1.0,min(1.0,dp/pc)) 
    
    rf = rc + d*deltac
    uf = uc + d*deltau
    pf = pc + d*deltap
    
    data[1] = rf
    data[2] = rf*uf 
    data[3] = 0.5*rf*uf*uf + (pf+gamma*pinf)/(gamma-1.0)
    nothing
end

