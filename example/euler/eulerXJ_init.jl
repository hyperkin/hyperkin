abstract type InitFunction end

struct contact <: InitFunction
    L     :: Float64
    gamma :: Float64
    u0    :: Float64
end

function (self::contact)(x, t)
    sigma = 0.05
    x0    = 0.5*self.L
    u=self.u0
    r     = exp(-0.5*((x-x0-u*t)/sigma)^2)+1.0
    p=1.0
    ru=r*u
    E=0.5*r*u*u+p/(gamma-1.0)

    [r,ru,E,ru,r*u*u+p,E*u+p*u]
end

struct contactAdv <: InitFunction
    L     :: Float64
    a     :: Float64
end

function (self::contactAdv)(x, t)
    sigma = 0.05
    x0    = 0.5*self.L
    r     = exp(-0.5*((x-x0-self.a*t)/sigma)^2)+1.0

    [r]
end