abstract type InitFunction end


struct Wave_d1q3n3 <: InitFunction
    L       :: Float64
    lambda_m  :: Float64
    lambda_0  :: Float64
    lambda_p  :: Float64
    scheme    :: Integer
    u0        :: Float64
    alpha     :: Float64
    gamma     :: Float64
end

function (self::Wave_d1q3n3)(x, t)
    lp   = self.lambda_p
    lm   = self.lambda_m
    l0   = self.lambda_0  
    sigma2 = 0.005
    x0    = 0.5*self.L
    r     = 0.1*exp(-0.5*((x-x0)^2)/sigma2)+1.0
    u     = u0
    ru    = r*u
    p     = r
    e     = 0.5*r*u*u+p/(self.gamma-1.0) 
    
    if self.scheme==1
        Fr_m=-lm/(lp-lm)*(r*u-lp*r)
        Fr_p=lp/(lp-lm)*(r*u-lm*r)
        Fru_m=-lm/(lp-lm)*(r*u*u+p-lp*ru)
        Fru_p=lp/(lp-lm)*(r*u*u+p-lm*ru)
        Fe_m=-lm/(lp-lm)*((e+p)*u-lp*e)
        Fe_p=lp/(lp-lm)*((e+p)*u-lm*e)
    else
        Fr_m= 0.5*(r*u +self.alpha*r*u*u/lm +p/lm)
        Fr_p= 0.5*(r*u +self.alpha*r*u*u/lp +p/lp)
        Fru_m= 0.5*(r*u*u +self.alpha*r*u*u*u/lm +p*(1+self.gamma*u/lm))
        Fru_p= 0.5*(r*u*u +self.alpha*r*u*u*u/lp +p*(1+self.gamma*u/lp))     
        Fe_m= 0.5*(e*u+ self.alpha*e*u*u/lm+(p*u+self.gamma*(u*u+lm*lm)*p/lm))
        Fe_p= 0.5*(e*u+ self.alpha*e*u*u/lp+(p*u+self.gamma*(u*u+lp*lp)*p/lp))
    end
 
    solr=[-Fr_m/(l0-lm), r-(Fr_p/(lp-l0) -Fr_m/(l0-lm)) ,Fr_p/(lp-l0)]
    solru=[-Fru_m/(l0-lm), ru-(Fru_p/(lp-l0) -Fru_m/(l0-lm)) ,Fru_p/(lp-l0)]
    sole=[-Fe_m/(l0-lm), e-(Fe_p/(lp-l0) -Fe_m/(l0-lm)) ,Fe_p/(lp-l0)]
    
    [solr[1],solr[2],solr[3],solru[1],solru[2],solru[3],sole[1],sole[2],sole[3]]
end

struct Contact_d1q3n3 <: InitFunction
    L       :: Float64
    lambda_m  :: Float64
    lambda_0  :: Float64
    lambda_p  :: Float64
    scheme    :: Integer
    u0        :: Float64
    alpha     :: Float64
    gamma     :: Float64
end

function (self::Contact_d1q3n3)(x, t)
    lp   = self.lambda_p
    lm   = self.lambda_m
    l0   = self.lambda_0  
    sigma = 0.05
    x0    = 0.5*self.L
    w     = 0.5*(1-tanh(10*(x-self.u0*t -x0)))
    r     = 2*w+(1.0-w)
    u     = self.u0
    ru    = r*u
    p     = 1.0
    e     = 0.5*r*u*u+p/(self.gamma-1.0) 
    
    if self.scheme==1
        Fr_m=-lm/(lp-lm)*(r*u-lp*r)
        Fr_p=lp/(lp-lm)*(r*u-lm*r)
        Fru_m=-lm/(lp-lm)*(r*u*u+p-lp*ru)
        Fru_p=lp/(lp-lm)*(r*u$u+p-lm*ru)
        Fe_m=-lm/(lp-lm)*((e+p)*u-lp*e)
        Fe_p=lp/(lp-lm)*((e+p)*u-lm*e)
    else
        Fr_m= 0.5*(r*u +self.alpha*r*u*u/lm +p/lm)
        Fr_p= 0.5*(r*u +self.alpha*r*u*u/lp +p/lp)
        Fru_m= 0.5*(r*u*u +self.alpha*r*u*u*u/lm +p*(1+self.gamma*u/lm))
        Fru_p= 0.5*(r*u*u +self.alpha*r*u*u*u/lp +p*(1+self.gamma*u/lp))     
        Fe_m= 0.5*(e*u+ self.alpha*e*u*u/lm+(p*u+self.gamma*(u*u+lm*lm)*p/lm))
        Fe_p= 0.5*(e*u+ self.alpha*e*u*u/lp+(p*u+self.gamma*(u*u+lp*lp)*p/lp))
    end
 
    solr=[-Fr_m/(l0-lm), r-(Fr_p/(lp-l0) -Fr_m/(l0-lm)) ,Fr_p/(lp-l0)]
    solru=[-Fru_m/(l0-lm), ru-(Fru_p/(lp-l0) -Fru_m/(l0-lm)) ,Fru_p/(lp-l0)]
    sole=[-Fe_m/(l0-lm), e-(Fe_p/(lp-l0) -Fe_m/(l0-lm)) ,Fe_p/(lp-l0)]
    
    [solr[1],solr[2],solr[3],solru[1],solru[2],solru[3],sole[1],sole[2],sole[3]]
end

 

