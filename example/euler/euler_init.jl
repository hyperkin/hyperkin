abstract type InitFunction end

struct GaussianE <: InitFunction
    L       :: Float64
    gamma :: Float64
    pinf  :: Float64    
end

function (self::GaussianE)(x, t)
    sigma = 0.04
    x0    = 0.5*self.L
    r     = 1.0 + 0.2*exp(-0.5*((x-x0)/sigma)^2)
    u     = 0.0
    p     = 0.1*exp(-0.5*((x-x0)/sigma)^2)+0.1
    
    [r, r*u, 0.5*r*u*u + (p+self.gamma*pinf)/(self.gamma-1.0)]
end


struct StationnaryContactE <: InitFunction
     L     :: Float64
     gamma :: Float64
     pinf  :: Float64
end

function (self::StationnaryContactE)(x, t)
    
    x0    = self.L/2.0
    sigma = 0.02
    u     = 0.0
    p     = 1.0
    rl    = 1.0
    rr    = 0.1
    w     = 0.5 * (1.0 - tanh(100000*(x-x0)))
    r     = w * rl + (1.0-w) * rr
    
    [r, r*u, 0.5*r*u*u + (p+self.gamma*pinf)/(self.gamma-1.0)]
end

export SmoothContactE

struct SmoothContactE <: InitFunction
     L     :: Float64
     gamma :: Float64
     pinf  :: Float64
end

function (self::SmoothContactE)(x, ti)
    
    x0    = self.L/2.0
    sigma = 0.02
    u     = 0.01
    p     = 1.0
    xr = x - u*ti
    r     = 0.1 +  0.2*(1.0/(sigma*sqrt(pi)))*exp(-0.5*((xr-x0)/sigma)^2)
    
    [ r, r*u, 0.5*r*u*u + (p+self.gamma*pinf)/(self.gamma-1.0)]

end


export LowMachPulseE

struct LowMachPulseE <: InitFunction
     L     :: Float64
     gamma :: Float64
     pinf  :: Float64
end

function (self::LowMachPulseE)(x, t)
    
    x0 = self.L/2.0
    Lx = 0.5 * self.L
    M_ref = 1.0/11.0
    scaling = 1.0/(M_ref^2)
    r = 0.995 + M_ref*(1.0-cos(2.0*pi*x/Lx))
    u = -sqrt(self.gamma)*sign(x)*(1.0-cos(2π*x/Lx))
    p = scaling*(1.0 + M_ref*self.gamma*(1.0-cos(2π*x/Lx)))
    [ r, r*u, 0.5*r*u*u + (p+self.gamma*pinf)/(self.gamma-1.0)]

end

export AcousticWaveE

struct AcousticWaveE <: InitFunction
     L     :: Float64
     gamma :: Float64
     pinf  :: Float64
end

function (self::AcousticWaveE)(x, t)

  x0    = self.L/2.0
  sigma = 0.02
  r     = 2.0 + 0.05*(1.0/(sigma*sqrt(pi)))*exp(-0.5*((x-x0)/sigma)^2)
  u     = 0.0
  p     = 0.5 + 0.1*(1.0/((4.0*sigma)*sqrt(pi)))*exp(-0.5*((x-x0)/(4.0*sigma))^2)
    
  [ r, r*u, 0.5*r*u*u + (p+self.gamma*pinf)/(self.gamma-1.0)]

end

export SODE

struct SODE <: InitFunction
     L     :: Float64
     gamma :: Float64
     pinf  :: Float64
end

function (self::SODE)(x, t)

    gamma = self.gamma
    L     = self.L
    pinf  = self.pinf

    M = 0.9
    x0  = L/2.0
    eps = 0.00000000001
    rl  = 1.0 
    pl  = 1.0
    rr  = 0.125 
    pr  = 1.0-M
    u0  = 0.0 # 0.0
    mu2 = (gamma-1.0)/(gamma +1.0)
    
    cl  = sqrt(gamma*(pl+pinf)/rl)
    cr  = sqrt(gamma*(pr+pinf)/rr)
    
    pm  = bisect(pr,pl,eps,gamma,pl,pr,cl,cr)
    rml = rl*(pm/pl)^(1/gamma)
    um  = 2.0*cl/(gamma-1.0)*(1.0-(pm/pl)^((gamma-1)/(2*gamma)))
    rmr = rr*((pm+mu2*pr)/(pr+mu2*pm))
    us  = um/(1-rr/rmr)
    ut  = cl-um/(1-mu2)
    
    if x<= -cl*t
        r = rl
        u = u0
        ei = (pl+gamma*pinf)/(gamma-1.0)    
    elseif x<= -ut*t
        r = rl *(-mu2* (x/(cl *t))+(1.0-mu2))^(2/(gamma-1))
        u = (1.0-mu2)*(x/t+cl)
        ei = (pl *(-mu2* (x/(cl *t))+(1.0-mu2))^((2*gamma)/(gamma-1)))/(gamma-1) + gamma*pinf/(gamma-1)
    elseif x<= um*t
        r = rml
        u = um 
        ei = (pm+gamma*pinf)/(gamma-1)
    elseif x <= us*t
        r = rmr
        u = um
        ei = (pm+gamma*pinf)/(gamma-1)
    else
        r = rr
        u = u0
        ei = (pr+gamma*pinf)/(gamma-1)
    end
   
    return [ r, r*u, 0.5*r*u*u + ei ]
end

function func_pressure(pm,gammaE,pL,pR,cL,cR)
    pp=0
    mu2= (gammaE-1.0)/(gammaE +1.0)

    pp1=-2*cL*(1-(pm/pL)^((-1+gammaE)/(2*gammaE)))/(cR*(-1+gammaE))
    pp2=(-1+pm/pR)*((1-mu2)/(gammaE*(mu2+pm/pR)))^0.5

    pp=pp1+pp2
    return pp
end

export bisect

function bisect(a,b,tol,gamma,pL,pR,cL,cR)

    it  = 0
    err = 0

    fa = func_pressure(a,gamma,pL,pR,cL,cR)
    fb = func_pressure(b,gamma,pL,pR,cL,cR)

    if sign(fa) == sign(fb)
        println("Root not in bracket")
    end
        
    while (b-a) > tol
        c  = a + 0.5 * (b-a)
        fc = func_pressure(c,gamma,pL,pR,cL,cR)
        if sign(fa)*sign(fc) <= 0
            b  = c
            fb = fc
        else
            a  = c
            fa = fc
        end
    end

    xb  = 0.5 * (a+b)
    err = 0.5 * abs(b-a)

    return xb

end


export SOD_2E

struct SOD_2E <: InitFunction
     L     :: Float64
     gamma :: Float64
     pinf  :: Float64
end

function (self::SOD_2E)(x, t)

    x0    = self.L/2.0
    w     = 0.5 * (1.0 - tanh(12*(x-x0)))
    rl=1.0
    rr=0.125
    u=0.0
    pl=1.0
    pr=0.1
    p=w*pl+(1-w)*pr
    r=w*rl+(1-w)*rr
    
    [r, r*u, 0.5*r*u*u + (p+self.gamma*pinf)/(self.gamma-1.0)]
end
