abstract type InitFunction end

struct StationnaryContact <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
     gamma  :: Float64
end

function (self::StationnaryContact)(x, t)
    
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    sigma = 0.02
    ux     = 0.0
    uy     = 0.0
    p     = 1.0
    rl    = 1.0
    rr    = 0.1
    w     = 0.5 * (1.0 - erf(1000*(x[1]-x0[1])))
    r     = w * rl + (1.0-w) * rr
    
    [r, r*ux, r*uy, 0.5*r*(ux*ux+uy*uy) + p/(self.gamma-1.0)]
end

struct SmoothContact <: InitFunction
     Lx    :: Float64
     Ly    :: Float64
     gamma :: Float64
end

function (self::SmoothContact)(x, t)
    
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    sigma = 0.05
    sigma2 = 0.05*0.05
    ux     = 0.01
    uy     = 0.01
    p     = 1.0
    norm2 = (x[1]-x0x-ux*t)^2 + (x[2]-x0y-uy*t)^2
    r     = 0.1 +  0.2*(1.0/(sigma*sqrt(pi)))*exp(-0.5*(norm2/sigma2))
    
    [r, r*ux, r*uy, 0.5*r*(ux*ux+uy*uy) + p/(self.gamma-1.0)]

end


struct SmoothContact2 <: InitFunction
     Lx    :: Float64
     Ly    :: Float64
     gamma :: Float64
end

function (self::SmoothContact2)(x, t)
    
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    sigma2 = 0.02
    sigma  = sqrt(0.02)
    
    norm2 = (x[1]-x0x)^2 + (x[2]-x0y)^2
    ux     = -0.001*(x[2]-x0y)*(1.0/(sigma*sqrt(pi)))*exp(-0.5*(norm2/sigma2))
    uy     = 0.001*(x[1]-x0x)*(1.0/(sigma*sqrt(pi)))*exp(-0.5*(norm2/sigma2))
    p     = 1.0
    
    norm2 = (x[1]-x0x-ux*t)^2 + (x[2]-x0y-uy*t)^2
    r     = 0.1 +  0.2*(1.0/(sigma*sqrt(pi)))*exp(-0.5*(norm2/sigma2))
    
    [r, r*ux, r*uy, 0.5*r*(ux*ux+uy*uy) + p/(self.gamma-1.0)]
end


struct DensityWave <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
     gamma  :: Float64
end

function (self::DensityWave)(x, t) 
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    sigma2 = 0.05*0.05
    sigma  = 0.05 
    norm2 = (x[1]-x0x)^2 + (x[2]-x0y)^2
    r     = 0.1 +  0.2*(1.0/(sigma*sqrt(pi)))*exp(-0.5*(norm2/sigma2))
    ux    = 0.0
    uy    = 0.0
    p     = 1.0
    [r, r*ux, r*uy, 0.5*r*(ux*ux+uy*uy)+p/(self.gamma-1.0)]
end

struct Shock <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
     gamma  :: Float64
end

function (self::Shock)(x, t) 
    x0x    = self.Lx/2.0
    x0y    = self.Ly/2.0
    sigma2 = 0.05*0.05
    sigma  = 0.05 
    norm2 = (x[1]-x0x)^2 + (x[2]-x0y)^2
    p = 0.1
    r = 0.125
    ux    = 0.0
    uy    = 0.0
    if sqrt(norm2) < 0.1
        p     = 1.0
        r     = 1.0
    end    
    [r, r*ux, r*uy, 0.5*r*(ux*ux+uy*uy)+p/(self.gamma-1.0)]
end

struct GreshoVortex <: InitFunction
     Lx    :: Float64
     Ly    :: Float64
     gamma :: Float64
end

function (self::GreshoVortex)(x, t)
    
    x0 = zeros(Float64,2)
    x0[1]    = self.Lx/2.0
    x0[2]    = self.Ly/2.0
    norm2 = (x[1]-x0[1])^2 + (x[2]-x0[2])^2
    
    sigma = 0.02
    r     = 1.0
    M = 0.1
    pc  = 1.0/(M*M)-1.0/2.0
    
    ep_x = -(x[2]-x0[2])/sqrt(norm2)
    ep_y = (x[1]-x0[1])/sqrt(norm2)
    
    if sqrt(norm2) <0.2  
        ux    = ep_x * 5*sqrt(norm2)
        uy    = ep_y * 5*sqrt(norm2)
        p     = pc + 25.0/2.0*norm2    
    elseif sqrt(norm2) <0.4
        ux    = ep_x * (2.0-5.0*sqrt(norm2))
        uy    = ep_y * (2.0-5.0*sqrt(norm2))
        p     = pc + 4.0*log(5.0*sqrt(norm2))+4-20*sqrt(norm2)+ 25.0/2.0*norm2           
    else 
        ux    = 0.0
        uy    = 0.0
        p     = pc + 4.0*log(2.0) -2.0     
    end  
        
    [r, r*ux, r*uy, 0.5*r*(ux*ux+uy*uy) + p/(self.gamma-1.0)]
end

export KevinH

struct KevinH <: InitFunction
     Lx    :: Float64
     Ly    :: Float64
     gamma :: Float64
end

function (self::KevinH)(x, t) 
    x0 = zeros(Float64,2)
    
    sigma = 0.02
    r     = 1.0
    M = 0.1
    pc  = 1.0/(M*M)
    r1 =1.0
    r2 =2.0
    rm =(r1-r2)/2.0
    u1 =0.5
    u2 = -0.5
    um = (u1-u2)/2.0
    L = 0.025
    
    if x[2] <0.25 
        r = r1-rm*exp((x[2]-0.25)/L)
        ux = u1-um*exp((x[2]-0.25)/L)
    elseif x[2] <0.5 && x[2] >= 0.25
        r = r2+rm*exp((-x[2]+0.25)/L)
        ux = u2+um*exp((-x[2]+0.25)/L)          
    elseif x[2] <0.75 && x[2] >= 0.5
        r = r2+rm*exp((x[2]-0.75)/L)
        ux = u2+um*exp((x[2]-0.75)/L) 
    else    
        r = r1-rm*exp(-(x[2]-0.75)/L)
        ux = u1-um*exp(-(x[2]-0.75)/L)      
    end  
    p = pc*2.5
    uy = 0.01*sin(2.0*pi*x[1])
        
    [r, r*ux, r*uy, 0.5*r*(ux*ux+uy*uy) + p/(self.gamma-1.0)]
end


struct Riemann2D <: InitFunction
     Lx    :: Float64
     Ly    :: Float64
     gamma :: Float64
end

function (self::Riemann2D)(x, t)
    
    if x[1] < 0.5 && x[2] > 0.5  
        r     = 0.5323
        ux    = 1.206
        uy    = 0.0
        p     = 0.3   
    elseif x[1] <0.5 && x[2] < 0.5  
        r     = 0.138
        ux    = 1.206
        uy    = 1.206
        p     = 0.029   
    elseif x[1] >0.5 && x[2] < 0.5  
        r     = 0.5323
        ux    = 0.0
        uy    = 1.206
        p     = 0.3          
    else 
        r     = 1.5
        ux    = 0.0
        uy    = 0.0
        p     = 1.5     
    end  
        
    [r, r*ux, r*uy, 0.5*r*(ux*ux+uy*uy) + p/(self.gamma-1.0)]
end
