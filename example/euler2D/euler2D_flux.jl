
struct Rusanov <: Flux
    Lx :: Float64
    Ly :: Float64
    gamma :: Float64
end

function (self::Rusanov)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    
    rl = vC[1]
    rr = vR[1]
    uxl = vC[2]/vC[1]
    uxr = vR[2]/vR[1]
    uyl = vC[3]/vC[1]
    uyr = vR[3]/vR[1]
    el = vC[4]
    er = vR[4]
    
    unl = uxl*n[1]+uyl*n[2]
    unr = uxr*n[1]+uyr*n[2]
    
    pl = (self.gamma-1.0)*(el -0.5*rl*(uxl*uxl+uyl*uyl))
    pr = (self.gamma-1.0)*(er -0.5*rr*(uxr*uxr+uyr*uyr))
    cl = sqrt(self.gamma*pl/rl)
    cr = sqrt(self.gamma*pr/rr)
    s = max(abs(unl+cl),abs(unr+cr),abs(unl-cl),abs(unr-cr))

    data[1] = 0.5 *(rl*unl+rr*unr) - 0.5 * s*  (rr-rl)
    data[2] = 0.5 *(rl*unl*uxl + rr*unr*uxr +(pl+pr)*n[1]) - 0.5 * s * (rr*uxr-rl*uxl)
    data[3] = 0.5 *(rl*unl*uyl + rr*unr*uyr +(pl+pr)*n[2]) - 0.5 * s * (rr*uyr-rl*uyl)
    data[4] = 0.5 *((el+pl)*unl+(er+pr)*unr) - 0.5 * s * (er-el)
end


struct euler_cfl <: Flux
    gamma :: Float64
end

function (self::euler_cfl)(x,v, res)
    r, rux, ruy, e   =   v
    ux = rux/r
    uy = ruy/r
    normu = sqrt(ux*ux+uy*uy)
    p = (self.gamma-1.0)*(e -0.5*r*(ux*ux+uy*uy))
    c = sqrt(self.gamma*p/r)
    lf      = max(abs(normu+c),abs(normu-c))
    max(res,lf)
end

struct l2_norm <: Flux
end

function (self::l2_norm)(v::Array{Float64}, data::Array{Float64})
    r, ux, uy, p  =  v
    data[1]  =  r*r
    data[2]  =  ux*ux + uy*uy
    data[3]  =  p*p
    nothing
end

struct primitive_euler_mapping <: Flux
    gamma :: Float64
end

function (self::primitive_euler_mapping)(v::Array{Float64}, data::Array{Float64})  
    r, rux, ruy, e   =   v
    ux = rux/r
    uy = ruy/r
    p = (self.gamma-1.0)*(e -0.5*r*(ux*ux+uy*uy))
    data[1] = r
    data[2] = ux
    data[3] = uy
    data[4] = p
    nothing
end

struct mach_euler_mapping <: Flux
    gamma :: Float64
end

function (self::mach_euler_mapping)(v::Array{Float64}, data::Array{Float64})  
    r, rux, ruy, e   =   v
    ux = rux/r
    uy = ruy/r
    p = (self.gamma-1.0)*(e -0.5*r*(ux*ux+uy*uy))
    c = sqrt(p*self.gamma/r)
    data[1] = sqrt(ux*ux+uy*uy)/c
    data[2] = ux
    data[3] = uy
    data[4] = p
    nothing
end
