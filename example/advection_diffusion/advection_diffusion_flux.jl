struct l1l2_norm <: Flux
end

function (self::l1l2_norm)(v::Array{Float64}, data::Array{Float64})
    r  =  v[1] # we cannot use direct v. why ?
    data[1]  =   abs(r)
    data[2]  =   r*r
    nothing
end

struct id_advdiff_mapping <: Flux
end

function (self::id_advdiff_mapping)(v::Array{Float64}, data::Array{Float64})  
    r  =  v[1]
    data[1] = r
    nothing
end

