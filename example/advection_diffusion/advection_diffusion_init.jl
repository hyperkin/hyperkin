abstract type InitFunction end

struct initadv_periodic <: InitFunction
    L     :: Float64
    a     :: Float64
    d     :: Float64
end

function (self::initadv_periodic)(x, t)
    sigma = 0.1
    x0    = 0.5*self.L
    xr    = (x-self.a*t) % self.L
    if xr < 0.0
             xr = xr + self.L
    elseif xr > self.L  
             xr = xr - self.L
    end
    r     = exp(-0.5*((xr-x0)/sigma)^2)+0.01

    [r]
end

struct initdiff <: InitFunction
    L     :: Float64
    a     :: Float64
    d     :: Float64
end

function (self::initdiff)(x, t)
    d  = self.d
    t0    = 0.02
    x0    = 0.5*self.L
    var = 4.0*d*(t+t0)
    r     = (1.0/sqrt(var*pi))*exp(-( (x-x0)^2/var ))

    [r]
end