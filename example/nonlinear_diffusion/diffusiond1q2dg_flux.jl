struct LocalLax_d1q2 <: Flux
    L :: Float64
    lambda :: Float64
    eps    :: Float64
end

function (self::LocalLax_d1q2)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    l = self.lambda
    eps = self.eps
    fml, fpl = vL
    fmr, fpr = vR

    data[1] =0.5 * -l/eps*(fml+fmr) - 0.5 * l/eps * (fmr-fml)
    data[2] =0.5 * l/eps*(fpl+fpr) - 0.5 * l/eps * (fpr-fpl)
    nothing
end

struct LocalLax_d1q3 <: Flux
    L :: Float64
    lambda :: Float64
    eps    :: Float64
end

function (self::LocalLax_d1q3)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    l = self.lambda
    eps = self.eps
    fml, f0l, fpl = vL
    fmr, f0r, fpr = vR

    data[1] =0.5 * -l/eps*(fml+fmr) - 0.5 * l/eps * (fmr-fml)
    data[2] =0.0
    data[3] =0.5 * l/eps*(fpl+fpr) - 0.5 * l/eps * (fpr-fpl)
    nothing
end

struct l2_norm <: Flux
end

function (self::l2_norm)(v::Vector{Float64},data::Vector{Float64})
    r, u   =  v
    data[1] = r*r
    nothing
end

struct eq_d1q2_diff <: Equilibrium
    lambda :: Float64
    p :: Float64
    d :: Float64
end

function (self::eq_d1q2_diff)(v::Vector{Float64},data::Vector{Float64})
    l = self.lambda
    fm, fp   =  v
    r = fm + fp

    data[1] = 0.5*r
    data[2] = 0.5*r
    nothing
end

struct eq_d1q3_diff <: Equilibrium
    lambda :: Float64
    p      :: Float64
    d      :: Float64
    w0     :: Float64
    w      :: Float64
end

function (self::eq_d1q3_diff)(v::Vector{Float64},data::Vector{Float64})
    l = self.lambda
    fm, f0, fp   =  v
    r = fm + f0 + fp

    data[1] = self.w*r
    data[2] = self.w0*r
    data[3] = self.w*r
    nothing
end

struct omega_d1q2_diff <: Equilibrium
    lambda :: Float64
    p :: Float64
    d :: Float64
    dt :: Float64
end

function (self::omega_d1q2_diff)(x,f::Vector{Float64},v::Vector{Float64},data::Vector{Float64})
    l = self.lambda
    alpha = l*l
    p = self.p
    d = self.d
    dt= self.dt
    fm, fp  =  f
    r = fm + fp
    
    eps = sqrt(dt)
    theta=0.5
    coeff_D = d*abs(r)^p
    A= zeros(2,2); B=zeros(2,2)
    A[1,1]=eps^2+theta*dt/coeff_D*alpha
    A[2,2]=eps^2+theta*dt/coeff_D*alpha
    B[1,1]=dt/coeff_D*alpha
    B[2,2]=dt/coeff_D*alpha
    C=inv(A)*B
    res=C*v
    data[1]=res[1]
    data[2]=res[2]
    nothing
end


struct omega_d1q3_diff <: Equilibrium
    lambda :: Float64
    p :: Float64
    d :: Float64
    dt :: Float64
    w0 :: Float64
    w  :: Float64
end

function (self::omega_d1q3_diff)(x,f::Vector{Float64},v::Vector{Float64},data::Vector{Float64})
    l = self.lambda
    alpha =l*l
    p = self.p
    d = self.d
    dt= self.dt
    fm, f0, fp  =  f
    r = fm + f0 + fp
    
    eps = sqrt(dt)
    theta=0.5
    coeff_D = d*abs(r)^p
    A= zeros(3,3); B=zeros(3,3)
    A[1,1]=eps^2+theta*dt/coeff_D*(1.0-self.w0)*alpha
    A[2,2]=eps^2+theta*dt/coeff_D*(1.0-self.w0)*alpha
    A[3,3]=eps^2+theta*dt/coeff_D*(1.0-self.w0)*alpha
    B[1,1]=dt/coeff_D*(1.0-self.w0)*alpha
    B[2,2]=dt/coeff_D*(1.0-self.w0)*alpha
    B[3,3]=dt/coeff_D*(1.0-self.w0)*alpha
    C=inv(A)*B
    res=C*v
    data[1]=res[1]
    data[2]=res[2]
    data[3]=res[3]
    nothing
end



struct d1q2_mapping <: Flux
    lambda :: Float64
end

function (self::d1q2_mapping)(v::Vector{Float64},data::Vector{Float64})
    r   =   v[1] + v[2]
    u   =   lambda*(v[2]-v[1])
    data[1] = r
    data[2] = u
    nothing
end


struct d1q3_mapping <: Flux
    lambda :: Float64
end

function (self::d1q3_mapping)(v::Vector{Float64},data::Vector{Float64})
    r   =   v[1] + v[2] + v[3]
    u   =   lambda*(v[3]-v[1])
    data[1] = r
    data[2] = u
    nothing
end

struct id_d1q2_mapping <: Flux
    lambda :: Float64
end

function (self::id_d1q2_mapping)(v::Vector{Float64},data::Vector{Float64})
    data[1] = v[1]
    data[2] = v[2]
    nothing
end

struct id_d1q3_mapping <: Flux
    lambda :: Float64
end

function (self::id_d1q2_mapping)(v::Vector{Float64},data::Vector{Float64})
    data[1] = v[1]
    data[2] = v[2]
    data[3] = v[3]
    nothing
end


struct ope_d1qndg <: Operator

end

function (self::ope_d1qndg)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.nvar*sp.ndof)
    
    for k in 1:sp.nvar
        sp.field[:,k] .= v[(k-1)*sp.ndof+1:k*sp.ndof]
    end   
    bc_neumann(sp)
    sp()
    for k in 1:sp.nvar
        res[(k-1)*sp.ndof+1:k*sp.ndof] .= sp.flux[:,k] ### The operator Dg is -Adx and the right side of ode gives is the same 
    end    
    return res
end   