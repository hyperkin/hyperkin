abstract type InitFunction end

struct solfondD1Q2 <: InitFunction
    L       :: Float64
    lambda  :: Float64
    p       :: Integer
    d       :: Float64
end

function (self::solfondD1Q2)(x, t)
    d     = self.d
    l     = self.lambda
    t0    = 0.02
    x0    = 0.5*self.L
    var   = 4.0*d*(t+t0)
    r     = 0.1+(1.0/sqrt(var*pi))*exp(-( (x-x0)^2/var ))

    [0.5*r, 0.5*r]
end

struct solfondD1Q3 <: InitFunction
    L       :: Float64
    lambda  :: Float64
    p       :: Integer
    d       :: Float64
    w0      :: Float64
    w       :: Float64
end

function (self::solfondD1Q3)(x, t)
    d     = self.d
    l     = self.lambda
    t0    = 0.02
    x0    = 0.5*self.L
    var   = 4.0*d*(t+t0)
    r     = 0.1+(1.0/sqrt(var*pi))*exp(-( (x-x0)^2/var ))

    [self.w*r, self.w0*r, self.w*r]
end
