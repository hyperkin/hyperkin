struct twoPointVF <: Flux
    L ::   Float64
    p ::   Integer
    d ::   Float64
end

function (self::twoPointVF)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})

    rl = vL[1]
    rr = vR[1]
    
    coef=0.5*(abs(rl)^p+abs(rr)^p)*d
    data[1] = - coef*(rr-rl)/h
end

struct  diffusion_cfl <: Flux
    L ::   Float64
    p ::   Integer
    d ::   Float64
end

function (self::diffusion_cfl)(x,v, res)
    r   =   v[1]
    lf      = abs(r)
    max(1.0/d,lf)
end


struct l1l2_norm <: Flux
end

function (self::l1l2_norm)(v::Array{Float64}, data::Array{Float64})
    r  =  v[1] # we cannot use direct v. why ?
    data[1]  =   abs(r)
    data[2]  =   r*r
    nothing
end

struct id_diff_mapping <: Flux
end

function (self::id_diff_mapping)(v::Array{Float64}, data::Array{Float64})  
    r  =  v[1]
    data[1] = r
    nothing
end

