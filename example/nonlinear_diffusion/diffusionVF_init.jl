abstract type InitFunction end


struct solfond <: InitFunction
    L     :: Float64
    p     :: Float64
    d     :: Float64
end

function (self::solfond)(x, t)
    d  = self.d
    t0    = 0.02
    x0    = 0.5*self.L
    var = 4.0*d*(t+t0)
    r     = 0.1+(1.0/sqrt(var*pi))*exp(-( (x-x0)^2/var ))

    [r]
end