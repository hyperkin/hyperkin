


struct LocalLax <: Flux
    Lx :: Float64
    Ly :: Float64
    ax :: Float64
    ay :: Float64
end

function (self::LocalLax)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})

    an = self.ax*n[1]+self.ay*n[2]
    rl = vC[1]
    rr = vR[1]
    data[1] = 0.5 * (an*rl+an*rr) - 0.5 * abs(an) * (rr-rl)
end

struct Diff <: Flux
    Lx :: Float64
    Ly :: Float64
    d  :: Float64
end

function (self::Diff)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})

    rl = vC[1]
    rr = vR[1]
    data[1] = - d * (rr-rl)/sqrt(h)
end

struct advection_cfl <: Flux
    ax :: Float64
    ay :: Float64
end

function (self::advection_cfl)(x,v, res)
    ax = self.ax 
    ay = self.ay

    r   =   v[1]
    lf      = sqrt(ax*ax+ay*ay)
    max(res,lf)
end

struct l1l2_norm <: Flux
end

function (self::l1l2_norm)(v::Array{Float64}, data::Array{Float64})
    r  =  v[1]
    data[1]  =   abs(r)
    data[2]  =   r*r
    nothing
end

struct id_adv_mapping <: Flux
end

function (self::id_adv_mapping)(v::Array{Float64}, data::Array{Float64})  
    r  =  v[1]
    data[1] = r
    nothing
end

struct ope_impvf2D <: Operator

end

function (self::ope_impvf2D)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.nvar*sp.ndof)
    
    for k in 1:sp.nvar
        sp.field[:,k] .= v[(k-1)*sp.ndof+1:k*sp.ndof]
    end   
    bc_periodic(sp)
    sp()
    for k in 1:sp.nvar
        res[(k-1)*sp.ndof+1:k*sp.ndof] .= sp.flux[:,k] ### The operator Dg is -Adx and the right side of ode gives is the same   
    end    
    return res
end   
