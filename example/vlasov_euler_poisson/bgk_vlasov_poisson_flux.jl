struct MinModLimiting <: Flux
    Mv      :: Mesh
end


function (self::MinModLimiting)(x,data::Vector{Float64}, vC::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64},d)
    for i in 1:self.Mv.Nc
        dv = 0.5*minmod(vC[i]-vL[i],vR[i]-vC[i])
        deltav = max(-abs(vC[i]),min(abs(vC[i]),dv))
        data[i] = vC[i] + d*deltav
    end
    data[end] = 0.0
    nothing 
end

struct LocalLax <: Flux
    L       :: Float64
    Mv      :: Mesh
end

function (self::LocalLax)(x,h,data::Vector{Float64}, vL::Vector{Float64},vR::Vector{Float64})
    for i in 1:self.Mv.Nc
        vel = self.Mv.centers[i] 
        data[i] = 0.5*vel*(vL[i]+vR[i]) - 0.5*abs(vel)*(vR[i]-vL[i])
    end  
    data[end] = 0.0
    nothing
end

struct Elecflux <: Flux
    L       :: Float64
    Mv      :: Mesh
end

function (self::Elecflux)(xl,xr,hl,hr,data::Vector{Float64}, vL::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64})
    h=0.5*(hl+hr)
    for i in 2:self.Mv.Nc-1
        data[i] = - h*vC[end]*(vC[i+1]-vC[i-1])/(2.0*self.Mv.h)
    end    
    if -vC[end] > 0.0
        data[1] = h*0.0
        data[end-1] = h*0.5*vC[end]*vC[end-1]
    else
        data[1] = -h*0.5*vC[end]*vC[1]
        data[end-1] = h*0.0
    end    
    data[end] = 0.0 
    nothing
end

struct l2_norm <: Flux
end

function (self::l2_norm)(v::Vector{Float64},data::Vector{Float64})
    r, u, p, e, kin, Q, m4, m5  =  v[1:8]
    data[1] = r*r
    data[2] = u*u
    data[3] = p*p
    data[4] = e*e
    data[5] = kin
    data[6] = Q
    data[7] = m4
    data[8] = m5
    nothing
end

struct eq_bgk <: Equilibrium
    Mv      :: Mesh
end

function (self::eq_bgk)(v::Vector{Float64},data::Vector{Float64})
    r = 0.0
    q = 0.0
    p = 0.0
    for i in 1:self.Mv.Nc
        r = r + self.Mv.areas[i]*v[i]
        q = q + self.Mv.areas[i]*self.Mv.centers[i]*v[i]   
    end  
    u = q/r
    for i in 1:self.Mv.Nc
        p = p + self.Mv.areas[i]*(u-self.Mv.centers[i])^2*v[i]   
    end 
    t = p/r
    
    for i in 1:self.Mv.Nc
        data[i] = (r/sqrt(2.0*pi*t))*exp(-(u-self.Mv.centers[i])^2/(2.0*t))
    end   
    data[end]  = v[end]
    nothing
end


struct moment_mapping <: Flux
    Mv      :: Mesh
end

function (self::moment_mapping)(v::Vector{Float64},data::Vector{Float64})
    r = 0.0
    q = 0.0
    p = 0.0
    M4 =0.0
    M5 =0.0
    kin =0.0
    for i in 1:self.Mv.Nc
        r = r + self.Mv.areas[i]*v[i]
        q = q + self.Mv.areas[i]*self.Mv.centers[i]*v[i] 
        kin = kin + 0.5*self.Mv.areas[i]*self.Mv.centers[i]^2*v[i] 
        M4 = M4 + self.Mv.areas[i]*self.Mv.centers[i]^4*v[i] 
        M5 = M5 + self.Mv.areas[i]*self.Mv.centers[i]^5*v[i] 
    end  
    u = q/r
 
    Q = 0.0
    for i in 1:self.Mv.Nc   
        p = p + self.Mv.areas[i]*(u-self.Mv.centers[i])^2*v[i]   
        Q = Q + 0.5*self.Mv.areas[i]*(u-self.Mv.centers[i])^3*v[i]
    end 
    data[1] = r
    data[2] = u
    data[3] = p
    data[4] = v[end] #electric field
    data[5] = kin # kin energy
    data[6] = Q
    data[7] = M4 # kin energy
    data[8] = M5
    nothing
end

struct ope_lap <: SpatialOperator
end

function (self::ope_lap)(v :: Vector{DeRhamSpace}) 
    # model 
    # tilde{G} H G 
    res = C1(v[1].mh,v[1].bc_type)

    temp1 = C1bar(v[1].mh,v[1].bc_type,v[1].order,v[1].ndiags)
    temp2 = C0(v[1].mh,v[1].bc_type,v[1].order,v[1].ndiags)

    graddual(v[1],temp1)
    H0(temp1,temp2)
    grad(temp2,res)
    
    [res]
end   

struct lap_imp <: Operator
end

function (self::lap_imp)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.ndof)
    sp.field[:,1] .= v[:]
    sp()
    res[:] .= sp.flux[:,1]
    return res
end   

function rhs_compute(space::vf,rhs::C1,Lv,hv)
    average =0.0
    rhovf=vf(space.mh,1,2,1)
    
    for i in 1:rhs.ndof
        rhovf.field[i] = 0.0
        for j in 1:space.nvar-1
            rhovf.field[i,1] = rhovf.field[i,1] + hv*space.field[i,j]
        end   
    end 
    vf_to_stagC1(rhovf,rhs,1) 
    for i in 1:rhs.ndof 
        average = average +rhs.field[i]
    end 
    average = average /(rhs.ndof)  
    
    for i in 1:rhs.ndof
        rhs.field[i] = average - rhs.field[i]
    end  
    
end

function E_compute(phi::C0bar,E::C1bar,space::vf,nbvar)
    graddual(phi,E) # graddual  =-grad  
    stagC1bar_to_vf(E,space,nbvar)
end