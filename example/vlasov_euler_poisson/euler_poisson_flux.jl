struct PrimLimiting <: Flux
    gamma :: Float64
end

function (self::PrimLimiting)(x,data::Vector{Float64},vC::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64},d)
    rc, qc, enc, ec = vC  
    uc = qc/rc
    pc = (gamma-1.0) * (enc- 0.5 * qc*uc)
    
    rl, ql, enl, ec = vL  
    ul = ql/rl
    pl = (gamma-1.0) * (enl- 0.5 * ql*ul)
    
    rr, qr, enr, ec = vR  
    ur = qr/rr
    pr = (gamma-1.0) * (enr- 0.5 * qr*ur)
    
    dr = 0.5*minmod(rc-rl,rr-rc)
    du = 0.5*minmod(uc-ul,ur-uc)
    dp = 0.5*minmod(pc-pl,pr-pc)
    
    deltac = rc*max(-1.0,min(1.0,dr/rc))
    pcc = (gamma-1.0)*rc*(1.0+2.0*deltac*deltac/(rc*rc))
    deltau = sign(du)*sqrt(min(du*du,pc/pcc))
    deltap = pc*max(-1.0,min(1.0,dp/pc)) 
    
    rf = rc + d*deltac
    uf = uc + d*deltau
    pf = pc + d*deltap
    enf = pf/(gamma-1.0)
    
    data[1] = rf
    data[2] = rf*uf 
    data[3] = 0.5*rf*uf*uf + enf
    data[4] = ec
    nothing
end

struct LocalLaxE <: Flux
    L     :: Float64
    gamma :: Float64
end

function (self::LocalLaxE)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    gamma = self.gamma

    rl, ql, enl, el = vL
    rr, qr, enr ,er = vR

    ul = ql/rl
    ur = qr/rr

    pl = (gamma-1.0) * (enl - 0.5 * ql*ul) 
    pr = (gamma-1.0) * (enr - 0.5 * qr*ur) 

    cl = sqrt(gamma *pl/rl)
    cr = sqrt(gamma *pr/rr)
    
    lf1 = max(abs(ul+cl),abs(ur+cr))
    lf2 = max(abs(ul-cl),abs(ur-cr))
    lf = max(lf1,lf2)
    
    data[1] = 0.5 * (ql+qr) - 0.5 * lf* (rr-rl)
    data[2] = 0.5 * (ql*ul+pl+qr*ur+pr) - 0.5 * lf* (qr-ql)
    data[3] = 0.5 * ((enl+pl)*ul +(enr+pr)*ur) - 0.5 * lf* (enr-enl)
    data[4] = 0.0
    nothing
end

struct Nsflux <: Flux
    L     :: Float64
    gamma :: Float64
    Kn    :: Float64
end

function (self::Nsflux)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    gamma = self.gamma

    rl, ql, enl, el = vL
    rr, qr, enr ,er = vR

    ul = ql/rl
    ur = qr/rr

    pl = (gamma-1.0) * (enl - 0.5 * ql*ul) 
    pr = (gamma-1.0) * (enr - 0.5 * qr*ur) 
    pc = 0.5*(pl+pr)
    tl = pl/rl
    tr = pr/rr
    
    data[1] = 0.0
    data[2] = 0.0
    data[3] = -self.Kn*3.0/2.0*pc*(tr-tl)/h
    data[4] = 0.0
    nothing
end


struct euler_cfl <: Flux
    gamma :: Float64
end

function (self::euler_cfl)(x, v, res)
    gamma = self.gamma
    
    r, q, en, e = v
    u       = q/r
    p      = (gamma-1.0) * (en- 0.5 * q*u) 
    c       = sqrt(gamma *p/r)
    lf      = max(abs(u-c),abs(u+c))
    max(res,lf)
end

struct Primitive_mapping <: Flux
    gamma :: Float64
end

function (self::Primitive_mapping)(v::Vector{Float64}, data::Vector{Float64})
    gamma = self.gamma

    r, q, en, e = v
    data[1] =  r 
    data[2] = q/r
    data[3] = (gamma-1.0) * (en- 0.5 * q*q/r) 
    data[4] = e
    nothing 
end

struct l2normE <: Flux
end

function (self::l2normE)(v::Vector{Float64}, data::Vector{Float64})
    r, u, p, e = v
    data[1] =r*r
    data[2] =u*u
    data[3] =p*p
    data[4] =e*e
    nothing
end

struct ElecSource <: Flux
end

function (self::ElecSource)(xl,xr,hl,hr,data::Vector{Float64}, vL::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64})
    h=0.5*(hl+hr)
    data[1] = 0.0
    data[2] =h*vC[1]*vC[4]
    data[3] =h*vC[2]*vC[4]
    data[4] = 0.0
    nothing
end



struct ope_lap <: SpatialOperator
end

function (self::ope_lap)(v :: Vector{DeRhamSpace}) 
    # model 
    # tilde{G} H G 
    res = C1(v[1].mh,v[1].bc_type)

    temp1 = C1bar(v[1].mh,v[1].bc_type,v[1].order,v[1].ndiags)
    temp2 = C0(v[1].mh,v[1].bc_type,v[1].order,v[1].ndiags)

    graddual(v[1],temp1)
    H0(temp1,temp2)
    grad(temp2,res)
    
    [res]
end   


struct lap_imp <: Operator
end

function (self::lap_imp)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.ndof)
    sp.field[:,1] .= v[:]
    sp()
    res[:] .= sp.flux[:,1]
    return res
end   

function rhs_computeE(space::vf,rhs::C1,nbvar)
    average =0.0  
    
    vf_to_stagC1(space,rhs,nbvar) 
    for i in 1:rhs.ndof 
        average = average +rhs.field[i]
    end 
    average = average /(rhs.ndof)  
    
    
    for i in 1:rhs.ndof
        rhs.field[i] = average - rhs.field[i]
    end  
end

function E_computeE(phi::C0bar,E::C1bar,space::vf,nbvar)
    graddual(phi,E) # graddual  =-grad  
    stagC1bar_to_vf(E,space,nbvar)
end