abstract type InitFunction end

struct LandauDamping <: InitFunction
    L       :: Float64
    Mv      :: Mesh
    eps     :: Float64
end

function (self::LandauDamping)(x, t)
    res = zeros(Float64,self.Mv.Nc+1)
    sigma = 0.06
    x0    = 0.5*self.L
    r     = 1.0
    u     = 0.0
    t     = 1.0
    
    for i in 1:self.Mv.Nc
        res[i] = (r/sqrt(2.0*pi*t))*exp(-(self.Mv.centers[i]-u)^2/(2.0*t))
        res[i] = res[i]*(1.0+self.eps*cos(2.0*pi/self.L*x))
    end 
    res[end] = 0.0
    return res
end
