abstract type InitFunction end

struct LandauDampingE <: InitFunction
    L       :: Float64
    gamma   :: Float64
    eps     :: Float64
end

function (self::LandauDampingE)(x, t)
    r     = 1.0 + self.eps*cos(2.0*pi/self.L*x)
    u     = 0.0
    ener  = 0.5*(1.0 + self.eps*cos(2.0*pi/self.L*x))
    E     = 0.0
    [r, r*u, ener, E]
end
