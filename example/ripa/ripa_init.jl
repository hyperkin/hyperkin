abstract type InitFunction end

struct Lake_at_rest <: InitFunction
    L      :: Float64
    g      :: Float64
end

function (self::Lake_at_rest)(x, t)
    sigma  = 0.06
    x0     = 0.5*self.L
    z      = (1.0/sqrt(2.0*pi*sigma))*exp(-0.5*((x-x0)/sigma)^2)+0.1
    h      = 8.0-z
    theta  = 1.0
    u      = 0.0
    
    [h, h*u, h*theta, z]
end

struct Isobaric_SteadyState <: InitFunction
    L      :: Float64
    g      :: Float64
end

function (self::Isobaric_SteadyState)(x, t)
    sigma  = 0.06
    x0     = 0.5*self.L
    h      = 0.2*(1.0/sqrt(2.0*pi*sigma))*exp(-0.5*((x-x0)/sigma)^2)+1.0
    z      = 1.0
    theta  = 1/(h*h)
    u      = 0.0
    
    [h, h*u, h*theta, z]
end


struct WaterHeight_SteadyState <: InitFunction
    L      :: Float64
    g      :: Float64
end

function (self::WaterHeight_SteadyState)(x, t)
    sigma  = 0.06
    x0     = 0.5*self.L
    h      = 1.0
    z      = x*(self.L-x)
    theta  = exp(-2.0*x*(self.L-x))
    u      = 0.0
    
    [h, h*u, h*theta, z]
end

struct Perturbed_nonlinear_SteadyState <: InitFunction
    L      :: Float64
    g      :: Float64
end

function (self::Perturbed_nonlinear_SteadyState)(x, t)
 
    sigma  = 0.06
    x0     = 0.5*self.L
    h  = 1.0 + 0.001*(1.0/sqrt(2.0*pi*sigma))*exp(-0.5*((x-0.7)/sigma)^2)
    z      = 0.1*(1.0/sqrt(2.0*pi*sigma))*exp(-0.5*((x-x0)/sigma)^2)
    theta  = exp(-2.0*z)
    u      = 0.0
    
    [h, h*u, h*theta, z]
end
