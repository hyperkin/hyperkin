struct LocalLax <: Flux
    L     :: Float64
    g     :: Float64
end

function (self::LocalLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    hl, ql, htl, zl = vL
    hr, qr, htr, zr = vR

    ul = ql/hl
    ur = qr/hr
    tl = htl/hl
    tr = htr/hr

    pl = self.g * 0.5*hl*hl*tl
    pr = self.g * 0.5*hr*hr*tr

    cl = sqrt(self.g*hl*tl)
    cr = sqrt(self.g*hr*tr)
    
    lf = max(abs(ul-cl),abs(ur+cr))
    
    data[1] = 0.5 * (ql+qr) - 0.5 * lf* (hr-hl)
    data[2] = 0.5 * (ql*ul+pl+qr*ur+pr) - 0.5 * lf* (qr-ql) 
    data[3] = 0.5 * (htl*ul +htr*ur) - 0.5 * lf* (htr-htl)
    data[4] = 0.0
end

struct Topography <: Flux
    L     :: Float64
    g     :: Float64
end

function (self::Topography)(x,h,data::Vector{Float64},vL::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64})
    hc, qc, htc, zc = vC
    hl, ql, htl, zl = vL
    
    data[1] = 0.0 
    data[2] = self.g * htc * (zc-zl) 
    data[3] = 0.0
    data[4] = 0.0
    nothing
end


struct ripa_cfl <: Flux
    g    :: Float64
end

function (self::ripa_cfl)(x,v, res)
    
    h, q, ht, z = v
    u       = q/h
    c       = sqrt(self.g*ht)
    lf      = max(abs(u-c),abs(u+c))
    max(res,lf)
end

struct  Primitive_mapping <: Flux
    g :: Float64
end

function (self::Primitive_mapping)(v::Vector{Float64},data::Vector{Float64})
    h, q, ht, z = v
    data[1]= h
    data[2]= q/h
    data[3]= ht/h
    data[4]= z
    nothing
end

struct l2norm <: Flux
end

function (self::l2norm)(v::Vector{Float64},data::Vector{Float64})
    h, u, t, z = v
    data[1]= h*h
    data[2]= u*u
    data[3]= t*t
    nothing
end

