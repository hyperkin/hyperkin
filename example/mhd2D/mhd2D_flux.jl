

function eigenvals(vC::Vector{Float64},n::Vector{Float64},gamma,ch)
    r, qx, qy, qz, e, bx, by, bz, psi = vC
    ux = qx/r
    uy = qy/r
    uz = qz/r
    p=(gamma-1.0)*(e-0.5*r*(ux*ux+uy*uy+uz*uz)-0.5*(bx*bx+by*by+bz*bz))
    c = sqrt(gamma*p/r)
    ca = (bx*n[1]+by*n[2])/sqrt(r)
    b = sqrt(bx*bx+by*by+bz*bz)
    un = ux*n[1]+uy*n[2]
    
    cs = sqrt(0.5*((ca^2+b^2/r)+c^2)-0.5*sqrt(((ca^2+b^2/r)+c^2)^2-4*c^2*ca^2))
    cf = sqrt(0.5*((ca^2+b^2/r)+c^2)+0.5*sqrt(((ca^2+b^2/r)+c^2)^2-4*c^2*ca^2))
    [un,un-cs,un+cs,un-ca,un+ca,un-cf,un+cf,-ch,ch] 
end

function eigenvals_wave(vC::Vector{Float64},n::Vector{Float64},gamma,ch)
    r, qx, qy, qz, e, bx, by, bz, psi = vC
    ux = qx/r
    uy = qy/r
    uz = qz/r
    p=(gamma-1.0)*(e-0.5*r*(ux*ux+uy*uy+uz*uz)-0.5*(bx*bx+by*by+bz*bz))
    c = sqrt(gamma*p/r)
    ca = (bx*n[1]+by*n[2])/sqrt(r)
    b = sqrt(bx*bx+by*by+bz*bz)
    
    cs = sqrt(0.5*((ca^2+b^2/r)+c^2)-0.5*sqrt(((ca^2+b^2/r)+c^2)^2-4*c^2*ca^2))
    cf = sqrt(0.5*((ca^2+b^2/r)+c^2)+0.5*sqrt(((ca^2+b^2/r)+c^2)^2-4*c^2*ca^2))
    [cs,ca,cf,ch] 
end


struct Rusanov <: Flux
    Lx :: Float64
    Ly :: Float64
    gamma :: Float64
    ch    :: Float64
end

function (self::Rusanov)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    rl, qxl, qyl, qzl, el, bxl, byl, bzl, psil = vC
    rr, qxr, qyr, qzr, er, bxr, byr, bzr, psir = vR
    
    lc = eigenvals(vC,n,self.gamma,self.ch)
    lr = eigenvals(vR,n,self.gamma,self.ch)
    
    absc=[abs(lc[1]),abs(lc[2]),abs(lc[3]),abs(lc[4]),abs(lc[5]),abs(lc[6]),abs(lc[7]),abs(lc[8]),abs(lc[9])] 
    absr=[abs(lr[1]),abs(lr[2]),abs(lr[3]),abs(lr[4]),abs(lr[5]),abs(lr[6]),abs(lr[7]),abs(lr[8]),abs(lr[9])] 
    
    sc = maximum(absc)
    sr = maximum(absr)
    s = max(sc,sr)
     
    uxl = qxl/rl; uyl = qyl/rl; uzl = qzl/rl
    uxr = qxr/rr; uyr = qyr/rr; uzr = qzr/rr
    pbl = 0.5*(bxl*bxl+byl*byl+bzl*bzl)
    pbr = 0.5*(bxr*bxr+byr*byr+bzr*bzr)
    
    pl = (self.gamma-1.0)*(el -0.5*rl*(uxl*uxl+uyl*uyl+uzl*uzl)-pbl)
    pr = (self.gamma-1.0)*(er -0.5*rr*(uxr*uxr+uyr*uyr+uzr*uzr)-pbr)
 
    
    unl =uxl*n[1]+uyl*n[2]
    unr =uxr*n[1]+uyr*n[2]
    bnl =bxl*n[1]+byl*n[2]
    bnr =bxr*n[1]+byr*n[2]
 
    ubl = uxl*bxl+uyl*byl+uzl*bzl
    ubr = uxr*bxr+uyr*byr+uzr*bzr

    data[1] = 0.5 *(rl*unl+rr*unr) - 0.5 * s*  (rr-rl)
    data[2] = 0.5 *(rl*unl*uxl + rr*unr*uxr +(pl+pbl+pr+pbr)*n[1]-bnl*bxl -bnr*bxr) - 0.5 * s * (qxr-qxl)
    data[3] = 0.5 *(rl*unl*uyl + rr*unr*uyr +(pl+pbl+pr+pbr)*n[2]-bnl*byl -bnr*byr) - 0.5 * s * (qyr-qyl)
    data[4] = 0.5 *(rl*unl*uzl + rr*unr*uzr - bnl*bzl - bnr*bzr) - 0.5 * s * (qzr-qzl)
    data[5] = 0.5 *((el+pl+pbl)*unl-bnl*ubl+(er+pr+pbr)*unr-bnr*ubr) - 0.5 * s * (er-el)
    data[6] = 0.5 *( unl*bxl + unr*bxr - bnl*uxl -bnr*uxr + (psil+psir)*n[1]) - 0.5 * s * (bxr-bxl)
    data[7] = 0.5 *( unl*byl + unr*byr - bnl*uyl -bnr*uyr + (psil+psir)*n[2]) - 0.5 * s * (byr-byl)
    data[8] = 0.5 *( unl*bzl + unr*bzr - bnl*uzl -bnr*uzr) - 0.5 * s * (bzr-bzl)
    data[9] = 0.5 *(self.ch^2*(bnl+bnr)) - 0.5 * s * (psir-psil)
end


struct HLL <: Flux
    Lx :: Float64
    Ly :: Float64
    gamma :: Float64
    ch    :: Float64
end

function (self::HLL)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    rl, qxl, qyl, qzl, el, bxl, byl, bzl, psil = vC
    rr, qxr, qyr, qzr, er, bxr, byr, bzr, psir = vR
    
    uxl = qxl/rl; uyl = qyl/rl; uzl = qzl/rl
    uxr = qxr/rr; uyr = qyr/rr; uzr = qzr/rr
    pbl = 0.5*(bxl*bxl+byl*byl+bzl*bzl)
    pbr = 0.5*(bxr*bxr+byr*byr+bzr*bzr)
    unl =uxl*n[1]+uyl*n[2]
    unr =uxr*n[1]+uyr*n[2]
    
    pl = (self.gamma-1.0)*(el -0.5*rl*(uxl*uxl+uyl*uyl+uzl*uzl)-pbl)
    pr = (self.gamma-1.0)*(er -0.5*rr*(uxr*uxr+uyr*uyr+uzr*uzr)-pbr)
    
    bnl =bxl*n[1]+byl*n[2]
    bnr =bxr*n[1]+byr*n[2]
 
    ubl = uxl*bxl+uyl*byl+uzl*bzl
    ubr = uxr*bxr+uyr*byr+uzr*bzr
    
    lc = eigenvals_wave(vC,n,self.gamma,self.ch)
    lr = eigenvals_wave(vR,n,self.gamma,self.ch)
    
    SL= min(0,min(unl,unr)-max(lr[3],lc[3],self.ch))
    SR= max(0,max(unl,unr)+max(lr[3],lc[3],self.ch))

    data[1] = (SR*rl*unl-SL*rr*unr + SL*SR*  (rr-rl))/(SR-SL)
    data[2] = (SR*(rl*unl*uxl+(pl+pbl)*n[1]-bnl*bxl)-SL*(rr*unr*uxr+(pr+pbr)*n[1]-bnr*bxr) + SL*SR*  (qxr-qxl))/(SR-SL)
    data[3] = (SR*(rl*unl*uyl+(pl+pbl)*n[2]-bnl*byl)-SL*(rr*unr*uyr+(pr+pbr)*n[2]-bnr*byr) + SL*SR*  (qyr-qyl))/(SR-SL)
    data[4] = (SR*(rl*unl*uzl-bnl*bzl)-SL*(rr*unr*uzr-bnr*bzr) + SL*SR*  (qzr-qzl))/(SR-SL)
    data[5] = (SR*((el+pl+pbl)*unl-bnl*ubl)-SL*((er+pr+pbr)*unr-bnr*ubr) + SL*SR*  (er-el))/(SR-SL) 
    data[6] = (SR*(unl*bxl- bnl*uxl+psil*n[1])-SL*(unr*bxr- bnr*uxr+psir*n[1]) + SL*SR*  (bxr-bxl))/(SR-SL)
    data[7] = (SR*(unl*byl- bnl*uyl+psil*n[2])-SL*(unr*byr- bnr*uyr+psir*n[2]) + SL*SR*  (byr-byl))/(SR-SL)
    data[8] = (SR*(unl*bzl- bnl*uzl)-SL*(unr*bzr- bnr*uzr) + SL*SR*  (bzr-bzl))/(SR-SL) 
    data[9] = (SR*(self.ch^2*bnl)-SL*(self.ch^2*bnr) + SL*SR*  (psir-psil))/(SR-SL)
end

struct SourceCleaning <: Flux
    Lx :: Float64
    Ly :: Float64
    gamma :: Float64
    ch    :: Float64
end

function (self::SourceCleaning)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    rl, qxl, qyl, qzl, el, bxl, byl, bzl, psil = vC
    rr, qxr, qyr, qzr, er, bxr, byr, bzr, psir = vR
    

    data[1] = 0.0
    data[2] = 0.0
    data[3] = 0.0
    data[4] = 0.0
    data[5] = 0.0
    data[6] = 0.0
    data[7] = 0.0
    data[8] = 0.0
    data[9] = -0.25*self.ch/sqrt(h)*psil
end


struct mhd_cfl <: Flux
    gamma :: Float64
    ch    :: Float64
end

function (self::mhd_cfl)(x,v, res)
    
    n =[1.0,1.0]
    l = eigenvals(v,n,self.gamma,self.ch)
    tabs=[abs(l[1]),abs(l[2]),abs(l[3]),abs(l[4]),abs(l[5]),abs(l[6]),abs(l[7]),abs(l[8]),abs(l[9])]
    lf      = maximum(tabs)
    max(res,lf)
end

struct l2_norm <: Flux
end

function (self::l2_norm)(v::Array{Float64}, data::Array{Float64})
    r, ux, uy, uz, p, bx, by, bz, psi  =  v
    data[1]  =  r*r
    data[2]  =  ux*ux + uy*uy +uz*uz
    data[3]  =  p*p
    data[4]  =  bx*bx + by*by +bz*bz
    data[5]  =  psi*psi
    nothing
end

struct primitive_mhd_mapping <: Flux
    gamma :: Float64
end

function (self::primitive_mhd_mapping)(v::Array{Float64}, data::Array{Float64})  
    r, rux, ruy, ruz, e, bx, by, bz, psi   =   v
    ux = rux/r
    uy = ruy/r
    uz = ruz/r
    p=(self.gamma-1.0)*(e-0.5*r*(ux*ux+uy*uy+uz*uz)-0.5*(bx*bx+by*by+bz*bz))
    data[1] = r
    data[2] = ux
    data[3] = uy
    data[4] = uz
    data[5] = p
    data[6] = bx
    data[7] = by
    data[8] = bz
    data[9] = 0.5*(bx*bx+by*by+bz*bz)
    nothing
end


