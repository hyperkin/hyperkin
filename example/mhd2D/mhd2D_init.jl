abstract type InitFunction end

struct Rotor <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
     gamma  :: Float64
end

function (self::Rotor)(x, t) 
    
    x0x = self.Lx/2.0
    x0y = self.Ly/2.0
    norm = sqrt((x[1]-x0x)^2+(x[2]-x0y)^2)
    p = 1.0
    uz = 0.0
    bx = 5.0/sqrt(4.0*pi)
    by = 0.0
    bz = 0.0
    f = (0.115-norm)/(0.115-0.1)
    if norm < 0.1
        r = 10
        ux = -(x[2]-x0y)/0.1
        uy = (x[1]-x0x)/0.1
    elseif  norm < 0.115 && norm > 0.1
        r = 1.0+9.0*f
        ux = -(x[2]-x0y)*f/norm
        uy = (x[1]-x0x)*f/norm
    else    
        r = 1.0
        ux = 0.0
        uy = 0.0
    end    
    e = 0.5*r*(ux*ux+uy*uy+uz*uz)+p/(self.gamma-1.0) +0.5*(bx*bx+by*by+bz*bz)
    phi = 0.0
    [r, r*ux, r*uy,r*uz, e, bx, by, bz, phi]
end

struct OrszagTang <: InitFunction
     Lx     :: Float64
     Ly     :: Float64
     gamma  :: Float64
end

function (self::OrszagTang)(x, t) 
    
    x0x = self.Lx/2.0
    x0y = self.Ly/2.0
    norm = sqrt((x[1]-x0x)^2+(x[2]-x0y)^2)
    r = self.gamma*self.gamma
    p = self.gamma
    ux = -sin(2.0*pi*x[2])
    uy = sin(2.0*pi*x[1])
    uz = 0.0
    bx = -sin(2.0*pi*x[2])
    by = sin(4.0*pi*x[1])
    bz = 0.0
     
    e = 0.5*r*(ux*ux+uy*uy+uz*uz)+p/(self.gamma-1.0) +0.5*(bx*bx+by*by+bz*bz)
    phi = 0.0
    [r, r*ux, r*uy,r*uz, e, bx, by, bz, phi]
end