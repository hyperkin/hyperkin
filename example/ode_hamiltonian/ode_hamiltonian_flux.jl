struct Hamiltonian_kepler <: Flux
end

function (self::Hamiltonian_kepler)(v::Vector)  
    q1, q2, p1, p2 =  v
    h =0.5*p1*p1+ 0.5*p2*p2 -1.0/(sqrt(q1^2+q2^2))
    return h
end


struct OutHamiltonian_kepler <: Flux
end

function (self::OutHamiltonian_kepler)(v::Array{Float64}, data::Array{Float64})
    q1, q2, p1, p2, h  =  v
    data[1]  =  abs(h)
    nothing
end

struct kepler_mapping <: Flux
end

function (self::kepler_mapping)(v::Array{Float64}, data::Array{Float64})  
    q1, q2, p1, p2 =  v
    data[1] = q1
    data[2] = q2
    data[3] = p1
    data[4] = p2
    data[5] = 0.5*p1*p1+ 0.5*p2*p2 -1.0/(sqrt(q1^2+q2^2))
    nothing
end

struct Hamiltonian_CentralForce <: Flux
    k  :: Float64
end
function (self::Hamiltonian_CentralForce)(v::Vector)  
    qx, qy, px, py =  v
    r = sqrt(qx*qx+qy*qy)
    
    h =0.5*(px*px+py*py) + 0.5/((0.5*(self.k+1)-1.0)*r^(0.5*(self.k-1.0))) ##nbot good
    return h
end


struct OutHamiltonian_CentralForce <: Flux
end

function (self::OutHamiltonian_CentralForce)(v::Array{Float64}, data::Array{Float64})
    qx, qy, px, py, h  =  v
    data[1]  =  abs(h)
    nothing
end

struct CentralForce_mapping <: Flux
    k  :: Float64
end

function (self::CentralForce_mapping)(v::Array{Float64}, data::Array{Float64})  
    qx, qy, px, py =  v
    r = sqrt(qx*qx+qy*qy)

    data[1] = qx
    data[2] = qy
    data[3] = px
    data[4] = py
    data[5] = 0.5*(px*px+py*py) + 0.5/((self.k+1)*r^(self.k))
    nothing
end


struct lorentz_matrix <: Flux
    r   :: Float64
    sig :: Float64
end
function (self::lorentz_matrix)(data::Vector{Float64},v::Vector{Float64})
   n= size(v)[1]
   data[1] = self.sig * v[2]
   data[2] = - self.sig * v[1] - x * v[3]
   data[3] =  x * v[2]
end 


struct Hamiltonian_lorentz <: Flux
    r   :: Float64
    sig :: Float64
end

function (self::Hamiltonian_lorentz)(v::Vector)  
    x, y, z =  v
    h =0.5*y*y + 0.5*z*z - self.r*z
    return h
end


struct OutHamiltonian_lorentz <: Flux
    r   :: Float64
    sig :: Float64
end

function (self::OutHamiltonian_lorentz)(v::Array{Float64}, data::Array{Float64})
    x, y, z, h, c1 =  v
    data[1]  =  abs(h)
    data[1]  =  abs(c1)
    nothing
end

struct lorentz_mapping <: Flux
    r   :: Float64
    sig :: Float64
end

function (self::lorentz_mapping)(v::Array{Float64}, data::Array{Float64})  
    x, y, z =  v
    data[1] = x
    data[2] = y
    data[3] = z
    data[4] = 0.5*y*y + 0.5*z*z - self.r*z
    data[5] = 0*5*x*x-self.sig*z 
    nothing
end

