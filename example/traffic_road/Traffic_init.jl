abstract type InitFunction end

struct Riemann <: InitFunction
    road  :: Road
    rl    :: Float64
    rr    :: Float64
end

# ----- Riemann solver -----
function (self::Riemann)(x, t)
    rl, rr   = self.rl, self.rr
    rmax     = self.road.rmax
    vmax     = self.road.vmax
    fl, fr   = self.road.fluxphy(rmax,vmax,rl), self.road.fluxphy(rmax,vmax,rr)
    dfl, dfr = self.road.dfluxphy(rmax,vmax,rl), self.road.dfluxphy(rmax,vmax,rr)
    
    xi = (x-0.5*self.road.mh.L)/t
    if rl > rr # rarefaction (concave flux)
        if xi < dfl
            r = rl
        elseif dfl < xi < dfr
                r = rmax*(1-xi/vmax)/2  #(t - x + 1)/2
        else 
            r = rr
        end      
    else # shock-wave
        s = (fr-fl) / (rr-rl)
        r = (xi<s) ? rl : rr 
    end
    [r]
end

