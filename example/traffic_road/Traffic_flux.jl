struct PhyFlux <: Flux
end

function (self::PhyFlux)(rmax, vmax, rho)
    return rho*vmax*(1 - rho/rmax)
end

struct DPhyFlux <: Flux
end

function (self::DPhyFlux)(rmax, vmax, rho)
    return vmax*(1 - 2*rho/rmax)
end

struct Sigma <: Flux
end

function (self::Sigma)(rmax::Float64)
   return rmax/2 
end

struct LocalTrafficLax <: Flux
    road :: Road
end

function (self::LocalTrafficLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    rmax = self.road.rmax
    vmax = self.road.vmax
    f = self.road.fluxphy
    df = self.road.dfluxphy
    lmax = max( abs( df(rmax,vmax,vL[1]) ) , abs( df(rmax,vmax,vR[1]) ) )
    data[1] = 0.5 * ( f(rmax,vmax,vR[1]) + f(rmax,vmax,vL[1]) ) - 0.5*lmax*(vR[1]-vL[1])
end

struct Traffic_cfl <: Flux
    road :: Road
end

function (self::Traffic_cfl)(x, v, res)
    rmax = self.road.rmax
    vmax = self.road.vmax
    df = self.road.dfluxphy
    lf  = abs(df(rmax,vmax,v[1]))
    max(res,lf)
end

struct l1_norm <: Flux
end

function (self::l1_norm)(v::Array{Float64}, data::Array{Float64})
    r        =  v[1] # we cannot use direct v. why ?
    data[1]  =   abs(r)
    nothing
end

struct id_traffic_mapping <: Flux
end

function (self::id_traffic_mapping)(v::Array{Float64}, data::Array{Float64})
    r       =  v[1]
    data[1] = r
    nothing
end
