struct l1l2_norm <: Flux
end

function (self::l1l2_norm)(v)
    [abs(v),v*v]
end

struct id_lap_mapping <: Flux
end

function (self::id_lap_mapping)(v)
    return v
end


struct ope_lap <: SpatialOperator

end

function (self::ope_lap)(v :: Vector{DeRhamSpace})  
    # model 
    # tilde{G} H G 
    res = C1(v[1].mh,v[1].bc_type)

    temp1 = C1bar(v[1].mh,v[1].bc_type,v[1].order,v[1].ndiags)
    temp2 = C0(v[1].mh,v[1].bc_type,v[1].order,v[1].ndiags)

    graddual(v[1],temp1)
    H0(temp1,temp2)
    grad(temp2,res)
    [res]
end   

struct lap_imp <: Operator

end

function (self::lap_imp)(sp :: SpatialDis,v :: Vector{Float64})
    res= zeros(Float64,sp.ndof)
    sp.field[:,1] .= v[:]
    sp()
  
    res[:] .= sp.flux[:,1]
    return res
end   