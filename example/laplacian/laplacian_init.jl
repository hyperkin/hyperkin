abstract type SolFunction end

struct harmonic <: SolFunction
    L     :: Float64
end

function (self::harmonic)(x, t)
    r = sin(2.0*pi/self.L*x)
    return r
end

struct source_harmonic <: SolFunction
    L     :: Float64
end

function (self::source_harmonic)(x)
    r = (2.0*pi/self.L)*(2.0*pi/self.L)sin(2.0*pi/self.L*x)
    return r
end

