function TestVF2D_init_cts(x, t)
    r     = 2.5
    [r]
end

function TestVF2D_init_linear(x, t)
    r     = 2.2*x[1]+1.7-0.5*x[2]
    [r]
end

function TestVF2D_init_linearbc(x)
    r     = 2.2*x[1]+1.7-0.5*x[2]
    [r]
end

function TestVF2D_norml1(v::Array{Float64}, data::Array{Float64})
    r  =  v[1] # we cannot use direct v. why ?
    data[1]  =   abs(r)
    nothing
end

function TestVF2D_id_mapping(v::Array{Float64}, data::Array{Float64}) 
    r  =  v[1]
    data[1] = r
    nothing
end

struct TestLocalLax2D <: Flux
    Lx :: Float64
    Ly :: Float64
    ax :: Float64
    ay :: Float64
end

function (self::TestLocalLax2D)(x,h,data::Vector{Float64},vC::Vector{Float64},vR::Vector{Float64},n::Vector{Float64})
    an = self.ax*n[1]+self.ay*n[2]
    rl = vC[1]
    rr = vR[1]
    data[1] = 0.5 * (an*rl+an*rr) - 0.5 * abs(an) * (rr-rl)
end

function test_vf2D()
  test =0
###### parameters
  Lx = 1.0
  Ly = 1.0  
  Nx = 10
  Ny = 10  
  test = 0
  order = 1
  a = 1.0
  eps = 0.0000000001
###### initialisation space
  Mh=Mesh2D(Lx,Ly,Nx,Ny,1)
  Mh(id_mesh2D)

  space=vf2D(Mh,1,2)
  local_lax = TestLocalLax2D(Lx,Ly,a,a)
  set_numflux(space, local_lax)

  initialization(space,TestVF2D_init_cts)
  bc_neumann(space)
  space()
  space.field[:].=-space.flux[:]

  field, diags= diagnostics(space,0,0.0,TestVF2D_init_cts,TestVF2D_id_mapping,TestVF2D_norml1)
  @test abs(diags[1]) < eps

  initialization(space,TestVF2D_init_linear)
  bc_dirichlet(space,TestVF2D_init_linearbc)
  space()
  space.field[:].=-space.flux[:]

  field, diags= diagnostics(space,0,0.0,TestVF2D_init_cts,TestVF2D_id_mapping,TestVF2D_norml1)
 
  @test abs(diags[1]-1.7) < eps
end
