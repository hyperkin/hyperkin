function TestD_pol(x)
    return x*(1.0-x)
end    

function TestD_cts(x)
    return 2.4
end   

function TestD_intpol(x)
    return 0.5*x*x-(1.0/3.0)*x*x*x
end    

function test_derham()

  ###### parameters
    L= 1.0
    Nx = 10
    order = 0
    a = 1.0
    eps = 0.0000000001
  ###### initialisation space
    Mh=Mesh(L,Nx,4)
    Mh(id_mesh)

  ####### test I circ R f = f
    X0 = C0(Mh,1)
    R0(X0,TestD_pol)
    err = 0.0
    for i in 1:Mh.Nv-1
        px=I0loc(X0,i)
        err = err + abs(px(Mh.nodes[i])-TestD_pol(Mh.nodes[i]))
    end   
    @test err < eps

    X0bar = C0bar(Mh,1)
    R0bar(X0bar,TestD_pol)
    err = 0.0
    for i in 1:Mh.Nc
        px=I0barloc(X0bar,i)
        err = err + abs(px(Mh.centers[i])-TestD_pol(Mh.centers[i]))
    end    
    @test err < eps

    X1 = C1(Mh,1)
    R1(X1,TestD_pol)
    err = 0.0
    for i in 1:Mh.Nc
        px=I1loc(X1,i)
        err = err + abs(Mh.h*px(Mh.centers[i])-(TestD_intpol(Mh.nodes[i+1])-TestD_intpol(Mh.nodes[i])))
    end    
    @test err < eps

    X1bar = C1bar(Mh,1)
    R1bar(X1bar,TestD_pol)
    err = 0.0
    for i in 2:Mh.Nc
       px=I1barloc(X1bar,i)
       err = err + abs(Mh.h*px(Mh.nodes[i])-(TestD_intpol(Mh.centers[i])-TestD_intpol(Mh.centers[i-1]))) 
    end    
    px=I1barloc(X1bar,1)
    err = err + abs(Mh.h*px(Mh.nodes[1])-(TestD_intpol(Mh.centers[1])-TestD_intpol(Mh.bc_centers[2])))
    @test err < eps

    ###### test hodge operator order 1
    Y1 = C1(Mh,1)
    R1(Y1,TestD_pol)
    Y0bar = C0bar(Mh,1)
    H0bar(Y1,Y0bar)
    for i in 1:Y1.ndof
        err = err + abs(Y0bar.field[i]/Y1.field[i]-1.0/Mh.h)
    end   
    @test err < eps

    Y0 = C0(Mh,1)
    R0(Y0,TestD_pol)
    Y1bar = C1bar(Mh,1)
    H1bar(Y0,Y1bar)
    err = err + abs(Y1bar.field[1]-(Mh.h/8.0)*(Y0.field[end]+6.0*Y0.field[1]+Y0.field[2]))
    for i in 2:Y1bar.ndof-1
        err = err + abs(Y1bar.field[i]-(Mh.h/8.0)*(Y0.field[i-1]+6.0*Y0.field[i]+Y0.field[i+1]))
    end  
    err = err + abs(Y1bar.field[end]-(Mh.h/8.0)*(Y0.field[end-1]+6.0*Y0.field[end]+Y0.field[1]))
    @test err < eps

    Z0bar = C0bar(Mh,1)
    R0bar(Z0bar,TestD_pol)
    Z1 = C1(Mh,1)
    H1(Z0bar,Z1)
    err = err + abs(Z1.field[1]-(Mh.h/8.0)*(Z0bar.field[end]+6.0*Z0bar.field[1]+Z0bar.field[2]))
    for i in 2:Z1.ndof-1
        err = err + abs(Z1.field[i]-(Mh.h/8.0)*(Z0bar.field[i-1]+6.0*Z0bar.field[i]+Z0bar.field[i+1]))
    end  
    err = err + abs(Z1.field[end]-(Mh.h/8.0)*(Z0bar.field[end-1]+6.0*Z0bar.field[end]+Z0bar.field[1]))  
    @test err < eps

    Z1bar = C1bar(Mh,1)
    R1bar(Z1bar,TestD_pol)
    Z0 = C0(Mh,1)
    H0(Y1bar,Y0)
    for i in 1:Y0.ndof
        err = err + abs(Y0.field[i]/Y1bar.field[i]-1.0/Mh.h)
    end   
    @test err < eps

    ###### test hodge operator order 1
    pho = 1
    err =0.0
    A1bar = C1bar(Mh,1,pho,1)
    R1bar(A1bar,TestD_pol)
    A0 = C0(Mh,1,pho,1)
    H0(A1bar,A0)
    err = err + abs(A0.field[1]-(1.0/(24.0*Mh.h))*(-A1bar.field[end]+26.0*A1bar.field[1]-A1bar.field[2]))
    for i in 2:A1bar.ndof-1
        err = err + abs(A0.field[i]-(1.0/(24.0*Mh.h))*(-A1bar.field[i-1]+26.0*A1bar.field[i]-A1bar.field[i+1]))
    end  
    err = err + abs(A0.field[end]-(1.0/(24.0*Mh.h))*(-A1bar.field[end-1]+26.0*A1bar.field[end]-A1bar.field[1]))
    @test err < eps
 
    
    pho = 1
    err =0.0
    A1 = C1(Mh,1,pho,1)
    R1(A1,TestD_pol)
    A0bar = C0bar(Mh,1,pho,1)
    H0bar(A1,A0bar)
    err = err + abs(A0bar.field[1]-(1.0/(24.0*Mh.h))*(-A1.field[end]+26.0*A1.field[1]-A1.field[2]))
    for i in 2:A1.ndof-1
        err = err + abs(A0bar.field[i]-(1.0/(24.0*Mh.h))*(-A1.field[i-1]+26.0*A1.field[i]-A1.field[i+1]))
    end  
    err = err + abs(A0bar.field[end]-(1.0/(24.0*Mh.h))*(-A1.field[end-1]+26.0*A1.field[end]-A1.field[1])) 
    @test err < eps
    
    pho = 1
    err =0.0
    A0 = C0(Mh,1,pho,1)
    R0(A0,TestD_pol)
    A1bar = C1bar(Mh,1,pho,1)
    H1bar(A0,A1bar)
    ref = (Mh.h/(384.0))*(-7.0*A0.field[end-1]+44*A0.field[end]+310.0*A0.field[1]+44*A0.field[2]-7.0*A0.field[3])
    err = err + abs(A1bar.field[1]-ref)
        
    ref = (Mh.h/(384.0))*(-7.0*A0.field[end]+44*A0.field[1]+310.0*A0.field[2]+44*A0.field[3]-7.0*A0.field[4])
    err = err + abs(A1bar.field[2]-ref)    
    for i in 3:A1bar.ndof-2
        ref = (Mh.h/(384.0))*(-7.0*A0.field[i-2]+44*A0.field[i-1]+310.0*A0.field[i]+44*A0.field[i+1]-7.0*A0.field[i+2])       
        err = err + abs(A1bar.field[i]-ref)
    end  
    ref = (Mh.h/(384.0))*(-7.0*A0.field[end-3]+44*A0.field[end-2]+310.0*A0.field[end-1]+44*A0.field[end]-7.0*A0.field[1])
    err = err + abs(A1bar.field[end-1]-ref)
        
    ref = (Mh.h/(384.0))*(-7.0*A0.field[end-2]+44*A0.field[end-1]+310.0*A0.field[end]+44*A0.field[1]-7.0*A0.field[2])
    err = err + abs(A1bar.field[end]-ref) 
    @test err < eps
    
    
    pho = 1
    err =0.0
    A0bar = C0bar(Mh,1,pho,1)
    R0bar(A0bar,TestD_pol)
    A1 = C1(Mh,1,pho,1)
    H1(A0bar,A1)
    ref = (Mh.h/(384.0))*(-7.0*A0bar.field[end-1]+44*A0bar.field[end]+310.0*A0bar.field[1]+44*A0bar.field[2]-7.0*A0bar.field[3])
    err = err + abs(A1.field[1]-ref)
        
    ref = (Mh.h/(384.0))*(-7.0*A0bar.field[end]+44*A0bar.field[1]+310.0*A0bar.field[2]+44*A0bar.field[3]-7.0*A0bar.field[4])
    err = err + abs(A1.field[2]-ref)    
    for i in 3:A1.ndof-2
        ref = (Mh.h/(384.0))*(-7.0*A0bar.field[i-2]+44*A0bar.field[i-1]+310.0*A0bar.field[i]+44*A0bar.field[i+1]-7.0*A0bar.field[i+2])       
        err = err + abs(A1.field[i]-ref)
    end  
    ref = (Mh.h/(384.0))*(-7.0*A0bar.field[end-3]+44*A0bar.field[end-2]+310.0*A0bar.field[end-1]+44*A0bar.field[end]-7.0*A0bar.field[1])
    err = err + abs(A1.field[end-1]-ref)
        
    ref = (Mh.h/(384.0))*(-7.0*A0bar.field[end-2]+44*A0bar.field[end-1]+310.0*A0bar.field[end]+44*A0bar.field[1]-7.0*A0bar.field[2])
    err = err + abs(A1.field[end]-ref) 
    @test err < eps

end
