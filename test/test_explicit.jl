function TestEx_init(x, t)
    [2.0]
end

struct TestEx_flux_ode <: Flux
end

function (self::TestEx_flux_ode)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    x = -vL[1]
    data[1] =x
end

function test_explicit()
  test =0
###### parameters
  L= 1.0
  Nx = 1
  test = 0
  order = 1
  eps = 0.0000000001
  global Tf = 0.1
  
###### initialisation space
  Mh=Mesh(L,Nx,1)
  Mh(id_mesh)

  space=vf(Mh,1,order,1)
  ode= TestEx_flux_ode()
  set_numflux(space, ode)
  limiting = NoLimiting()
  set_limiting(space,limiting)
  
  Tscheme=explicit_mstage(space,2)

  initialization(space,TestEx_init)
  global dt = 0.01
  global n_iter = 0
  while Tscheme.time < Tf 
    global dt, n_iter  
    if Tscheme.time + dt >  Tf
      global dt = Tf - Tscheme.time
    end
  
    Rk(Tscheme, dt)  
    
    global n_iter += 1    
  end
  x = space.field[1,1]
  err1 = x-2.0*exp(-Tf)

  Tscheme2=explicit_mstage(space,2)
  initialization(space,TestEx_init)
  global dt = 0.005
  global n_iter = 0
  while Tscheme2.time < Tf 
    global dt, n_iter  
    if Tscheme2.time + dt >  Tf
      global dt = Tf - Tscheme2.time
    end
  
    Rk(Tscheme2, dt)  
    
    global n_iter += 1    
  end
  x = space.field[1,1]
  err2= x-2.0*exp(-Tf)
 
  order2 = log(err2/err1)/log(0.5)
  @test order2 > 1.9


  global Tf = 0.2
  Tscheme3=explicit_mstage(space,4)

  initialization(space,TestEx_init)
  global dt = 0.1
  global n_iter = 0
  while Tscheme3.time < Tf 
    global dt, n_iter  
    if Tscheme3.time + dt >  Tf
      global dt = Tf - Tscheme3.time
    end
    
    Rk(Tscheme3, dt) 

    global n_iter += 1    
  end
  x = space.field[1,1]
  err1 = x-2.0*exp(-Tf)

  Tscheme4=explicit_mstage(space,4)
  initialization(space,TestEx_init)
  global dt = 0.05
  global n_iter = 0
  while Tscheme4.time < Tf 
    global dt, n_iter  
    if Tscheme4.time + dt >  Tf
      global dt = Tf - Tscheme4.time
    end

    Rk(Tscheme4, dt)  
    
    global n_iter += 1    
  end
  x = space.field[1,1]
  err2= x-2.0*exp(-Tf)
 
  order4 = log(err2/err1)/log(0.5)
  @test order4 > 3.9  

  Tf = 0.1
  order = 2
  Tscheme5=explicit_mstep(space,order)

  initialization(space,TestEx_init)
  global dt = 0.01
  global n_iter = init_mstep(Tscheme5,dt)

  while Tscheme5.time < Tf 
    global dt, n_iter  
    if Tscheme5.time + dt >  Tf
      global dt = Tf - Tscheme5.time
    end

    AB(Tscheme5, [dt,dt]) 

    global n_iter += 1    
  end
  x = space.field[1,1]
  err1 = x-2.0*exp(-Tf)

  Tscheme6=explicit_mstep(space,order)

  initialization(space,TestEx_init)
  global dt = 0.005
  global n_iter = init_mstep(Tscheme6,dt)
  
  while Tscheme6.time < Tf 
    global dt, n_iter  
    if Tscheme6.time + dt >  Tf
      global dt = Tf - Tscheme6.time
    end

    AB(Tscheme6, [dt,dt]) 

    global n_iter += 1    
  end
  x = space.field[1,1]
  err2 = x-2.0*exp(-Tf)
 
  order2 = log(err2/err1)/log(0.5)
  @test order2 > 1.9 

end
