function TestIm_init(x, t)
    [2.0,1.5]
end

struct TestIm_ode <: Operator
    a :: Float64
    b :: Float64
end

function (self::TestIm_ode)(sp,v)
  res1 = -self.a*v[1] + self.b*v[2]
  res2 = self.b*v[1]
  [res1, res2]
end

function test_implicit()
  test =0
###### parameters
  L= 1.0
  Nx = 1
  test = 0
  order = 1
  eps = 0.0000000001
  global Tf = 0.1
  alpha = 1.0
  beta = 0.2
  
  ###### initialisation space
  Mh=Mesh(L,Nx,1)
  Mh(id_mesh)

  space=vf(Mh,2,order,1)
  lm = -0.5*alpha-0.5*sqrt(alpha*alpha+4.0*beta*beta)
  lr = -0.5*alpha+0.5*sqrt(alpha*alpha+4.0*beta*beta)

  ###### order 1 coarse  dt
  Tscheme = Implicit_mstage(space,1,1,1)
  ode = TestIm_ode(alpha,beta)
  set_spaceop(Tscheme,2,ode)

  initialization(space,TestIm_init)
  global dt = 0.01
  global n_iter = 0
  while Tscheme.time < Tf 
    global dt, n_iter  
    if Tscheme.time + dt >  Tf
      global dt = Tf - Tscheme.time
    end

    space.field[1,:] = SDirk(Tscheme, space.field[1,:], dt)  
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err1 = abs((lm*x+beta*y)-(lm*2.0+beta*1.5)*exp(lm*Tf)) + abs((lr*x+beta*y)-(lr*2.0+beta*1.5)*exp(lr*Tf))

  ###### order 1 fine dt
  Tscheme2 = Implicit_mstage(space,1,1,1)
  ode = TestIm_ode(alpha,beta)
  set_spaceop(Tscheme2,2,ode)

  initialization(space,TestIm_init)
  global dt = 0.005
  global n_iter = 0
  while Tscheme2.time < Tf 
    global dt, n_iter  
    if Tscheme2.time + dt >  Tf
      global dt = Tf - Tscheme2.time
    end

    space.field[1,:]=SDirk(Tscheme2, space.field[1,:], dt) 
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err2 = abs((lm*x+beta*y)-(lm*2.0+beta*1.5)*exp(lm*Tf)) + abs((lr*x+beta*y)-(lr*2.0+beta*1.5)*exp(lr*Tf))
  
  order2 = log(err2/err1)/log(0.5)
  @test order2 > 0.9  

  ###### order 2 coarse  dt

  Tscheme3 = Implicit_mstage(space,1,2,1)
  ode = TestIm_ode(alpha,beta)
  set_spaceop(Tscheme3,2,ode)

  initialization(space,TestIm_init)
  global dt = 0.02
  global n_iter = 0
  while Tscheme3.time < Tf 
    global dt, n_iter  
    if Tscheme3.time + dt >  Tf
      global dt = Tf - Tscheme3.time
    end

    space.field[1,:] = SDirk(Tscheme3, space.field[1,:], dt)  
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err1 = abs((lm*x+beta*y)-(lm*2.0+beta*1.5)*exp(lm*Tf)) + abs((lr*x+beta*y)-(lr*2.0+beta*1.5)*exp(lr*Tf))

  ###### order 2 fine  dt
  Tscheme4 = Implicit_mstage(space,1,2,1)
  ode = TestIm_ode(alpha,beta)
  set_spaceop(Tscheme4,2,ode)

  initialization(space,TestIm_init)
  global dt = 0.01
  global n_iter = 0
  while Tscheme4.time < Tf 
    global dt, n_iter  
    if Tscheme4.time + dt >  Tf
      global dt = Tf - Tscheme4.time
    end

    space.field[1,:]=SDirk(Tscheme4, space.field[1,:], dt)  
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err2 = abs((lm*x+beta*y)-(lm*2.0+beta*1.5)*exp(lm*Tf)) + abs((lr*x+beta*y)-(lr*2.0+beta*1.5)*exp(lr*Tf))

  order2 = log(err2/err1)/log(0.5)
  @test order2 > 1.9  

  ###### order 3 coarse  dt
  Tscheme5 = Implicit_mstage(space,2,3,1)
  ode = TestIm_ode(alpha,beta)
  set_spaceop(Tscheme5,2,ode)

  initialization(space,TestIm_init)
  global dt = 0.025
  global n_iter = 0
  while Tscheme5.time < Tf 
    global dt, n_iter  
    if Tscheme5.time + dt >  Tf
      global dt = Tf - Tscheme5.time
    end

    space.field[1,:] = SDirk(Tscheme5, space.field[1,:], dt)  
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err1 = abs((lm*x+beta*y)-(lm*2.0+beta*1.5)*exp(lm*Tf)) + abs((lr*x+beta*y)-(lr*2.0+beta*1.5)*exp(lr*Tf))

  ###### order 3 fine  dt
  Tscheme6 = Implicit_mstage(space,2,3,1)
  ode = TestIm_ode(alpha,beta)
  set_spaceop(Tscheme6,2,ode)

  initialization(space,TestIm_init)
  global dt = 0.0125
  global n_iter = 0
  while Tscheme6.time < Tf 
    global dt, n_iter  
    if Tscheme6.time + dt >  Tf
      global dt = Tf - Tscheme6.time
    end

    space.field[1,:]=SDirk(Tscheme6, space.field[1,:], dt)  
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err2 = abs((lm*x+beta*y)-(lm*2.0+beta*1.5)*exp(lm*Tf)) + abs((lr*x+beta*y)-(lr*2.0+beta*1.5)*exp(lr*Tf))

  order3 = log(err2/err1)/log(0.5)
  @test order3 > 2.9
  
end
