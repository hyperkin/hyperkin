function TestVF_init_cts(x, t)
    r     = 2.5
    [r]
end

function TestVF_init_linear(x, t)
    r     = 2.2*x+1.7
    [r]
end

function TestVF_norml1(v::Array{Float64}, data::Array{Float64})
    r  =  v[1] # we cannot use direct v. why ?
    data[1]  =   abs(r)
    nothing
end

function TestVF_id_mapping(v::Array{Float64}, data::Array{Float64}) 
    r  =  v[1]
    data[1] = r
    nothing
end

struct TestLocalLax <: Flux
    L :: Float64
    a :: Float64
end

function (self::TestLocalLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})

    a = self.a
    rl = vL[1]
    rr = vR[1]
    data[1]= 0.5 * (a*rl+a*rr) - 0.5 * abs(a) * (rr-rl)
end

function test_vf()
  test =0
###### parameters
  L= 1.0
  Nx = 10
  test = 0
  order = 1
  a = 1.0
  eps = 0.0000000001
###### initialisation space
  Mh=Mesh(L,Nx,1)
  Mh(id_mesh)

  space=vf(Mh,1,order,1)
  local_lax = TestLocalLax(L,a)
  set_numflux(space, local_lax)
  limiting = NoLimiting()
  set_limiting(space,limiting)

  initialization(space,TestVF_init_cts)
  bc_neumann(space)
  space()
  space.field[:].=-space.flux[:]

  field, diags= diagnostics(space,0,0.0,TestVF_init_cts,TestVF_id_mapping,TestVF_norml1)
  @test abs(diags[1]) < eps

  initialization(space,TestVF_init_linear)
  bc_dirichlet(space,[2.2(-0.5*Mh.h)+1.7],[2.2(L+0.5*Mh.h)+1.7])
  space()
  space.field[:].=-space.flux[:]

  field, diags= diagnostics(space,0,0.0,TestVF_init_cts,TestVF_id_mapping,TestVF_norml1)
 
  @test abs(diags[1]-2.2) < eps
end
