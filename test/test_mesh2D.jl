function test_mesh2D()

  
###### parameters
  Lx = 2.5
  Ly = 3.0  
  Nx = 10
  Ny = 15  
  h2x = 0.5*Lx/Nx
  h2y = 0.5*Ly/Ny  
  order = 1
  eps = 0.0000000001
###### initialisation space
  Mh=Mesh2D(Lx,Ly,Nx,Ny,order)
  Mh(id_mesh2D)

  sum = 0.0
  for i in 1:Mh.Nc
    if Mh.labels[i] >= 0     
        sum = sum +Mh.areas[i]
    end
  end

  @test abs(sum - Lx*Ly) < eps
    
  x,y = Meshes1D(Mh) 
  @test abs(x[10]-(2.5-h2x)+x[1]-h2x+y[15]-(3.0-h2y)+y[1]-h2y) <eps  
   
    
  Mh2=Mesh2D(Lx,Ly,Nx,Ny,order)
  Mh2(translate_mesh2D)

  sum = 0.0
  for i in 1:Mh2.Nc
    if Mh2.labels[i] >= 0     
        sum = sum +Mh2.areas[i]
    end
  end

  @test abs(sum - Lx*Ly) < eps
    
  x2,y2 = Meshes1D(Mh2)
  @test abs(x2[10]-(1.25-h2x)+x2[1]+1.25-h2x+y2[15]-(1.5-h2y)+y2[1]+1.5-h2y) <eps    
    
    
  Mh3=Mesh2D(1,1,3,5,1)
  Mh3(id_mesh2D)  
  ni = 18  
  t=neighbors(Mh3, ni)
  @test abs(t[2]-17+t[4]-19+t[1]-13+t[3]-23) <eps  
    
    
  Mh4=Mesh2D(1,1,3,5,1,1)
  Mh4(id_mesh2D)  
  ni = 28  
  t=neighbors(Mh4, ni)
  @test abs(t[2]-27+t[4]-29+t[1]-23+t[3]-8) <eps 
    
  ni = 24  
  t=neighbors(Mh4, ni)
  @test abs(t[2]-23+t[4]-22+t[1]-19+t[3]-29) <eps    
    
  ni = 12  
  t=neighbors(Mh4, ni)
  @test abs(t[2]-14+t[4]-13+t[1]-7+t[3]-17) <eps  
    
  ni = 9  
  t=neighbors(Mh4, ni)
  @test abs(t[2]-8+t[4]-7+t[1]-29+t[3]-14) <eps    
  


end
