function test_interpo()
###### parameters
  L= 1.0
  Nx = 10

  eps = 0.0000000001
###### initialisation space
  Mh=Mesh(L,Nx,1)
  Mh(id_mesh)

  x = [ Mh.nodes[2], Mh.nodes[3] ]
  y = [ 2.0*x[1]+3.0 , 2.0*x[2]+3.0 ]
  inter=lagrange(0)
  set_xyi(inter,x,y)

  @test abs(interpolator(inter,0.16)-3.32) < eps

  x = [ Mh.nodes[2], Mh.nodes[3], Mh.nodes[4], Mh.nodes[5] ]
  y = [ x[1]*x[1] , x[2]*x[2], x[3]*x[3], x[4]*x[4] ]
  inter=lagrange(1)
  set_xyi(inter,x,y)

  @test abs(interpolator(inter,0.32)-0.1024) < eps

  x = [ Mh.nodes[2], Mh.nodes[3] ]
  y = [ 2.0, 2.0 ]
  xh = [ Mh.centers[2] ]
  yh = [ 0.2 ]
  inter=lagrange(0)
  set_xi(inter,x)
  set_xyh(inter,xh,yh)

  @test abs(Mh.h*histopolator(inter,0.17)-0.2) < eps

  x = [ Mh.nodes[2], Mh.nodes[3], Mh.nodes[4], Mh.nodes[5] ]
  y = [ 2.0*x[1]+3.0 , 2.0*x[2]+3.0, 2.0*x[3]+3.0, 2.0*x[4]+3.0 ]
  xh = [ Mh.centers[2], Mh.centers[3], Mh.centers[4] ]
  yh = [ 0.33, 0.35, 0.37 ]
  inter=lagrange(1)
  set_xi(inter,x)
  set_xyh(inter,xh,yh)

  @test abs(Mh.h*histopolator(inter,0.25)-0.35) < eps

end
