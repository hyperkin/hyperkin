function TestSta_pol(x)
    return x*(1.0-x)
end 

function TestSta_int(x)
    return 0.5*x*x-0.3333333*x*x*x
end 

function TestSta_zero(x)
    return 0.0
end 

struct ope <: SpatialOperator
end

function (self::ope)(v :: Vector{DeRhamSpace})
    res1 = C0(v[1].mh,v[1].bc_type)
    res2 = C1(v[2].mh,v[2].bc_type)
    grad(v[1],res2)
    res1.field[:] .= v[1].field[:]
    [res1, res2]
end   

struct ope2 <: SpatialOperator
end

function (self::ope2)(v :: Vector{DeRhamSpace})
    res1 = C0(v[1].mh,v[1].bc_type)
    res2 = C1bar(v[2].mh,v[2].bc_type)
    H1bar(v[1],res2)
    res1.field[:] .= v[1].field[:]
    [res1, res2]
end   

function test_staggered()
    test =0
  ###### parameters
    L= 1.0
    Nx = 20
    test = 0
    order = 0
    a = 1.0
    eps = 0.0000000001
    eps2 = 0.01 
  ###### initialisation space
    Mh=Mesh(L,Nx,4)
    Mh(id_mesh)

    X0 = C0(Mh,1)
    R0(X0,TestSta_pol)
    X1 = C1(Mh,1)
    R1(X1,TestSta_zero)

    wave = staggered(Mh,2)
    set_space(wave,X0,1)
    set_space(wave,X1,2)
    grad = ope()
    space_to_field(wave)
    set_operator(wave,grad)
    wave()
    wave.field .= wave.flux
    field_to_space(wave)

    err = 0.0
    for i in 1:Mh.Nc
        px=I0loc(wave.spaces[1],i)
        err = err + abs(px(Mh.nodes[i])-(TestSta_pol(Mh.nodes[i])))
        px=I1loc(wave.spaces[2],i)
        err = err + abs(Mh.h*px(Mh.centers[i])-(TestSta_pol(Mh.nodes[i+1]) -  TestSta_pol(Mh.nodes[i] )))
    end    
    
    @test err < eps

    Y0 = C0(Mh,1)
    R0(Y0,TestSta_pol)
    Y1bar = C1bar(Mh,1)
    R1bar(Y1bar,TestSta_zero)

    testh = staggered(Mh,2)
    set_space(testh,Y0,1)
    set_space(testh,Y1bar,2)
    hodge = ope2()
    set_operator(testh,hodge)

    space_to_field(testh)
    testh()
    testh.field .= testh.flux
    field_to_space(testh)
    for i in 2:Mh.Nc
        px=I0loc(testh.spaces[1],i)
        err = err + abs(px(Mh.nodes[i])-(TestSta_pol(Mh.nodes[i])))
        
        px=I1barloc(testh.spaces[2],i)
        err = err + abs(testh.spaces[2].field[i]-(TestSta_int(Mh.centers[i]) -  TestSta_int(Mh.centers[i-1] )))
    end   

    @test err < eps2
end
