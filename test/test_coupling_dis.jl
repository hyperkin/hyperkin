function TestCou_pol(x, t)
    return x*(1.0-x)
end 

function TestCou_polinit(x)
    return x*(1.0-x)
end 

function TestCou_poldg(x, t)
   res= x*(1.0-x)
   [res]
end  

struct l2_norm <: Flux
end

function (self::l2_norm)(v)
    [v*v]
end

struct l2_normdg <: Flux
end

function (self::l2_normdg)(v::Array{Float64}, data::Array{Float64})
    r= v[1]
    data[1] =r*r
end

struct id_testc_mapping <: Flux
end

function (self::id_testc_mapping)(v)
    return v
end

struct id_dg_mapping <: Flux
end

function (self::id_dg_mapping)(v::Array{Float64}, data::Array{Float64})
    r= v[1]
    data[1] =r*r
end

function test_coupling_dis1()
    test =0
  ###### parameters
    L= 1.0
    Nx = 10
    test = 0
    p = 0
    eps = 0.0001
  ###### initialisation space
    Mh=Mesh(L,Nx,1)
    Mh(id_mesh)

    spacevf=vf(Mh,1,1,1)
    initialization(spacevf,TestCou_poldg)
    bcl = TestCou_poldg(Mh.bc_centers[2],0.0)
    bcr = TestCou_poldg(Mh.bc_centers[3],0.0)
    bc_dirichlet(spacevf,bcl,bcr)
    compute_diags = l2_norm()
    var_mapping = id_testc_mapping()


    v0bar =C0bar(Mh,1,p,1)
    vf_to_stagC0bar(spacevf,v0bar,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(v0bar,1,0.0,TestCou_pol,var_mapping,compute_diags)
    @test abs(diagsr[1]) < eps

    v0 =C0(Mh,1,p,1)
    vf_to_stagC0(spacevf,v0,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(v0,1,0.0,TestCou_pol,var_mapping,compute_diags)
    @test abs(diagsr[1]) < eps  
    
    v1 =C1(Mh,1,p,1)
    vf_to_stagC1(spacevf,v1,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(v1,1,0.0,TestCou_pol,var_mapping,compute_diags)
    @test abs(diagsr[1]) < eps 
    
    v1bar =C1bar(Mh,1,p,1)
    vf_to_stagC1bar(spacevf,v1bar,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(v1bar,1,0.0,TestCou_pol,var_mapping,compute_diags)
    @test abs(diagsr[1]) < eps 
    
    Mh2=Mesh(L,2*Nx,1)
    Mh2(id_mesh)
    spacevf2=vf(Mh2,1,1,1)
    compute_diags2 = l2_normdg()
    var_mapping2 = id_dg_mapping()
  
    v0 =C0(Mh2,1,p,1)
    R0(v0,TestCou_polinit)
    stagC0_to_vf(v0,spacevf2,1)
    fieldr, r_ref, diagsr= diagnostics(spacevf2,1,0.0,TestCou_poldg,var_mapping2,compute_diags2)
    @test abs(diagsr[1]) < 0.001  
    
    v0bar =C0bar(Mh2,1,p,1)
    R0bar(v0bar,TestCou_polinit)
    stagC0bar_to_vf(v0bar,spacevf2,1)
    fieldr, r_ref, diagsr= diagnostics(spacevf2,1,0.0,TestCou_poldg,var_mapping2,compute_diags2)
    @test abs(diagsr[1]) < 0.001
    
    v1 =C1(Mh2,1,p,1)
    R1(v1,TestCou_polinit)
    stagC1_to_vf(v1,spacevf2,1)
    fieldr, r_ref, diagsr= diagnostics(spacevf2,1,0.0,TestCou_poldg,var_mapping2,compute_diags2)
    @test abs(diagsr[1]) < 0.001 
    
    v1bar =C1bar(Mh2,1,p,1)
    R1bar(v1bar,TestCou_polinit)
    stagC1bar_to_vf(v1bar,spacevf2,1)
    fieldr, r_ref, diagsr= diagnostics(spacevf2,1,0.0,TestCou_poldg,var_mapping2,compute_diags2)
    @test abs(diagsr[1]) < 0.001 

    # essayer de changer les conditions limite car l'ibtrpolation periodique pause probleme sur les valeur obtenu dans la premiere maille. NEUMANNN ?
end   


function test_coupling_dis2()
    test =0
  ###### parameters
    L= 1.0
    Nx = 10
    test = 0
    p = 1
    deg = 3 
    eps = 0.0000000001
  ###### initialisation space
    Mh=Mesh(L,Nx,deg)
    Mh(id_mesh)

    spacedg=dg(Mh,1,deg,1)
    bcl = zeros(Float64,(deg+1,1))
    bcr = zeros(Float64,(deg+1,1))
    initialization(spacedg,TestCou_poldg)
    for k in 1:Mh.Deg+1
      bcl[k,:] = TestCou_poldg(pgl(Mh,k),0.0)
      bcr[k,:] = TestCou_poldg(pgr(Mh,k),0.0)
    end
    bc_dirichlet(spacedg,bcl,bcr)
    compute_diags = l2_norm()
    var_mapping = id_testc_mapping()


    v0bar =C0bar(Mh,1,p,1)
    dg_to_stagC0bar(spacedg,v0bar,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(v0bar,1,0.0,TestCou_pol,var_mapping,compute_diags)
    @test abs(diagsr[1]) < eps    

    v0 =C0(Mh,1,p,1)
    dg_to_stagC0(spacedg,v0,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(v0,1,0.0,TestCou_pol,var_mapping,compute_diags)
    @test abs(diagsr[1]) < eps  
    
    v1 =C1(Mh,1,p,1)
    dg_to_stagC1(spacedg,v1,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(v1,1,0.0,TestCou_pol,var_mapping,compute_diags)
    @test abs(diagsr[1]) < eps 
    
    v1bar =C1bar(Mh,1,p,1)
    dg_to_stagC1bar(spacedg,v1bar,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(v1bar,1,0.0,TestCou_pol,var_mapping,compute_diags)
    @test abs(diagsr[1]) < eps 
    
    Mh2=Mesh(L,2*Nx,deg)
    Mh2(id_mesh)
    spacedg2=dg(Mh2,1,deg,1)
    compute_diags2 = l2_normdg()
    var_mapping2 = id_dg_mapping()
  
    v0 =C0(Mh2,1,1,1)
    R0(v0,TestCou_polinit)
    stagC0_to_dg(v0,spacedg2,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(spacedg2,1,0.0,TestCou_poldg,var_mapping2,compute_diags2)
    @test abs(diagsr[1]) < 0.00001  
    
    v0bar =C0bar(Mh2,1,1,1)
    R0bar(v0bar,TestCou_polinit)
    stagC0bar_to_dg(v0bar,spacedg2,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(spacedg2,1,0.0,TestCou_poldg,var_mapping2,compute_diags2)
    @test abs(diagsr[1]) < 0.00001 
    
    v1 =C1(Mh2,1,1,1)
    R1(v1,TestCou_polinit)
    stagC1_to_dg(v1,spacedg2,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(spacedg2,1,0.0,TestCou_poldg,var_mapping2,compute_diags2)
    @test abs(diagsr[1]) < 0.00001
    
    v1bar =C1bar(Mh2,1,1,1)
    R1bar(v1bar,TestCou_polinit)
    stagC1bar_to_dg(v1bar,spacedg2,1)
    x_r_ref, fieldr, r_ref, diagsr= diagnostics(spacedg2,1,0.0,TestCou_poldg,var_mapping2,compute_diags2)
    @test abs(diagsr[1]) < 0.00001

    # essayer de changer les conditions limite car l'ibtrpolation periodique pause probleme sur les valeur obtenu dans la premiere maille. NEUMANNN ?
end   
