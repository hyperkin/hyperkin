import Pkg
Pkg.instantiate()
using Test
using Hyperkin


@testset "Mesh" begin
include("test_mesh.jl")
test_mesh()
end

@testset "Mesh2D" begin
include("test_mesh2D.jl")
test_mesh2D()
end

@testset "Interpol" begin
include("test_interpo.jl")
test_interpo()
end

@testset "vf" begin
include("test_vf.jl")
test_vf()
end

@testset "vf2D" begin
include("test_vf2D.jl")
test_vf2D()
end

@testset "DG" begin
include("test_dg.jl")
test_dg()
end

@testset "Relax" begin
include("test_relaxation.jl")
test_relaxation()
end

@testset "Explicit" begin
include("test_explicit.jl")
test_explicit()
end

@testset "Implicit" begin
include("test_implicit.jl")
test_implicit()
end

@testset "Implicit Schur" begin
include("test_implicitschur.jl")
test_implicitschur()
end

@testset "Derham" begin
include("test_derham.jl")
test_derham()
end

include("test_staggered.jl")
@testset "Staggered" begin
test_staggered()
end

include("test_coupling_dis.jl")
@testset "Dis 1" begin
test_coupling_dis1()
end
@testset "Dis 2" begin
test_coupling_dis2()
end
