function TestImS_init(x, t)
    [2.0,1.5]
end

struct TestImS_ode1 <: Operator
    b :: Float64
end

function (self::TestImS_ode1)(sp,v)
  res1 = self.b*v[1]
  [res1]
end

struct TestImS_ode2 <: Operator
    b :: Float64
end

function (self::TestImS_ode2)(sp,v)
  res2 = self.b*v[1]
  [res2]
end

struct TestImS_ode3 <: Operator
    b :: Float64
end

function (self::TestImS_ode3)(sp,v)
  res = self.b*self.b*v[1]
  [res]
end

function test_implicitschur()
  test =0
###### parameters
  L= 1.0
  Nx = 1
  test = 0
  order = 1
  eps = 0.0000000001
  global Tf = 0.1
  beta = 1.0
  
  ###### initialisation space
  Mh=Mesh(L,Nx,1)
  Mh(id_mesh)

  space=vf(Mh,2,order,1)
  lm = - beta
  lr = beta
  op1 = TestImS_ode1(beta)
  op2 = TestImS_ode2(beta)
  schur = TestImS_ode3(beta)

  ###### order 1 coarse  dt
  Tscheme = SchurImplicit_mstage(space,1,1,1)
  set_spaceop1(Tscheme,1,op1)
  set_spaceop2(Tscheme,1,op2)
  set_schur(Tscheme,1,schur)

  initialization(space,TestImS_init)
  global dt = 0.01
  global n_iter = 0
  while Tscheme.time < Tf 
    global dt, n_iter  
    if Tscheme.time + dt >  Tf
      global dt = Tf - Tscheme.time
    end

    space.field[1,:] = SDirk_Schur(Tscheme, space.field[1,:], dt) 
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err1 = abs((-x+y)-(-2.0+1.5)*exp(lm*Tf)) + abs((x+y)-(2.0+1.5)*exp(lr*Tf))

  ###### order 1 fine  dt
  Tscheme2 = SchurImplicit_mstage(space,1,1,1)
  set_spaceop1(Tscheme2,1,op1)
  set_spaceop2(Tscheme2,1,op2)
  set_schur(Tscheme2,1,schur)

  initialization(space,TestImS_init)
  global dt = 0.005
  global n_iter = 0
  while Tscheme2.time < Tf 
    global dt, n_iter  
    if Tscheme2.time + dt >  Tf
      global dt = Tf - Tscheme2.time
    end

    space.field[1,:] = SDirk_Schur(Tscheme2, space.field[1,:], dt) 
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err2 = abs((-x+y)-(-2.0+1.5)*exp(lm*Tf)) + abs((x+y)-(2.0+1.5)*exp(lr*Tf))

  order1 = log(err2/err1)/log(0.5)
  @test order1 > 0.9  

  ###### order 2 coarse  dt
  Tscheme3 = SchurImplicit_mstage(space,1,2,1)
  set_spaceop1(Tscheme3,1,op1)
  set_spaceop2(Tscheme3,1,op2)
  set_schur(Tscheme3,1,schur)

  initialization(space,TestImS_init)
  global dt = 0.02
  global n_iter = 0
  while Tscheme3.time < Tf 
    global dt, n_iter  
    if Tscheme3.time + dt >  Tf
      global dt = Tf - Tscheme3.time
    end

    space.field[1,:] = SDirk_Schur(Tscheme3, space.field[1,:], dt) 
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err1 = abs((-x+y)-(-2.0+1.5)*exp(lm*Tf)) + abs((x+y)-(2.0+1.5)*exp(lr*Tf))

  ###### order 2 fine  dt
  Tscheme4 = SchurImplicit_mstage(space,1,2,1)
  set_spaceop1(Tscheme4,1,op1)
  set_spaceop2(Tscheme4,1,op2)
  set_schur(Tscheme4,1,schur)

  initialization(space,TestImS_init)
  global dt = 0.01
  global n_iter = 0
  while Tscheme4.time < Tf 
    global dt, n_iter  
    if Tscheme4.time + dt >  Tf
      global dt = Tf - Tscheme4.time
    end

    space.field[1,:] = SDirk_Schur(Tscheme4, space.field[1,:], dt) 
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err2 = abs((-x+y)-(-2.0+1.5)*exp(lm*Tf)) + abs((x+y)-(2.0+1.5)*exp(lr*Tf))

  order2 = log(err2/err1)/log(0.5)
  @test order2 > 1.9 

  ###### order 2 coarse  dt
  Tscheme5 = SchurImplicit_mstage(space,2,3,1)
  set_spaceop1(Tscheme5,1,op1)
  set_spaceop2(Tscheme5,1,op2)
  set_schur(Tscheme5,1,schur)

  initialization(space,TestImS_init)
  global dt = 0.02
  global n_iter = 0
  while Tscheme5.time < Tf 
    global dt, n_iter  
    if Tscheme5.time + dt >  Tf
      global dt = Tf - Tscheme5.time
    end

    space.field[1,:] = SDirk_Schur(Tscheme5, space.field[1,:], dt) 
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err1 = abs((-x+y)-(-2.0+1.5)*exp(lm*Tf)) + abs((x+y)-(2.0+1.5)*exp(lr*Tf))

  ###### order 3 fine  dt
  Tscheme6 = SchurImplicit_mstage(space,2,3,1)
  set_spaceop1(Tscheme6,1,op1)
  set_spaceop2(Tscheme6,1,op2)
  set_schur(Tscheme6,1,schur)

  initialization(space,TestImS_init)
  global dt = 0.01
  global n_iter = 0
  while Tscheme6.time < Tf 
    global dt, n_iter  
    if Tscheme6.time + dt >  Tf
      global dt = Tf - Tscheme6.time
    end

    space.field[1,:] = SDirk_Schur(Tscheme6, space.field[1,:], dt) 
    global n_iter += 1    
  end
  x = space.field[1,1]
  y = space.field[1,2]
  err2 = abs((-x+y)-(-2.0+1.5)*exp(lm*Tf)) + abs((x+y)-(2.0+1.5)*exp(lr*Tf))

  order3 = log(err2/err1)/log(0.5)
  @test order3 > 2.9 

  
end
