struct TestRelax_init1   
end
function (self::TestRelax_init1)(x, t)
    
    res = -2.0*x+4.0 
    [res]
end

struct TestRelax_init2  
end
function (self::TestRelax_init2)(x, t)

    res = (-2.0*x+4.0)*(-2.0*x+4.0)   
    [res]
end

struct TestRelax_init3  
end
function (self::TestRelax_init3)(x, t)

    res = 2.0*(-2.0*x+4.0)^4  - (-2.0*x+4.0)^2   
    [res]
end

struct eq_project <: Equilibrium
end
function (self::eq_project)(v::Vector{Float64},data::Vector{Float64})
    r = v[1]
    data[1] = r*r
    nothing
end


function TestRelax_norml1(v::Array{Float64}, data::Array{Float64})
    r  =  v[1] # we cannot use direct v. why ?
    data[1]  =   abs(r)
    nothing
end

function TestRelax_id_mapping(v::Array{Float64}, data::Array{Float64}) 
    r  =  v[1]
    data[1] = r
    nothing
end

struct TestLocalLaxRelax <: Flux
    L :: Float64
    a :: Float64
end

function (self::TestLocalLaxRelax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})
    a = self.a
    rl = vL[1]
    rr = vR[1]
    data[1]= 0.5 * (a*rl+a*rr) - 0.5 * abs(a) * (rr-rl)
end

function test_relaxation()
  test =0
###### parameters
  L= 1.0
  Nx = 10
  test = 0
  order = 1
  a = 1.0
  eps = 0.0000000001
  w=1.0
###### initialisation space
  Mh=Mesh(L,Nx,1)
  Mh(id_mesh)

  space=vf(Mh,1,order,1)
  local_lax = TestLocalLaxRelax(L,a)
  set_numflux(space, local_lax)
  limiting = NoLimiting()
  set_limiting(space,limiting)
    
  init_data=TestRelax_init1()
  final_data=TestRelax_init2()
  final_data2=TestRelax_init3()
  initialization(space,init_data)  

  relax=relaxation(1,space,w)
  eq          = eq_project()
  set_eq(relax, eq)   
  relax()

  field, fieldref, diags= diagnostics(space,1,0.0,final_data,TestRelax_id_mapping,TestRelax_norml1)
  @test abs(diags[1]) < eps
    
  set_w(relax,2.0)
  relax()  
  field, fieldref, diags= diagnostics(space,1,0.0,final_data2,TestRelax_id_mapping,TestRelax_norml1)
  @test abs(diags[1]) < eps
end
