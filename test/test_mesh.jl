function test_mesh()

  
###### parameters
  L= 2.5
  Nx = 10
  order = 1
  eps = 0.0000000001
###### initialisation space
  Mh=Mesh(L,Nx,order)
  Mh(id_mesh)

  sum = 0.0
  for i in 1:Mh.Nc
    sum = sum +Mh.areas[i]
  end

  @test abs(sum - L) < eps

  sum = 0.0
  for i in 1:Mh.Nv
    sum = sum +Mh.dualareas[i]
  end

  @test abs(sum - (L + 2.5/Nx)) < eps

  ###### initialisation space
  Mh=Mesh(L,Nx,order)
  Mh(smooth_mesh)

  sum = 0.0
  for i in 1:Mh.Nc
    sum = sum +Mh.areas[i]
  end

  @test abs(sum - L) < eps

  sum = 0.0
  for i in 1:Mh.Nv
    sum = sum +Mh.dualareas[i]
  end

  @test abs(sum - (L + 2.5/Nx)) < eps

  Mh=Mesh(L,Nx,4)
  Mh(translate_mesh)
  x = Mh.nodes[4]
  y = Mh.nodes[5]
  ex = y*y*y-x*x*x# exact fucntion 3x^2

  sum = 0.0
  for k in 1:Mh.Deg+1
    sum = sum + wg(Mh,4,k)*3.0*pg(Mh,4,k)*pg(Mh,4,k)
  end  

  @test abs(sum - ex) < eps

  x = Mh.centers[4]
  y = Mh.centers[5]
  ex = y*y*y-x*x*x# exact fucntion 3x^2

  sum = 0.0
  for k in 1:Mh.Deg+1
    sum = sum + wgdual(Mh,5,k)*3.0*pgdual(Mh,5,k)*pgdual(Mh,5,k)
  end  

  @test abs(sum - ex) < eps

  x = Mh.centers[4]
  y = Mh.nodes[5]
  ex = y*y*y-x*x*x# exact fucntion 3x^2

  sum = 0.0
  for k in 1:Mh.Deg+1
    sum = sum + wgdual1(Mh,5,k)*3.0*pgdual1(Mh,5,k)*pgdual1(Mh,5,k)
  end  

  @test abs(sum - ex) < eps

  x = Mh.nodes[5]
  y = Mh.centers[5]
  ex = y*y*y-x*x*x# exact fucntion 3x^2

  sum = 0.0
  for k in 1:Mh.Deg+1
    sum = sum + wgdual2(Mh,5,k)*3.0*pgdual2(Mh,5,k)*pgdual2(Mh,5,k)
  end  

  @test abs(sum - ex) < eps

  x = Mh.nodes[4]
  y = Mh.centers[4]
  ex = y*y*y-x*x*x# exact fucntion 3x^2

  sum = 0.0
  for k in 1:Mh.Deg+1
    sum = sum + wg1(Mh,4,k)*3.0*pg1(Mh,4,k)*pg1(Mh,4,k)
  end  

  @test abs(sum - ex) < eps

  x = Mh.centers[4]
  y = Mh.nodes[5]
  ex = y*y*y-x*x*x# exact fucntion 3x^2

  sum = 0.0
  for k in 1:Mh.Deg+1
    sum = sum + wg2(Mh,4,k)*3.0*pg2(Mh,4,k)*pg2(Mh,4,k)
  end  

  @test abs(sum - ex) < eps

end
