function TestDG_polynome(x, t)
    r     = 2.2*x*x+1.7*x-1.0
    [r]
end

function TestDG_polynome2(x)
  r     = 2.2*x*x+1.7*x-1.0
  return r
end

function TestDG_dev_polynome(x, t)
    r     = 4.4*x+1.7
    [r]
end

function TestDG_dev2_polynome(x, t)
    r     = 4.4
    [r]
end

function TestDG_norml1(v::Array{Float64}, data::Array{Float64})
    r  =  v[1] # we cannot use direct v. why ?
    data[1]  =   abs(r)
    nothing
end

function TestDG_id_mapping(v::Array{Float64}, data::Array{Float64}) 
    r  =  v[1]
    data[1] = r
    nothing
end

struct TestDG_LocalLax <: Flux
    L :: Float64
    a :: Float64
end

function (self::TestDG_LocalLax)(x,h,data::Vector{Float64},vL::Vector{Float64},vR::Vector{Float64})

    a = self.a
    rl = vL[1]
    rr = vR[1]
    data[1]= 0.5 * (a*rl+a*rr) - 0.5 * abs(a) * (rr-rl)
end

function test_dg()
  
###### parameters
  L= 1.0
  Nx = 10
  deg = 3
  a = 1.0
  eps = 0.00000001
###### initialisation space
  Mh=Mesh(L,Nx,deg)
  Mh(id_mesh)

  space=dg(Mh,1,deg,1)
  local_lax = TestDG_LocalLax(L,a)
  set_numflux(space, local_lax)

  bcl = zeros(Float64,(deg+1,1))
  bcr = zeros(Float64,(deg+1,1))
  initialization(space,TestDG_polynome)
 
  for k in 1:Mh.Deg+1
    bcl[k,:] = TestDG_polynome(pgl(Mh,k),0.0)
    bcr[k,:] = TestDG_polynome(pgr(Mh,k),0.0)
  end  
  bc_dirichlet(space,bcl,bcr)
  space()
  space.field[:,1].= -space.flux[:,1]

  for k in 1:Mh.Deg+1
    bcl[k,:] = TestDG_dev_polynome(pgl(Mh,k),0.0)
    bcr[k,:] = TestDG_dev_polynome(pgr(Mh,k),0.0)
  end
  bc_dirichlet(space,bcl,bcr)
  space()
  space.field[:,1].= -space.flux[:,1]   

   for k in 1:Mh.Deg+1
    bcl[k,:] = TestDG_dev2_polynome(pgl(Mh,k),0.0)
    bcr[k,:] = TestDG_dev2_polynome(pgr(Mh,k),0.0)
  end
  bc_dirichlet(space,bcl,bcr)
  space()
  space.field[:,1].= -space.flux[:,1] 

  xplot, field, diags= diagnostics(space,0,0.0,TestDG_dev_polynome,TestDG_id_mapping,TestDG_norml1) 
  @test abs(diags[1]) < eps

  space2=dg(Mh,1,deg,1)
  restriction_loc(space2, 5, 1, TestDG_polynome2)
  px = interpolator_loc(space2, 5, 1)
  err = abs(px(Mh.centers[5]+0.00425)-TestDG_polynome2(Mh.centers[5]+0.00425))
  @test  err < eps

end
